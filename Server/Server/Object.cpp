#include "pch.h"
#include "Object.h"

CObject::CObject()
	: m_isMove(false), m_isAttack(false), m_isHeal(false), m_canTransform(true), m_isDefence(false), m_isDummy(false)
{
	for (int i = 0; i < 4; ++i) m_moveDirection[i] = false;
}

CObject::~CObject()
{

}