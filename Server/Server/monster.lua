my_id = -1

function set_npc_id(id)
   my_id = id
end

function event_player_attack(player_id, x, y)
	my_x = API_get_x_position(my_id)
	my_y = API_get_y_position(my_id)
	if x == my_x then 
		if y == my_y then
			API_npc_attack(player_id, my_id)
		end
	end
end

function event_target_move(player_id, x, y)
	print(player_id)
	my_x = API_get_x_position(my_id)
	my_y = API_get_y_position(my_id)
	
	target_x = my_x - x
	target_y = my_y - y

	API_target_move(player_id, my_id, target_x, target_y)
end