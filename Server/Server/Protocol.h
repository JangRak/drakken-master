#pragma once

#include <iostream>
#include <chrono>

#define	WM_SOCKET			WM_USER + 1

#define MAX_BUFFER			1024
#define MAX_PACKET_SIZE		255

#define SERVER_PORT			7420

#define NPC_ID_START		10000
#define BARGHEST_ID_END		NPC_ID_START + 15
#define GRIFFON_ID_END		BARGHEST_ID_END + 11
#define DRAGON_ID_END		GRIFFON_ID_END + 1

#define NUM_OBSTACLES		500

#define CS_LOGIN				1
#define CS_LOGOUT				2	
#define CS_WALK					3
#define CS_RUN					4
#define CS_STOP					5
#define CS_ATTACK				6
#define CS_TRANSFORM			7
#define CS_TELEPORT				8
#define CS_CHAT					9
#define CS_MOUSE_MOVE_PACKET	10
#define CS_POSITION				11
#define CS_DEFENCE				12
#define CS_QUEST_DONE			13
#define CS_INV					14

#define SC_LOGIN_OK			1
#define SC_LOGIN_FAIL		2
#define SC_MOVE_DIRECTION	3
#define SC_MOVE				4
#define SC_STOP				5
#define SC_ENTER			6
#define SC_LEAVE			7
#define SC_STAT_CHANGE		8
#define SC_CHAT				9
#define SC_NPC_DIE			10
#define SC_TRANSFORM		11
#define SC_NPC_ATTACK		12
#define SC_PLAYER_ATTACK	13
#define SC_HP_CHANGE		14
#define SC_PLAYER_DIE		15
#define SC_DEFENCE			16
#define SC_RETURN			17

#define VIKING_WALK_SPEED			120.f
#define VIKING_RUN_SPEED			180.f

#define DRAGON_WALK_SPEED			150.f
#define DRAGON_RUN_SPEED			200.f

#define BARGHEST_WALK_SPEED			80.f
#define BARGHEST_RUN_SPEED			120.f

#define GRIFFON_WALK_SPEED			80.f

constexpr int MAX_PW_LEN = 12;
constexpr int MAX_ID_LEN = 50;
constexpr int MAX_STR_LEN = 50;

#pragma pack(push ,1)

struct sc_packet_login_ok {
	char size;
	char type;
	int id;
	float x, y, z;
	short damage;
	short c_hp, m_hp;
	short level;
	int	c_exp, m_exp;
};

struct sc_packet_login_fail {
	char size;
	char type;
};

struct sc_packet_return {
	char size;
	char type;
	int id;
	unsigned moveTime;
};

struct sc_packet_move_direction {
	char size;
	char type;
	int id;
	int direction;
	float x, y, z;
};

struct sc_packet_player_attack {
	char size;
	char type;
	int id;
};

constexpr unsigned char WALK = 0;
constexpr unsigned char RUN = 1;

struct sc_packet_move {
	char size;
	char type;
	int id;
	float x, y, z;
	float D_x, D_y, D_z;
	char status;
	std::chrono::time_point<std::chrono::system_clock> time;
};

struct sc_packet_stop {
	char size;
	char type;
	int id;
};

constexpr unsigned char O_PLAYER = 0;
constexpr unsigned char O_DRAKKEN = 1;
constexpr unsigned char O_GRIFFON = 2;
constexpr unsigned char O_BARGHEST = 3;
constexpr unsigned char O_DRAGON = 4;

struct sc_packet_enter {
	char size;
	char type;
	int id;
	char name[MAX_ID_LEN];
	unsigned char objectType;
	float x, y, z;
};

struct sc_packet_leave {
	char size;
	char type;
	int id;
	unsigned char objectType;
};

struct sc_packet_chat {
	char size;
	char type;
	int	 id;
	char name[MAX_STR_LEN];
};

struct sc_packet_stat_change {
	char size;
	char type;
	short level;
	short c_hp;
	short m_hp;
	short c_exp;
	short m_exp;
};

struct sc_packet_npc_die {
	char size;
	char type;
	int id;
	char objectType;
};

struct sc_packet_transform {
	char size;
	char type;
	int id;
	bool kind;
};

struct sc_packet_npc_attack {
	char size;
	char type;
	int id;
	float x;
	//float y;
	float z;
};

struct sc_packet_player_die {
	char size;
	char type;
	int id;
};

struct sc_packet_hp {
	char size;
	char type;
	short c_hp;
};

struct sc_packet_defence {
	char size;
	char type;
	bool kind;
	int id;
};

constexpr unsigned char NORMAL = 0;
constexpr unsigned char DUMMY = 1;

struct cs_packet_login {
	char	size;
	char	type;
	char	name[MAX_ID_LEN];
	char	pw[MAX_PW_LEN];
	char	loginType;
};

constexpr unsigned char D_UP = 0;
constexpr unsigned char D_DOWN = 1;
constexpr unsigned char D_LEFT = 2;
constexpr unsigned char D_RIGHT = 3;

struct cs_packet_mouse_move {
	char size;
	char type;
	float look[3];
	float right[3];
	float up[3];
};

struct cs_packet_position {
	char size;
	char type;
	float xPos;
	float yPos;
	float zPos;
	float xLook;
	float yLook;
	float zLook;
	unsigned moveTime;
};

struct cs_packet_move {
	char	size;
	char	type;
	char	direction;
};

struct cs_packet_run {
	char	size;
	char	type;
	bool	running;
};

struct cs_packet_quest_done {
	char size;
	char type;
};

struct cs_packet_move_stop {
	char	size;
	char	type;
	char	direction;
};

constexpr unsigned char A_BITE = 0;
constexpr unsigned char A_CRAW = 1;

struct cs_packet_attack {
	char	size;
	char	type;
	char	attackType;
};

struct cs_packet_defence {
	char size;
	char type;
	bool kind;
};

struct cs_packet_invincibility {
	char size;
	char type;
	bool kind;
};

struct cs_packet_transform {
	char size;
	char type;
};

struct cs_packet_chat {
	char	size;
	char	type;
	char	chat_str[100];
};

struct cs_packet_logout {
	char	size;
	char	type;
};

struct cs_packet_teleport {
	char	size;
	char	type;
};

#pragma pack (pop)


