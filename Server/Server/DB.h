#pragma once

#include <sqlext.h>
#include "Shared.h"

class CDB
{
public:
	CDB();
	~CDB();

public:
	void HandleDiagnosticRecord(SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode);
	int CheckID(int id, int pw);
	int CheckPW(int id, int pw);
	void SavePlayerInform(char* name, short level, short exp, short hp);
	DB_INFORM GetInform(int id);

private:
	SQLRETURN retcode;
	SQLINTEGER nLEVEL, nHP, nEXP;
	SQLLEN cbLEVEL, cbEXP, cbHP;
};