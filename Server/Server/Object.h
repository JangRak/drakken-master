#pragma once

#include <mutex>
#include <unordered_set>
#include <atomic>

#include "Shared.h"

class CObject
{
public:
	CObject();
	~CObject();

public:
	SOCKET GetSocket() const { return m_socket; }
	void SetSocket(SOCKET s) { m_socket = s; }

	int GetID() const { return m_id; }
	void SetID(int id) { m_id = id; }

	void SetMyName(const char* name) { strcpy_s(m_name, name); }
	void GetMyName(char*& des) { des = m_name; }

	void ClientLock() { m_clientLock.lock(); }
	void ClientUnLock() { m_clientLock.unlock(); }

	Vec3 GetPos() const { return m_pos; }
	void SetPos(Vec3 p) { m_pos = p; }
	void SetPos(float x, float y, float z) { m_pos.x = x; m_pos.y = y; m_pos.z = z; }
	void SetxPos(float x) { m_pos.x = x; }
	void SetyPos(float y) { m_pos.y = y; }
	void SetzPos(float z) { m_pos.z = z; }

	Vec3 GetLook() const { return m_look; }
	void SetLook(Vec3 l) { m_look = l; }

	Vec3 GetUp() const { return m_up; }
	void SetUp(Vec3 u) { m_up = u; }

	Vec3 GetRight() const { return m_right; }
	void SetRight(Vec3 r) { m_right = r; }

	float GetSpeed() const { return m_speed; }
	void SetSpeed(float speed) { m_speed = speed; }

	short GetCurrentHp() const { return m_currentHp; }
	void SetCurrentHp(short hp) { m_currentHp = hp; }

	short GetMaxHp() const { return m_maxHp; }
	void SetMaxHp(short hp) { m_maxHp = hp; }

	short GetDamage() const { return m_damage; }
	void SetDamage(short damage) { m_damage = damage; }

	short GetCurrentExp() const { return m_currentExp; }
	void SetCurrentExp(short exp) { m_currentExp = exp; }

	short GetMaxExp() const { return m_maxExp; }
	void SetMaxExp(short exp) { m_maxExp = exp; }

	short GetLevel() const { return m_level; }
	void SetLevel(short lv) { m_level = lv; }

	int GetPrevSize() const { return m_prevSize; }
	void SetPrevSize(int size) { m_prevSize = size; }

	int GetMyType() const { return m_myType; }
	void SetMyType(int type) { m_myType = type; }

	bool GetIsMove() const { return m_isMove; }
	void SetIsMove(bool b) { m_isMove = b; }

	int GetWalkStatus() const { return m_walkStatus; }
	void SetWalkStatus(int s) { m_walkStatus = s; }

	void SetMoveDirection(int direction, bool b) { m_moveDirection[direction] = b; }
	bool GetMoveDirection(int direction) { return m_moveDirection[direction]; }

	void InsertViewList(int id) { m_viewList.insert(id); }
	void EraseViewList(int id) { m_viewList.erase(id); }
	void ClearViewList() { m_viewList.clear(); }
	size_t GetViewListCount(int id) const { return m_viewList.count(id); }
	std::unordered_set<int> GetViewList() const { return m_viewList; }

	bool GetIsAttack() const { return m_isAttack; }
	void SetIsAttack(bool b) { m_isAttack = b; }

	bool GetIsHeal() const { return m_isHeal; }
	void SetIsHeal(bool b) { m_isHeal = b; }

	bool GetCanTransform() const { return m_canTransform; }
	void SetCanTransform(bool b) { m_canTransform = b; }

	bool GetIsDefence() const { return m_isDefence; }
	void SetIsDefence(bool b) { m_isDefence = b; }

	bool GetIsDummy() const { return m_isDummy; }
	void SetIsDummy(bool b) { m_isDummy = b; }

	// NPC
	MOVE_TYPE GetMoveType() const { return m_moveType; }
	void SetMoveType(MOVE_TYPE m) { m_moveType = m; }

	int GetTargetID() const { return m_targetID; }
	void SetTargetID(int id) { m_targetID = id; }

	Vec3 GetNextPos(int i) const { return m_nextPos[i]; }
	void SetNextPos(int i, float x, float y, float z) { m_nextPos[i].x = x;  m_nextPos[i].y = y;  m_nextPos[i].z = z; }

	int GetNextPosIndex() const { return m_nextPosIndex; }
	void SetNextPosIndex(int i) { m_nextPosIndex = i; }

private:
	SOCKET				m_socket;

	int					m_id;
	char				m_name[MAX_ID_LEN];

	Vec3				m_pos;
	Vec3				m_look;
	Vec3				m_up;
	Vec3				m_right;

	float				m_speed;

	short				m_currentHp, m_maxHp;
	short				m_damage;
	short				m_currentExp, m_maxExp;
	short				m_level;

	std::mutex			m_clientLock;

	int					m_prevSize;

	int					m_myType;

	bool				m_isMove;

	int					m_walkStatus;

	bool				m_moveDirection[4];

	std::unordered_set<int> m_viewList;

	bool				m_isAttack;	// ������?

	bool				m_isHeal;

	bool				m_canTransform;

	bool				m_isDefence;

	bool				m_isDummy;

	// Only NPC
	MOVE_TYPE			m_moveType;
	int					m_targetID;

	Vec3				m_nextPos[3];
	int					m_nextPosIndex;

public:
	OVER_EX				m_recvOver;
	char				m_packetBuf[MAX_PACKET_SIZE];

	std::atomic<STATUS>	m_status = ST_FREE;
};