#include "pch.h"
#include "ServerFrame.h"
#include "GameTimer.h"
#include "DB.h"
#include "Error.h"
#include "Player.h"
#include "Sender.h"
#include "NPC.h"
#include "SimpleMath.h"

#include <iostream>
#include <random>

constexpr int WORKER_THREAD_NUM{ 4 };

CServerFrame::CServerFrame()
{
	m_useDB = false;

	m_dummyIndex = 0;

	m_prevTime = std::chrono::system_clock::now();

	m_error = new CError;
	m_sender = new CSender;
	m_DB = new CDB;
}

CServerFrame::~CServerFrame()
{
	if (nullptr != m_error) {
		delete m_error;
		m_error = nullptr;
	}

	if (nullptr != m_sender) {
		delete m_sender;
		m_sender = nullptr;
	}

	m_timerThread.join();
	for (std::thread& t : m_workerThread) t.join();
}

// public function

void CServerFrame::Progress()
{
	InitServer();
}
// private function

void CServerFrame::InitServer()
{
	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 2), &WSAData);
	m_listenSocket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (INVALID_SOCKET == m_listenSocket) {
		int err_no = WSAGetLastError();
		if (WSA_IO_PENDING != err_no)
			m_error->error_display("Listen Socket Error : ", err_no);
	}

	SOCKADDR_IN serverAddr;
	memset(&serverAddr, 0, sizeof(SOCKADDR_IN));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(SERVER_PORT);
	serverAddr.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
	int ret = ::bind(m_listenSocket, reinterpret_cast<sockaddr *>(&serverAddr), sizeof(serverAddr));
	if (0 != ret) {
		int err_no = WSAGetLastError();
		if (WSA_IO_PENDING != err_no)
			m_error->error_display("Bind Error : ", err_no);
	}

	listen(m_listenSocket, SOMAXCONN);

	m_iocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0);

	InitClients();
	CreateNPC();
	//CreateNPCNone();
	CreateIoCompletionPort(reinterpret_cast<HANDLE>(m_listenSocket), m_iocp, 9999999, 0);
	SOCKET c_sock = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	OVER_EX acceptOver;
	ZeroMemory(&acceptOver.over, sizeof(acceptOver.over));
	acceptOver.event_type = EV_ACCEPT;
	acceptOver.c_sock = c_sock;
	AcceptEx(m_listenSocket, c_sock, acceptOver.net_buf, NULL, sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16, NULL, &acceptOver.over);

	// Create worker thread
	m_workerThread.reserve(WORKER_THREAD_NUM);
	for (int i = 0; i < WORKER_THREAD_NUM; ++i)
		m_workerThread.emplace_back(CreateWorkerThread());

	// Create timer thread
	m_timerThread = CreateTimerThread();

	EVENT ev{ MASTER, std::chrono::high_resolution_clock::now() + 200ms, EV_BROADCAST, 0 };
	AddTimer(ev);
}

std::thread CServerFrame::CreateWorkerThread()
{
	return std::thread([this] { this->DoWorker(); });
}

void CServerFrame::DoWorker()
{
	printf("Start worker_thread\n");

	while (true) {
		DWORD ioBytes;
		ULONG_PTR key;
		WSAOVERLAPPED* over;

		GetQueuedCompletionStatus(m_iocp, &ioBytes, &key, &over, INFINITE);

		OVER_EX* over_ex = reinterpret_cast<OVER_EX*>(over);
		int id = static_cast<int>(key);
		CObject& cl = m_objects[id];

		//std::cout << over_ex->event_type << std::endl;
		//std::cout << over_ex->c_sock << std::endl;

		switch (over_ex->event_type) {
		case EV_ACCEPT: {
			printf("Accept Player\n");
			int user_id = -1;
			for (int i = 0; i < NPC_ID_START; ++i) {
				m_objects[i].ClientLock();
				if (ST_FREE == m_objects[i].m_status) {
					m_objects[i].m_status = ST_ALLOC;
					m_objects[i].ClientUnLock();
					user_id = i;
					break;
				}
				m_objects[i].ClientUnLock();
			}

			//printf("%d", user_id);
			SOCKET c_sock = over_ex->c_sock;
			if (-1 == user_id) {
				closesocket(c_sock);
			}
			else {
				CreateIoCompletionPort(reinterpret_cast<HANDLE>(c_sock), m_iocp, user_id, 0);
				CObject& newPlayer = m_objects[user_id];
				newPlayer.SetSocket(c_sock);
				newPlayer.SetID(user_id);
				newPlayer.SetPrevSize(0);
				newPlayer.ClearViewList();
				newPlayer.m_recvOver.wsabuf.buf = newPlayer.m_recvOver.net_buf;
				newPlayer.m_recvOver.wsabuf.len = MAX_BUFFER;
				newPlayer.m_recvOver.event_type = EV_RECV;
				DWORD flags = 0;
				WSARecv(c_sock, &newPlayer.m_recvOver.wsabuf, 1, NULL, &flags, &newPlayer.m_recvOver.over, NULL);
			}

			c_sock = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
			over_ex->c_sock = c_sock;
			ZeroMemory(&over_ex->over, sizeof(over_ex->over));
			AcceptEx(m_listenSocket, c_sock, over_ex->net_buf, NULL, sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16, NULL, &over_ex->over);

			break;
		}

		case EV_RECV: {
			if (0 == ioBytes) {
				Disconnect(id);
			}
			else {
				RecvPacketConstruct(id, ioBytes);
				ZeroMemory(&m_objects[id].m_recvOver.over, sizeof(m_objects[id].m_recvOver.over));
				DWORD flags = 0;
				WSARecv(m_objects[id].GetSocket(), &m_objects[id].m_recvOver.wsabuf, 1, NULL, &flags, &m_objects[id].m_recvOver.over, NULL);
			}
			break;
		}

		case EV_SEND:
			if (0 == ioBytes) {
				Disconnect(id);
			}
			delete over_ex;
			break;

		case EV_BROADCAST:
			MoveUpdate();
			break;

		case EV_ATTACK:
			m_objects[id].SetIsAttack(false);
			break;

		case EV_TRANSFORM:
			//std::cout << "변신 풀림\n";
			PlayerTransform(id, false);
			break;

		case EV_RESPAWN:
			RespawnNPC(id);
			break;

		case EV_HEAL:
			PlayerHeal(id);
			break;

		case EV_TRANSFORM_COOLDOWNTIME:
			m_objects[id].SetCanTransform(true);
			break;

		default:
			std::cout << "Unknown EVENT\n";
			while (true);
		}
	}
}

void CServerFrame::Disconnect(int id)
{
	m_sender->SendLeavePacket(m_objects[id].GetSocket(), id, m_objects[id].GetMyType());
	m_objects[id].ClientLock();
	m_objects[id].m_status = ST_ALLOC;
	closesocket(m_objects[id].GetSocket());
	for (int i = 0; i < NPC_ID_START; ++i) {
		CObject& cl = m_objects[i];
		if (id == cl.GetID()) continue;
		//cl.ClientLock();
		if (ST_ACTIVE == cl.m_status) {
			cl.ClientLock();
			cl.EraseViewList(id);
			cl.ClientUnLock();
			m_sender->SendLeavePacket(cl.GetSocket(), id, m_objects[id].GetMyType());
		}
		//cl.ClientUnLock();
	}
	m_objects[id].m_status = ST_FREE;
	m_objects[id].ClientUnLock();
}

void CServerFrame::RecvPacketConstruct(int id, int ioBytes)
{
	OVER_EX& recv_over = m_objects[id].m_recvOver;

	int restBytes = ioBytes;
	char* p = recv_over.net_buf;
	int packetSize = 0;
	if (0 != m_objects[id].GetPrevSize()) {
		packetSize = m_objects[id].m_packetBuf[0];   // size
	}

	while (restBytes > 0) {
		if (0 == packetSize) {
			packetSize = *p;   //p[0]
		}
		if (packetSize <= restBytes + m_objects[id].GetPrevSize()) {
			memcpy(m_objects[id].m_packetBuf + m_objects[id].GetPrevSize(), p, packetSize - m_objects[id].GetPrevSize());
			p += packetSize - m_objects[id].GetPrevSize();
			restBytes -= packetSize - m_objects[id].GetPrevSize();
			packetSize = 0;
			ProcessPacket(id, m_objects[id].m_packetBuf);
			m_objects[id].SetPrevSize(0);
		}
		else {
			memcpy(m_objects[id].m_packetBuf + m_objects[id].GetPrevSize(), p, restBytes);
			m_objects[id].SetPrevSize(m_objects[id].GetPrevSize() + restBytes);
			restBytes = 0;
			p += restBytes;
		}
	}
}

std::thread CServerFrame::CreateTimerThread()
{
	return std::thread([this] { this->DoTimer(); });
}

void CServerFrame::AddTimer(EVENT ev)
{
	m_timerLock.lock();
	m_timerQueue.push(ev);
	m_timerLock.unlock();
}

void CServerFrame::DoTimer()
{
	printf("Start timer thread\n");

	while (true) {
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
		while (true) {
			m_timerLock.lock();
			if (true == m_timerQueue.empty()) {
				m_timerLock.unlock();
				break;
			}

			if (m_timerQueue.top().wakeup_time > std::chrono::high_resolution_clock::now()) {
				m_timerLock.unlock();
				break;
			}

			EVENT ev = m_timerQueue.top();
			m_timerQueue.pop();
			m_timerLock.unlock();

			OVER_EX* overEx = new OVER_EX;
			overEx->event_type = ev.event_type;
			*(reinterpret_cast<int*>(overEx->net_buf)) = ev.target_obj;
			PostQueuedCompletionStatus(m_iocp, 1, ev.obj_id, &overEx->over);
		}
	}
}

bool CServerFrame::IsPlayer(int id)
{
	return id < NPC_ID_START;
}

void CServerFrame::EnterGame(int id, const char* name)
{
	m_objects[id].ClientLock();
	m_objects[id].SetMyName(name);
	m_sender->SendLoginOkPacket(m_objects[id].GetSocket(), id, m_objects[id].GetPos().x, m_objects[id].GetPos().y, m_objects[id].GetPos().z, m_objects[id].GetDamage(), m_objects[id].GetCurrentHp(), m_objects[id].GetMaxHp(), m_objects[id].GetLevel(), m_objects[id].GetCurrentExp(), m_objects[id].GetMaxExp());
	m_objects[id].m_status = ST_ACTIVE;
	m_objects[id].ClientUnLock();

	for (auto& cl : m_objects) {
		int i = cl.GetID();
		if (id == i) continue;
		if (true == IsNear(id, i)) {
			//g_clients[i].m_cl.lock();
			if (ST_SLEEP == m_objects[i].m_status) {
				ActivateNPC(i);
			}
			if (ST_ACTIVE == m_objects[i].m_status) {
				m_objects[id].ClientLock();
				m_objects[id].InsertViewList(i);
				m_objects[id].ClientUnLock();
				//printf("보낼게 %d에게 %d를\n", id, i);
				m_sender->SendEnterPacket(m_objects[id].GetSocket(), i, m_objects[i].GetPos().x, m_objects[i].GetPos().y, m_objects[i].GetPos().z, m_objects[i].GetMyType());
				if (true == IsPlayer(i)) {
					m_objects[i].ClientLock();
					m_objects[i].InsertViewList(id);
					m_objects[i].ClientUnLock();
					m_sender->SendEnterPacket(m_objects[i].GetSocket(), id, m_objects[id].GetPos().x, m_objects[id].GetPos().y, m_objects[id].GetPos().z, m_objects[id].GetMyType());
				}
			}
			//g_clients[i].m_cl.unlock();
		}
	}
}

void CServerFrame::DoRandomMove(int npc_id)
{
	float x = m_objects[npc_id].GetPos().x;
	float y = m_objects[npc_id].GetPos().y;
	float z = m_objects[npc_id].GetPos().z;
	float speed = m_objects[npc_id].GetSpeed();

	int index = m_objects[npc_id].GetNextPosIndex();
	Vec3 nextPos = m_objects[npc_id].GetNextPos(index);

	Vec3 dir;
	dir.x = nextPos.x - x;
	dir.y = nextPos.y - y;
	dir.z = nextPos.z - z;
	Vec3 nor = dir.Normalize();

	x += m_elapsedTime.count() * speed * nor.x;
	y += m_elapsedTime.count() * speed * nor.y;
	z += m_elapsedTime.count() * speed * nor.z;

	m_objects[npc_id].SetPos(x, y, z);
	m_objects[npc_id].SetLook(nor);

	float distance = Vec3::Distance(nextPos, m_objects[npc_id].GetPos());

	if (distance < 50.f) {
		index = index + 1;
		if (index >= 3) index = 0;
		m_objects[npc_id].SetNextPosIndex(index);
	}

	//Vec3 SendPos;
	//SendPos.x = x + m_elapsedTime.count() * speed * nor.x;
	//SendPos.y = y + m_elapsedTime.count() * speed * nor.y;
	//SendPos.z = z + m_elapsedTime.count() * speed * nor.z;

	//std::cout << x << ", " << y << ", " << z << std::endl;

	for (int i = 0; i < NPC_ID_START; ++i) {
		if (ST_ACTIVE != m_objects[i].m_status) continue;
		if (true == IsNear(i, npc_id)) {
			m_objects[i].ClientLock();
			if (0 != m_objects[i].GetViewListCount(npc_id)) {
				m_objects[i].ClientUnLock();
				m_sender->SendMovePacket(m_objects[i].GetSocket(), npc_id, m_objects[npc_id].GetPos().x, m_objects[npc_id].GetPos().y, m_objects[npc_id].GetPos().z, m_objects[npc_id].GetLook().x, m_objects[npc_id].GetLook().y, m_objects[npc_id].GetLook().z, WALK, std::chrono::system_clock::now());
			}
			else {
				m_objects[i].InsertViewList(npc_id);
				m_objects[i].ClientUnLock();
				m_sender->SendEnterPacket(m_objects[i].GetSocket(), npc_id, m_objects[npc_id].GetPos().x, m_objects[npc_id].GetPos().y, m_objects[npc_id].GetPos().z, m_objects[npc_id].GetMyType());
			}
		}
		else {
			m_objects[i].ClientLock();
			if (0 != m_objects[i].GetViewListCount(npc_id)) {
				m_objects[i].EraseViewList(npc_id);
				m_objects[i].ClientUnLock();
				m_sender->SendLeavePacket(m_objects[i].GetSocket(), npc_id, m_objects[npc_id].GetMyType());
			}
			else {
				m_objects[i].ClientUnLock();
			}
		}
	}

	for (int i = 0; i < NPC_ID_START; ++i) {
		if (true == IsNear(npc_id, i)) {
			if (ST_ACTIVE == m_objects[i].m_status) {
				return;
			}
		}
	}
	m_objects[npc_id].m_status = ST_SLEEP;
}

void CServerFrame::DoTargetMove(int npc_id)
{
	int player_id = m_objects[npc_id].GetTargetID();

	if (ST_ACTIVE != m_objects[npc_id].m_status) return;
	if (ST_ACTIVE != m_objects[player_id].m_status) return;
	if (RANDOM == m_objects[npc_id].GetMoveType()) return;

	if (true == IsPlayer(npc_id)) {
		std::cout << "ID :" << npc_id << " is not NPC!!\n";
		while (true);
	}

	if (false == IsNearNPC(player_id, npc_id)) {
		m_objects[npc_id].SetMoveType(RANDOM);
		m_objects[npc_id].SetTargetID(-1);
		char type = m_objects[npc_id].GetMyType();
		switch (type) {
		case O_BARGHEST: m_objects[npc_id].SetSpeed(BARGHEST_WALK_SPEED); break;
		case O_GRIFFON: m_objects[npc_id].SetSpeed(GRIFFON_WALK_SPEED); break;
		case O_DRAGON:m_objects[npc_id].SetSpeed(DRAGON_WALK_SPEED); break;
		}
		return;
	}

	float speed = m_objects[npc_id].GetSpeed();
	float x = m_objects[npc_id].GetPos().x;
	float y = m_objects[npc_id].GetPos().y;
	float z = m_objects[npc_id].GetPos().z;
	Vec3 dir;
	dir.x = m_objects[player_id].GetPos().x - x;
	dir.y = m_objects[player_id].GetPos().y - y;
	dir.z = m_objects[player_id].GetPos().z - z;
	Vec3 nor = dir.Normalize();

	//std::cout << nor.x << ", " << nor.y << ", " << nor.z << std::endl;
	//std::cout << m_elapsedTime.count() << std::endl;

	Vec3 A_vPos;
	A_vPos = m_objects[player_id].GetPos();

	Vec3 B_vPos;
	B_vPos = m_objects[npc_id].GetPos();

	float distance = Vec3::Distance(A_vPos, B_vPos);

	bool closed = false;
	switch (m_objects[npc_id].GetMyType()) {
	case O_DRAGON: 
		if (O_DRAKKEN == m_objects[player_id].GetMyType()) {
			if (distance < 490.f) closed = true;
		}
		else if (O_PLAYER == m_objects[player_id].GetMyType()) {
			if (distance < 310.f) closed = true;
		}
		break;
	case O_BARGHEST:
	case O_GRIFFON:
		if (O_DRAKKEN == m_objects[player_id].GetMyType()) {
			if (distance < 310.f) closed = true;
		}
		else if (O_PLAYER == m_objects[player_id].GetMyType()) {
			if (distance < 130.f) closed = true;
		}
		break;
	}

	if (true == closed) {
		if (false == m_objects[npc_id].GetIsAttack()) {
			m_objects[npc_id].SetIsAttack(true);

			EVENT new_ev{ npc_id, std::chrono::high_resolution_clock::now() + 2s, EV_ATTACK, 0 };
			AddTimer(new_ev);

			// 방어 넣으면 여기
			if (false == m_objects[player_id].GetIsDefence()) {
				m_objects[player_id].SetCurrentHp(m_objects[player_id].GetCurrentHp() - m_objects[npc_id].GetDamage());
				m_sender->SendHpPacket(m_objects[player_id].GetSocket(), m_objects[player_id].GetCurrentHp());
				//printf("Hp: %d\n", m_objects[player_id].GetCurrentHp());

				if (0 >= m_objects[player_id].GetCurrentHp()) {
					// 죽음
					short level = m_objects[player_id].GetLevel();
					short hp = m_objects[player_id].GetCurrentHp();
					short changeHp = hp - (50 * level);
					if (0 > changeHp) changeHp = 0;
					m_objects[player_id].SetCurrentExp(changeHp);
					m_objects[player_id].SetCurrentHp(50);
					m_objects[player_id].SetPos(VEC3_TOWN_ENTRANCE_POS);
					m_sender->SendPlayerDiePacket(m_objects[player_id].GetSocket(), player_id);
					std::unordered_set<int> vl = m_objects[player_id].GetViewList();
					for (const auto& id : vl) {
						if (false == IsPlayer(id)) continue;
						if (ST_ACTIVE != m_objects[id].m_status) continue;
						m_sender->SendPlayerDiePacket(m_objects[id].GetSocket(), player_id);
					}
				}
			}
			//////////////////////

			for (auto& cl : m_objects) {
				if (false == IsPlayer(cl.GetID())) continue;
				if (false == IsNear(cl.GetID(), npc_id)) continue;
				cl.ClientLock();
				if (ST_ACTIVE == cl.m_status) {
					m_sender->SendNPCAttackPacket(cl.GetSocket(), npc_id, x, z);
				}
				cl.ClientUnLock();
			}
		}
		return;
	}

	x += m_elapsedTime.count() * speed * nor.x;
	y += m_elapsedTime.count() * speed * nor.y;
	z += m_elapsedTime.count() * speed * nor.z;

	bool check = false;
	for (int i = 0; i < NUM_OBSTACLES; ++i) {
		if (-999 == m_obstacles[i].xScale) break;

		Vec3 pPos;
		pPos.x = x;
		pPos.y = y;
		pPos.z = z;
		Vec3 oPos;
		oPos.x = m_obstacles[i].xPos;
		oPos.y = m_obstacles[i].yPos;
		oPos.z = m_obstacles[i].zPos;
		float r = m_obstacles[i].zScale;

		float distance = Vec3::Distance(pPos, oPos);

		if (distance <= r) {
			check = true;
			break;
		}
	}

	if (false == check) {
		m_objects[npc_id].SetPos(x, y, z);
	}

	m_objects[npc_id].SetLook(nor);

	char status;
	if (O_BARGHEST == m_objects[npc_id].GetMyType()) status = RUN;
	else status = WALK;

	for (int i = 0; i < NPC_ID_START; ++i) {
		if (ST_ACTIVE != m_objects[i].m_status) continue;
		if (true == IsNear(i, npc_id)) {
			m_objects[i].ClientLock();
			if (0 != m_objects[i].GetViewListCount(npc_id)) {
				m_objects[i].ClientUnLock();
				m_sender->SendMovePacket(m_objects[i].GetSocket(), npc_id, m_objects[npc_id].GetPos().x, m_objects[npc_id].GetPos().y, m_objects[npc_id].GetPos().z, m_objects[npc_id].GetLook().x, m_objects[npc_id].GetLook().y, m_objects[npc_id].GetLook().z, status, std::chrono::system_clock::now());
			}
			else {
				m_objects[i].InsertViewList(npc_id);
				m_objects[i].ClientUnLock();
				m_sender->SendEnterPacket(m_objects[i].GetSocket(), npc_id, m_objects[npc_id].GetPos().x, m_objects[npc_id].GetPos().y, m_objects[npc_id].GetPos().z, m_objects[npc_id].GetMyType());
			}
		}
		else {
			m_objects[i].ClientLock();
			if (0 != m_objects[i].GetViewListCount(npc_id)) {
				m_objects[i].EraseViewList(npc_id);
				m_objects[i].ClientUnLock();
				m_sender->SendLeavePacket(m_objects[i].GetSocket(), npc_id, m_objects[npc_id].GetMyType());
			}
			else {
				m_objects[i].ClientUnLock();
			}
		}
	}
}

void CServerFrame::UpdatePlayerPos(int id)
{
	unordered_set<int> vl = m_objects[id].GetViewList();
	for (const int& npc : vl) {
		if (true == IsPlayer(npc)) continue;
		if (ST_ACTIVE != m_objects[npc].m_status) continue;
		if (true == IsNearNPC(id, npc)) {
			m_objects[npc].SetMoveType(TARGET);
			m_objects[npc].SetTargetID(id);
			char type = m_objects[npc].GetMyType();
			switch (type) {
			case O_BARGHEST: m_objects[npc].SetSpeed(BARGHEST_RUN_SPEED); break;
			case O_GRIFFON: m_objects[npc].SetSpeed(GRIFFON_WALK_SPEED); break;
			case O_DRAGON:m_objects[npc].SetSpeed(DRAGON_RUN_SPEED); break;
			}
		}
	}

	//if (false == m_objects[id].GetIsMove()) { 
		//if (false == m_objects[id].GetIsHeal()) {
		//	if (m_objects[id].GetMaxHp() > m_objects[id].GetCurrentHp()) {
		//		m_objects[id].SetIsHeal(true);
		//		EVENT ev{ id, std::chrono::high_resolution_clock::now() + 3s, EV_HEAL, 0 };
		//		AddTimer(ev);
		//	}
		//}
		//return;
	//}

	Vec3 pos = m_objects[id].GetPos();
	Vec3 look = m_objects[id].GetLook();

	m_objects[id].ClientLock();

	std::unordered_set<int> oldViewList = m_objects[id].GetViewList();
	m_objects[id].ClientUnLock();
	std::unordered_set<int> newViewList;

	for (auto& cl : m_objects) {
		if (false == IsNear(cl.GetID(), id)) continue;
		if (ST_SLEEP == cl.m_status) ActivateNPC(cl.GetID());
		if (ST_ACTIVE != cl.m_status) continue;
		if (cl.GetID() == id) continue;
		newViewList.insert(cl.GetID());
	}

	for (auto& np : newViewList) {
		if (0 == oldViewList.count(np)) {	// Object가 시야에 새로 들어왔을 때.
			m_objects[id].ClientLock();
			m_objects[id].InsertViewList(np);
			m_objects[id].ClientUnLock();
			m_sender->SendEnterPacket(m_objects[id].GetSocket(), np, m_objects[np].GetPos().x, m_objects[np].GetPos().y, m_objects[np].GetPos().z, m_objects[np].GetMyType());
			if (false == IsPlayer(np)) continue;
			m_objects[np].ClientLock();
			if (0 == m_objects[np].GetViewListCount(id)) {
				m_objects[np].InsertViewList(id);
				m_objects[np].ClientUnLock();
				m_sender->SendEnterPacket(m_objects[np].GetSocket(), id, m_objects[id].GetPos().x, m_objects[id].GetPos().y, m_objects[id].GetPos().z, m_objects[id].GetMyType());
			}
			else {
				m_objects[np].ClientUnLock();
				m_sender->SendMovePacket(m_objects[np].GetSocket(), id, m_objects[id].GetPos().x, m_objects[id].GetPos().y, m_objects[id].GetPos().z, m_objects[id].GetLook().x, m_objects[id].GetLook().y, m_objects[id].GetLook().z, m_objects[id].GetWalkStatus(), std::chrono::system_clock::now());
			}
		}
		else {							// Object가 계속 시야에 존재하고 있을 떄.
			if (false == IsPlayer(np)) continue;
			m_objects[np].ClientLock();
			if (0 != m_objects[np].GetViewListCount(id)) {
				m_objects[np].ClientUnLock();
				m_sender->SendMovePacket(m_objects[np].GetSocket(), id, m_objects[id].GetPos().x, m_objects[id].GetPos().y, m_objects[id].GetPos().z, m_objects[id].GetLook().x, m_objects[id].GetLook().y, m_objects[id].GetLook().z, m_objects[id].GetWalkStatus(), std::chrono::system_clock::now());
			}
			else {
				m_objects[np].ClientUnLock();
				m_objects[np].InsertViewList(id);
				m_sender->SendEnterPacket(m_objects[np].GetSocket(), id, m_objects[id].GetPos().x, m_objects[id].GetPos().y, m_objects[id].GetPos().z, m_objects[id].GetMyType());
			}
		}
	}

	for (auto& op : oldViewList) {		// Object가 시야에서 벗어났을 때.
		if (0 == newViewList.count(op)) {
			m_objects[id].ClientLock();
			m_objects[id].EraseViewList(op);
			m_objects[id].ClientUnLock();
			m_sender->SendLeavePacket(m_objects[id].GetSocket(), op, m_objects[op].GetMyType());
			if (false == IsPlayer (op)) continue;
			m_objects[op].ClientLock();
			if (0 != m_objects[op].GetViewListCount(id)) {
				m_objects[op].EraseViewList(id);
				m_objects[op].ClientUnLock();
				m_sender->SendLeavePacket(m_objects[op].GetSocket(), id, m_objects[id].GetMyType());
			}
			else {
				m_objects[op].ClientUnLock();
			}
		}
	}

	//////////////////////////////////////////////
	//for (auto& cl : m_objects) {
	//	if (false == IsPlayer(cl.GetID())) continue;
	//	cl.ClientLock();
	//	if (ST_ACTIVE == cl.m_status) {
	//		m_sender->SendMovePacket(cl.GetSocket(), id,
	//			m_objects[id].GetPos().x, m_objects[id].GetPos().y, m_objects[id].GetPos().z,
	//			m_objects[id].GetLook().x, m_objects[id].GetLook().y, m_objects[id].GetLook().z, m_objects[id].GetWalkStatus());
	//	}
	//	cl.ClientUnLock();
	//}
	//////////////////////////////////////////////
}

void CServerFrame::ChangePlayerStat(int id, bool kind)
{
	short maxHp = m_objects[id].GetMaxHp();
	short currentHp = m_objects[id].GetCurrentHp();
	short damage = m_objects[id].GetDamage();

	if (true == kind) {	// 사람 -> 용
		maxHp = maxHp * 2;
		if (currentHp * 2 + 100 < maxHp)
			currentHp = currentHp * 2 + 100	;
		else
			currentHp = maxHp;
		damage = damage * 2;
	}
	else if (false == kind) { // 용 -> 사람
		maxHp = maxHp / 2;
		currentHp = currentHp / 2;
		damage = damage / 2;
	}

	m_objects[id].SetMaxHp(maxHp);
	m_objects[id].SetCurrentHp(currentHp);
	m_objects[id].SetDamage(damage);
}

void CServerFrame::PlayerTransform(int id, bool kind)
{
	// kind가 true면 사람->용 / false면 용->사람
	if (true == kind) {
		m_objects[id].SetMyType(O_DRAKKEN);
		m_objects[id].SetSpeed(DRAGON_WALK_SPEED);
		ChangePlayerStat(id, true);
		EVENT ev{ id, std::chrono::high_resolution_clock::now() + 15s, EV_TRANSFORM, 0 };	// 15초 뒤에 변신 풀리게 만들기
		AddTimer(ev);
	}
	else if (false == kind) {
		m_objects[id].SetMyType(O_PLAYER);
		m_objects[id].SetSpeed(VIKING_WALK_SPEED);
		ChangePlayerStat(id, false);
		EVENT ev{ id, std::chrono::high_resolution_clock::now() + 20s, EV_TRANSFORM_COOLDOWNTIME, 0 };	// 변신 풀린 후 쿨타임 20초
		AddTimer(ev);
	}

	unordered_set<int> vl = m_objects[id].GetViewList();

	m_sender->SendTransformPacket(m_objects[id].GetSocket(), id, kind);

	for (const auto player : vl) {
		if (ST_ACTIVE != m_objects[player].m_status) continue;
		if (false == IsPlayer(player)) continue;
		m_sender->SendTransformPacket(m_objects[player].GetSocket(), id, kind);
	}
}

void CServerFrame::MoveUpdate()
{
	std::chrono::time_point<std::chrono::system_clock> curTime
		= std::chrono::system_clock::now();

	//std::cout << (std::chrono::high_resolution_clock::now().time_since_epoch().count()) << std::endl;

	for (auto& obj : m_objects) {
		if (ST_ACTIVE != obj.m_status) continue;
		if (false == IsPlayer(obj.GetID())) {
			if (false == obj.GetIsAttack()) {
				if (obj.GetMoveType() == RANDOM) DoRandomMove(obj.GetID());
				else DoTargetMove(obj.GetID());
			}
		}
		else UpdatePlayerPos(obj.GetID());
	}
	m_elapsedTime = curTime - m_prevTime;
	//std::cout << m_elapsedTime.count() << std::endl;

	m_prevTime = curTime;

	EVENT ev{ MASTER, std::chrono::high_resolution_clock::now() + 200ms, EV_BROADCAST, 0 };
	AddTimer(ev);
}

bool CServerFrame::IsNear(int a, int b)
{
	if (abs(m_objects[a].GetPos().x - m_objects[b].GetPos().x) > VIEW_RADIUS) return false;
	if (abs(m_objects[a].GetPos().z - m_objects[b].GetPos().z) > VIEW_RADIUS) return false;
	return true;
}

void CServerFrame::ActivateNPC(int id)
{
	STATUS oldState = ST_SLEEP;
	std::atomic_compare_exchange_strong(&m_objects[id].m_status, &oldState, ST_ACTIVE);
	//std::cout << m_objects[id].m_status << std::endl;
}

void CServerFrame::PlayerHeal(int id)
{
	if (false == m_objects[id].GetIsHeal()) return;

	m_objects[id].SetCurrentHp(m_objects[id].GetCurrentHp() + 10);

	short maxHp = m_objects[id].GetMaxHp();
	short currentHp = m_objects[id].GetCurrentHp();

	if (maxHp < currentHp) {	// 최대 체력을 넘으면 더 이상 HEAL 필요 없음.
		m_objects[id].SetCurrentHp(m_objects[id].GetMaxHp());
		m_objects[id].SetIsHeal(false);
		m_sender->SendHpPacket(m_objects[id].GetSocket(), maxHp);
		return;
	}

	m_sender->SendHpPacket(m_objects[id].GetSocket(), currentHp);
	EVENT ev{ id, std::chrono::high_resolution_clock::now() + 3s, EV_HEAL, 0 };
	AddTimer(ev);
}

bool CServerFrame::IsNearNPC(int player, int npc)
{
	Vec3 A_vPos;
	A_vPos = m_objects[player].GetPos();

	Vec3 B_vPos;
	B_vPos = m_objects[npc].GetPos();

	float distance = Vec3::Distance(A_vPos, B_vPos);

	if (O_DRAKKEN == m_objects[player].GetMyType()) {
		if (distance > 70.f) return false;
	}
	else if (O_PLAYER == m_objects[player].GetMyType()) {
		if (distance > 500.f) return false;
	}

	return true;
}

void CServerFrame::RespawnNPC(int npc_id)
{
	m_objects[npc_id].m_status = ST_SLEEP;
	switch (m_objects[npc_id].GetMyType()) {
	case O_BARGHEST:
		m_objects[npc_id].SetCurrentHp(150);
		m_objects[npc_id].SetCurrentHp(150);
		break;
	case O_GRIFFON:
		m_objects[npc_id].SetCurrentHp(250);
		m_objects[npc_id].SetCurrentHp(250);
		break;

	case O_DRAGON:
		m_objects[npc_id].SetCurrentHp(1000);
		m_objects[npc_id].SetCurrentHp(1000);
		break;

	default:
		printf("Respawn NPC error\n");
		while (true);
	}
}

void CServerFrame::SetMoveDirection(int id, char direction, bool b)
{
	m_objects[id].SetMoveDirection(direction, b);

	//for (int i = 0; i < 4; ++i)
		//std::cout << m_objects[id].GetMoveDirection(i) << std::endl;

	for (int i = 0; i < 4; ++i) {
		if (true == m_objects[id].GetMoveDirection(i)) {
			if (false == m_objects[id].GetIsMove()) {
				m_objects[id].SetIsMove(true);
				return;
			}
			return;
		}
	}

	m_objects[id].SetIsMove(false);

	// 안 움직이면 클라이언트에서 IDLE 애니메이션을 보여주기 위해 멈췄음을 알려준다.
	// 내 시야 안에 있는 PC에게만 보내주면 된다.
	std::unordered_set<int> viewList = m_objects[id].GetViewList();

	for (const auto& vl : viewList) {
		if (ST_ACTIVE != m_objects[vl].m_status) continue;
		if (false == IsPlayer(vl)) continue;
		m_sender->SendStopPacket(m_objects[vl].GetSocket(), id);
	}
}

void CServerFrame::Attack(int id, char type)
{
	if (true == m_objects[id].GetIsAttack()) return;
	else {
		m_objects[id].SetIsAttack(true);

		// 시야 내에 있는 npc에 대해서 체크
		std::unordered_set<int> viewList = m_objects[id].GetViewList();

		for (const auto& vl : viewList) {
			if (ST_ACTIVE != m_objects[vl].m_status) continue;
			if (true == IsPlayer(vl)) {
				m_sender->SendPlayerAttackPacket(m_objects[vl].GetSocket(), id);
			}
			else tempCollisionCheck(id, vl);
		}
	}
}

void CServerFrame::tempCollisionCheck(int A, int B)
{
	Vec3 A_vPos;
	A_vPos = m_objects[A].GetPos();

	Vec3 B_vPos;
	B_vPos = m_objects[B].GetPos();

	float distance = Vec3::Distance(A_vPos, B_vPos);

	//std::cout << B << std::endl;
	//std::cout << distance << std::endl;

	if (distance > 130.f) return;
	else {
		m_objects[B].SetMoveType(TARGET);
		m_objects[B].SetTargetID(A);

		short damage = m_objects[A].GetDamage();		// A 데미지
		short ex_hp = m_objects[B].GetCurrentHp();      // B 현재 체력
		short cu_hp = ex_hp - damage;					// 현재 체력 - 데미지 -> 현재 체력으로
		m_objects[B].SetCurrentHp(cu_hp);

		if (0 > cu_hp) {
			m_objects[B].m_status = ST_FREE;

			// 스텟 바꾼다
			m_objects[A].SetCurrentExp(m_objects[A].GetCurrentExp() + m_objects[B].GetLevel() * m_objects[B].GetLevel() * 5);
			while (m_objects[A].GetCurrentExp() >= m_objects[A].GetMaxExp()) {
				m_objects[A].SetCurrentExp(m_objects[A].GetCurrentExp() - m_objects[A].GetMaxExp());
				m_objects[A].SetLevel(m_objects[A].GetLevel() + 1);
				m_objects[A].SetDamage(35 + m_objects[A].GetLevel() * 5);
				m_objects[A].SetMaxHp(100 + (20 * (m_objects[A].GetLevel() - 1)));
				m_objects[A].SetCurrentHp(m_objects[A].GetMaxHp());
				m_objects[A].SetMaxExp(100 + (100 * (m_objects[A].GetLevel() - 1)));
			}

			m_sender->SendStatChangePacket(m_objects[A].GetSocket(), m_objects[A].GetLevel(), m_objects[A].GetCurrentHp(), m_objects[A].GetMaxHp(), m_objects[A].GetCurrentExp(), m_objects[A].GetMaxExp());

			EVENT ev{ B, std::chrono::high_resolution_clock::now() + 20s, EV_RESPAWN, 0 };
			AddTimer(ev);

			for (auto& cl : m_objects) {
				if (ST_ACTIVE != cl.m_status) continue;
				if (false == IsPlayer(cl.GetID())) continue;
				m_sender->SendNPCDiePacket(cl.GetSocket(), B, m_objects[B].GetMyType());
			}
		}
	}
}

void CServerFrame::PlayerDefence(int id, bool kind)
{
	m_objects[id].SetIsDefence(kind);

	unordered_set<int> vl = m_objects[id].GetViewList();

	for (const auto& cl : vl) {
		if (ST_ACTIVE != m_objects[cl].m_status) continue;
		if (false == IsPlayer(cl)) continue;
		m_sender->SendDefencePacket(m_objects[cl].GetSocket(), id, kind);
	}
}

void CServerFrame::ProcessPacket(int id, char* buf)
{
	switch (buf[1]) {
	case CS_LOGIN: {
		printf("%d번 Player Login\n", id);
		cs_packet_login* packet = reinterpret_cast<cs_packet_login*>(buf);

		if (true == m_useDB) {
			if (NORMAL == packet->loginType) {
				// DB에서 확인하자
				int i_to = atoi(packet->name);						// DB에는 INT 형태로 저장 -> char* to int
				int p_to = atoi(packet->pw);
				int ret = m_DB->CheckID(i_to, p_to);				// DB에 없다면 -> return 값이 0이다.

				if (0 == ret) {	// DB에 정보 없음
					//std::cout << "플레이어 정보 없음\n";
					m_sender->SendLoginFailPacket(m_objects[id].GetSocket());
					break;
				}
				else {		// DB에 정보 있음
					//std::cout << "플레이어 정보 있음\n";
					DB_INFORM inform = m_DB->GetInform(i_to);
					m_objects[id].SetLevel((short)inform.level);
					m_objects[id].SetCurrentHp((short)inform.hp);
					m_objects[id].SetCurrentExp((short)inform.exp);
					short level = m_objects[id].GetLevel();
					m_objects[id].SetMaxExp(100 + (100 * (level - 1)));
					m_objects[id].SetMaxHp(100 + (20 * (level - 1)));
					m_objects[id].SetDamage(35 + (level * 5));
					std::cout << inform.level << ", " << inform.hp << ", " << inform.exp << std::endl;
				}
			}
		} 
		if (DUMMY == packet->loginType) {
			Vec3 pos = SetDummyPosition();
			m_objects[id].SetPos(pos);
			m_objects[id].SetIsDummy(true);
		}
		printf("Name: %s\n", packet->name);
		EnterGame(id, packet->name);
		break;
	}

	case CS_LOGOUT: {
		cs_packet_logout* packet = reinterpret_cast<cs_packet_logout*>(buf);
		printf("%d번 플레이어 Logout\n", id);
		if (true == m_useDB) {
			char* tmp;
			m_objects[id].GetMyName(tmp);
			m_DB->SavePlayerInform(tmp, m_objects[id].GetLevel(), m_objects[id].GetCurrentExp(), m_objects[id].GetCurrentHp());
		}
		ResetPlayer(id);
		break;
	}

	case CS_WALK: {
		//std::cout << "무브 받음\n";
		cs_packet_move* packet = reinterpret_cast<cs_packet_move*>(buf);
		m_objects[id].SetIsHeal(false);
		SetMoveDirection(id, packet->direction, true);
		//DoMove(id, packet->direction);
		break;
	}

	case CS_STOP: {
		cs_packet_move_stop* packet = reinterpret_cast<cs_packet_move_stop*>(buf);
		SetMoveDirection(id, packet->direction, false);
		break;
	}

	case CS_MOUSE_MOVE_PACKET: {
		cs_packet_mouse_move* packet = reinterpret_cast<cs_packet_mouse_move*>(buf);
		Vec3 look;
		look.x = packet->look[0];
		look.y = packet->look[1];
		look.z = packet->look[2];
		m_objects[id].SetLook(look);
		Vec3 right;
		right.x = packet->right[0];
		right.y = packet->right[1];
		right.z = packet->right[2];
		m_objects[id].SetRight(right);
		Vec3 up;
		up.x = packet->up[0];
		up.y = packet->up[1];
		up.z = packet->up[2];
		m_objects[id].SetUp(up);
		break;
	}

	case CS_POSITION: {
		cs_packet_position* packet = reinterpret_cast<cs_packet_position*>(buf);
		Vec3 pos;
		pos.x = packet->xPos;
		pos.y = packet->yPos;
		pos.z = packet->zPos;
		m_objects[id].SetPos(pos);
		Vec3 look;
		look.x = packet->xLook;
		look.y = packet->yLook;
		look.z = packet->zLook;
		m_objects[id].SetLook(look);
		if (true == m_objects[id].GetIsDummy()) m_sender->SendDummyPacket(m_objects[id].GetSocket(), id, packet->moveTime);

		break;
	}

	case CS_DEFENCE: {
		cs_packet_defence* packet = reinterpret_cast<cs_packet_defence*>(buf);

		std::cout << "방어 받음\n";

		PlayerDefence(id, packet->kind);

		break;
	}

	case CS_ATTACK: {
		cs_packet_attack* packet = reinterpret_cast<cs_packet_attack*>(buf);
	
		Attack(id, packet->attackType);
	
		EVENT new_ev{ id, std::chrono::high_resolution_clock::now() + 2s, EV_ATTACK, 0 };
		AddTimer(new_ev);
	
		break;
	}

	case CS_RUN: {
		cs_packet_run* packet = reinterpret_cast<cs_packet_run*>(buf);
		bool b = packet->running;
		if (true == b) {
			m_objects[id].SetSpeed(VIKING_RUN_SPEED);
			m_objects[id].SetWalkStatus(RUN);
		}
		else {
			m_objects[id].SetSpeed(VIKING_WALK_SPEED);
			m_objects[id].SetWalkStatus(WALK);
		}

		break;
	}

	case CS_TRANSFORM: {
		cs_packet_transform* packet = reinterpret_cast<cs_packet_transform*>(buf);

		if (O_PLAYER == m_objects[id].GetMyType()) {
			if (true == m_objects[id].GetCanTransform()) {
				m_objects[id].SetCanTransform(false);
				PlayerTransform(id, true);
			}
		}

		break;
	}

	case CS_QUEST_DONE: {
		cs_packet_quest_done* packet = reinterpret_cast<cs_packet_quest_done*>(buf);

		QuestDone(id);

		break;
	}

	case CS_INV: {
		cs_packet_invincibility* packet = reinterpret_cast<cs_packet_invincibility*>(buf);

		m_objects[id].SetIsDefence(packet->kind);

		break;
	}

	default:
		std::cout << "Unknown packet type error\n";
		DebugBreak();
		exit(-1);
	}
}

void CServerFrame::CreateNPCNone()
{
	for (int i = NPC_ID_START; i < GRIFFON_ID_END; ++i) {
		m_objects[i].m_status = ST_FREE;
		m_objects[i].SetID(i);
	}
}

void CServerFrame::InitClients()
{
	for (int i = 0; i < NPC_ID_START; ++i) {
		m_objects[i].SetIsDummy(false);
		m_objects[i].m_status = ST_FREE;
		m_objects[i].SetID(i);
		m_objects[i].SetSpeed(VIKING_WALK_SPEED);
		m_objects[i].SetMyType(O_PLAYER);

		int idx = 0;
		float x, z;
		switch (idx) {
		case 0:
			x = -1700.f;
			z = 3800.f;
			break;
		case 1:
			x = -1900.f;
			z = 4600.f;
			break;
		case 2:
			x = -2100.f;
			z = 5300.f;
			break;
		case 3:
			x = -2300.f;
			z = 4300.f;
			break;
		case 4:
			x = -2500.f;
			z = 5000.f;
			break;
		case 5:
			x = -2800.f;
			z = 4000.f;
			break;
		}
		idx = (idx + 1) % 6;
		Vec3 pos;
		pos.x = x;
		pos.y = 0.f;
		pos.z = z;
		m_objects[i].SetCurrentExp(0);
		m_objects[i].SetMaxExp(100);
		m_objects[i].SetCurrentHp(200);
		m_objects[i].SetMaxHp(200);
		m_objects[i].SetPos(pos);
		m_objects[i].SetDamage(50);
		m_objects[i].SetLevel(1);
		m_objects[i].SetIsAttack(false);
		//
	}
}

void CServerFrame::ResetPlayer(int id)
{
	m_objects[id].m_status = ST_FREE;
	m_objects[id].SetID(id);
	m_objects[id].SetSpeed(VIKING_WALK_SPEED);
	m_objects[id].SetMyType(O_PLAYER);
	int idx = 0;
	float x, z;
	switch (idx) {
	case 0:
		x = -1700.f;
		z = 3800.f;
		break;
	case 1:
		x = -1900.f;
		z = 4600.f;
		break;
	case 2:
		x = -2100.f;
		z = 5300.f;
		break;
	case 3:
		x = -2300.f;
		z = 4300.f;
		break;
	case 4:
		x = -2500.f;
		z = 5000.f;
		break;
	case 5:
		x = -2800.f;
		z = 4000.f;
		break;
	}
	idx = (idx + 1) % 6;
	Vec3 pos;
	pos.x = x;
	pos.y = 0.f;
	pos.z = z;
	m_objects[id].SetCurrentExp(0);
	m_objects[id].SetMaxExp(100);
	m_objects[id].SetCurrentHp(200);
	m_objects[id].SetMaxHp(200);
	m_objects[id].SetPos(pos);
	m_objects[id].SetDamage(50);
	m_objects[id].SetLevel(1);
	m_objects[id].SetIsAttack(false);
}

void CServerFrame::CreateNPC()
{
	std::cout << "Initializing NPC\n";

	for (int npc_id = NPC_ID_START; npc_id < BARGHEST_ID_END; ++npc_id) {
		m_objects[npc_id].SetID(npc_id);
		m_objects[npc_id].m_status = ST_SLEEP;
		m_objects[npc_id].SetSpeed(BARGHEST_WALK_SPEED);

		m_objects[npc_id].SetCurrentHp(150);
		m_objects[npc_id].SetMaxHp(150);

		m_objects[npc_id].SetLevel(3);

		m_objects[npc_id].SetMoveType(RANDOM);

		m_objects[npc_id].SetIsAttack(false);

		m_objects[npc_id].SetMyType(O_BARGHEST);

		m_objects[npc_id].SetNextPosIndex(0);

		m_objects[npc_id].SetDamage(m_objects[npc_id].GetLevel() * 10);
	}

	m_objects[NPC_ID_START].SetPos(900.f, 0.f, 1200.f);
	m_objects[NPC_ID_START].SetNextPos(0, 900.f, 0.f, 1200.f);
	m_objects[NPC_ID_START].SetNextPos(1, 1300.f, 0.f, 900.f);
	m_objects[NPC_ID_START].SetNextPos(2, 400.f, 0.f, 1500.f);

	m_objects[NPC_ID_START + 1].SetPos(70.f, 0.f, 450.f);
	m_objects[NPC_ID_START + 1].SetNextPos(0, 70.f, 0.f, 450.f);
	m_objects[NPC_ID_START + 1].SetNextPos(1, 370.f, 0.f, 750.f);
	m_objects[NPC_ID_START + 1].SetNextPos(2, 770.f, 0.f, 750.f);

	m_objects[NPC_ID_START + 2].SetPos(150.f, 0.f, -300.f);
	m_objects[NPC_ID_START + 2].SetNextPos(0, 150.f, 0.f, -300.f);
	m_objects[NPC_ID_START + 2].SetNextPos(1, 450.f, 0.f, -700.f);
	m_objects[NPC_ID_START + 2].SetNextPos(2, 750.f, 0.f, -300.f);

	m_objects[NPC_ID_START + 3].SetPos(-1200.f, 0.f, 800.f);
	m_objects[NPC_ID_START + 3].SetNextPos(0, -1200.f, 0.f, 800.f);
	m_objects[NPC_ID_START + 3].SetNextPos(1, -1500.f, 0.f, 1200.f);
	m_objects[NPC_ID_START + 3].SetNextPos(2, -900.f, 0.f, 800.f);

	m_objects[NPC_ID_START + 4].SetPos(-550.f, 0.f, 330.f);
	m_objects[NPC_ID_START + 4].SetNextPos(0, -550.f, 0.f, 330.f);
	m_objects[NPC_ID_START + 4].SetNextPos(1, -150.f, 0.f, 30.f);
	m_objects[NPC_ID_START + 4].SetNextPos(2, -150.f, 0.f, -330.f);

	m_objects[NPC_ID_START + 5].SetPos(450.f, 0.f, -300.f);
	m_objects[NPC_ID_START + 5].SetNextPos(0, 450.f, 0.f, -300.f);
	m_objects[NPC_ID_START + 5].SetNextPos(1, 750.f, 0.f, -600.f);
	m_objects[NPC_ID_START + 5].SetNextPos(2, 1050.f, 0.f, -600.f);

	m_objects[NPC_ID_START + 6].SetPos(-350.f, 0.f, 630.f);
	m_objects[NPC_ID_START + 6].SetNextPos(0, -350.f, 0.f, 630.f);
	m_objects[NPC_ID_START + 6].SetNextPos(1, -650.f, 0.f, 330.f);
	m_objects[NPC_ID_START + 6].SetNextPos(2, -650.f, 0.f, 30.f);

	m_objects[NPC_ID_START + 7].SetPos(-750.f, 0.f, 700.f);
	m_objects[NPC_ID_START + 7].SetNextPos(0, -750.f, 0.f, 700.f);
	m_objects[NPC_ID_START + 7].SetNextPos(1, -1050.f, 0.f, 400.f);
	m_objects[NPC_ID_START + 7].SetNextPos(2, -1350.f, 0.f, 400.f);

	m_objects[NPC_ID_START + 8].SetPos(750.f, 0.f, 330.f);
	m_objects[NPC_ID_START + 8].SetNextPos(0, 750.f, 0.f, 730.f);
	m_objects[NPC_ID_START + 8].SetNextPos(1, 450.f, 0.f, 330.f);
	m_objects[NPC_ID_START + 8].SetNextPos(2, 450.f, 0.f, 30.f);

	m_objects[NPC_ID_START + 9].SetPos(-1150.f, 0.f, -700.f);
	m_objects[NPC_ID_START + 9].SetNextPos(0, -1150.f, 0.f, -700.f);
	m_objects[NPC_ID_START + 9].SetNextPos(1, -850.f, 0.f, -600.f);
	m_objects[NPC_ID_START + 9].SetNextPos(2, -550.f, 0.f, -600.f);

	m_objects[NPC_ID_START + 10].SetPos(-1350.f, 0.f, -700.f);
	m_objects[NPC_ID_START + 10].SetNextPos(0, -1350.f, 0.f, -700.f);
	m_objects[NPC_ID_START + 10].SetNextPos(1, -1050.f, 0.f, -1000.f);
	m_objects[NPC_ID_START + 10].SetNextPos(2, -1350.f, 0.f, -1000.f);

	m_objects[NPC_ID_START + 11].SetPos(-750.f, 0.f, 1600.f);
	m_objects[NPC_ID_START + 11].SetNextPos(0, -750.f, 0.f, 1600.f);
	m_objects[NPC_ID_START + 11].SetNextPos(1, -450.f, 0.f, 1300.f);
	m_objects[NPC_ID_START + 11].SetNextPos(2, -150.f, 0.f, 1300.f);

	m_objects[NPC_ID_START + 12].SetPos(11500.f, 0.f, -2700.f);
	m_objects[NPC_ID_START + 12].SetNextPos(0, 11150.f, 0.f, -2700.f);
	m_objects[NPC_ID_START + 12].SetNextPos(1, 9850.f, 0.f, -2400.f);
	m_objects[NPC_ID_START + 12].SetNextPos(2, 9550.f, 0.f, -2400.f);

	m_objects[NPC_ID_START + 13].SetPos(8350.f, 0.f, -1700.f);
	m_objects[NPC_ID_START + 13].SetNextPos(0, 8350.f, 0.f, -1700.f);
	m_objects[NPC_ID_START + 13].SetNextPos(1, 8050.f, 0.f, -2000.f);
	m_objects[NPC_ID_START + 13].SetNextPos(2, 8350.f, 0.f, -2000.f);

	m_objects[NPC_ID_START + 14].SetPos(12550.f, 0.f, -1550.f);
	m_objects[NPC_ID_START + 14].SetNextPos(0, 12550.f, 0.f, -1550.f);
	m_objects[NPC_ID_START + 14].SetNextPos(1, 12250.f, 0.f, -1850.f);
	m_objects[NPC_ID_START + 14].SetNextPos(2, 11950.f, 0.f, -1850.f);


	for (int npc_id = BARGHEST_ID_END; npc_id < GRIFFON_ID_END; ++npc_id) {
		m_objects[npc_id].SetID(npc_id);
		m_objects[npc_id].m_status = ST_SLEEP;
		m_objects[npc_id].SetSpeed(GRIFFON_WALK_SPEED);

		m_objects[npc_id].SetCurrentHp(250);
		m_objects[npc_id].SetMaxHp(250);

		m_objects[npc_id].SetLevel(6);

		m_objects[npc_id].SetMoveType(RANDOM);

		m_objects[npc_id].SetIsAttack(false);

		m_objects[npc_id].SetMyType(O_GRIFFON);

		m_objects[npc_id].SetNextPosIndex(0);

		m_objects[npc_id].SetDamage(m_objects[npc_id].GetLevel() * 10);
	}

	m_objects[BARGHEST_ID_END].SetPos(300.f, 0.f, 9500.f);
	m_objects[BARGHEST_ID_END].SetNextPos(0, 300.f, 0.f, 9500.f);
	m_objects[BARGHEST_ID_END].SetNextPos(1, -100.f, 0.f, 9900.f);
	m_objects[BARGHEST_ID_END].SetNextPos(2, 700.f, 0.f, 9900.f);

	m_objects[BARGHEST_ID_END + 1].SetPos(70.f, 0.f, 10050.f);
	m_objects[BARGHEST_ID_END + 1].SetNextPos(0, 70.f, 0.f, 10450.f);
	m_objects[BARGHEST_ID_END + 1].SetNextPos(1, -450.f, 0.f, 10450.f);
	m_objects[BARGHEST_ID_END + 1].SetNextPos(2, 470.f, 0.f, 10450.f);

	m_objects[BARGHEST_ID_END + 2].SetPos(-1150.f, 0.f, 12000.f);
	m_objects[BARGHEST_ID_END + 2].SetNextPos(0, -1150.f, 0.f, 12000.f);
	m_objects[BARGHEST_ID_END + 2].SetNextPos(1, -1550.f, 0.f, 12400.f);
	m_objects[BARGHEST_ID_END + 2].SetNextPos(2, -1550.f, 0.f, 11600.f);

	m_objects[BARGHEST_ID_END + 3].SetPos(-2400.f, 0.f, 12100.f);
	m_objects[BARGHEST_ID_END + 3].SetNextPos(0, -2400.f, 0.f, 12100.f);
	m_objects[BARGHEST_ID_END + 3].SetNextPos(1, -2000, 0.f, 12500.f);
	m_objects[BARGHEST_ID_END + 3].SetNextPos(2, -2800.f, 0.f, 12500.f);

	m_objects[BARGHEST_ID_END + 4].SetPos(-1550.f, 0.f, 11830.f);
	m_objects[BARGHEST_ID_END + 4].SetNextPos(0, -1550.f, 0.f, 11830.f);
	m_objects[BARGHEST_ID_END + 4].SetNextPos(1, -1150.f, 0.f, 11430.f);
	m_objects[BARGHEST_ID_END + 4].SetNextPos(2, -1550.f, 0.f, 11030.f);

	m_objects[BARGHEST_ID_END + 5].SetPos(-350.f, 0.f, 11100.f);
	m_objects[BARGHEST_ID_END + 5].SetNextPos(0, -350.f, 0.f, 11100.f);
	m_objects[BARGHEST_ID_END + 5].SetNextPos(1, 150.f, 0.f, 10700.f);
	m_objects[BARGHEST_ID_END + 5].SetNextPos(2, -750.f, 0.f, 10700.f);

	m_objects[BARGHEST_ID_END + 6].SetPos(450.f, 0.f, 11500.f);
	m_objects[BARGHEST_ID_END + 6].SetNextPos(0, 450.f, 0.f, 11500.f);
	m_objects[BARGHEST_ID_END + 6].SetNextPos(1, 50.f, 0.f, 11100.f);
	m_objects[BARGHEST_ID_END + 6].SetNextPos(2, 850.f, 0.f, 11100.f);

	m_objects[BARGHEST_ID_END + 7].SetPos(650.f, 0.f, 12500.f);
	m_objects[BARGHEST_ID_END + 7].SetNextPos(0, 650.f, 0.f, 12500.f);
	m_objects[BARGHEST_ID_END + 7].SetNextPos(1, 250.f, 0.f, 12100.f);
	m_objects[BARGHEST_ID_END + 7].SetNextPos(2, 1050.f, 0.f, 12100.f);

	m_objects[BARGHEST_ID_END + 8].SetPos(9350.f, 0.f, -1600.f);
	m_objects[BARGHEST_ID_END + 8].SetNextPos(0, 9350.f, 0.f, -1600.f);
	m_objects[BARGHEST_ID_END + 8].SetNextPos(1, 9050.f, 0.f, -1900.f);
	m_objects[BARGHEST_ID_END + 8].SetNextPos(2, 9650.f, 0.f, -1900.f);

	m_objects[BARGHEST_ID_END + 9].SetPos(10450.f, 0.f, -500.f);
	m_objects[BARGHEST_ID_END + 9].SetNextPos(0, 10450.f, 0.f, -500.f);
	m_objects[BARGHEST_ID_END + 9].SetNextPos(1, 10050.f, 0.f, -900.f);
	m_objects[BARGHEST_ID_END + 9].SetNextPos(2, 10850.f, 0.f, -900.f);

	m_objects[BARGHEST_ID_END + 10].SetPos(12250.f, 0.f, -250.f);
	m_objects[BARGHEST_ID_END + 10].SetNextPos(0, 12250.f, 0.f, -650.f);
	m_objects[BARGHEST_ID_END + 10].SetNextPos(1, 11850.f, 0.f, -950.f);
	m_objects[BARGHEST_ID_END + 10].SetNextPos(2, 12650.f, 0.f, -950.f);

	/////////////////// 드래곤 하면 적용 ///////////////////
	for (int npc_id = GRIFFON_ID_END; npc_id < DRAGON_ID_END; ++npc_id) {
		m_objects[npc_id].SetID(npc_id);
		m_objects[npc_id].m_status = ST_SLEEP;
		m_objects[npc_id].SetSpeed(DRAGON_WALK_SPEED);

		m_objects[npc_id].SetCurrentHp(1000);
		m_objects[npc_id].SetMaxHp(1000);

		m_objects[npc_id].SetLevel(15);

		m_objects[npc_id].SetMoveType(RANDOM);

		m_objects[npc_id].SetIsAttack(false);

		m_objects[npc_id].SetMyType(O_DRAGON);

		m_objects[npc_id].SetNextPosIndex(0);

		m_objects[npc_id].SetDamage(m_objects[npc_id].GetLevel() * 10);
	}
	///////////////////////////////////////////////////////////

	m_objects[GRIFFON_ID_END].SetPos(12550.f, 0.f, 4600.f);
	m_objects[GRIFFON_ID_END].SetNextPos(0, 12550.f, 0.f, 4600.f);
	m_objects[GRIFFON_ID_END].SetNextPos(1, 12950.f, 0.f, 4200.f);
	m_objects[GRIFFON_ID_END].SetNextPos(2, 12950.f, 0.f, 5000.f);

	printf("NPC Initialization finished.\n");
}

void CServerFrame::LoadObstacles()
{
	wstring strFullPath = L"../../Client/02. File/bin/content/Scene/HuntingGround4.dat";

	LPCWSTR temp = strFullPath.c_str();
	HANDLE hFile = CreateFile(temp, GENERIC_READ, 0, 0,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	// 잘못된 경로일 경우 리턴
	if (INVALID_HANDLE_VALUE == hFile)
		return;

	// 우리가 읽어들일때는 몇개의 파일이 어떤 자료형으로 있는지 모른다.
	float fValue = 0;
	DWORD dwByte = 0;

	int cnt = 0;

	while (1) {
		// 그러므로 우리는 사이즈 / 메모리 순서로 저장을 하거나 통일된 자료형(구조체)를
		// 사용하는 것이 맞다.

		// dwByte : 현재 
		ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);


		// 더 이상 읽을 것이 없다면 dwByte가 0이 된다.
		if (dwByte == 0)
		{
			break;
		}

		bool is_collider = false;

		switch ((int)fValue) {
		case (int)OBJECT_KIND::BIGROCK:
		case (int)OBJECT_KIND::GRAVE:
		case (int)OBJECT_KIND::HOUSE:
		case (int)OBJECT_KIND::HOUSE_RIGHT:
		case (int)OBJECT_KIND::HOUSE_SMALL:
		case (int)OBJECT_KIND::ROCK:
		case (int)OBJECT_KIND::HOUSE_LEFT:
		case (int)OBJECT_KIND::DAMWALL:
		case (int)OBJECT_KIND::GATE:
		case (int)OBJECT_KIND::GRAVE_PILLAR:
		case (int)OBJECT_KIND::SHANTY:
		case (int)OBJECT_KIND::SHANTY_UP:
		case (int)OBJECT_KIND::BRIDGE_BUILDING:
		case (int)OBJECT_KIND::PLANE_ROCK:
			is_collider = false;
			break;
		case (int)OBJECT_KIND::BIGROCK_COLLIDER:
		case (int)OBJECT_KIND::BRIDGE_BUILDING_COLLIDER:
		case (int)OBJECT_KIND::DAMWALL_COLLIDER:
		case (int)OBJECT_KIND::GATE_COLLIDER:
		case (int)OBJECT_KIND::GRAVE_COLLIDER:
		case (int)OBJECT_KIND::GRAVE_PILLAR_COLLIDER:
		case (int)OBJECT_KIND::SHANTY_COLLIDER:
		case (int)OBJECT_KIND::SHANTY_UP_COLLIDER:
		case (int)OBJECT_KIND::HOUSE_COLLIDER:
		case (int)OBJECT_KIND::HOUSE_RIGHT_COLLIDER:
		case (int)OBJECT_KIND::HOUSE_LEFT_COLLIDER:
		case (int)OBJECT_KIND::HOUSE_SMALL_COLLIDER:
		case (int)OBJECT_KIND::ROCK_COLLIDER:
		case (int)OBJECT_KIND::TREE1_COLLIDER:
		case (int)OBJECT_KIND::PLANE_ROCK_COLLIDER:
			is_collider = true;
			break;
		default:
			assert(nullptr);
			break;
		}

		if (true == is_collider) {
			// Pos
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			m_obstacles[cnt].xPos = fValue;
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			m_obstacles[cnt].yPos = fValue;
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			m_obstacles[cnt].zPos = fValue;

			// Rot 생략
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);

			// Scale
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			m_obstacles[cnt].xScale = fValue;
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			m_obstacles[cnt].yScale = fValue;
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			m_obstacles[cnt++].zScale = fValue;
		}
		else {
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
		}

		// 더 이상 읽을 것이 없다면 dwByte가 0이 된다.
		if (dwByte == 0)
		{
			break;
		}
	}
	std::cout << "Init Obstacles\n";
	for (int i = 0; i < 10; ++i) {
		cout << m_obstacles[i].xPos << endl;
	}
}

//int CServerFrame::LuaGetxPosition(lua_State *L)
//{
//   int npc_id = (int)lua_tonumber(L, -1);
//   lua_pop(L, 2);
//   float x = m_objects[npc_id].GetxPos();
//   lua_pushnumber(L, x);
//   return 1;
//}
//
//int CServerFrame::LuaGetyPosition(lua_State *L)
//{
//   int npc_id = (int)lua_tonumber(L, -1);
//   lua_pop(L, 2);
//   float y = m_objects[npc_id].GetyPos();
//   lua_pushnumber(L, y);
//   return 1;
//}
//
//int CServerFrame::LuaNPCAttack(lua_State* L)
//{
//   //clients[player]->c_hp -= clients[npc]->damage;
//   //
//   //if (0 >= clients[player]->c_hp) {
//   //   clients[player]->x = 100;
//   //   clients[player]->y = 100;
//   //   clients[player]->c_hp = clients[player]->m_hp;
//   //   if (0 < clients[player]->c_exp)
//   //      clients[player]->c_exp = clients[player]->c_exp / 2;
//   //}
//   //
//   //send_stat_change_packet(player, player);
//   //for (auto& cl : clients[player]->near_id) {
//   //   if (true == Is_NPC(cl)) continue;
//   //   send_stat_change_packet(cl, player);
//   //}
//}

Vec3 CServerFrame::SetDummyPosition()
{
	Vec3 pos;

	switch (m_dummyIndex) {
	case 0: pos.x = 3350.f; pos.z = 5720.f; break;
	case 1: pos.x = 4850.f; pos.z = 5830.f; break;
	case 2: pos.x = 6760.f; pos.z = 5910.f; break;
	case 3: pos.x = 8750.f; pos.z = 4680.f; break;
	}

	m_dummyIndex = (m_dummyIndex + 1) % 40;

	return pos;
}

void CServerFrame::QuestDone(int id)
{
	m_objects[id].SetCurrentExp(m_objects[id].GetCurrentExp() + 100);

	while (m_objects[id].GetCurrentExp() >= m_objects[id].GetMaxExp()) {
		m_objects[id].SetCurrentExp(m_objects[id].GetCurrentExp() - m_objects[id].GetMaxExp());
		m_objects[id].SetLevel(m_objects[id].GetLevel() + 1);
		m_objects[id].SetDamage(35 + m_objects[id].GetLevel() * 5);
		m_objects[id].SetMaxHp(100 + (20 * (m_objects[id].GetLevel() - 1)));
		m_objects[id].SetCurrentHp(m_objects[id].GetMaxHp());
		m_objects[id].SetMaxExp(100 + (100 * (m_objects[id].GetLevel() - 1)));
	}

	m_sender->SendStatChangePacket(m_objects[id].GetSocket(), m_objects[id].GetLevel(), m_objects[id].GetCurrentHp(), m_objects[id].GetMaxHp(), m_objects[id].GetCurrentExp(), m_objects[id].GetMaxExp());
}