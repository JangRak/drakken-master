#pragma once

#include <vector>
#include <thread>
#include <queue>

#include <MSWSock.h>
#pragma comment (lib, "mswsock.lib")

#include "Object.h"

class CError;
class CSender;
class CDB;

class CServerFrame {
public:
	CServerFrame();
	~CServerFrame();

public:
	void Progress();

private:
	void InitServer();

	std::thread CreateWorkerThread();
	std::thread CreateTimerThread();

	void DoWorker();

	void Disconnect(int id);
	void RecvPacketConstruct(int id, int ioBytes);

	void AddTimer(EVENT ev);
	void DoTimer();

	void InitClients();
	void ResetPlayer(int id);
	void CreateNPC();
	void LoadObstacles();

	bool IsPlayer(int id);
	bool IsNearNPC(int player, int npc);

	void RespawnNPC(int npc_id);

	void DoRandomMove(int npc_id);
	void DoTargetMove(int npc_id);
	void UpdatePlayerPos(int id);

	void ChangePlayerStat(int id, bool kind);

	void PlayerTransform(int id, bool kind);

	void MoveUpdate();

	bool IsNear(int a, int b);

	void ActivateNPC(int id);

	void PlayerHeal(int id);

	void EnterGame(int id, const char* name);

	void SetMoveDirection(int id, char direction, bool b);

	void Attack(int id, char type);
	void tempCollisionCheck(int A, int B);

	void PlayerDefence(int id, bool kind);

	void ProcessPacket(int id, char* buf);

	void CreateNPCNone();

	Vec3 SetDummyPosition();

	void QuestDone(int id);

private:
	// socket
	SOCKET m_listenSocket;
	SOCKET m_clientSocket;
	SOCKADDR_IN m_clientAddr;
	DWORD m_flags;
	int m_addrLen;

	// class
	CError* m_error;
	CSender* m_sender;
	CDB* m_DB;

	// thread
	std::thread m_timerThread;
	std::vector<std::thread> m_workerThread;

	HANDLE m_iocp;

	CObject m_objects[DRAGON_ID_END];

	// timer
	std::mutex m_timerLock;
	std::priority_queue<EVENT> m_timerQueue;

	std::chrono::time_point<std::chrono::system_clock> m_prevTime;
	std::chrono::duration<float> m_elapsedTime;

	bool	m_useDB;

	int		m_dummyIndex;

	OBSTACLE m_obstacles[NUM_OBSTACLES];
};