#pragma once

#include <chrono>

class CGameTimer
{
public:
	CGameTimer();
	~CGameTimer();

public:
	void CreateGameTimer();
	std::chrono::time_point<std::chrono::system_clock> GetPrevTime() const { return m_prevTime; }
	std::chrono::duration<float> GetElapsedTime() const { return m_elapsedTime; }

private:
	std::chrono::time_point<std::chrono::system_clock> m_prevTime;
	std::chrono::duration<float> m_elapsedTime;
};