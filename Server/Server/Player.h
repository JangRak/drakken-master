#pragma once

#include "Object.h"

class CPlayer : public CObject
{
public:
	CPlayer();
	~CPlayer();

public:
	SOCKET GetSocket() const { return m_socket; }

private:
	SOCKET m_socket;

	short m_transformationGauge;
};