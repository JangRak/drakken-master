#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <WinSock2.h>
#include <winsock.h>
#include <Windows.h>
#include <iostream>
#include <thread>
#include <vector>
#include <unordered_set>
#include <mutex>
#include <atomic>
#include <chrono>
#include <queue>
#include <array>
#include <memory>
#include <assert.h>

#include "../../Server/Server/Protocol.h"

#include <d3d12.h>
#include "SimpleMath.h"

#pragma comment(lib, "d3d12")

typedef DirectX::SimpleMath::Vector2 Vec2;
typedef DirectX::SimpleMath::Vector3 Vec3;
typedef DirectX::SimpleMath::Vector4 Vec4;
using DirectX::SimpleMath::Matrix;

using namespace std;
using namespace chrono;

extern HWND		hWnd;

const static int MAX_TEST = 10;
const static int MAX_CLIENTS = MAX_TEST * 2;
const static int INVALID_ID = -1;

struct OBSTACLE {
	float xPos;
	float yPos;
	float zPos;
	float xScale;
	float yScale;
	float zScale;
};

#pragma comment (lib, "ws2_32.lib")

HANDLE g_hiocp;

enum class OBJECT_KIND
{
	BIGROCK,				// 0
	BRIDGE_BUILDING,
	DAMWALL,
	GATE,
	GRAVE,
	GRAVE_PILLAR,
	SHANTY,
	SHANTY_UP,
	HOUSE,
	HOUSE_RIGHT,
	HOUSE_LEFT,
	HOUSE_SMALL,
	ROCK,
	TREE1,
	PLANE_ROCK,


	// Collider
	BIGROCK_COLLIDER,
	BRIDGE_BUILDING_COLLIDER,
	DAMWALL_COLLIDER,
	GATE_COLLIDER,
	GRAVE_COLLIDER,
	GRAVE_PILLAR_COLLIDER,
	SHANTY_COLLIDER,
	SHANTY_UP_COLLIDER,

	HOUSE_COLLIDER,
	HOUSE_RIGHT_COLLIDER,
	HOUSE_LEFT_COLLIDER,
	HOUSE_SMALL_COLLIDER,
	ROCK_COLLIDER,
	TREE1_COLLIDER,
	PLANE_ROCK_COLLIDER,

};

high_resolution_clock::time_point last_connect_time;
enum EVENT_TYPE { EV_ACCEPT, EV_RECV, EV_SEND, EV_ATTACK, EV_MOVE, EV_UPDATE };

struct OverlappedEx {
	WSAOVERLAPPED over;
	WSABUF wsabuf;
	unsigned char IOCP_buf[MAX_PACKET_SIZE];
	EVENT_TYPE event_type;
	int event_target;
};

struct CLIENT {
	int id;
	float x;
	float y;
	float z;
	atomic_bool connected;

	float D_x = 0.f;
	float D_y = 0.f;
	float D_z = 0.f;

	bool isMove = false;
	char status;

	SOCKET client_socket;
	OverlappedEx recv_over;
	unsigned char packet_buf[MAX_PACKET_SIZE];
	int prev_packet_data;
	int curr_packet_size;
	high_resolution_clock::time_point last_move_time;
};

struct EVENT {
	int obj_id;
	std::chrono::high_resolution_clock::time_point wakeup_time;
	EVENT_TYPE event_type;
	int target_obj;

	constexpr bool operator < (const EVENT& left) const
	{
		return wakeup_time > left.wakeup_time;
	}
};

array<int, MAX_CLIENTS> client_map;
array<CLIENT, MAX_CLIENTS> g_clients;
atomic_int num_connections;
atomic_int client_to_close;
atomic_int active_clients;

int			global_delay;				// ms단위, 1000이 넘으면 클라이언트 증가 종료

OBSTACLE obstacles[NUM_OBSTACLES];

vector <thread*> worker_threads;
//thread test_thread;

thread timer_thread;
priority_queue<EVENT> timerQueue;
mutex timerLock;

thread update_thread;

float point_cloud[MAX_TEST * 2];
float point_obstacles[NUM_OBSTACLES * 2];

int g_first = false;

// 나중에 NPC까지 추가 확장 용
struct ALIEN {
	int id;
	int x, y;
	int visible_count;
};

void error_display(const char* msg, int err_no)
{
	WCHAR* lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, err_no,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	std::cout << msg;
	std::wcout << L"에러" << lpMsgBuf << std::endl;

	MessageBox(hWnd, lpMsgBuf, L"ERROR", 0);
	LocalFree(lpMsgBuf);
	// while (true);
}

void DisconnectClient(int ci)
{
	bool status = true;
	if (true == atomic_compare_exchange_strong(&g_clients[ci].connected, &status, false)) {
		closesocket(g_clients[ci].client_socket);
		active_clients--;
	}
	// cout << "Client [" << ci << "] Disconnected!\n";
}

void SendPacket(int cl, void* packet)
{
	int psize = reinterpret_cast<unsigned char*>(packet)[0];
	int ptype = reinterpret_cast<unsigned char*>(packet)[1];
	OverlappedEx* over = new OverlappedEx;
	over->event_type = EV_SEND;
	memcpy(over->IOCP_buf, packet, psize);
	ZeroMemory(&over->over, sizeof(over->over));
	over->wsabuf.buf = reinterpret_cast<CHAR*>(over->IOCP_buf);
	over->wsabuf.len = psize;
	int ret = WSASend(g_clients[cl].client_socket, &over->wsabuf, 1, NULL, 0,
		&over->over, NULL);
	if (0 != ret) {
		int err_no = WSAGetLastError();
		if (WSA_IO_PENDING != err_no)
			error_display("Error in SendPacket:", err_no);
	}
	// std::cout << "Send Packet [" << ptype << "] To Client : " << cl << std::endl;
}

void AddTimer(EVENT ev)
{
	timerLock.lock();
	timerQueue.push(ev);
	timerLock.unlock();
}

void LoadObstacles()
{
	for (int i = 0; i < NUM_OBSTACLES; ++i) {
		obstacles[i].xScale = -999;
	}

	wstring strFullPath = L"../../Client/02. File/bin/content/Scene/HuntingGround4.dat";

	LPCWSTR temp = strFullPath.c_str();
	HANDLE hFile = CreateFile(temp, GENERIC_READ, 0, 0,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	// 잘못된 경로일 경우 리턴
	if (INVALID_HANDLE_VALUE == hFile)
		return;

	// 우리가 읽어들일때는 몇개의 파일이 어떤 자료형으로 있는지 모른다.
	float fValue = 0;
	DWORD dwByte = 0;

	int cnt = 0;

	while (1) {
		// 그러므로 우리는 사이즈 / 메모리 순서로 저장을 하거나 통일된 자료형(구조체)를
		// 사용하는 것이 맞다.

		// dwByte : 현재 
		ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);


		// 더 이상 읽을 것이 없다면 dwByte가 0이 된다.
		if (dwByte == 0)
		{
			break;
		}

		bool is_collider = false;

		switch ((int)fValue) {
		case (int)OBJECT_KIND::BIGROCK:
		case (int)OBJECT_KIND::GRAVE:
		case (int)OBJECT_KIND::HOUSE:
		case (int)OBJECT_KIND::HOUSE_RIGHT:
		case (int)OBJECT_KIND::HOUSE_SMALL:
		case (int)OBJECT_KIND::ROCK:
		case (int)OBJECT_KIND::HOUSE_LEFT:
		case (int)OBJECT_KIND::DAMWALL:
		case (int)OBJECT_KIND::GATE:
		case (int)OBJECT_KIND::GRAVE_PILLAR:
		case (int)OBJECT_KIND::SHANTY:
		case (int)OBJECT_KIND::SHANTY_UP:
		case (int)OBJECT_KIND::BRIDGE_BUILDING:
		case (int)OBJECT_KIND::PLANE_ROCK:
			is_collider = false;
			break;
		case (int)OBJECT_KIND::BIGROCK_COLLIDER:
		case (int)OBJECT_KIND::BRIDGE_BUILDING_COLLIDER:
		case (int)OBJECT_KIND::DAMWALL_COLLIDER:
		case (int)OBJECT_KIND::GATE_COLLIDER:
		case (int)OBJECT_KIND::GRAVE_COLLIDER:
		case (int)OBJECT_KIND::GRAVE_PILLAR_COLLIDER:
		case (int)OBJECT_KIND::SHANTY_COLLIDER:
		case (int)OBJECT_KIND::SHANTY_UP_COLLIDER:
		case (int)OBJECT_KIND::HOUSE_COLLIDER:
		case (int)OBJECT_KIND::HOUSE_RIGHT_COLLIDER:
		case (int)OBJECT_KIND::HOUSE_LEFT_COLLIDER:
		case (int)OBJECT_KIND::HOUSE_SMALL_COLLIDER:
		case (int)OBJECT_KIND::ROCK_COLLIDER:
		case (int)OBJECT_KIND::TREE1_COLLIDER:
		case (int)OBJECT_KIND::PLANE_ROCK_COLLIDER:
			is_collider = true;
			break;
		default:
			//assert(nullptr);
			break;
		}

		if (true == is_collider) {
			// Pos
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			obstacles[cnt].xPos = fValue;
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			obstacles[cnt].yPos = fValue;
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			obstacles[cnt].zPos = fValue;

			// Rot 생략
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);

			// Scale
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			obstacles[cnt].xScale = fValue;
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			obstacles[cnt].yScale = fValue;
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			obstacles[cnt++].zScale = fValue;
		}
		else {
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
		}

		// 더 이상 읽을 것이 없다면 dwByte가 0이 된다.
		if (dwByte == 0)
		{
			break;
		}
	}
	std::cout << "Init Obstacles\n";
	//for (int i = 0; i < 10; ++i) {
	//	cout << obstacles[i].xPos << endl;
	//}
}

constexpr int DELAY_LIMIT = 100;
constexpr int DELAY_LIMIT2 = 150;
constexpr int ACCEPT_DELY = 50;

void Adjust_Number_Of_Client()
{
	static int delay_multiplier = 1;
	static int max_limit = MAXINT;
	static bool increasing = true;

	if (active_clients >= MAX_TEST) return;
	if (num_connections >= MAX_CLIENTS) return;

	auto duration = high_resolution_clock::now() - last_connect_time;
	if (ACCEPT_DELY * delay_multiplier > duration_cast<milliseconds>(duration).count()) return;

	int t_delay = global_delay;
	if (DELAY_LIMIT2 < t_delay) {
		if (true == increasing) {
			max_limit = active_clients;
			increasing = false;
		}
		if (100 > active_clients) return;
		if (ACCEPT_DELY * 10 > duration_cast<milliseconds>(duration).count()) return;
		last_connect_time = high_resolution_clock::now();
		DisconnectClient(client_to_close);
		client_to_close++;
		return;
	}
	else
		if (DELAY_LIMIT < t_delay) {
			delay_multiplier = 10;
			return;
		}
	if (max_limit - (max_limit / 20) < active_clients) return;

	increasing = true;
	last_connect_time = high_resolution_clock::now();
	g_clients[num_connections].client_socket = WSASocketW(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);

	SOCKADDR_IN ServerAddr;
	ZeroMemory(&ServerAddr, sizeof(SOCKADDR_IN));
	ServerAddr.sin_family = AF_INET;
	ServerAddr.sin_port = htons(SERVER_PORT);
	ServerAddr.sin_addr.s_addr = inet_addr("127.0.0.1");


	int Result = WSAConnect(g_clients[num_connections].client_socket, (sockaddr*)&ServerAddr, sizeof(ServerAddr), NULL, NULL, NULL, NULL);
	if (0 != Result) {
		error_display("WSAConnect : ", GetLastError());
	}

	g_clients[num_connections].curr_packet_size = 0;
	g_clients[num_connections].prev_packet_data = 0;
	ZeroMemory(&g_clients[num_connections].recv_over, sizeof(g_clients[num_connections].recv_over));
	g_clients[num_connections].recv_over.event_type = EV_RECV;
	g_clients[num_connections].recv_over.wsabuf.buf =
		reinterpret_cast<CHAR*>(g_clients[num_connections].recv_over.IOCP_buf);
	g_clients[num_connections].recv_over.wsabuf.len = sizeof(g_clients[num_connections].recv_over.IOCP_buf);

	DWORD recv_flag = 0;
	CreateIoCompletionPort(reinterpret_cast<HANDLE>(g_clients[num_connections].client_socket), g_hiocp, num_connections, 0);

	cs_packet_login l_packet;

	int temp = num_connections;
	sprintf_s(l_packet.name, "%d", temp);
	l_packet.size = sizeof(l_packet);
	l_packet.type = CS_LOGIN;
	l_packet.loginType = DUMMY;
	SendPacket(num_connections, &l_packet);	

	EVENT ev{ num_connections, std::chrono::high_resolution_clock::now() + 3s, EV_MOVE, 0 };
	AddTimer(ev);

	if (false == g_first) {
		g_first = true;
		EVENT ev{ 999999, std::chrono::high_resolution_clock::now() + 50ms, EV_UPDATE, 0 };
		AddTimer(ev);
	}

	int ret = WSARecv(g_clients[num_connections].client_socket, &g_clients[num_connections].recv_over.wsabuf, 1,
		NULL, &recv_flag, &g_clients[num_connections].recv_over.over, NULL);
	if (SOCKET_ERROR == ret) {
		int err_no = WSAGetLastError();
		if (err_no != WSA_IO_PENDING)
		{
			error_display("RECV ERROR", err_no);
			goto fail_to_connect;
		}
	}
	num_connections++;

fail_to_connect:
	return;
}

void ProcessPacket(int ci, unsigned char packet[])
{
	switch (packet[1]) {
	case SC_MOVE: {
		sc_packet_move* move_packet = reinterpret_cast<sc_packet_move*>(packet);
		if (move_packet->id < MAX_CLIENTS) {
			int my_id = client_map[move_packet->id];
			if (-1 != my_id) {
				g_clients[my_id].x = move_packet->x;
				g_clients[my_id].y = move_packet->y;
				g_clients[my_id].z = move_packet->z;
				if (WALK == move_packet->status) {
					g_clients[my_id].status = WALK;
				}
				else if (RUN == move_packet->status) {
					g_clients[my_id].status = RUN;
				}
			}
		}
	}
				  break;
	case SC_RETURN: {
		sc_packet_return* return_packet = reinterpret_cast<sc_packet_return*>(packet);
		if (return_packet->id < MAX_CLIENTS) {
			int my_id = client_map[return_packet->id];
			if (ci == my_id) {
				if (0 != return_packet->moveTime) {
					auto d_ms = duration_cast<milliseconds>(high_resolution_clock::now().time_since_epoch()).count() - return_packet->moveTime;
					if (global_delay < d_ms) global_delay++;
					else if (global_delay > d_ms) global_delay--;
				}
			}
		}
		break;
	}
	case SC_ENTER: break;
	case SC_LEAVE: break;
	case SC_LOGIN_FAIL: break;
	case SC_MOVE_DIRECTION: break;
	case SC_STOP: break;
	case SC_STAT_CHANGE: break;
	case SC_CHAT: break;
	case SC_NPC_DIE: break;
	case SC_TRANSFORM: break;
	case SC_NPC_ATTACK: break;
	case SC_PLAYER_ATTACK: break;
	case SC_HP_CHANGE: break;
	case SC_PLAYER_DIE: break;
	case SC_LOGIN_OK: {
		g_clients[ci].connected = true;
		active_clients++;
		sc_packet_login_ok* login_packet = reinterpret_cast<sc_packet_login_ok*>(packet);
		int my_id = ci;
		client_map[login_packet->id] = my_id;
		g_clients[my_id].id = login_packet->id;
		g_clients[my_id].x = login_packet->x;
		g_clients[my_id].y = login_packet->y;
		g_clients[my_id].z = login_packet->z;

		//cs_packet_teleport t_packet;
		//t_packet.size = sizeof(t_packet);
		//t_packet.type = CS_TELEPORT;
		//SendPacket(my_id, &t_packet);
	}
	break;
	default: 
		break;
		
	//default:
		//MessageBox(hWnd, L"Unknown Packet Type", L"ERROR", 0);
		//while (true);
	}
}

void DoTimer()
{
	printf("Start timer thread\n");

	while (true) {
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
		while (true) {
			timerLock.lock();
			if (true == timerQueue.empty()) {
				timerLock.unlock();
				break;
			}

			if (timerQueue.top().wakeup_time > std::chrono::high_resolution_clock::now()) {
				timerLock.unlock();
				break;
			}

			EVENT ev = timerQueue.top();
			timerQueue.pop();
			timerLock.unlock();

			OverlappedEx* overEx = new OverlappedEx;
			overEx->event_type = ev.event_type;
			*(reinterpret_cast<int*>(overEx->IOCP_buf)) = ev.target_obj;
			PostQueuedCompletionStatus(g_hiocp, 1, ev.obj_id, &overEx->over);
		}
	}
}

void SendPlayerPos(int id)
{
	if (false == g_clients[id].isMove) return;

	cs_packet_position packet;
	packet.type = CS_POSITION;
	packet.size = sizeof(cs_packet_position);
	packet.xPos = g_clients[id].x;
	packet.yPos = g_clients[id].y;
	packet.zPos = g_clients[id].z;
	packet.xLook = g_clients[id].D_x;
	packet.yLook = g_clients[id].D_y;
	packet.zLook = g_clients[id].D_z;

	packet.moveTime = static_cast<unsigned>(duration_cast<milliseconds>(high_resolution_clock::now().time_since_epoch()).count());

	SendPacket(id, &packet);
}


chrono::duration<float> g_elapsedTime;
time_point<std::chrono::system_clock> prevTime = chrono::system_clock::now();

void Update()
{
	std::chrono::time_point<std::chrono::system_clock> curTime
		= std::chrono::system_clock::now();

	for (int i = 0; i < num_connections; ++i) {
		if (false == g_clients[i].connected) continue;
		if (false == g_clients[i].isMove) continue;
		SendPlayerPos(i);
	}
	g_elapsedTime = curTime - prevTime;
	prevTime = curTime;

	EVENT ev{ 999999, std::chrono::high_resolution_clock::now() + 50ms, EV_UPDATE, 0 };
	AddTimer(ev);
}

void MoveDirection(int i)
{
	if (false == g_clients[i].connected) return;
	//if (g_clients[i].last_move_time + 3s > high_resolution_clock::now()) continue;
	//g_clients[i].last_move_time = high_resolution_clock::now();

	switch (rand() % 2) {
	case 0: {	// 움직인다.
		if (false == g_clients[i].isMove) {
			g_clients[i].isMove = true;
			cs_packet_move my_packet;
			my_packet.size = sizeof(my_packet);
			my_packet.type = CS_WALK;
			my_packet.direction = D_UP;
			//my_packet.moveTime = static_cast<unsigned>(duration_cast<milliseconds>(high_resolution_clock::now().time_since_epoch()).count());
			SendPacket(i, &my_packet);
		}
		break;
	}
	case 1: {	// 안움직인다.
		if (true == g_clients[i].isMove) {
			g_clients[i].isMove = false;
			cs_packet_move_stop my_packet;
			my_packet.size = sizeof(my_packet);
			my_packet.type = CS_STOP;
			my_packet.direction = D_UP;
			SendPacket(i, &my_packet);
		}
		break;
	}
	}

	int r_x = rand() % 2000;
	int r_z = rand() % 2000;

	float x = ((float)r_x - 1000.f) * 0.001f;
	float y = 0.f;
	float z = ((float)r_z - 1000.f) * 0.001f;

	g_clients[i].D_x = x;
	g_clients[i].D_y = y;
	g_clients[i].D_z = z;

	EVENT ev{ i, std::chrono::high_resolution_clock::now() + 3s, EV_MOVE, 0 };
	AddTimer(ev);
}

void UpdatePos()
{
	while (true) {
		Adjust_Number_Of_Client();
		for (int i = 0; i < num_connections; ++i) {
			if (false == g_clients[i].connected) continue;
			if (false == g_clients[i].isMove) continue;
			if (g_clients[i].last_move_time + 50ms > high_resolution_clock::now()) continue;
			g_clients[i].last_move_time = high_resolution_clock::now();
			float x = g_clients[i].x;
			float y = g_clients[i].y;
			float z = g_clients[i].z;
			float xLook = g_clients[i].D_x;
			float yLook = g_clients[i].D_y;
			float zLook = g_clients[i].D_z;

			x -= g_elapsedTime.count() * VIKING_WALK_SPEED * xLook;
			y -= g_elapsedTime.count() * VIKING_WALK_SPEED * yLook;
			z -= g_elapsedTime.count() * VIKING_WALK_SPEED * zLook;

			bool check = false;
			for (int i = 0; i < NUM_OBSTACLES; ++i) {
				if (-999 == obstacles[i].xScale) break;

				Vec3 pPos;
				pPos.x = x;
				pPos.y = y;
				pPos.z = z;
				Vec3 oPos;
				oPos.x = obstacles[i].xPos;
				oPos.y = obstacles[i].yPos;
				oPos.z = obstacles[i].zPos;
				float r = obstacles[i].zScale;

				float distance = Vec3::Distance(pPos, oPos);

				if (distance <= r) {
					check = true;
					break;
				}
			}

			if (false == check) {
				g_clients[i].x = x;
				g_clients[i].y = y;
				g_clients[i].z = z;
			}
		}
	}
}

void Worker_Thread()
{
	while (true) {
		DWORD io_size;
		unsigned long long ci;
		OverlappedEx* over;
		BOOL ret = GetQueuedCompletionStatus(g_hiocp, &io_size, &ci,
			reinterpret_cast<LPWSAOVERLAPPED*>(&over), INFINITE);
		// std::cout << "GQCS :";
		int client_id = static_cast<int>(ci);
		if (FALSE == ret) {
			int err_no = WSAGetLastError();
			if (64 == err_no) DisconnectClient(client_id);
			else {
				// error_display("GQCS : ", WSAGetLastError());
				DisconnectClient(client_id);
			}
			if (EV_SEND == over->event_type) delete over;
		}
		if (0 == io_size) {
			DisconnectClient(client_id);
			continue;
		}
		if (EV_RECV == over->event_type) {
			//std::cout << "RECV from Client :" << ci;
			//std::cout << "  IO_SIZE : " << io_size << std::endl;
			unsigned char* buf = g_clients[ci].recv_over.IOCP_buf;
			unsigned psize = g_clients[ci].curr_packet_size;
			unsigned pr_size = g_clients[ci].prev_packet_data;
			while (io_size > 0) {
				if (0 == psize) psize = buf[0];
				if (io_size + pr_size >= psize) {
					// 지금 패킷 완성 가능
					unsigned char packet[MAX_PACKET_SIZE];
					memcpy(packet, g_clients[ci].packet_buf, pr_size);
					memcpy(packet + pr_size, buf, psize - pr_size);
					ProcessPacket(static_cast<int>(ci), packet);
					io_size -= psize - pr_size;
					buf += psize - pr_size;
					psize = 0; pr_size = 0;
				}
				else {
					memcpy(g_clients[ci].packet_buf + pr_size, buf, io_size);
					pr_size += io_size;
					io_size = 0;
				}
			}
			g_clients[ci].curr_packet_size = psize;
			g_clients[ci].prev_packet_data = pr_size;
			DWORD recv_flag = 0;
			int ret = WSARecv(g_clients[ci].client_socket,
				&g_clients[ci].recv_over.wsabuf, 1,
				NULL, &recv_flag, &g_clients[ci].recv_over.over, NULL);
			if (SOCKET_ERROR == ret) {
				int err_no = WSAGetLastError();
				if (err_no != WSA_IO_PENDING)
				{
					//error_display("RECV ERROR", err_no);
					DisconnectClient(client_id);
				}
			}
		}
		else if (EV_SEND == over->event_type) {
			if (io_size != over->wsabuf.len) {
				// std::cout << "Send Incomplete Error!\n";
				DisconnectClient(client_id);
			}
			delete over;
		}
		else if (EV_MOVE == over->event_type) {
			MoveDirection(client_id);
			delete over;
		}
		else if (EV_UPDATE == over->event_type) {
			Update();
			//UpdatePos();
			delete over;
		}
		else {
			std::cout << "Unknown GQCS event!\n";
			while (true);
		}
	}
}

void InitializeNetwork()
{
	for (auto &cl : g_clients) {
		cl.connected = false;
		cl.id = INVALID_ID;
	}

	for (auto& cl : client_map) cl = -1;
	num_connections = 0;
	last_connect_time = high_resolution_clock::now();

	WSADATA	wsadata;
	WSAStartup(MAKEWORD(2, 2), &wsadata);

	g_hiocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, 0, NULL, 0);

	for (int i = 0; i < 5; ++i)
		worker_threads.push_back(new std::thread{ Worker_Thread });


	//test_thread = thread{ Test_Thread };
	timer_thread = thread{ DoTimer };

	update_thread = thread{ UpdatePos };
}

void ShutdownNetwork()
{
	//test_thread.join();
	timer_thread.join();
	for (auto pth : worker_threads) {
		pth->join();
		delete pth;
	}
}

void Do_Network()
{
	return;
}

void GetPointCloud(int* size, float** points)
{
	int index = 0;
	for (int i = 0; i < num_connections; ++i)
		if (true == g_clients[i].connected) {
			point_cloud[index * 2] = static_cast<float>(g_clients[i].x);
			point_cloud[index * 2 + 1] = static_cast<float>(g_clients[i].z);
			index++;
		}

	*size = index;
	*points = point_cloud;
}

void GetPointObs(int* size, float** points)
{
	int index = 0;
	for (int i = 0; i < NUM_OBSTACLES; ++i)
		if (-999 != obstacles[i].xScale) {
			point_obstacles[index * 2] = static_cast<float>(obstacles[i].xPos);
			point_obstacles[index * 2 + 1] = static_cast<float>(obstacles[i].zPos);
			index++;
		}

	*size = index;
	*points = point_obstacles;
}
