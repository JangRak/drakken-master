#pragma once
#include "Component.h"
class CStatus :
	public CComponent
{
private:
	float		m_fHP;
	float		m_fRunSpeed;
	float		m_fWalkSpeed;
	float		m_fExp;
	float		m_fAttack;
	float		m_fAttackCoolTime;
	int			m_iState;

	float		m_fMaxHP;
	float		m_fMaxExp;

	int			m_iLevel;

public:
	const float GetHP() const { return m_fHP; }
	void SetHP(const float& _fHP) { m_fHP = _fHP; }

	const float GetExp() const { return m_fExp; }
	void SetExp(const float& _fMP) { m_fExp = _fMP; }

	const float GetMaxHP() const { return m_fMaxHP; }
	void SetMaxHP(const float& _fHP) { m_fMaxHP = _fHP; }

	const float GetMaxExp() const { return m_fMaxExp; }
	void SetMaxExp(const float& _fMP) { m_fMaxExp = _fMP; }



	const float GetWalkSpeed() const { return m_fWalkSpeed; }
	void SetWalkSpeed(const float& _fWalkSpeed) { m_fWalkSpeed = _fWalkSpeed; }

	const float GetRunSpeed() const { return m_fRunSpeed; }
	void SetRunSpeed(const float& _fRunSpeed) { m_fRunSpeed = _fRunSpeed; }

	const float GetAttack() const { return m_fAttack; }
	void SetAttack(const float& _fAttack) { m_fAttack = _fAttack; }

	const float GetAttackCoolTime() const { return m_fAttackCoolTime; }
	void SetAttackCoolTime(const float& _fAttackCoolTime) { m_fAttackCoolTime = _fAttackCoolTime; }

	const int GetState() const { return m_iState; }
	void SetState(const int& _iState) { m_iState = _iState; }

	const int GetLevel() const { return m_iLevel; }
	void SetLevel(const int& _iLevel) { m_iLevel = _iLevel; }

public:
	virtual void finalupdate();
	virtual void SaveToScene(FILE* _pFile);
	virtual void LoadFromScene(FILE* _pFile);
public:
	virtual CStatus* Clone() { return new CStatus(*this); }

public:
	CStatus();
	virtual ~CStatus();

};



