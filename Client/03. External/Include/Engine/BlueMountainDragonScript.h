#pragma once
#include "Script.h"
class CBlueMountainDragonScript :
	public CScript
{
public:
	virtual void awake();
	virtual void update();

public:
	CLONE(CBlueMountainDragonScript);

public:
	CBlueMountainDragonScript();
	virtual ~CBlueMountainDragonScript();
};

