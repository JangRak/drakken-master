#pragma once
#include "Script.h"
class CQuestNPCScript :
	public CScript
{
public:
	virtual void awake();
	virtual void update();

public:
	CLONE(CQuestNPCScript);

public:
	CQuestNPCScript();
	virtual ~CQuestNPCScript();
};

