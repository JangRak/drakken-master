#pragma once
#include "Script.h"
class CBarghestScript :
	public CScript
{
private:
	Ptr<CMaterial>		m_pOriginMtrl;
	Ptr<CMaterial>		m_pCloneMtrl;

public:
	virtual void awake();
	virtual void update();

public:
	CLONE(CBarghestScript);

public:
	CBarghestScript();
	virtual ~CBarghestScript();
};

