#pragma once

#include "FBXLoader.h"
#include "Ptr.h"
#include "Material.h"
#include "Mesh.h"

class CGameObject;
class CScript;

class CMeshData :
	public CResource
{
private:
	Ptr<CMesh>				m_pMesh;
	vector<Ptr<CMaterial>> m_vecMtrl;


public:
	static CMeshData* LoadFromFBX(const wstring& _strFilePath);

	virtual void Load(const wstring& _strFilePath);
	virtual void Save(const wstring& _strFilePath);

	CGameObject* Instantiate();



	//void SetMesh(CMesh* _pMesh) { m_pMesh = _pMesh; }
	//Ptr<CMesh> GetMesh() { return m_pMesh; }

public:
	CMeshData();
	virtual ~CMeshData();
};

