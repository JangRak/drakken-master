#pragma once
#include "Script.h"

class CGameObject;
class CPlayerLightScript :
	public CScript
{
private:
	CGameObject*	m_pPlayerObj;

public:
	virtual void awake();
	virtual void update();

public:
	void SetPlayer(CGameObject* pPlayer) { m_pPlayerObj = pPlayer; }
	

public:
	CLONE(CPlayerLightScript);

public:
	CPlayerLightScript();
	virtual ~CPlayerLightScript();
};

