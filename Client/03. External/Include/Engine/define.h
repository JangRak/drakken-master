#pragma once
#define INSTANCING_COUNT 2


#define SINGLE(type) private: type(); ~type();\
public:\
static type* GetInst()\
{\
	static type mgr;\
	return &mgr;\
}

#define SAFE_RELEASE(p) if(nullptr != p) p->Release()
#define SAFE_DELETE(p) if(nullptr != p) delete p; p = nullptr;

#define DEVICE CDevice::GetInst()->GetDevice()
#define CMDLIST CDevice::GetInst()->GetCmdList()
#define CMDLIST_RES CDevice::GetInst()->GetCmdListRes()
#define CMDLIST_CS CDevice::GetInst()->GetCmdListCompute()

#define KEY(Key, State) (CKeyMgr::GetInst()->GetKeyState(Key) == State)
#define KEY_HOLD(Key) KEY(Key, KEY_STATE::STATE_HOLD)
#define KEY_TAB(Key) KEY(Key, KEY_STATE::STATE_TAB)
#define KEY_AWAY(Key) KEY(Key, KEY_STATE::STATE_AWAY)
#define KEY_NONE(Key) KEY(Key, KEY_STATE::STATE_NONE)

#define DT CTimeMgr::GetInst()->GetDeltaTime()


#define CLONE(type) public: type* Clone() { return new type(*this); }
#define CLONE_DISABLE(type) type* Clone() { assert(nullptr); return nullptr;} \
                            type(const type& _class){assert(nullptr);}

#define DBG_WARNNING(pStr) CDebugMgr::GetInst()->AddDbgMsg(DBG_TYPE::DBG_WARNNING, pStr);
#define DBG_ERROR(pStr) CDebugMgr::GetInst()->AddDbgMsg(DBG_TYPE::DBG_ERROR, pStr);
#define DBG_MSG(pStr) CDebugMgr::GetInst()->AddDbgMsg(DBG_TYPE::DBG_MSG, pStr);

#define MAX_LAYER 32



#define VEC3_TOWN_ENTRANCE_POS Vec3(-3000.f, 0.f, 5683.f)
#define VEC3_QUESTNPC_POS Vec3(-2250.f, 0.f, 7150.f)

#define VEC3_PLAYER_VIKING_OFFSET Vec3

typedef DirectX::SimpleMath::Vector2 Vec2;
typedef DirectX::SimpleMath::Vector3 Vec3;
typedef DirectX::SimpleMath::Vector4 Vec4;
using DirectX::SimpleMath::Matrix;

enum class CONST_REGISTER
{
	b0 = 0,
	b1 = 1,
	b2 = 2,
	b3 = 3,
	b4 = 4,
	b5 = 5,
	END,
};

enum class TEXTURE_REGISTER
{
	t0 = (UINT)CONST_REGISTER::END,
	t1,
	t2,
	t3,
	t4,
	t5,
	t6,
	t7,
	t8,
	t9,
	t10,
	t11,
	t12,
	END,
};

enum class UAV_REGISTER
{
	u0 = (UINT)TEXTURE_REGISTER::END,
	u1,
	u2,
	u3,
	END,
};




enum class RES_TYPE
{
	MATERIAL,
	MESHDATA,
	MESH,
	TEXTURE,
	SHADER,
	SOUND,
	END,
};

enum class ROOT_SIG_TYPE
{
	RENDER,
	COMPUTE,
	END,
};

enum class RS_TYPE
{
	CULL_BACK,
	CULL_FRONT,
	CULL_NONE,
	WIRE_FRAME,
	END,
};


namespace RES_TYPE_STR
{
	extern const wchar_t* MATERIAL;
	extern const wchar_t* MESH;
	extern const wchar_t* TEXTURE;
	extern const wchar_t* SOUND;
	extern const wchar_t* SHADER;
}

extern const wchar_t* RES_TYPE_NAME[(UINT)RES_TYPE::END];


enum class SHADER_TYPE
{
	ST_VERTEX = 0x01,
	ST_HULL = 0x02,
	ST_DOMAIN = 0x04,
	ST_GEOMETRY = 0x08,
	ST_COMPUTE = 0x10,
	ST_PIXEL = 0x20,
	ST_END
};

#define ST_ALL (UINT)SHADER_TYPE::ST_VERTEX | (UINT)SHADER_TYPE::ST_HULL\
 | (UINT)SHADER_TYPE::ST_DOMAIN | (UINT)SHADER_TYPE::ST_GEOMETRY | (UINT)SHADER_TYPE::ST_COMPUTE | (UINT)SHADER_TYPE::ST_PIXEL

enum class COMPONENT_TYPE
{
	TRANSFORM,
	MESHRENDER,
	CAMERA,
	COLLIDER2D,
	COLLIDER3D,
	ANIMATOR2D,
	ANIMATOR3D,
	LIGHT2D,
	LIGHT3D,
	SCRIPT,
	PARTICLESYSTEM,
	STATUS,
	END,
};

enum class COLLIDER2D_TYPE
{
	RECT,
	CIRCLE,
};

enum class SHADER_PARAM
{
	INT_0,
	INT_1,
	INT_2,
	INT_3,
	INT_END,

	FLOAT_0,
	FLOAT_1,
	FLOAT_2,
	FLOAT_3,
	FLOAT_END,

	VEC2_0,
	VEC2_1,
	VEC2_2,
	VEC2_3,
	VEC2_END,

	VEC4_0,
	VEC4_1,
	VEC4_2,
	VEC4_3,
	VEC4_END,

	MATRIX_0,
	MATRIX_1,
	MATRIX_2,
	MATRIX_3,
	MATRIX_END,

	TEX_0,
	TEX_1,
	TEX_2,
	TEX_3,
	TEX_END,
};


enum class BLEND_TYPE
{
	DEFAULT,
	ALPHABLEND,
	ONEBLEND,
	END,
};

// Stencil �˻�� ���� ���� ����
enum class DEPTH_STENCIL_TYPE
{
	LESS,
	LESS_EQUAL,
	GREATER,
	GREATER_EQUAL,

	NO_DEPTHTEST,				// ���� �׽�Ʈ x, ���� ��� o
	NO_DEPTHTEST_NO_WRITE,		// ���� �׽�Ʈ x, ���� ��� x
	LESS_NO_WRITE,				// Less ���� �׽�Ʈ, ���� ��� x

	END,
};

enum class DBG_TYPE
{
	DBG_ERROR,
	DBG_WARNNING,
	DBG_MSG,
};

enum class EVENT_TYPE
{
	CREATE_OBJECT,	// wParam : GameObject, lParam : Layer Idx
	DELETE_OBJECT,	// wParam : GameObject,
	ADD_CHILD,		// wParam : Parent Object, lParam : Child Object
	CLEAR_PARENT,	// wParam : Target Object
	TRANSFER_LAYER,	// wParam : Target Object, lParam : (HIWORD)Layer Index (LOWORD)bMoveAll
	TRANSFER_SCENE,
	ACTIVATE_GAMEOBJECT,	// wParam : GameObject Adress
	DEACTIVATE_GAMEOBJECT,	// wParam : GameObject Adress

	ACTIVATE_COMPONENT,		// wParam : Component Adress
	DEACTIVATE_COMPONENT,	// wParam : Component Adress

	END,
};

enum class LIGHT_TYPE
{
	DIR,
	POINT,
	SPOT,
	END,
};

enum class DIR_TYPE
{
	RIGHT,
	UP,
	FRONT,
	END,
};

enum class FACE_TYPE
{
	FT_NEAR,
	FT_FAR,
	FT_UP,
	FT_DOWN,
	FT_LEFT,
	FT_RIGHT,
	FT_END,
};

enum class RT_TYPE
{
	SWAPCHAIN,

	DIFFUSE,
	NORMAL,
	POSITION,

	LIGHT,
	SPECULAR,

	SHADOWMAP,

	POSTEFFECT1,
	POSTEFFECT2,

	END,
};

enum class MRT_TYPE
{
	// MRT			RT ����
	SWAPCHAIN,  // SWAPCHAIN
	DEFERRED,	// DIFFUSE, NORMAL, POSITION
	LIGHT,		// LIGHT, SPECULAR
	SHADOWMAP,	// SHADOWMAP
	POSTEFFECT, // POSTEFFECT1, POSTEFFECT2
	END,
};

enum class SHADER_POV
{
	DEFERRED,
	FORWARD,
	POST_EFFECT,
	LIGHTING,
	COMPUTE,
	PARTICLE,
	SHADOWMAP,
};

enum class MOUNTAINDRAGON_STATE
{
	//WALK,
	//BITE,
	//CLAWATTACK,
	//DEATH,
	//IDLE,
	//RUN,
	//BREATH,

	//BREATH,
	//WALK,
	//BITE,
	//CLAWATTACK,
	//DEATH,
	//IDLE,
	//RUN,


	IDLE,
	WALK,
	RUN,
	BITE,
	CLAWATTACK,
	BREATH,
	DEATH,
};

enum class BARGHEST_STATE
{
	IDLE,
	WALK,
	RUN,
	BITE,
	DEATH,
};

enum class GRIFFON_STATE
{
	IDLE,
	WALK,
	BITE,
	CLAWATTACK,
	DEATH,
};
enum class QUESTNPC_STATE
{
	IDLE,
	WALK,
};
enum class VIKING_STATE
{
	IDLE,
	WALK,
	RUN,
	ATTACK,
	DEATH,
	DEFEND,
	DEFENDING,
	JUMP,
};

// 0 : not Death
// 1 : Dying
// 2 : Death
enum class PLAYER_FAKE_DEATH
{
	ALIVE,
	DYING,
	DEATH,
};


enum class PICK_TYPE
{
	CREATE,
	REMOVE,
	MODIFY,
};

enum class OBJECT_KIND
{
	BIGROCK,				// 0
	BRIDGE_BUILDING,
	DAMWALL,
	GATE,
	GRAVE,
	GRAVE_PILLAR,
	SHANTY,
	SHANTY_UP,
	HOUSE,
	HOUSE_RIGHT,
	HOUSE_LEFT,
	HOUSE_SMALL,
	ROCK,
	TREE1,
	PLANE_ROCK,



	// Collider
	BIGROCK_COLLIDER,
	BRIDGE_BUILDING_COLLIDER,
	DAMWALL_COLLIDER,
	GATE_COLLIDER,
	GRAVE_COLLIDER,
	GRAVE_PILLAR_COLLIDER,
	SHANTY_COLLIDER,
	SHANTY_UP_COLLIDER,

	HOUSE_COLLIDER,
	HOUSE_RIGHT_COLLIDER,
	HOUSE_LEFT_COLLIDER,
	HOUSE_SMALL_COLLIDER,
	ROCK_COLLIDER,
	TREE1_COLLIDER,
	PLANE_ROCK_COLLIDER,

	SPRUCEA,
	PLAINS_GRASS,


};

enum class COLLIDER_KIND
{
	SPHERE,
	BOX,
	EMPTY,
};

enum class PLAYER_KIND
{
	VIKING,
	MOUNTAIN_DRAGON,
};


enum class QUEST_STATE
{
	GOTOTOWNLEADER,
	GOING,
	QUESTACCEPT,
	QUESTSTATUS,
	QUESTING,
	QUESTCLEAR,
	QUESTEND,
};