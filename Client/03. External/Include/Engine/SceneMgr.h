#pragma once

class CScene;
class CCamera;
class CGameObject;

class CSceneMgr
{
	SINGLE(CSceneMgr);

private:
	CScene*		m_pCurScene;


// Tool
private:
	Vec3 m_vPlanePoint1, m_vPlanePoint2, m_vPlanePoint3;
	CGameObject* m_pCurGameObject;


// Game Scene
private:
	CGameObject* m_pDragonPlayerObj;
	CGameObject* m_pVikingPlayerObj;

	CGameObject* m_pCurPlayerObj;

private:
	CGameObject* m_pLoginFontObj;
	CGameObject* m_pQuestObj;
	CGameObject* m_pLevelObj;


private:
	CGameObject* m_pUIObj;

public:
	CGameObject* GetUIObj() { return m_pUIObj; }

private:
	CGameObject* m_pPlayerCamera;
public:
	CGameObject* GetPlayerCamera() { return m_pPlayerCamera; }

public:
	void SetCurGameObjectPos(const float& fX, const float& fY, const float& fZ);
	void SetCurGameObjectRot(const float& fX, const float& fY, const float& fZ);
	void SetCurGameObjectScale(const float& fX, const float& fY, const float& fZ);

public:
	void LoadEnviroment();
	void LoadPlane();
	void Login();


public:
	void initLoginScene();


	void initTool();
	void initGameScene();

	void ClearCurrentScene();

	CGameObject* GetQuestObj() { return m_pQuestObj; }
	CGameObject* GetLevelObj() { return m_pLevelObj; }
	void update();
	//void update_tool();


public:
	CGameObject* GetFontObj() { return m_pLoginFontObj; }

public:
	void CreateLoadingSceneTexture();

public:
	CScene* GetCurScene();
	void ChangeScene(CScene* _pNextScene);
	void FindGameObjectByTag(const wstring& _strTag, vector<CGameObject*>& _vecFindObj);
	void Pick(int iScreenX, int iScreenY, HWND _hWnd, int ePickType, int eObjectKind, int eColliderKind, CGameObject** pGameObj, CGameObject** pCollider);

public:
	CGameObject* GetDragonObj() { return m_pDragonPlayerObj; }
	CGameObject* GetVikingObj() { return m_pVikingPlayerObj; }



public:
	void CreateTargetUI();
	void CreateHPMPUI();
};

