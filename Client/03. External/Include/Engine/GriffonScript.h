#pragma once
#include "Script.h"
class CGriffonScript :
	public CScript
{
private:
	Ptr<CMaterial>		m_pOriginMtrl;
	Ptr<CMaterial>		m_pCloneMtrl;

public:
	virtual void awake();
	virtual void update();

public:
	CLONE(CGriffonScript);

public:
	CGriffonScript();
	virtual ~CGriffonScript();
};

