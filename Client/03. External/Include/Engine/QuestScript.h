#pragma once
#include "Script.h"
class CQuestScript :
	public CScript
{
private:
	Vec4 m_vFontColor;
	Vec4 m_vFontBackColor;


private:
	vector<CGameObject*> m_vecBarghestNumFonts;
	wstring	m_strBarghestNum;
	int m_iBarghestNum;
	vector<CGameObject*> m_vecGriffonNumFonts;
	wstring	m_strGriffonNum;
	int m_iGriffonNum;
	vector<CGameObject*> m_vecDragonNumFonts;
	wstring	m_strDragonNum;
	int m_iDragonNum;

private:
	// 이거 게임 스테이트로 enum 값 하나로 관리해야할 것

	int m_eQuestState;
public:
	void SetQuestState(int _iState) { m_eQuestState = _iState; }
	const int GetQuestState() const { return m_eQuestState; }

private:


	CGameObject* m_pQuest01;
	CGameObject* m_pGoToTownLeader;
	CGameObject* m_pQuestStatus;
	CGameObject* m_pQuestClear;
public:
	int GetBarghestNum() { return m_iBarghestNum; }
	int GetGriffonNum() { return m_iGriffonNum; }
	int GetDragonNum() { return m_iDragonNum; }

public:
	void SetBarghestNum(int _iNum);
	void SetGriffonNum(int _iNum);
	void SetDragonNum(int _iNum);
	


public:
	void Init(Vec4 vFrontColor, Vec4 vFontBackColor);

public:
	virtual void awake();
	virtual void update();

public:
	void UpdateFont();

public:
	CLONE(CQuestScript);


public:
	CQuestScript();
	virtual ~CQuestScript();

};

