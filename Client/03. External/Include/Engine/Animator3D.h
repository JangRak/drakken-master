#pragma once
#include "Component.h"

#include "Ptr.h"
#include "Texture.h"
#include "Material.h"
#include "Mesh.h"


class CStructuredBuffer;

class CAnimator3D :
	public CComponent
{
private:
	const vector<tMTBone>*		m_pVecBones;
	const vector<tMTAnimClip>*	m_pVecClip;

	vector<float>				m_vecClipUpdateTime;
	vector<Matrix>				m_vecFinalBoneMatrix;	// 텍스쳐에 전달할 최종 행렬 정보
	int							m_iFrameCount;			// 30 Frame
	double						m_dCurTime;
	int							m_iCurClip;				// 클립 인덱스
	int							m_iFrameIdx;			// 클립의 현재 프레임
	int							m_iNextFrameIdx; // 다음 프레임 인덱스
	float						m_fRatio;	// 프레임 사이 비율



	Ptr<CMaterial>				m_pBoneMtrl;			// BoneData Update Compute Shader
	CStructuredBuffer*			m_pBoneFinalMat;		// 특정 프레임의 최종 행렬
	bool						m_bFinalMatUpdate;		// 최종 행렬 연산 수행여부

public:
	void SetBones(const vector<tMTBone>* _vecBones) 
	{ 
		m_pVecBones = _vecBones; 
		m_vecFinalBoneMatrix.resize(m_pVecBones->size()); 
	}
	void SetAnimClip(const vector<tMTAnimClip>* _vecAnimClip);
	void UpdateData();
	void UpdateData_Inst(CStructuredBuffer* _pBoneBuffer, UINT _iRow);
	void SetClipTime(int _iClipIdx, float _fTime) { m_vecClipUpdateTime[_iClipIdx] = _fTime; }

	const vector<tMTBone>* GetBones() { return m_pVecBones;	}
	const vector<tMTAnimClip>* GetAnimClip() { return m_pVecClip; }
	



	CStructuredBuffer* GetFinalBoneMat() { return m_pBoneFinalMat; }
	UINT GetBoneCount() { return (UINT)m_pVecBones->size(); }




private:
	vector<pair<int, int>> m_vecAnimFrameIdices;	//	각 애니메이션 시작, 종료 프레임 인덱스
	int m_iCurAnimIdx;								//	현재 재생할 애니메이션 종류 인덱스
	bool m_bIsEnd;					

public:
	void SetAnimFrameIndices(vector<pair<int, int>> _vecAnimFrameIdices);
	void SetCurAnimIdx(const int& idx) { m_iCurAnimIdx = idx; }
	const double GetCurAnimStartTime() const;
	const double GetCurAnimLength() const;

	void SetAnimIsEnd(const bool& _bIsEnd) { m_bIsEnd = _bIsEnd; }
	const bool GetIsEnd() const { return m_bIsEnd; };

	void InitCurAnimFrame();
private:
	void check_mesh(Ptr<CMesh> _pMesh);

public:
	virtual void awake() {};
	virtual void start() {};
	virtual void update();
	virtual void lateupdate();
	virtual void finalupdate();

public:
	virtual void SaveToScene(FILE* _pFile);
	virtual void LoadFromScene(FILE* _pFile);
	CAnimator3D* Clone() { return new CAnimator3D(*this); }

public:
	CAnimator3D();
	virtual ~CAnimator3D();
};

