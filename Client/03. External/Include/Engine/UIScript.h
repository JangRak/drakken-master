#pragma once
#include "Script.h"
class CUIScript :
	public CScript
{
private:
	CGameObject* m_pHPHide;
	CGameObject* m_pMPHide;

private:
	float		m_fHideUp;
public:
	void Init();

public:
	virtual void awake();
	virtual void update();


public:
	//void SetHPObject(CGameObject* _pHP);
	//void SetMPObject(CGameObject* _pMP);
	void SetHP(float MaxHP, float CurHP);
	void SetMP(float MaxMP, float CurMP);


public:
	CLONE(CUIScript);


public:
	CUIScript();
	virtual ~CUIScript();
};

