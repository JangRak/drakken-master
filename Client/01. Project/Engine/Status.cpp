#include "stdafx.h"
#include "Status.h"
#include "SceneMgr.h"
#include "UIScript.h"

CStatus::CStatus()
	: CComponent(COMPONENT_TYPE::STATUS)
	, m_fHP(0.f)
	, m_fRunSpeed(0.f)
	, m_fWalkSpeed(0.f)
	, m_fExp(0.f)
	, m_fAttack(0.f)
	, m_fAttackCoolTime(0.f)
	, m_iState(0)
	, m_fMaxHP(0.f)
	, m_fMaxExp(0.f)
{
}

CStatus::~CStatus()
{
}

void CStatus::finalupdate()
{

	if (GetObj()->GetName() == L"My Player")
	{
		CGameObject* pTempUIObj = CSceneMgr::GetInst()->GetUIObj();
		if (!pTempUIObj)
		{
			return;
		}
		pTempUIObj->GetScript<CUIScript>()->SetHP(m_fMaxHP, m_fHP);
		pTempUIObj->GetScript<CUIScript>()->SetExp(m_fMaxExp, m_fExp);

	}

}

void CStatus::SaveToScene(FILE * _pFile)
{
	UINT iType = (UINT)GetComponentType();
	fwrite(&iType, sizeof(UINT), 1, _pFile);

	fwrite(&m_fHP, sizeof(float), 1, _pFile);
	fwrite(&m_fRunSpeed, sizeof(float), 1, _pFile);
	fwrite(&m_fWalkSpeed, sizeof(float), 1, _pFile);
	fwrite(&m_fExp, sizeof(float), 1, _pFile);
	fwrite(&m_fAttack, sizeof(float), 1, _pFile);
	fwrite(&m_fAttackCoolTime, sizeof(float), 1, _pFile);
	fwrite(&m_iState, sizeof(int), 1, _pFile);


}

void CStatus::LoadFromScene(FILE * _pFile)
{
	fread(&m_fHP, sizeof(float), 1, _pFile);
	fread(&m_fRunSpeed, sizeof(float), 1, _pFile);
	fread(&m_fWalkSpeed, sizeof(float), 1, _pFile);
	fread(&m_fExp, sizeof(float), 1, _pFile);
	fread(&m_fAttack, sizeof(float), 1, _pFile);
	fread(&m_fAttackCoolTime, sizeof(float), 1, _pFile);
	fread(&m_iState, sizeof(int), 1, _pFile);
}






