#pragma once

class CScene;
class CCamera;
class CGameObject;

class CToolMgr
{
	SINGLE(CToolMgr);

private:
	CScene*		m_pCurScene;


private:
	DirectX::SimpleMath::Vector3 m_vPlanePoint1;
	DirectX::SimpleMath::Vector3 m_vPlanePoint2;
	DirectX::SimpleMath::Vector3 m_vPlanePoint3;

public:
	void init();

	void update();
	void update_tool();

public:
	CScene* GetCurScene();
	void ChangeScene(CScene* _pNextScene);
	void FindGameObjectByTag(const wstring& _strTag, vector<CGameObject*>& _vecFindObj);

	void Pick(int iScreenX, int iScreenY, HWND _hWnd, int ePickType, int eObjectKind);



};

