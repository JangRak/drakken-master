#include "value.fx"
//#include "func.fx"

#ifndef _FONT
#define _FONT

//struct VS_OUTPUT
//{
//    float4 vOutPos : SV_Position; // 버텍스 쉐이더까지만 사용할 것
//    float4 vOutColor : COLOR;
//    float3 vViewNormal : NORMAL;
//    float3 vViewTangent : TANGENT;
//    float3 vViewBinormal : BINORMAL;
//    float3 vViewPos : POSITION;
//    float2 vUV : TEXCOORD;
//};

struct TEXT_INPUT
{
    float3 vPos : POSITION;
    float2 vUV : TEXCOORD;
};

struct TEXT_OUTPUT
{
    float4 vPos : SV_Position;
    float2 vUV : TEXCOORD;
};

TEXT_OUTPUT VS_Font( TEXT_INPUT _input)
{
    TEXT_OUTPUT output = (TEXT_OUTPUT) 0;
    
    // 투영 좌표계를 봔한할 때는 4번째 w 요소에 1.f를 넣어준다.
    float4 vWorldPos = mul(float4(_input.vPos, 1.f), g_matWorld);
    float4 vViewPos = mul(vWorldPos, g_matView);
    float4 vProjPos = mul(vViewPos, g_matProj);
    
    output.vPos = vProjPos;
    output.vUV = _input.vUV;
    
    return output;
}

// 0일때는 start
// 1일때는 start + width
float4 PS_Font (TEXT_OUTPUT _input) : SV_Target
{
    float4 vColor = (float4) 0.f;
    // g_float_0 : Start U
    // g_float_1 : Width U
    // g_float_2 : Start V
    // g_float_3 : Height V
    _input.vUV.x = _input.vUV.x * g_float_1 + g_float_0;
    _input.vUV.y = _input.vUV.y * g_float_3 + g_float_2;
    // 0일때 0.2, 1일때 0.4
    
    vColor = g_tex_0.Sample(g_sam_1, _input.vUV);
    vColor = vColor * g_vec4_0;
    
    //if (vColor.a < 0.5f)
    //{
    //    vColor = g_vec4_1;
    //    return vColor;
    //}
    
    if (vColor.x < 0.2f && vColor.y < 0.2f && vColor.z < 0.2f)
    {
        discard;
    }

    
    //vColor = float4(1.f, 0.f, 0.f, 1.f);
    return vColor;
}



#endif