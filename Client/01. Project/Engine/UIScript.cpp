#include "stdafx.h"
#include "UIScript.h"
#include "Texture.h"
#include "RenderMgr.h"
#include "SceneMgr.h"


CUIScript::CUIScript()
	: CScript((UINT)SCRIPT_TYPE::UISCRIPT)
{
}

CUIScript::~CUIScript()
{
}

void CUIScript::awake()
{
}

void CUIScript::Init()
{
	// UI 

	Ptr<CTexture> pUIMoldTexture = CResMgr::GetInst()->Load<CTexture>(L"UIMold", L"Texture\\UI\\UIMold.png");
	Ptr<CTexture> pHPTexture = CResMgr::GetInst()->Load<CTexture>(L"HP", L"Texture\\UI\\HP.png");
	Ptr<CTexture> pMPTexture = CResMgr::GetInst()->Load<CTexture>(L"MP", L"Texture\\UI\\Exp.png");

	tResolution res = CRenderMgr::GetInst()->GetResolution();


	CGameObject* pObject = nullptr;


	// HP

	pObject = new CGameObject;
	pObject->AddComponent(new CMeshRender);
	pObject->AddComponent(new CTransform);
	pObject->Transform()->SetLocalPos(Vec3(-113.f * res.fWidth / 1280.f, 355.f * res.fHeight / 768.f, 2.f));
	pObject->Transform()->SetLocalScale(Vec3(1100 * res.fWidth / 1920.f, 10 * res.fHeight / 1080.f, 3.f));
	m_fMaxLength = pObject->Transform()->GetLocalScale().x;
	m_fFirstXPos = pObject->Transform()->GetLocalPos().x;

	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl"));
	pObject->MeshRender()->GetCloneMaterial()->SetData(SHADER_PARAM::TEX_0, pHPTexture.GetPointer());
	pObject->SetName(L"HP");

	m_pHP = pObject;
	CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(pObject);



	// MP

	pObject = new CGameObject;
	pObject->AddComponent(new CMeshRender);
	pObject->AddComponent(new CTransform);
	pObject->Transform()->SetLocalPos(Vec3(-113.f * res.fWidth / 1280.f, 340.f * res.fHeight / 768.f, 2.f));
	pObject->Transform()->SetLocalScale(Vec3(1100 * res.fWidth / 1920.f, 6 * res.fHeight / 1080.f, 1.f));



	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl"));
	pObject->MeshRender()->GetCloneMaterial()->SetData(SHADER_PARAM::TEX_0, pMPTexture.GetPointer());
	pObject->SetName(L"MP");

	m_pExp = pObject;
	CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(pObject);






	// UIMold

	pObject = new CGameObject;
	pObject->AddComponent(new CMeshRender);
	pObject->AddComponent(new CTransform);
	pObject->Transform()->SetLocalPos(Vec3(0.f, 0.f, 1.f));
	pObject->Transform()->SetLocalScale(Vec3(res.fWidth, res.fHeight, 1.f));

	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl"));
	pObject->MeshRender()->GetCloneMaterial()->SetData(SHADER_PARAM::TEX_0, pUIMoldTexture.GetPointer());
	pObject->SetName(L"UIMold");

	CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(pObject);
}

 

void CUIScript::update()
{
}

void CUIScript::SetHP(float MaxHP, float CurHP)
{
	Vec3 tempScale = m_pHP->Transform()->GetLocalScale();
	Vec3 tempPos = m_pHP->Transform()->GetLocalPos();
	float CurRatio = (CurHP / MaxHP);

	m_pHP->Transform()->SetLocalScale(Vec3(m_fMaxLength * CurRatio, tempScale.y, tempScale.z));

	float temp = m_fMaxLength - m_fMaxLength * CurRatio;

	m_pHP->Transform()->SetLocalPos(Vec3(m_fFirstXPos - temp / 2, tempPos.y, tempPos.z));

}

void CUIScript::SetExp(float MaxExp, float CurExp)
{
	Vec3 tempScale = m_pExp->Transform()->GetLocalScale();
	Vec3 tempPos = m_pExp->Transform()->GetLocalPos();
	float CurRatio = (CurExp / MaxExp);

	m_pExp->Transform()->SetLocalScale(Vec3(m_fMaxLength * CurRatio, tempScale.y, tempScale.z));

	float temp = m_fMaxLength - m_fMaxLength * CurRatio;

	m_pExp->Transform()->SetLocalPos(Vec3(m_fFirstXPos - temp / 2, tempPos.y, tempPos.z));


}



