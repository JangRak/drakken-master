//#pragma once
//#include "Script.h"
//
//class CVikingScript	:
//	public CScript
//{
//private:
//	Ptr<CMaterial>		m_pOriginMtrl;
//	Ptr<CMaterial>		m_pCloneMtrl;
//
//public:
//	virtual void awake();
//	virtual void update();
//
//public:
//	CLONE(CVikingScript);
//
//public:
//	CVikingScript();
//	virtual ~CVikingScript();
//};

#pragma once
#include "Script.h"


class CVikingScript :
	public CScript
{
private:
	bool m_bChange;
	bool m_bIsActive;

private:
	int m_ePlayerKind;

private:
	// 0 : not Death
	// 1 : Dying
	// 2 : Death
	int m_iFakeDeath;

public:
	void SetPlayerFakeDeath(int _iState) { m_iFakeDeath = _iState; }
	int GetPlayerFakeDeath() { return m_iFakeDeath; }



public:
	void SetPlayerKind(int _eKind) { m_ePlayerKind = _eKind; }
	int GetPlayerKind() { return m_ePlayerKind; }


public:
	const bool GetIsPlayerChange() const { return m_bChange; }
	void SetPlayerChange(const bool& _bChange) { m_bChange = _bChange; }
	const bool GetIsScriptActive() const { return m_bIsActive; }
	void SetScriptActive(const bool& _bActive) { m_bIsActive = _bActive; }

public:
	virtual void awake();
	virtual void update();

public:
	CLONE(CVikingScript);

public:
	CVikingScript();
	virtual ~CVikingScript();
};


