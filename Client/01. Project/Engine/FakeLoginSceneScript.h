#pragma once
#include "Script.h"


class CFakeLoginSceneScript :
	public CScript
{
private:
	//wstring m_strValue;
	Vec4 m_vFontColor;
	Vec4 m_vFontBackColor;

private:
	//Ptr<CMaterial>		m_pOriginMtrl;
	//Ptr<CMaterial>		m_pCloneMtrl;

private:
	vector<CGameObject*> m_vecIDFonts;
	wstring	m_strID;
	vector<CGameObject*> m_vecPWFonts;
	wstring	m_strPW;

private:
	bool m_bNext;
	bool m_bDone;

public:
	bool GetIsNext() { return m_bNext; }
	bool GetIsDone() { return m_bDone; }

	wstring GetID() { return m_strID; }
	wstring GetPW() { return m_strPW; }

public:
	void Init(Vec4 vFrontColor, Vec4 vFontBackColor);

public:
	virtual void awake();
	virtual void update();

public:
	CLONE(CFakeLoginSceneScript);


public:
	CFakeLoginSceneScript();
	virtual ~CFakeLoginSceneScript();


};

