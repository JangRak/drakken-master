#include "stdafx.h"
#include "MountainDragonScript.h"
#include "Animator3D.h"
#include "Status.h"
CMountainDragonScript::CMountainDragonScript()
	: CScript((UINT)SCRIPT_TYPE::MOUNTAINDRAGONSCRIPT)
	, m_pOriginMtrl(nullptr)
	, m_pCloneMtrl(nullptr)
	, m_bChange(false)
	, m_bIsActive(false)

{
}

CMountainDragonScript::~CMountainDragonScript()
{
}

void CMountainDragonScript::awake()
{
	m_pOriginMtrl = MeshRender()->GetSharedMaterial();
	m_pCloneMtrl = m_pOriginMtrl->Clone();

	int a = 1;
	m_pCloneMtrl->SetData(SHADER_PARAM::INT_0, &a);
}

void CMountainDragonScript::update()
{
	Vec3 vPos = Transform()->GetLocalPos();
	Vec3 vRot = Transform()->GetLocalRot();

	Matrix matRot = XMMatrixRotationX(vRot.x);
	matRot *= XMMatrixRotationY(vRot.y);
	matRot *= XMMatrixRotationZ(vRot.z);

	Vec3 vLook = XMVector3TransformNormal(Vec3::Front, matRot);
	Vec3 vRight = XMVector3TransformNormal(Vec3::Right, matRot);
	Vec3 vUp = XMVector3TransformNormal(Vec3::Up, matRot);

	MOUNTAINDRAGON_STATE tempState = MOUNTAINDRAGON_STATE::IDLE;

	if (KEY_HOLD(KEY_TYPE::KEY_LSHIFT))
	{
		tempState = MOUNTAINDRAGON_STATE::RUN;
	}



	if (KEY_HOLD(KEY_TYPE::KEY_W))
	{
		// 쉬프트 안눌렸으면
		if (tempState != MOUNTAINDRAGON_STATE::RUN)
		{
			tempState = MOUNTAINDRAGON_STATE::WALK;
			vPos.x -= DT * Status()->GetWalkSpeed() * vLook.x;
			vPos.y -= DT * Status()->GetWalkSpeed() * vLook.y;
			vPos.z -= DT * Status()->GetWalkSpeed() * vLook.z;
		}
		// 쉬프트 눌렸으면
		else
		{
			vPos.x -= DT * Status()->GetRunSpeed() * vLook.x;
			vPos.y -= DT * Status()->GetRunSpeed() * vLook.y;
			vPos.z -= DT * Status()->GetRunSpeed() * vLook.z;
		}
	}

	if (KEY_HOLD(KEY_TYPE::KEY_S))
	{
		// 쉬프트 안눌렸으면
		if (tempState != MOUNTAINDRAGON_STATE::RUN)
		{
			tempState = MOUNTAINDRAGON_STATE::WALK;
			vPos.x += DT * Status()->GetWalkSpeed() * vLook.x / 2;
			vPos.y += DT * Status()->GetWalkSpeed() * vLook.y / 2;
			vPos.z += DT * Status()->GetWalkSpeed() * vLook.z / 2;
		}
		else
		{
			vPos.x += DT * Status()->GetRunSpeed() * vLook.x / 2;
			vPos.y += DT * Status()->GetRunSpeed() * vLook.y / 2;
			vPos.z += DT * Status()->GetRunSpeed() * vLook.z / 2;

		}
	}

	if (KEY_HOLD(KEY_TYPE::KEY_A))
	{
		if (tempState != MOUNTAINDRAGON_STATE::RUN)
		{
			tempState = MOUNTAINDRAGON_STATE::WALK;
			vPos.x += DT * Status()->GetWalkSpeed() * vRight.x;
			vPos.y += DT * Status()->GetWalkSpeed() * vRight.y;
			vPos.z += DT * Status()->GetWalkSpeed() * vRight.z;
		}
		else
		{
			vPos.x += DT * Status()->GetRunSpeed() * vRight.x;
			vPos.y += DT * Status()->GetRunSpeed() * vRight.y;
			vPos.z += DT * Status()->GetRunSpeed() * vRight.z;
		}
	}

	if (KEY_HOLD(KEY_TYPE::KEY_D))
	{
		if (tempState != MOUNTAINDRAGON_STATE::RUN)
		{
			tempState = MOUNTAINDRAGON_STATE::WALK;
			vPos.x -= DT * Status()->GetWalkSpeed() * vRight.x;
			vPos.y -= DT * Status()->GetWalkSpeed() * vRight.y;
			vPos.z -= DT * Status()->GetWalkSpeed() * vRight.z;
		}
		else
		{
			vPos.x -= DT * Status()->GetRunSpeed() * vRight.x;
			vPos.y -= DT * Status()->GetRunSpeed() * vRight.y;
			vPos.z -= DT * Status()->GetRunSpeed() * vRight.z;

		}
	}

	if (KEY_TAB(KEY_TYPE::KEY_SPACE))
	{
	}
	if (KEY_TAB(KEY_TYPE::KEY_Q))
	{
		tempState = MOUNTAINDRAGON_STATE::BREATH;
	}


	if (KEY_TAB(KEY_TYPE::KEY_LBTN))
	{
		tempState = MOUNTAINDRAGON_STATE::BITE;
	}
	if (KEY_TAB(KEY_TYPE::KEY_RBTN))
	{
		tempState = MOUNTAINDRAGON_STATE::CLAWATTACK;
	}



	Transform()->SetLocalPos(vPos);
	Transform()->SetLocalRot(vRot);

	if (Status()->GetState() != (int)tempState)
	{
		if (Animator3D()->GetIsEnd() == true ||
			Status()->GetState() == (int)MOUNTAINDRAGON_STATE::WALK ||
			Status()->GetState() == (int)MOUNTAINDRAGON_STATE::IDLE)
		{
			Animator3D()->InitCurAnimFrame();
		}
		else
		{
			tempState = (MOUNTAINDRAGON_STATE)Status()->GetState();
		}
	}

	Status()->SetState((int)tempState);
	Animator3D()->SetCurAnimIdx(Status()->GetState());

}
