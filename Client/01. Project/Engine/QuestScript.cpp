#include "stdafx.h"
#include "QuestScript.h"
#include "Texture.h"
#include "FontMgr.h"
#include "RenderMgr.h"

CQuestScript::CQuestScript()
	: CScript((UINT)SCRIPT_TYPE::QUESTSCRIPT)
	, m_iGriffonNum(0)
	, m_iBarghestNum(0)
	, m_iDragonNum(0)
	, m_eQuestState(int(QUEST_STATE::GOTOTOWNLEADER))
{
}

CQuestScript::~CQuestScript()
{
}


void CQuestScript::SetBarghestNum(int _iNum)
{
	m_iBarghestNum = _iNum;
	m_strBarghestNum.clear();
	m_strBarghestNum = std::to_wstring(_iNum);
}

void CQuestScript::SetGriffonNum(int _iNum)
{
	m_iGriffonNum = _iNum;
	m_strGriffonNum.clear();
	m_strGriffonNum = std::to_wstring(_iNum);
}

void CQuestScript::SetDragonNum(int _iNum)
{
	m_iDragonNum = _iNum;
	m_strDragonNum.clear();
	m_strDragonNum = std::to_wstring(_iNum);
}

void CQuestScript::Init(Vec4 vFontColor, Vec4 vFontBackColor)
{
	m_vecBarghestNumFonts.reserve(20);
	m_vecGriffonNumFonts.reserve(20);
	m_vecDragonNumFonts.reserve(20);


	//m_strValue = str;
	m_vFontColor = vFontColor;
	m_vFontBackColor = vFontBackColor;


	Ptr<CTexture> pTex = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Quest\\GoToTownLeader.png", L"Texture\\Quest\\GoToTownLeader.png");

	tResolution res = CRenderMgr::GetInst()->GetResolution();

	m_pGoToTownLeader = new CGameObject;
	m_pGoToTownLeader->SetName(L"GoToTownLeader");
	m_pGoToTownLeader->AddComponent(new CTransform);
	m_pGoToTownLeader->Transform()->SetLocalPos(Vec3(0.f, 0.f, 10.f));
	//m_pQuest01->Transform()->SetLocalPos(Vec3(res.fWidth / 2, res.fHeight / 2, 1.f));
	m_pGoToTownLeader->Transform()->SetLocalScale(Vec3(400.f, 200.f, 1.f));


	m_pGoToTownLeader->AddComponent(new CMeshRender);
	m_pGoToTownLeader->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	m_pGoToTownLeader->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl"));
	m_pGoToTownLeader->MeshRender()->GetCloneMaterial()->SetData(SHADER_PARAM::TEX_0, pTex.GetPointer());

	m_pGoToTownLeader->FrustumCheck(false);
	CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(m_pGoToTownLeader);

	SetBarghestNum(0);
	SetGriffonNum(0);
	SetDragonNum(0);


	//Ptr<CTexture> pTex = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Quest\\Quest01.png", L"Texture\\Quest\\Quest01.png");

	//tResolution res = CRenderMgr::GetInst()->GetResolution();

	//m_pQuest01 = new CGameObject;
	//m_pQuest01->SetName(L"Quest01");
	//m_pQuest01->AddComponent(new CTransform);
	//m_pQuest01->Transform()->SetLocalPos(Vec3(0.f, 0.f, 10.f));
	////m_pQuest01->Transform()->SetLocalPos(Vec3(res.fWidth / 2, res.fHeight / 2, 1.f));
	//m_pQuest01->Transform()->SetLocalScale(Vec3(400.f, 200.f, 1.f));


	//m_pQuest01->AddComponent(new CMeshRender);
	//m_pQuest01->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	//m_pQuest01->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl"));
	//m_pQuest01->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pTex.GetPointer());

	//m_pQuest01->FrustumCheck(false);
	//CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(m_pQuest01);

	//UpdateFont();
}

void CQuestScript::awake()
{
}

void CQuestScript::update()
{
	Ptr<CTexture> pTex;
	tResolution res = CRenderMgr::GetInst()->GetResolution();

	switch (m_eQuestState)
	{
	case (int)QUEST_STATE::GOTOTOWNLEADER:
		if (KEY_AWAY(KEY_TYPE::KEY_ENTER))
		{
			m_pGoToTownLeader->SetDead();
			m_pQuestClear = nullptr;
			m_eQuestState = (int)QUEST_STATE::GOING;
		}

		break;
	case (int)QUEST_STATE::GOING:

		break;

	// 퀘스트 NPC에게 말을 걸면 호출
	case (int)QUEST_STATE::QUESTACCEPT:
		
		pTex = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Quest\\Quest01.png", L"Texture\\Quest\\Quest01.png");


		m_pQuest01 = new CGameObject;
		m_pQuest01->SetName(L"Quest01");
		m_pQuest01->AddComponent(new CTransform);
		m_pQuest01->Transform()->SetLocalPos(Vec3(0.f, 0.f, 10.f));
		m_pQuest01->Transform()->SetLocalScale(Vec3(400.f, 200.f, 1.f));


		m_pQuest01->AddComponent(new CMeshRender);
		m_pQuest01->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
		m_pQuest01->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl"));
		m_pQuest01->MeshRender()->GetCloneMaterial()->SetData(SHADER_PARAM::TEX_0, pTex.GetPointer());

		m_pQuest01->FrustumCheck(false);
		CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(m_pQuest01);
		m_eQuestState = (int)QUEST_STATE::QUESTSTATUS;

		break;
	// 퀘스트 수락
	case (int)QUEST_STATE::QUESTSTATUS:
		if (KEY_AWAY(KEY_TYPE::KEY_ENTER))
		{
			m_pQuest01->SetDead();
			m_pQuest01 = nullptr;
			m_eQuestState = (int)QUEST_STATE::QUESTING;


			pTex = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Quest\\Quest02_Status.png", L"Texture\\Quest\\Quest02_Status.png");

			m_pQuestStatus = new CGameObject;
			m_pQuestStatus->SetName(L"Quest02_Status");
			m_pQuestStatus->AddComponent(new CTransform);
			m_pQuestStatus->Transform()->SetLocalPos(Vec3(res.fWidth / 2.f - 150.f, 0.f, 1.f));
			m_pQuestStatus->Transform()->SetLocalScale(Vec3(200.f, 250.f, 1.f));


			m_pQuestStatus->AddComponent(new CMeshRender);
			m_pQuestStatus->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
			m_pQuestStatus->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl"));

			m_pQuestStatus->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pTex.GetPointer());
			m_pQuestStatus->FrustumCheck(false);
			CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(m_pQuestStatus);
			//UpdateFont();

		}
		break;

	// 퀘스트 진행중
	case (int)QUEST_STATE::QUESTING:
		if (m_iBarghestNum >= 4 && m_iGriffonNum >= 4 && m_iDragonNum >= 4)
		{
			m_pQuestStatus->SetDead();
			m_pQuestStatus = nullptr;

			// 폰트 삭제
			for (size_t i = 0; i < m_vecBarghestNumFonts.size(); ++i)
			{
				m_vecBarghestNumFonts[i]->SetDead();
				m_vecBarghestNumFonts[i] = nullptr;
			}
			m_vecBarghestNumFonts.clear();
			for (size_t i = 0; i < m_vecGriffonNumFonts.size(); ++i)
			{
				m_vecGriffonNumFonts[i]->SetDead();
				m_vecGriffonNumFonts[i] = nullptr;
			}
			m_vecGriffonNumFonts.clear();
			for (size_t i = 0; i < m_vecDragonNumFonts.size(); ++i)
			{
				m_vecDragonNumFonts[i]->SetDead();
				m_vecDragonNumFonts[i] = nullptr;
			}
			m_vecDragonNumFonts.clear();


			// 퀘스트 완료창 생성
			pTex = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Quest\\QuestClear.png", L"Texture\\Quest\\QuestClear.png");

			m_pQuestClear = new CGameObject;
			m_pQuestClear->SetName(L"QuestClear");
			m_pQuestClear->AddComponent(new CTransform);

			m_pQuestClear->Transform()->SetLocalPos(Vec3(0.f, 0.f, 1.f));
			m_pQuestClear->Transform()->SetLocalScale(Vec3(400.f, 250.f, 1.f));


			m_pQuestClear->AddComponent(new CMeshRender);
			m_pQuestClear->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
			m_pQuestClear->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl"));

			m_pQuestClear->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pTex.GetPointer());
			m_pQuestClear->FrustumCheck(false);
			CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(m_pQuestClear);

			m_eQuestState = (int)QUEST_STATE::QUESTCLEAR;
		}
		if (KEY_AWAY(KEY_TYPE::KEY_2))
		{
			SetBarghestNum(GetBarghestNum() + 1);
			SetGriffonNum(GetGriffonNum() + 1);
			SetDragonNum(GetDragonNum() + 1);
			UpdateFont();
		}

		break;
	case (int)QUEST_STATE::QUESTCLEAR:
		if (m_pQuest01)
		{
			if (KEY_AWAY(KEY_TYPE::KEY_ENTER))
			{
				m_pQuest01->SetDead();
				m_pQuest01 = nullptr;
				m_eQuestState = (int)QUEST_STATE::QUESTEND;
			}
		}

		if (m_pQuestClear)
		{
			m_pQuestClear->SetDead();
			m_pQuestClear = nullptr;

			pTex = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Quest\\QuestClear.png", L"Texture\\Quest\\QuestClear.png");

			m_pQuest01 = new CGameObject;
			m_pQuest01->SetName(L"QuestClear");
			m_pQuest01->AddComponent(new CTransform);
			m_pQuest01->Transform()->SetLocalPos(Vec3(0.f, 0.f, 1.f));
			m_pQuest01->Transform()->SetLocalScale(Vec3(400.f, 300.f, 1.f));


			m_pQuest01->AddComponent(new CMeshRender);
			m_pQuest01->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
			m_pQuest01->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl"));

			m_pQuest01->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pTex.GetPointer());
			m_pQuest01->FrustumCheck(false);
			CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(m_pQuest01);

		}
		break;
	default:
		break;
	}


	


}

void CQuestScript::UpdateFont()
{
	if (m_pQuestStatus)
	{
		Ptr<CTexture> pFontText = CFontMgr::GetInst()->GetFontTex();

		Vec3 vPos = m_pQuestStatus->Transform()->GetLocalPos();
		Vec3 vScale = m_pQuestStatus->Transform()->GetLocalScale();

		vPos.x += 40.f;
		vPos.y += 30.f;

		////////////////////
		// Barghest Num Font
		////////////////////

		for (int i = 0; i < m_vecBarghestNumFonts.size(); ++i)
		{
			m_vecBarghestNumFonts[i]->SetDead();
			m_vecBarghestNumFonts[i] = nullptr;

		}
		m_vecBarghestNumFonts.clear();


		for (int i = 0; i < m_strBarghestNum.size(); ++i)
		{
			CGameObject * pFont = new CGameObject();

			pFont->AddComponent(new CTransform);
			pFont->AddComponent(new CMeshRender);
			pFont->Transform()->SetLocalPos(Vec3(vPos.x + (vScale.x * i), vPos.y, 1.f));
			pFont->Transform()->SetLocalScale(Vec3(10.f, 20.f, 1.f));

			pFont->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
			pFont->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"FontMtrl"));
			pFont->MeshRender()->GetCloneMaterial()->SetData(SHADER_PARAM::TEX_0, pFontText.GetPointer());

			CharInfo tInfo = CFontMgr::GetInst()->GetFontInfo().mCharInfo[m_strBarghestNum[i]];
			Ptr<CMaterial> pMtrl = pFont->MeshRender()->GetSharedMaterial();
			float sizeX = (float)CFontMgr::GetInst()->GetFontInfo().iScaleX;
			float sizeY = (float)CFontMgr::GetInst()->GetFontInfo().iScaleY;
			float startU = tInfo.ix / sizeX;
			float startV = tInfo.iy / sizeY;
			float widthU = tInfo.iWidth / sizeX;
			float HeightV = tInfo.iHeight / sizeY;

			pMtrl->SetData(SHADER_PARAM::FLOAT_0, &startU);
			pMtrl->SetData(SHADER_PARAM::FLOAT_1, &widthU);
			pMtrl->SetData(SHADER_PARAM::FLOAT_2, &startV);
			pMtrl->SetData(SHADER_PARAM::FLOAT_3, &HeightV);
			pMtrl->SetData(SHADER_PARAM::VEC4_0, &m_vFontColor);
			pMtrl->SetData(SHADER_PARAM::VEC4_1, &m_vFontBackColor);

			m_vecBarghestNumFonts.push_back(pFont);
			CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(pFont);
		}

		///////////////////
		// Griffon Num Font
		///////////////////


		for (int i = 0; i < m_vecGriffonNumFonts.size(); ++i)
		{
			m_vecGriffonNumFonts[i]->SetDead();
			m_vecGriffonNumFonts[i] = nullptr;

		}
		m_vecGriffonNumFonts.clear();
		vPos.y -= 60.f;

		for (int i = 0; i < m_strGriffonNum.size(); ++i)
		{
			CGameObject * pFont = new CGameObject();

			pFont->AddComponent(new CTransform);
			pFont->AddComponent(new CMeshRender);
			pFont->Transform()->SetLocalPos(Vec3(vPos.x + (vScale.x * i), vPos.y, 1.f));
			pFont->Transform()->SetLocalScale(Vec3(10.f, 20.f, 1.f));

			pFont->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
			pFont->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"FontMtrl"));
			pFont->MeshRender()->GetCloneMaterial()->SetData(SHADER_PARAM::TEX_0, pFontText.GetPointer());

			CharInfo tInfo = CFontMgr::GetInst()->GetFontInfo().mCharInfo[m_strGriffonNum[i]];
			Ptr<CMaterial> pMtrl = pFont->MeshRender()->GetSharedMaterial();
			float sizeX = (float)CFontMgr::GetInst()->GetFontInfo().iScaleX;
			float sizeY = (float)CFontMgr::GetInst()->GetFontInfo().iScaleY;
			float startU = tInfo.ix / sizeX;
			float startV = tInfo.iy / sizeY;
			float widthU = tInfo.iWidth / sizeX;
			float HeightV = tInfo.iHeight / sizeY;

			pMtrl->SetData(SHADER_PARAM::FLOAT_0, &startU);
			pMtrl->SetData(SHADER_PARAM::FLOAT_1, &widthU);
			pMtrl->SetData(SHADER_PARAM::FLOAT_2, &startV);
			pMtrl->SetData(SHADER_PARAM::FLOAT_3, &HeightV);
			pMtrl->SetData(SHADER_PARAM::VEC4_0, &m_vFontColor);
			pMtrl->SetData(SHADER_PARAM::VEC4_1, &m_vFontBackColor);

			m_vecGriffonNumFonts.push_back(pFont);
			CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(pFont);
		}


		///////////////////
		// Dragon Num Font
		///////////////////


		for (int i = 0; i < m_vecDragonNumFonts.size(); ++i)
		{
			m_vecDragonNumFonts[i]->SetDead();
			m_vecDragonNumFonts[i] = nullptr;
		}
		m_vecDragonNumFonts.clear();
		vPos.y -= 60.f;

		for (int i = 0; i < m_strDragonNum.size(); ++i)
		{
			CGameObject * pFont = new CGameObject();

			pFont->AddComponent(new CTransform);
			pFont->AddComponent(new CMeshRender);
			pFont->Transform()->SetLocalPos(Vec3(vPos.x + (vScale.x * i), vPos.y, 1.f));
			pFont->Transform()->SetLocalScale(Vec3(10.f, 20.f, 1.f));

			pFont->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
			pFont->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"FontMtrl"));
			pFont->MeshRender()->GetCloneMaterial()->SetData(SHADER_PARAM::TEX_0, pFontText.GetPointer());

			CharInfo tInfo = CFontMgr::GetInst()->GetFontInfo().mCharInfo[m_strDragonNum[i]];
			Ptr<CMaterial> pMtrl = pFont->MeshRender()->GetSharedMaterial();
			float sizeX = (float)CFontMgr::GetInst()->GetFontInfo().iScaleX;
			float sizeY = (float)CFontMgr::GetInst()->GetFontInfo().iScaleY;
			float startU = tInfo.ix / sizeX;
			float startV = tInfo.iy / sizeY;
			float widthU = tInfo.iWidth / sizeX;
			float HeightV = tInfo.iHeight / sizeY;

			pMtrl->SetData(SHADER_PARAM::FLOAT_0, &startU);
			pMtrl->SetData(SHADER_PARAM::FLOAT_1, &widthU);
			pMtrl->SetData(SHADER_PARAM::FLOAT_2, &startV);
			pMtrl->SetData(SHADER_PARAM::FLOAT_3, &HeightV);
			pMtrl->SetData(SHADER_PARAM::VEC4_0, &m_vFontColor);
			pMtrl->SetData(SHADER_PARAM::VEC4_1, &m_vFontBackColor);

			m_vecDragonNumFonts.push_back(pFont);
			CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(pFont);
		}
	}

}

