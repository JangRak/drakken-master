#include "stdafx.h"
#include "SceneMgr.h"

#include "Scene.h"
#include "Layer.h"
#include "GameObject.h"

#include "ResMgr.h"
#include "Shader.h"
#include "Mesh.h"
#include "Texture.h"
#include "Transform.h"
#include "MeshRender.h"
#include "Collider2D.h"
#include "Light3D.h"
#include "ParticleSystem.h"
#include "Animator3D.h"
#include "Status.h"

#include "TimeMgr.h"
#include "KeyMgr.h"
#include "Camera.h"

#include "CollisionMgr.h"
#include "EventMgr.h"
#include "RenderMgr.h"
#include "Device.h"
#include "Core.h"

#include "PlayerScript.h"
#include "MonsterScript.h"
#include "ToolCamScript.h"
#include "GridScript.h"
#include "GriffonScript.h"
#include "BarghestScript.h"
#include "MountainDragonScript.h"
#include "PlayerCameraScript.h"
#include "VikingScript.h"
#include "ConstantStringScript.h"
#include "QuestScript.h"
#include "FakeLoginSceneScript.h"
#include "UIScript.h"
#include "QuestNPCScript.h"
#include "BlueMountainDragonScript.h"
#include "PlayerLightScript.h"
#include "LevelScript.h"

#include "Transform.h"

#include "NetworkMgr.h"

#include <fstream>
#include <cmath>



CScene * CSceneMgr::GetCurScene()
{
	return m_pCurScene;
}

void CSceneMgr::ChangeScene(CScene * _pNextScene)
{
	SAFE_DELETE(m_pCurScene);
	m_pCurScene = _pNextScene;
}

CSceneMgr::CSceneMgr()
	: m_pCurScene(nullptr)
	, m_vPlanePoint1{}
	, m_vPlanePoint2{}
	, m_vPlanePoint3{}
	, m_pCurGameObject(nullptr)
	, m_pLoginFontObj(nullptr)
{
}

CSceneMgr::~CSceneMgr()
{
	SAFE_DELETE(m_pCurScene);
	SAFE_DELETE(m_pCurGameObject);
}


void CSceneMgr::CreateTargetUI()
{
	Vec3 vScale(150.f, 150.f, 1.f);

	Ptr<CTexture> arrTex[5] = {
		  CResMgr::GetInst()->FindRes<CTexture>(L"DiffuseTargetTex")
		, CResMgr::GetInst()->FindRes<CTexture>(L"NormalTargetTex")
		, CResMgr::GetInst()->FindRes<CTexture>(L"PositionTargetTex")
		, CResMgr::GetInst()->FindRes<CTexture>(L"DiffuseLightTargetTex")
		, CResMgr::GetInst()->FindRes<CTexture>(L"SpecularLightTargetTex")
	};

	for (UINT i = 0; i < 5; ++i)
	{
		CGameObject* pObject = new CGameObject;
		pObject->SetName(L"UI Object");
		pObject->FrustumCheck(false);
		pObject->AddComponent(new CTransform);
		pObject->AddComponent(new CMeshRender);

		// Transform
		tResolution res = CRenderMgr::GetInst()->GetResolution();

		// for문을 돌며 MRT의 좌표 설정
		pObject->Transform()->SetLocalPos
		(Vec3(-(res.fWidth / 2.f) + (vScale.x / 2.f) + (i * vScale.x)
			, (res.fHeight / 2.f) - (vScale.y / 2.f)
			, 1.f));

		pObject->Transform()->SetLocalScale(vScale);

		// MeshRender 설정
		pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
		Ptr<CMaterial> pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl");
		pObject->MeshRender()->SetMaterial(pMtrl->Clone());
		pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, arrTex[i].GetPointer());


		// Add GameObject
		m_pCurScene->FindLayer(L"UI")->AddGameObject(pObject);
	}

}

void CSceneMgr::CreateHPMPUI()
{

	tResolution res = CRenderMgr::GetInst()->GetResolution();
	CGameObject* pUIObject = nullptr;

	pUIObject = new CGameObject;
	pUIObject->AddComponent(new CTransform);
	pUIObject->AddComponent(new CUIScript);
	pUIObject->Transform()->SetLocalPos(Vec3(0.f, 0.f, 1.f));
	pUIObject->Transform()->SetLocalScale(Vec3(res.fWidth, res.fHeight, 1.f));
	pUIObject->SetName(L"HPMPUI");
	pUIObject->GetScript<CUIScript>()->Init();

	CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(pUIObject);

	m_pUIObj = pUIObject;
}




void CSceneMgr::initLoginScene()
{
	m_pCurScene = new CScene;
	m_pCurScene->SetName(L"Login Scene");

	// ===============
	// Layer 이름 지정
	// ===============
	m_pCurScene->GetLayer(0)->SetName(L"Default");
	m_pCurScene->GetLayer(30)->SetName(L"UI");



	CGameObject* pObject = nullptr;

	// //UI Camera 
	CGameObject* pUICam = new CGameObject;
	pUICam->SetName(L"UICam");
	pUICam->AddComponent(new CTransform);
	pUICam->AddComponent(new CCamera);

	pUICam->Camera()->SetProjType(PROJ_TYPE::ORTHOGRAPHIC);
	pUICam->Camera()->SetFar(100.f);
	//pUICam->Camera()->SetLayerCheck(30, true);
	pUICam->Camera()->SetLayerAllCheck();
	pUICam->Camera()->SetWidth(CRenderMgr::GetInst()->GetResolution().fWidth);
	pUICam->Camera()->SetHeight(CRenderMgr::GetInst()->GetResolution().fHeight);

	m_pCurScene->FindLayer(L"Default")->AddGameObject(pUICam);


	// Font
	tResolution res = CRenderMgr::GetInst()->GetResolution();

		
	pObject = new CGameObject;
	pObject->SetName(L"FontGenerator1");
	pObject->AddComponent(new CTransform);
	pObject->Transform()->SetLocalPos(Vec3(-120.f, -200.f, 1.f));
	pObject->Transform()->SetLocalScale(Vec3(20.f, 30.f, 1.f));
	pObject->AddComponent(new CConstantStringScript);
	pObject->FrustumCheck(false);
	m_pCurScene->FindLayer(L"Default")->AddGameObject(pObject);
	pObject->GetScript<CConstantStringScript>()->Init(Vec4(1.0f, 1.0f, 1.0f, 1.f), Vec4(0.0f, 0.0f, 0.0f, 0.f));

	m_pLoginFontObj = pObject;

	// Background

	Ptr<CTexture> pTex = CResMgr::GetInst()->Load<CTexture>(L"Texture\\Login.png", L"Texture\\Login.png");


	pObject = new CGameObject;
	pObject->SetName(L"LoginTexture");
	pObject->AddComponent(new CTransform);
	pObject->Transform()->SetLocalScale(Vec3(res.fWidth, res.fHeight, 1.f));
	pObject->Transform()->SetLocalPos(Vec3(0.f, 0.f, 10.f));


	pObject->AddComponent(new CMeshRender);
	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"PlaneMtrl"));
	//int a = 1;
	//pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &a);
	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pTex.GetPointer());


	//pObject->Transform()->SetLocalPos(Vec3(0.f, 0.f, 1.f));
	//pObject->Transform()->SetLocalScale(Vec3(150.f, 150.f, 1.f));
	pObject->FrustumCheck(false);
	m_pCurScene->FindLayer(L"UI")->AddGameObject(pObject);

	//CreateLoadingSceneTexture();

	// ====================
	// 3D Light Object 추가
	// ====================
	pObject = new CGameObject;
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CLight3D);

	pObject->Light3D()->SetLightPos(Vec3(0.f, 0.f, -1000.f));
	pObject->Light3D()->SetLightType(LIGHT_TYPE::DIR);
	pObject->Light3D()->SetDiffuseColor(Vec3(1.f, 1.f, 1.f));
	pObject->Light3D()->SetSpecular(Vec3(0.f, 0.f, 0.f));
	pObject->Light3D()->SetAmbient(Vec3(0.1f, 0.1f, 0.1f));
	pObject->Light3D()->SetLightDir(Vec3(0.f, 0.f, 1.f));
	pObject->Light3D()->SetLightRange(10000.f);

	m_pCurScene->FindLayer(L"Default")->AddGameObject(pObject);




	m_pCurScene->awake();
	m_pCurScene->start();
}

void CSceneMgr::initTool()
{
	// ===============
	// Test Scene 생성
	// ===============
	m_pCurScene = new CScene;
	m_pCurScene->SetName(L"Tool Scene");

	// ===============
	// Layer 이름 지정
	// ===============
	m_pCurScene->GetLayer(0)->SetName(L"Default");
	m_pCurScene->GetLayer(1)->SetName(L"Player");
	m_pCurScene->GetLayer(2)->SetName(L"Monster");
	m_pCurScene->GetLayer(3)->SetName(L"Bullet");
	m_pCurScene->GetLayer(4)->SetName(L"Enviroment");
	m_pCurScene->GetLayer(5)->SetName(L"Enviroment_Collider");


	m_pCurScene->GetLayer(30)->SetName(L"UI");
	m_pCurScene->GetLayer(31)->SetName(L"Tool");



	// ===============
	// Test Scene 생성
	// ===============

		// 평면 좌표 설정
	m_vPlanePoint1 = Vec3(-100000.f, 0.f, -100000.f);
	m_vPlanePoint2 = Vec3(0.f, 0.f, 100000.f);
	m_vPlanePoint3 = Vec3(100000.f, 0.f, -100000.f);


	// =================
	// 필요한 리소스 로딩
	// =================
	// Texture 로드
	Ptr<CTexture> pSky02 = CResMgr::GetInst()->Load<CTexture>(L"Sky02", L"Texture\\Skybox\\Sky02.jpg");

	Ptr<CTexture> pColor = CResMgr::GetInst()->Load<CTexture>(L"Tile", L"Texture\\Tile\\TILE_03.tga");
	Ptr<CTexture> pNormal = CResMgr::GetInst()->Load<CTexture>(L"Tile_n", L"Texture\\Tile\\TILE_03_N.tga");



	CGameObject* pObject = nullptr;






	// ==================
	// Camera Object 생성
	// ==================
	CGameObject* pMainCam = new CGameObject;
	pMainCam->SetName(L"MainCam");
	pMainCam->AddComponent(new CTransform);
	pMainCam->AddComponent(new CCamera);
	pMainCam->AddComponent(new CToolCamScript);

	pMainCam->Camera()->SetProjType(PROJ_TYPE::PERSPECTIVE);
	pMainCam->Camera()->SetFar(100000.f);
	pMainCam->Camera()->SetLayerAllCheck();

	pMainCam->Transform()->SetLocalPos(Vec3(0.f, 1000.f, 0.f));

	m_pCurScene->FindLayer(L"Default")->AddGameObject(pMainCam);



	// ====================
	// 3D Light Object 추가
	// ====================
	pObject = new CGameObject;
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CLight3D);

	pObject->Light3D()->SetLightPos(Vec3(0.f, 200.f, 1000.f));
	pObject->Light3D()->SetLightType(LIGHT_TYPE::DIR);
	pObject->Light3D()->SetDiffuseColor(Vec3(1.f, 1.f, 1.f));
	pObject->Light3D()->SetSpecular(Vec3(0.3f, 0.3f, 0.3f));
	pObject->Light3D()->SetAmbient(Vec3(0.1f, 0.1f, 0.1f));
	pObject->Light3D()->SetLightDir(Vec3(1.f, -1.f, 1.f));
	pObject->Light3D()->SetLightRange(500.f);

	m_pCurScene->FindLayer(L"Default")->AddGameObject(pObject);




	// ====================
	// Skybox 오브젝트 생성
	// ====================
	pObject = new CGameObject;
	pObject->SetName(L"SkyBox");
	pObject->FrustumCheck(false);
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CMeshRender);

	// MeshRender 설정
	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
	pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"SkyboxMtrl"));
	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pSky02.GetPointer());

	// AddGameObject
	m_pCurScene->FindLayer(L"Default")->AddGameObject(pObject);

	// ====================
	// Grid 오브젝트 생성
	// ====================
	pObject = new CGameObject;
	pObject->SetName(L"Grid");
	pObject->FrustumCheck(false);
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CMeshRender);
	pObject->AddComponent(new CGridScript);

	// Transform 설정
	pObject->Transform()->SetLocalScale(Vec3(100000.f, 100000.f, 1.f));
	pObject->Transform()->SetLocalRot(Vec3(XM_PI / 2.f, 0.f, 0.f));

	// MeshRender 설정
	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"GridMtrl"));

	// Script 설정	
	pObject->GetScript<CGridScript>()->SetToolCamera(pMainCam);
	pObject->GetScript<CGridScript>()->SetGridColor(Vec3(0.8f, 0.2f, 0.2f));

	// AddGameObject
	m_pCurScene->FindLayer(L"Tool")->AddGameObject(pObject);



	for (int i = 0; i < 20; ++i) {
		for (int j = 0; j < 20; ++j) {
			pObject = new CGameObject;
			pObject->SetName(L"plane_rock");
			pObject->AddComponent(new CTransform);
			pObject->AddComponent(new CMeshRender);
			float x = (float)i * 1000.f - 4000.f;
			float z = (float)j * 1000.f - 4000.f;
			// Transform 설정
			pObject->Transform()->SetLocalPos(Vec3(x, 1.f, z));
			pObject->Transform()->SetLocalScale(Vec3(1000.f, 1000.f, 1.f));
			pObject->Transform()->SetLocalRot(Vec3(XM_PI / 2.f, 0.f, 0.f));
			// MeshRender 설정
			pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
			pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"Std3DMtrl"));
			pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pColor.GetPointer());
			pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_1, pNormal.GetPointer());
			//int a = 1;
			//pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &a);
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pObject);


		}
	}





	// Font Test

	//CGameObject* pObject = new CGameObject;
	//pObject->SetName(L"FontGenerator");
	//pObject->AddComponent(new CTransform);
	//pObject->AddComponent(new CConstantStringScript);
	//m_pCurScene->FindLayer(L"UI")->AddGameObject(pObject);
	//pObject->GetScript<CConstantStringScript>()->Init(L"test", Vec4(1.0f, 0.0f, 0.0f, 1.f), Vec4(1.0f, 1.0f, 1.0f, 1.f));


	//CGameObject* pObject = new CGameObject;
	//pObject->SetName(L"Particle");
	//pObject->AddComponent(new CTransform);
	//pObject->AddComponent(new CParticleSystem);

	//pObject->FrustumCheck(false);
	//Vec3 tempVec3 = m_pCurPlayerObj->Transform()->GetLocalPos();
	//pObject->Transform()->SetLocalPos(Vec3(0.f, 100.f, 0.f));

	////pObject->Particlesystem()->SetParticleImage(L"Texture\\Particle\\smokeparticle.png", L"Texture\\Particle\\smokeparticle.png");
	////pObject->Particlesystem()->SetMaintainTime(50000.0f);
	//m_pCurScene->FindLayer(L"Default")->AddGameObject(pObject);


	m_pCurScene->awake();
	m_pCurScene->start();

}

void CSceneMgr::initGameScene()
{
	// ===============
	// Test Scene 생성
	// ===============
	CScene* pGameScene = new CScene;
	m_pCurScene = pGameScene;
	pGameScene->SetName(L"Game Scene");

	// ===============
	// Layer 이름 지정
	// ===============
	pGameScene->GetLayer(0)->SetName(L"Default");
	pGameScene->GetLayer(1)->SetName(L"Player");
	pGameScene->GetLayer(2)->SetName(L"Monster");
	pGameScene->GetLayer(3)->SetName(L"Bullet");
	pGameScene->GetLayer(4)->SetName(L"Enviroment");
	pGameScene->GetLayer(5)->SetName(L"Enviroment_Collider");
	pGameScene->GetLayer(6)->SetName(L"Particle");
	pGameScene->GetLayer(7)->SetName(L"NPC");


	pGameScene->GetLayer(30)->SetName(L"UI");
	pGameScene->GetLayer(31)->SetName(L"Tool");



	// ===============
	// Test Scene 생성
	// ===============

		// ================
		// 필요한 리소스 로딩
		// ================


		// Texture 로드
		//Ptr<CTexture> pTex = CResMgr::GetInst()->Load<CTexture>(L"TestTex", L"Texture\\pickachu.png");
		//Ptr<CTexture> pMonsterTex = CResMgr::GetInst()->Load<CTexture>(L"MonsterTex", L"Texture\\firy.png");
		//Ptr<CTexture> pMonster2Tex = CResMgr::GetInst()->Load<CTexture>(L"Monster2Tex", L"Texture\\firy.png");

	Ptr<CTexture> pSky01 = CResMgr::GetInst()->Load<CTexture>(L"Sky01", L"Texture\\Skybox\\Sky01.png");
	Ptr<CTexture> pSky02 = CResMgr::GetInst()->Load<CTexture>(L"Sky02", L"Texture\\Skybox\\Sky02.jpg");

	//Ptr<CTexture> pColor = CResMgr::GetInst()->Load<CTexture>(L"Tile", L"Texture\\Tile\\TILE_03.tga");
	//Ptr<CTexture> pNormal = CResMgr::GetInst()->Load<CTexture>(L"Tile_n", L"Texture\\Tile\\TILE_03_N.tga");

	Ptr<CTexture> pDiffuseTargetTex = CResMgr::GetInst()->FindRes<CTexture>(L"DiffuseTargetTex");
	Ptr<CTexture> pNormalTargetTex = CResMgr::GetInst()->FindRes<CTexture>(L"NormalTargetTex");
	Ptr<CTexture> pPositionTargetTex = CResMgr::GetInst()->FindRes<CTexture>(L"PositionTargetTex");

	// UAV 용 Texture 생성
	Ptr<CTexture> pTestUAVTexture = CResMgr::GetInst()->CreateTexture(L"UAVTexture", 1024, 1024
		, DXGI_FORMAT_R8G8B8A8_UNORM, CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT), D3D12_HEAP_FLAG_NONE
		, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);

	CGameObject* pObject = nullptr;

	// ==================
	// 카메라 오브젝트 생성
	// ==================

	// Main Camera
	CGameObject* pMainCam = new CGameObject;
	pMainCam->SetName(L"MainCam");
	pMainCam->AddComponent(new CTransform);
	pMainCam->AddComponent(new CCamera);
	//pMainCam->AddComponent(new CToolCamScript);
	pMainCam->AddComponent(new CPlayerCameraScript);

	pMainCam->Camera()->SetProjType(PROJ_TYPE::PERSPECTIVE);
	//pMainCam->Camera()->SetProjType(PROJ_TYPE::ORTHOGRAPHIC);
	pMainCam->Camera()->SetFar(20000.f);
	pMainCam->Camera()->SetLayerAllCheck();
	pMainCam->Camera()->SetLayerCheck(30, false);

	//pMainCam->Transform()->SetLocalPos({ 0.f, 100.f, -300.f });
	pMainCam->Transform()->SetLocalPos(pMainCam->GetScript<CPlayerCameraScript>()->GetOffset());

	pGameScene->FindLayer(L"Default")->AddGameObject(pMainCam);

	m_pPlayerCamera = pMainCam;

	// UI Camera 
	CGameObject* pUICam = new CGameObject;
	pUICam->SetName(L"UICam");
	pUICam->AddComponent(new CTransform);
	pUICam->AddComponent(new CCamera);

	pUICam->Camera()->SetProjType(PROJ_TYPE::ORTHOGRAPHIC);
	pUICam->Camera()->SetFar(100.f);
	pUICam->Camera()->SetLayerCheck(30, true);
	pUICam->Camera()->SetWidth(CRenderMgr::GetInst()->GetResolution().fWidth);
	pUICam->Camera()->SetHeight(CRenderMgr::GetInst()->GetResolution().fHeight);

	pGameScene->FindLayer(L"Default")->AddGameObject(pUICam);
	//
	//CreateTargetUI();

	// ====================
	// 3D Light Object 추가
	// ====================
	CGameObject* LightObj = nullptr;

	LightObj = new CGameObject;
	LightObj->AddComponent(new CTransform);
	LightObj->AddComponent(new CLight3D);

	LightObj->Light3D()->SetLightPos(Vec3(0.f, 300.f, 0.f));
	LightObj->Light3D()->SetLightType(LIGHT_TYPE::DIR);
	LightObj->Light3D()->SetDiffuseColor(Vec3(1.f, 1.f, 1.f));
	LightObj->Light3D()->SetSpecular(Vec3(0.3f, 0.3f, 0.3f));
	LightObj->Light3D()->SetAmbient(Vec3(0.1f, 0.1f, 0.1f));
	LightObj->Light3D()->SetLightDir(Vec3(1.f, -1.f, 1.f));
	LightObj->Light3D()->SetLightRange(10000.f);

	LightObj->AddComponent(new CPlayerLightScript);

	pGameScene->FindLayer(L"Default")->AddGameObject(LightObj);



	// =================
	// Quest Object 추가
	// =================
	m_pQuestObj = new CGameObject;
	m_pQuestObj->AddComponent(new CTransform);
	//m_pQuestObj->Transform()->SetLocalPos(Vec3(m_pQuestObj->Transform()->GetLocalPos().x, 10.f, m_pQuestObj->Transform()->GetLocalPos().z));
	m_pQuestObj->AddComponent(new CQuestScript);
	m_pQuestObj->GetScript<CQuestScript>()->Init(Vec4(1.0f, 1.0f, 1.0f, 1.0f), Vec4(0.0f, 0.0f, 0.0f, 0.0f));
	m_pQuestObj->GetScript<CQuestScript>()->SetBarghestNum(0);
	m_pQuestObj->GetScript<CQuestScript>()->SetGriffonNum(0);
	pGameScene->AddGameObject(L"UI", m_pQuestObj, false);


	// =============
	// Level
	// =============
	pObject = new CGameObject;
	pObject->AddComponent(new CTransform);
	//m_pQuestObj->Transform()->SetLocalPos(Vec3(m_pQuestObj->Transform()->GetLocalPos().x, 10.f, m_pQuestObj->Transform()->GetLocalPos().z));
	pObject->AddComponent(new CLevelScript);
	//pObject->GetScript<CLevelScript>()->Init(Vec4(0.0f, 0.0f, 0.0f, 1.0f), Vec4(1.0f, 1.0f, 1.0f, 1.0f));
	pObject->GetScript<CLevelScript>()->Init(Vec4(1.0f, 1.0f, 1.0f, 1.0f), Vec4(0.0f, 0.0f, 0.0f, 1.0f));
	pObject->GetScript<CLevelScript>()->SetLevelNum(1);
	pObject->GetScript<CLevelScript>()->UpdateFont();

	m_pLevelObj = pObject;

	pGameScene->AddGameObject(L"UI", pObject, false);


	Ptr<CMeshData> pMeshData;
	CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Barghest.mdat", L"MeshData\\Barghest.mdat");
	CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Griffon.mdat", L"MeshData\\Griffon.mdat");
	CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\MountainDragon.mdat", L"MeshData\\MountainDragon.mdat");
	CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\BlueMountainDragon.mdat", L"MeshData\\BlueMountainDragon.mdat");

	// ===========
	// Player Load
	// ===========

	//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Viking.fbx");
	//pMeshData->Save(pMeshData->GetPath());

	pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Viking.mdat", L"MeshData\\Viking.mdat");
	pObject = pMeshData->Instantiate();
	pObject->AddComponent(new CStatus);
	pObject->Status()->SetMaxHP(100.f);
	pObject->Status()->SetHP(80.f);
	pObject->Status()->SetMaxExp(100.f);
	pObject->Status()->SetExp(70.f);

	pObject->SetName(L"My Player");
	pObject->FrustumCheck(false);
	pObject->Transform()->SetLocalPos(Vec3(0.f, 0.f, 0.f));
	pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
	pObject->Transform()->SetLocalRot(Vec3(0.f, XM_PI, 0.f));
	pObject->MeshRender()->SetDynamicShadow(true);

	pObject->AddComponent(new CVikingScript);
	pGameScene->AddGameObject(L"Player", pObject, false);

	vector<pair<int, int>> vecAnimIndcies;
	vecAnimIndcies.clear();
	vecAnimIndcies.reserve(7);
	vecAnimIndcies.push_back(make_pair(0, 60));
	vecAnimIndcies.push_back(make_pair(61, 93));	// WALK
	vecAnimIndcies.push_back(make_pair(94, 112));	// RUN
	vecAnimIndcies.push_back(make_pair(113, 160));
	vecAnimIndcies.push_back(make_pair(161, 230));
	vecAnimIndcies.push_back(make_pair(231, 250));
	vecAnimIndcies.push_back(make_pair(249, 250));
	vecAnimIndcies.push_back(make_pair(251, 307));


	pObject->Animator3D()->SetAnimFrameIndices(vecAnimIndcies);
	pObject->Status()->SetWalkSpeed(VIKING_WALK_SPEED);		// 이거 수정
	pObject->Status()->SetRunSpeed(VIKING_RUN_SPEED);
	pObject->Status()->SetState((int)VIKING_STATE::IDLE);
	pMainCam->GetScript<CPlayerCameraScript>()->SetPlayer(pObject);

	m_pCurPlayerObj = pObject;

	LightObj->GetScript<CPlayerLightScript>()->SetPlayer(pObject);


	// Test

	pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\QuestNPC_Y.fbx");
	pMeshData->Save(pMeshData->GetPath());

	//pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Viking.mdat", L"MeshData\\Viking.mdat");
	pObject = pMeshData->Instantiate();
	pObject->AddComponent(new CStatus);

	pObject->SetName(L"QuestNPC");
	pObject->Transform()->SetLocalPos(VEC3_QUESTNPC_POS);
	pObject->Transform()->SetLocalScale(Vec3(0.5f, 0.5f, 0.5f));
	pObject->Transform()->SetLocalRot(Vec3(4.65f, 0.f, 0.f));
	pObject->MeshRender()->SetDynamicShadow(true);

	pObject->AddComponent(new CQuestNPCScript);
	pGameScene->AddGameObject(L"NPC", pObject, false);

	vecAnimIndcies.clear();
	vecAnimIndcies.reserve(2);
	vecAnimIndcies.push_back(make_pair(0, 127));	// IDLE
	vecAnimIndcies.push_back(make_pair(128, 157));	// WALK

	pObject->Animator3D()->SetAnimFrameIndices(vecAnimIndcies);
	pObject->Status()->SetWalkSpeed(100.f);		// 이거 수정
	pObject->Status()->SetState((int)QUESTNPC_STATE::IDLE);



	// Test

	////pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\BlueMountainDragon.fbx");
	////pMeshData->Save(pMeshData->GetPath());
	//pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\BlueMountainDragon.mdat", L"MeshData\\BlueMountainDragon.mdat");
	//pObject = pMeshData->Instantiate();
	//pObject->AddComponent(new CStatus);
	//pObject->SetName(L"BlueMountainDragon");
	//pObject->Transform()->SetLocalPos(Vec3(0.f, 0.f, 0.f));
	//pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
	//pObject->Transform()->SetLocalRot(Vec3(0.f, XM_PI, 0.f));
	//pObject->MeshRender()->SetDynamicShadow(true);
	//pObject->AddComponent(new CBlueMountainDragonScript);
	//pGameScene->AddGameObject(L"NPC", pObject, false);
	//vecAnimIndcies.clear();
	//vecAnimIndcies.reserve(7);
	//vecAnimIndcies.push_back(make_pair(0, 180));
	//vecAnimIndcies.push_back(make_pair(181, 211));
	//vecAnimIndcies.push_back(make_pair(212, 236));
	//vecAnimIndcies.push_back(make_pair(237, 277));
	//vecAnimIndcies.push_back(make_pair(278, 351));
	//vecAnimIndcies.push_back(make_pair(352, 481));
	//vecAnimIndcies.push_back(make_pair(482, 590));
	//pObject->Animator3D()->SetAnimFrameIndices(vecAnimIndcies);
	//pObject->Status()->SetWalkSpeed(100.f);	
	//pObject->Status()->SetState((int)MOUNTAINDRAGON_STATE::IDLE);



	// MountainDragon Load
	//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\MountainDragon.fbx");
	//pMeshData->Save(pMeshData->GetPath());

	pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\MountainDragon.mdat", L"MeshData\\MountainDragon.mdat");
	m_pDragonPlayerObj = pMeshData->Instantiate();

	pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Viking.mdat", L"MeshData\\Viking.mdat");
	m_pVikingPlayerObj = pMeshData->Instantiate();





	/////////////////////////////////////////
	//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Barghest.fbx");
	//*Ptr<CMeshData>*/ pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Barghest.mdat", L"MeshData\\Barghest.mdat");
	//pObject = pMeshData->Instantiate();
	//pObject->AddComponent(new CStatus);
	//pObject->SetName(L"Barghest");
	//pObject->FrustumCheck(false);
	//pObject->Transform()->SetLocalPos(Vec3(-120.f, 0.f, 0.f));
	//pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
	//pObject->AddComponent(new CBarghestScript);
	//pGameScene->AddGameObject(L"Default", pObject, false);
	//pObject->MeshRender()->SetDynamicShadow(true);
	//vecAnimIndcies.clear();
	//vecAnimIndcies.reserve(5);
	//vecAnimIndcies.push_back(make_pair(0, 370));
	//vecAnimIndcies.push_back(make_pair(371, 400));
	//vecAnimIndcies.push_back(make_pair(402, 420));
	//vecAnimIndcies.push_back(make_pair(421, 450));
	//vecAnimIndcies.push_back(make_pair(451, 515));
	//pObject->Animator3D()->SetAnimFrameIndices(vecAnimIndcies);
	//pObject->Status()->SetWalkSpeed(200.f);
	//pObject->Status()->SetState((int)BARGHEST_STATE::IDLE);






	// Enviroment Object Generate
	LoadEnviroment();

	// ===================
	// Plane 오브젝트 생성
	// ===================
	LoadPlane();




	// ===================
	// Skybox 오브젝트 생성
	// =================== 
	pObject = new CGameObject;
	pObject->SetName(L"SkyBox");
	pObject->FrustumCheck(false);
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CMeshRender);

	// MeshRender 설정
	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
	pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"SkyboxMtrl"));
	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pSky01.GetPointer());

	// AddGameObject
	pGameScene->FindLayer(L"Default")->AddGameObject(pObject);


	
	CreateHPMPUI();


	// =================================
	// CollisionMgr 충돌 그룹(Layer) 지정
	// =================================
	// Player Layer 와 Monster Layer 는 충돌 검사 진행
	//CCollisionMgr::GetInst()->CheckCollisionLayer(L"Player", L"Monster");
	//CCollisionMgr::GetInst()->CheckCollisionLayer(L"Bullet", L"Monster");






	// 연기 이펙트 생성

	for (size_t i = 0; i < 3; i++)
	{
		pObject = new CGameObject;
		pObject->SetName(L"Particle");
		pObject->AddComponent(new CTransform);
		pObject->AddComponent(new CParticleSystem);
		pObject->FrustumCheck(false);
		Vec3 tempVec3 = m_pCurPlayerObj->Transform()->GetLocalPos();
		tempVec3.y += i * 100.f;
		pObject->Transform()->SetLocalPos(tempVec3);

		//pObject->Particlesystem()->SetParticleImage(L"Texture\\Particle\\smokeparticle.png", L"Texture\\Particle\\smokeparticle.png");
		pObject->Particlesystem()->SetMaintainTime(5.0f);
		pGameScene->FindLayer(L"Particle")->AddGameObject(pObject);
	}


	pGameScene->awake();
	pGameScene->start();
}

void CSceneMgr::ClearCurrentScene()
{
	m_pCurScene->Clear();
	SAFE_DELETE(m_pCurScene);
}

char* WCharToChar(const wchar_t* pwstrSrc)
{
#if !defined _DEBUG
	int len = 0;
	len = (wcslen(pwstrSrc) + 1) * 2;
	char* pstr = (char*)malloc(sizeof(char) * len);

	WideCharToMultiByte(949, 0, pwstrSrc, -1, pstr, len, NULL, NULL);
#else

	int nLen = wcslen(pwstrSrc);

	char* pstr = (char*)malloc(sizeof(char) * nLen + 1);
	wcstombs(pstr, pwstrSrc, nLen + 1);
#endif

	return pstr;
}

void CSceneMgr::update()
{
	Login();

	m_pCurScene->update();
	m_pCurScene->lateupdate();

	// RenderMgr 카메라 초기화
	CRenderMgr::GetInst()->ClearCamera();

	m_pCurScene->finalupdate();


	if (m_pCurScene->GetName() == L"Game Scene")
	{


		// 플레이어 오브젝트 변경
		if (m_pCurPlayerObj->GetScripts().back()->GetScriptType() == (UINT)SCRIPT_TYPE::VIKINGSCRIPT)
		{
			if (m_pCurPlayerObj->GetScript<CVikingScript>()->GetIsPlayerChange())
			{
				if (m_pCurPlayerObj->GetScript<CVikingScript>()->GetPlayerKind() == (int)PLAYER_KIND::VIKING)
				{
					m_pPlayerCamera->GetScript<CPlayerCameraScript>()->SetOffset(VEC3_PLAYERCAMERA_DRAGON_OFFSET);


					m_pCurPlayerObj->ChangeMeshData(L"MeshData\\MountainDragon.mdat", L"MeshData\\MountainDragon.mdat");
					m_pCurPlayerObj->GetScript<CVikingScript>()->SetPlayerKind((int)PLAYER_KIND::MOUNTAIN_DRAGON);
					m_pCurPlayerObj->Status()->SetState((int)MOUNTAINDRAGON_STATE::IDLE);

					m_pCurPlayerObj->GetScript<CVikingScript>()->SetPlayerChange(false);


					//// 연기 이펙트 생성

					//CGameObject* pObject = new CGameObject;
					//pObject->SetName(L"Particle");
					//pObject->AddComponent(new CTransform);
					//pObject->AddComponent(new CParticleSystem);

					//pObject->FrustumCheck(false);
					//Vec3 tempVec3 = m_pCurPlayerObj->Transform()->GetLocalPos();
					//tempVec3.y += 100.f;
					//pObject->Transform()->SetLocalPos(tempVec3);

					////pObject->Particlesystem()->SetParticleImage(L"Texture\\Particle\\smokeparticle.png", L"Texture\\Particle\\smokeparticle.png");
					//pObject->Particlesystem()->SetMaintainTime(5.0f);
					//m_pCurScene->FindLayer(L"Particle")->AddGameObject(pObject);

				}
				else if (m_pCurPlayerObj->GetScript<CVikingScript>()->GetPlayerKind() == (int)PLAYER_KIND::MOUNTAIN_DRAGON)
				{
					m_pPlayerCamera->GetScript<CPlayerCameraScript>()->SetOffset(VEC3_PLAYERCAMERA_VIKING_OFFSET);


					m_pCurPlayerObj->ChangeMeshData(L"MeshData\\Viking.mdat", L"MeshData\\Viking.mdat");
					m_pCurPlayerObj->GetScript<CVikingScript>()->SetPlayerKind((int)PLAYER_KIND::VIKING);
					m_pCurPlayerObj->Status()->SetState((int)VIKING_STATE::IDLE);

					m_pCurPlayerObj->GetScript<CVikingScript>()->SetPlayerChange(false);


					//// 연기 이펙트 생성

					//CGameObject* pObject = new CGameObject;
					//pObject->SetName(L"Particle");
					//pObject->AddComponent(new CTransform);
					//pObject->AddComponent(new CParticleSystem);

					//pObject->FrustumCheck(false);
					//Vec3 tempVec3 = m_pCurPlayerObj->Transform()->GetLocalPos();
					//tempVec3.y += 100.f;
					//pObject->Transform()->SetLocalPos(tempVec3);

					////pObject->Particlesystem()->SetParticleImage(L"Texture\\Particle\\smokeparticle.png", L"Texture\\Particle\\smokeparticle.png");
					//pObject->Particlesystem()->SetMaintainTime(5.0f);
					//m_pCurScene->FindLayer(L"Particle")->AddGameObject(pObject);

				}
			}
		}
	}
	// 충돌처리
	CCollisionMgr::GetInst()->update();
}

void CSceneMgr::Login()
{
	if (m_pCurScene->GetName() == L"Login Scene")
	{
		if (GetFontObj()->GetScript<CConstantStringScript>()->GetIsDone())
		{
			wstring name = GetFontObj()->GetScript<CConstantStringScript>()->GetID();
			wstring pw = GetFontObj()->GetScript<CConstantStringScript>()->GetPW();

			const wchar_t* n_from = name.c_str();
			const wchar_t* p_from = pw.c_str();

			char* n_to = WCharToChar(n_from);
			char* p_to = WCharToChar(p_from);

			//std::cout << to << std::endl;

			// 서버 로그인 
			//CCore::GetInst()->InitNetworkThread();
			CNetworkMgr::GetInst()->SendLoginPacket(n_to, p_to);

			ClearCurrentScene();
			initGameScene();
		}
	}
}


void CSceneMgr::FindGameObjectByTag(const wstring & _strTag, vector<CGameObject*>& _vecFindObj)
{
	for (int i = 0; i < MAX_LAYER; ++i)
	{
		const vector<CGameObject*>& vecObject = m_pCurScene->GetLayer(i)->GetObjects();
		for (size_t j = 0; j < vecObject.size(); ++j)
		{
			if (_strTag == vecObject[j]->GetName())
			{
				_vecFindObj.push_back(vecObject[j]);
			}
		}
	}
}

void CSceneMgr::Pick(int iScreenX, int iScreenY, HWND _hWnd, int ePickType, int eObjectKind, int eColliderKind, CGameObject** pGameObj, CGameObject** pCollider)
{
	// ===========================
	// 마우스 피킹을 위한 좌표계 변환
	// ===========================
	RECT rcRect = {};
	GetWindowRect(_hWnd, &rcRect);

	Vec3 vMousePos = {};

	Matrix MatProj = m_pCurScene->GetLayer(0)->GetParentObj().front()->Camera()->GetProjMat();
	Matrix MatInvProj = XMMatrixInverse(nullptr, MatProj);

	vMousePos.x = ((2.0f * iScreenX) / (rcRect.right - rcRect.left) - 1.0f);
	vMousePos.y = -((2.0f * iScreenY) / (rcRect.bottom - rcRect.top) - 1.0f);
	vMousePos.z = 0.f;

	// 투영좌표계로 변환
	vMousePos = XMVector3TransformCoord(vMousePos, MatInvProj);


	// 카메라 좌표가 Ray의 시작점
	Vec3 CameraPos = CSceneMgr::GetInst()->GetCurScene()->GetLayer(0)->GetParentObj().front()->Transform()->GetLocalPos();
	Vec3 vRayDir = { vMousePos }, vRayPos = { CameraPos };
	Matrix MatView = m_pCurScene->GetLayer(0)->GetParentObj().front()->Camera()->GetViewMat();
	wstring tempString = m_pCurScene->GetLayer(0)->GetParentObj().front()->GetName();
	Matrix MatInvView = XMMatrixInverse(nullptr, MatView);

	// 방향을 월드 좌표계로 변환
	vRayDir = XMVector3TransformNormal(vRayDir, MatInvView);

	// Ray 생성 및 초기화
	DirectX::SimpleMath::Ray ray = DirectX::SimpleMath::Ray();
	ray.position = vRayPos;
	ray.direction = vRayDir;
	ray.direction.Normalize();

	float fDist = 0.f;
	// 충돌하지 않는다면 리턴
	if (!ray.Intersects(m_vPlanePoint1, m_vPlanePoint2, m_vPlanePoint3, fDist))
	{
		return;
	}

	// 충돌 위치
	Vec3 vPickedPosition = ray.position + (fDist * ray.direction);


	// ===================
	// 피킹 종류에 따른 작업
	// ===================

	CGameObject* pObject = nullptr;
	Ptr<CTexture> pColor = CResMgr::GetInst()->FindRes<CTexture>(L"Tile");
	Ptr<CTexture> pNormal = CResMgr::GetInst()->FindRes<CTexture>(L"Tile_n");


	Ptr<CMeshData> pMeshData = nullptr;
	//Ptr<CMeshData> pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\House.mdat", L"MeshData\\monster.mdat");
	int a = 1;



	// 0 : not show
	// 1 : show
	int bColliderShow = 1;

	Vec3 vTempVec3 = { 1.f, 1.f, 1.f };

	switch (ePickType)
	{
	case (int)PICK_TYPE::CREATE:
		switch (eObjectKind)
		{
		case (int)OBJECT_KIND::BIGROCK:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\BigRock.fbx");
			//pMeshData->Save(pMeshData->GetPath());
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\BigRock.mdat", L"MeshData\\BigRock.mdat");



			pObject = pMeshData->Instantiate();
			pObject->SetName(L"bigrock");
			pObject->FrustumCheck(false);
			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));

			vTempVec3 = pObject->Transform()->GetLocalScale();
			m_pCurScene->AddGameObject(L"Enviroment", pObject, false);

			*pGameObj = pObject;

			pObject = new CGameObject;
			pObject->SetName(L"bigrock_collider");
			pObject->FrustumCheck(false);
			pObject->AddComponent(new CTransform);
			pObject->AddComponent(new CMeshRender);

			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(vTempVec3.x * 450.f, vTempVec3.y * 450.f, vTempVec3.z * 450.f));

			// MeshRender 설정
			switch (eColliderKind)
			{
			case (int)COLLIDER_KIND::SPHERE:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::BOX:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"BoxMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::EMPTY:
				break;
			default:
				break;
			}

			*pCollider = pObject;
			// AddGameObject
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pObject);


			break;
		case (int)OBJECT_KIND::GRAVE:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Grave.fbx");
			//pMeshData->Save(pMeshData->GetPath());
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Grave.mdat", L"MeshData\\Grave.mdat");

			pMeshData->Save(pMeshData->GetPath());

			pObject = pMeshData->Instantiate();
			pObject->SetName(L"grave");
			pObject->FrustumCheck(false);
			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			m_pCurScene->AddGameObject(L"Enviroment", pObject, false);
			vTempVec3 = pObject->Transform()->GetLocalScale();

			*pGameObj = pObject;


			pObject = new CGameObject;
			pObject->SetName(L"grave_collider");
			pObject->FrustumCheck(false);
			pObject->AddComponent(new CTransform);
			pObject->AddComponent(new CMeshRender);

			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(vTempVec3.x * 50.f, vTempVec3.y * 50.f, vTempVec3.z * 50.f));

			// MeshRender 설정
			switch (eColliderKind)
			{
			case (int)COLLIDER_KIND::SPHERE:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::BOX:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"BoxMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::EMPTY:
				break;
			default:
				break;
			}

			*pCollider = pObject;

			// AddGameObject
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pObject);

			break;
		case (int)OBJECT_KIND::HOUSE:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\House.fbx");
			//pMeshData->Save(pMeshData->GetPath());
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\House.mdat", L"MeshData\\House.mdat");

			pObject = pMeshData->Instantiate();
			pObject->SetName(L"house");
			pObject->FrustumCheck(false);
			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			m_pCurScene->AddGameObject(L"Enviroment", pObject, false);
			vTempVec3 = pObject->Transform()->GetLocalScale();

			*pGameObj = pObject;

			pObject = new CGameObject;
			pObject->SetName(L"house_collider");
			pObject->FrustumCheck(false);
			pObject->AddComponent(new CTransform);
			pObject->AddComponent(new CMeshRender);

			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(vTempVec3.x * 200.f, vTempVec3.y * 200.f, vTempVec3.z * 200.f));

			// MeshRender 설정
			switch (eColliderKind)
			{
			case (int)COLLIDER_KIND::SPHERE:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::BOX:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"BoxMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::EMPTY:
				break;
			default:
				assert(nullptr);
			}
			*pCollider = pObject;

			// AddGameObject
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pObject);

			break;
		case (int)OBJECT_KIND::HOUSE_RIGHT:
			*pGameObj = nullptr;
			*pCollider = nullptr;
			return;
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\house_right.fbx");
			//pMeshData->Save(pMeshData->GetPath());
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\house_right.mdat", L"MeshData\\house_right.mdat");
			pObject = pMeshData->Instantiate();
			pObject->SetName(L"house_right");
			pObject->FrustumCheck(false);
			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			m_pCurScene->AddGameObject(L"Enviroment", pObject, false);
			vTempVec3 = pObject->Transform()->GetLocalScale();

			*pGameObj = pObject;

			pObject = new CGameObject;
			pObject->SetName(L"house_right_collider");
			pObject->FrustumCheck(false);
			pObject->AddComponent(new CTransform);
			pObject->AddComponent(new CMeshRender);
			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(vTempVec3.x * 1000.f, vTempVec3.y * 1000.f, vTempVec3.z * 1000.f));

			// MeshRender 설정
			switch (eColliderKind)
			{
			case (int)COLLIDER_KIND::SPHERE:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::BOX:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"BoxMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::EMPTY:
				break;
			default:
				assert(nullptr);
			}
			*pCollider = pObject;

			// AddGameObject
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pObject);


			break;
		case (int)OBJECT_KIND::HOUSE_SMALL:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\house_small.fbx");
			//pMeshData->Save(pMeshData->GetPath());
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\house_small.mdat", L"MeshData\\house_small.mdat");

			pObject = pMeshData->Instantiate();
			pObject->SetName(L"house_small");
			pObject->FrustumCheck(false);
			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			m_pCurScene->AddGameObject(L"Enviroment", pObject, false);
			vTempVec3 = pObject->Transform()->GetLocalScale();

			*pGameObj = pObject;


			pObject = new CGameObject;
			pObject->SetName(L"house_small_collider");
			pObject->FrustumCheck(false);
			pObject->AddComponent(new CTransform);
			pObject->AddComponent(new CMeshRender);

			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(vTempVec3.x * 350.f, vTempVec3.y * 350.f, vTempVec3.z * 350.f));

			// MeshRender 설정
			switch (eColliderKind)
			{
			case (int)COLLIDER_KIND::SPHERE:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::BOX:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"BoxMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::EMPTY:
				break;
			default:
				assert(nullptr);
			}

			*pCollider = pObject;


			// AddGameObject
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pObject);

			break;
		case (int)OBJECT_KIND::ROCK:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\rock.fbx");
			//pMeshData->Save(pMeshData->GetPath());
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\rock.mdat", L"MeshData\\rock.mdat");

			pObject = pMeshData->Instantiate();
			pObject->SetName(L"rock");
			pObject->FrustumCheck(false);
			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			m_pCurScene->AddGameObject(L"Enviroment", pObject, false);
			vTempVec3 = pObject->Transform()->GetLocalScale();

			*pGameObj = pObject;


			pObject = new CGameObject;
			pObject->SetName(L"rock_collider");
			pObject->FrustumCheck(false);
			pObject->AddComponent(new CTransform);
			pObject->AddComponent(new CMeshRender);

			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(vTempVec3.x * 50.f, vTempVec3.y * 50.f, vTempVec3.z * 50.f));

			// MeshRender 설정
			switch (eColliderKind)
			{
			case (int)COLLIDER_KIND::SPHERE:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::BOX:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"BoxMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::EMPTY:
				break;
			default:
				assert(nullptr);
			}
			*pCollider = pObject;

			// AddGameObject
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pObject);
			break;
		case (int)OBJECT_KIND::HOUSE_LEFT:
			*pGameObj = nullptr;
			*pCollider = nullptr;
			return;
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\house_left.fbx");
			//pMeshData->Save(pMeshData->GetPath());
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\house_left.mdat", L"MeshData\\house_left.mdat");

			pObject = pMeshData->Instantiate();
			pObject->SetName(L"house_left");
			pObject->FrustumCheck(false);
			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			m_pCurScene->AddGameObject(L"Enviroment", pObject, false);

			vTempVec3 = pObject->Transform()->GetLocalScale();

			*pGameObj = pObject;


			pObject = new CGameObject;
			pObject->SetName(L"house_left_collider");
			pObject->FrustumCheck(false);
			pObject->AddComponent(new CTransform);
			pObject->AddComponent(new CMeshRender);

			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(vTempVec3.x * 100.f, vTempVec3.y * 100.f, vTempVec3.z * 100.f));

			// MeshRender 설정
			switch (eColliderKind)
			{
			case (int)COLLIDER_KIND::SPHERE:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::BOX:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"BoxMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::EMPTY:
				break;
			default:
				assert(nullptr);
			}

			*pCollider = pObject;

			// AddGameObject
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pObject);

			break;

		case (int)OBJECT_KIND::BRIDGE_BUILDING:
			*pGameObj = nullptr;
			*pCollider = nullptr;
			return;
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Bridge_Building.fbx");
			//pMeshData->Save(pMeshData->GetPath());
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Bridge_Building.mdat", L"MeshData\\Bridge_Building.mdat");

			pObject = pMeshData->Instantiate();
			pObject->SetName(L"bridge_building");
			pObject->FrustumCheck(false);
			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			vTempVec3 = pObject->Transform()->GetLocalScale();

			m_pCurScene->AddGameObject(L"Enviroment", pObject, false);
			*pGameObj = pObject;


			pObject = new CGameObject;
			pObject->SetName(L"bridge_building_collider");
			pObject->FrustumCheck(false);
			pObject->AddComponent(new CTransform);
			pObject->AddComponent(new CMeshRender);

			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(vTempVec3.x * 100.f, vTempVec3.y * 100.f, vTempVec3.z * 100.f));

			// MeshRender 설정
			switch (eColliderKind)
			{
			case (int)COLLIDER_KIND::SPHERE:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::BOX:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"BoxMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::EMPTY:
				break;
			default:
				assert(nullptr);
			}

			*pCollider = pObject;

			// AddGameObject
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pObject);

			break;
		case (int)OBJECT_KIND::DAMWALL:
			*pGameObj = nullptr;
			*pCollider = nullptr;
			return;

			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\DamWall.fbx");
			//pMeshData->Save(pMeshData->GetPath());
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\DamWall.mdat", L"MeshData\\DamWall.mdat");

			pObject = pMeshData->Instantiate();
			pObject->SetName(L"damwall");
			pObject->FrustumCheck(false);
			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			m_pCurScene->AddGameObject(L"Enviroment", pObject, false);
			vTempVec3 = pObject->Transform()->GetLocalScale();

			*pGameObj = pObject;

			pObject = new CGameObject;
			pObject->SetName(L"damwall_collider");
			pObject->FrustumCheck(false);
			pObject->AddComponent(new CTransform);
			pObject->AddComponent(new CMeshRender);

			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(10.f, 10.f, 10.f));

			// MeshRender 설정
			switch (eColliderKind)
			{
			case (int)COLLIDER_KIND::SPHERE:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::BOX:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"BoxMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::EMPTY:
				break;
			default:
				assert(nullptr);
			}
			*pCollider = pObject;

			// AddGameObject
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pObject);



			break;

		case (int)OBJECT_KIND::GATE:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Gate.fbx");
			//pMeshData->Save(pMeshData->GetPath());
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Gate.mdat", L"MeshData\\Gate.mdat");

			pObject = pMeshData->Instantiate();
			pObject->SetName(L"gate");
			pObject->FrustumCheck(false);
			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			m_pCurScene->AddGameObject(L"Enviroment", pObject, false);

			vTempVec3 = pObject->Transform()->GetLocalScale();

			*pGameObj = pObject;

			pObject = new CGameObject;
			pObject->SetName(L"gate_collider");
			pObject->FrustumCheck(false);
			pObject->AddComponent(new CTransform);
			pObject->AddComponent(new CMeshRender);

			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(vTempVec3.x * 150.f, vTempVec3.y * 150.f, vTempVec3.z * 150.f));

			// MeshRender 설정
			switch (eColliderKind)
			{
			case (int)COLLIDER_KIND::SPHERE:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::BOX:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"BoxMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::EMPTY:
				break;
			default:
				assert(nullptr);
			}

			*pCollider = pObject;

			// AddGameObject
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pObject);

			break;
		case (int)OBJECT_KIND::GRAVE_PILLAR:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Grave_Pillar.fbx");
			//pMeshData->Save(pMeshData->GetPath());
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Grave_Pillar.mdat", L"MeshData\\Grave_Pillar.mdat");
			pObject = pMeshData->Instantiate();
			pObject->SetName(L"grave_pillar");
			pObject->FrustumCheck(false);
			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			m_pCurScene->AddGameObject(L"Enviroment", pObject, false);
			*pGameObj = pObject;

			pObject = new CGameObject;
			pObject->SetName(L"grave_pillar_collider");
			pObject->FrustumCheck(false);
			pObject->AddComponent(new CTransform);
			pObject->AddComponent(new CMeshRender);
			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(vTempVec3.x * 100.f, vTempVec3.y * 100.f, vTempVec3.z * 100.f));

			// MeshRender 설정
			switch (eColliderKind)
			{
			case (int)COLLIDER_KIND::SPHERE:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::BOX:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"BoxMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::EMPTY:
				break;
			default:
				assert(nullptr);
			}

			*pCollider = pObject;

			// AddGameObject
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pObject);

			break;
		case (int)OBJECT_KIND::SHANTY:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Shanty.fbx");
			//pMeshData->Save(pMeshData->GetPath());
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Shanty.mdat", L"MeshData\\Shanty.mdat");

			pObject = pMeshData->Instantiate();
			pObject->SetName(L"shanty");
			pObject->FrustumCheck(false);
			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			m_pCurScene->AddGameObject(L"Enviroment", pObject, false);

			*pGameObj = pObject;

			pObject = new CGameObject;
			pObject->SetName(L"shanty_collider");
			pObject->FrustumCheck(false);
			pObject->AddComponent(new CTransform);
			pObject->AddComponent(new CMeshRender);

			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(vTempVec3.x * 350.f, vTempVec3.y * 350.f, vTempVec3.z * 350.f));

			// MeshRender 설정
			switch (eColliderKind)
			{
			case (int)COLLIDER_KIND::SPHERE:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::BOX:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"BoxMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::EMPTY:
				break;
			default:
				assert(nullptr);
			}

			*pCollider = pObject;

			// AddGameObject
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pObject);
			break;
		case (int)OBJECT_KIND::SHANTY_UP:
			//*pGameObj = nullptr;
			//*pCollider = nullptr;
			//return;
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Shanty_Up.fbx");
			//pMeshData->Save(pMeshData->GetPath());
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Shanty_Up.mdat", L"MeshData\\Shanty_Up.mdat");

			pObject = pMeshData->Instantiate();
			pObject->SetName(L"shanty_up");
			pObject->FrustumCheck(false);
			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			m_pCurScene->AddGameObject(L"Enviroment", pObject, false);

			*pGameObj = pObject;

			pObject = new CGameObject;
			pObject->SetName(L"shanty_up_collider");
			pObject->FrustumCheck(false);
			pObject->AddComponent(new CTransform);
			pObject->AddComponent(new CMeshRender);

			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(10.f, 10.f, 10.f));

			// MeshRender 설정
			switch (eColliderKind)
			{
			case (int)COLLIDER_KIND::SPHERE:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::BOX:
				pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"BoxMesh"));
				pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
				break;
			case (int)COLLIDER_KIND::EMPTY:
				break;
			default:
				assert(nullptr);
			}

			*pCollider = pObject;

			// AddGameObject
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pObject);
			break;
		case (int)OBJECT_KIND::PLANE_ROCK:
			*pGameObj = nullptr;
			*pCollider = nullptr;
			return;
			pObject = new CGameObject;
			pObject->SetName(L"plane_rock");
			pObject->AddComponent(new CTransform);
			pObject->AddComponent(new CMeshRender);



			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(1000.f, 1000.f, 1.f));
			pObject->Transform()->SetLocalRot(Vec3(XM_PI / 2.f, 0.f, 0.f));


			// MeshRender 설정
			pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
			pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"Std3DMtrl"));
			pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pColor.GetPointer());
			pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_1, pNormal.GetPointer());
			pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &a);
			m_pCurScene->AddGameObject(L"Enviroment", pObject, false);


			*pGameObj = pObject;

		case (int)OBJECT_KIND::SPRUCEA:
			pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\SpruceA.fbx");
			pMeshData->Save(pMeshData->GetPath());
			//pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\SpruceA.mdat", L"MeshData\\SpruceA.mdat");

			pObject = pMeshData->Instantiate();
			pObject->SetName(L"sprucea");
			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			m_pCurScene->AddGameObject(L"Enviroment", pObject, false);

			*pGameObj = pObject;


			*pCollider = nullptr;

			// AddGameObject
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pObject);
			break;

		case (int)OBJECT_KIND::PLAINS_GRASS:
			pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Plains_Grass.fbx");
			pMeshData->Save(pMeshData->GetPath());
			//pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Plains_Grass.mdat", L"MeshData\\Plains_Grass.mdat");

			pObject = pMeshData->Instantiate();
			pObject->SetName(L"plains_grass");
			pObject->Transform()->SetLocalPos(vPickedPosition);
			pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			m_pCurScene->AddGameObject(L"Enviroment", pObject, false);

			*pGameObj = pObject;


			*pCollider = nullptr;

			// AddGameObject
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pObject);
			break;
		default:
			assert(nullptr);
		}
		break;
	case (int)PICK_TYPE::REMOVE:
		break;
	case (int)PICK_TYPE::MODIFY:
		break;
	default:
		assert(nullptr);
	}

	m_pCurGameObject = *pGameObj;
	return;
}

void CSceneMgr::SetCurGameObjectPos(const float& fX, const float& fY, const float& fZ)
{
	m_pCurGameObject->Transform()->SetLocalPos(Vec3{ fX, fY, fZ });
}
void CSceneMgr::SetCurGameObjectRot(const float& fX, const float& fY, const float& fZ)
{
	m_pCurGameObject->Transform()->SetLocalRot(Vec3{ fX, fY, fZ });
}
void CSceneMgr::SetCurGameObjectScale(const float& fX, const float& fY, const float& fZ)
{
	m_pCurGameObject->Transform()->SetLocalScale(Vec3{ fX, fY, fZ });
}

void CSceneMgr::LoadEnviroment()
{
	//wstring strFileName = L"SurroundRock";
	wstring strFileName = L"HuntingGround4";

	wstring strName = L"Scene\\";
	strName += strFileName + L".dat";
	wstring strFullPath = CPathMgr::GetResPath();




	strFullPath += strName;

	LPCWSTR temp = strFullPath.c_str();
	HANDLE hFile = CreateFile(temp, GENERIC_READ, 0, 0,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

	// 잘못된 경로일 경우 리턴
	if (INVALID_HANDLE_VALUE == hFile)
		return;

	// 우리가 읽어들일때는 몇개의 파일이 어떤 자료형으로 있는지 모른다.
	float fValue = 0;
	DWORD dwByte = 0;


	CGameObject * pNewGameObj = nullptr;
	Ptr<CMeshData> pMeshData = nullptr;
	Ptr<CTexture> pColor = nullptr;
	Ptr<CTexture> pNormal = nullptr;
	int a = 1;
	Vec3 vVec3 = {};

	CGameObject * pColliderObject = nullptr;
	int bColliderShow = 1;

	while (1)
	{

		// 그러므로 우리는 사이즈 / 메모리 순서로 저장을 하거나 통일된 자료형(구조체)를
		// 사용하는 것이 맞다.

		// dwByte : 현재 
		ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);


		// 더 이상 읽을 것이 없다면 dwByte가 0이 된다.
		if (dwByte == 0)
		{
			break;
		}
		//pColliderObject = new CGameObject;

		switch ((int)fValue)
		{
		case (int)OBJECT_KIND::BIGROCK:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\BigRock.fbx");
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\BigRock.mdat", L"MeshData\\BigRock.mdat");

			pNewGameObj = pMeshData->Instantiate();
			pNewGameObj->SetName(L"bigrock");

			//pColliderObject->SetName(L"bigrock_collider");


			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pNewGameObj);
			pNewGameObj->MeshRender()->SetDynamicShadow(true);


			break;
		case (int)OBJECT_KIND::GRAVE:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Grave.fbx");
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Grave.mdat", L"MeshData\\Grave.mdat");

			pNewGameObj = pMeshData->Instantiate();
			pNewGameObj->SetName(L"grave");

			//pColliderObject->SetName(L"grave_collider");
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pNewGameObj);
			pNewGameObj->MeshRender()->SetDynamicShadow(true);

			break;
		case (int)OBJECT_KIND::HOUSE:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\house.fbx");
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\house.mdat", L"MeshData\\house.mdat");


			pNewGameObj = pMeshData->Instantiate();
			pNewGameObj->SetName(L"house");

			//pColliderObject->SetName(L"house_collider");
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pNewGameObj);
			pNewGameObj->MeshRender()->SetDynamicShadow(true);

			break;
		case (int)OBJECT_KIND::HOUSE_RIGHT:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\house_right.fbx");
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\house_right.mdat", L"MeshData\\house_right.mdat");

			pNewGameObj = pMeshData->Instantiate();
			pNewGameObj->SetName(L"house_right");

			//pColliderObject->SetName(L"grave_pillar_collider");
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pNewGameObj);
			pNewGameObj->MeshRender()->SetDynamicShadow(true);

			break;
		case (int)OBJECT_KIND::HOUSE_SMALL:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\house_small.fbx");
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\house_small.mdat", L"MeshData\\house_small.mdat");

			pNewGameObj = pMeshData->Instantiate();
			pNewGameObj->SetName(L"house_small");

			//pColliderObject->SetName(L"grave_pillar_collider");
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pNewGameObj);
			pNewGameObj->MeshRender()->SetDynamicShadow(true);

			break;
		case (int)OBJECT_KIND::ROCK:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\rock.fbx");
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\rock.mdat", L"MeshData\\rock.mdat");

			pNewGameObj = pMeshData->Instantiate();
			pNewGameObj->SetName(L"rock");

			//pColliderObject->SetName(L"grave_pillar_collider");
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pNewGameObj);
			pNewGameObj->MeshRender()->SetDynamicShadow(true);

			break;
		case (int)OBJECT_KIND::HOUSE_LEFT:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\house_left.fbx");
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\house_left.mdat", L"MeshData\\house_left.mdat");

			pNewGameObj = pMeshData->Instantiate();
			pNewGameObj->SetName(L"house_left");

			//pColliderObject->SetName(L"grave_pillar_collider");
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pNewGameObj);
			pNewGameObj->MeshRender()->SetDynamicShadow(true);

			break;

			// 추가

		//case (int)OBJECT_KIND::BRIDGE_BUILDING:
		//	//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Bridge_Building.fbx");
		//	pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Bridge_Building.mdat", L"MeshData\\Bridge_Building.mdat");
		//	pNewGameObj = pMeshData->Instantiate();
		//	pNewGameObj->SetName(L"bridge_building");
		//	break;
		case (int)OBJECT_KIND::DAMWALL:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\DamWall.fbx");
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\DamWall.mdat", L"MeshData\\DamWall.mdat");
			pNewGameObj = pMeshData->Instantiate();
			pNewGameObj->SetName(L"damwall");
			//pColliderObject->SetName(L"grave_pillar_collider");
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pNewGameObj);
			pNewGameObj->MeshRender()->SetDynamicShadow(true);

			break;
		case (int)OBJECT_KIND::GATE:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Gate.fbx");
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Gate.mdat", L"MeshData\\Gate.mdat");
			pNewGameObj = pMeshData->Instantiate();
			pNewGameObj->SetName(L"gate");
			//pColliderObject->SetName(L"grave_pillar_collider");
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pNewGameObj);
			pNewGameObj->MeshRender()->SetDynamicShadow(true);

			break;
		case (int)OBJECT_KIND::GRAVE_PILLAR:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Grave_Pillar.fbx");
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Grave_Pillar.mdat", L"MeshData\\Grave_Pillar.mdat");
			pNewGameObj = pMeshData->Instantiate();
			pNewGameObj->SetName(L"grave_pillar");
			//pColliderObject->SetName(L"grave_pillar_collider");
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pNewGameObj);
			pNewGameObj->MeshRender()->SetDynamicShadow(true);

			break;
		case (int)OBJECT_KIND::SHANTY:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Shanty.fbx");
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Shanty.mdat", L"MeshData\\Shanty.mdat");
			pNewGameObj = pMeshData->Instantiate();
			pNewGameObj->SetName(L"shanty");
			//pColliderObject->SetName(L"grave_pillar_collider");

			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pNewGameObj);
			pNewGameObj->MeshRender()->SetDynamicShadow(true);

			break;
		case (int)OBJECT_KIND::SHANTY_UP:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Shanty_Up.fbx");
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Shanty_Up.mdat", L"MeshData\\Shanty_Up.mdat");
			pNewGameObj = pMeshData->Instantiate();
			pNewGameObj->SetName(L"shanty_up");
			//pColliderObject->SetName(L"grave_pillar_collider");
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pNewGameObj);
			pNewGameObj->MeshRender()->SetDynamicShadow(true);

			break;
		case (int)OBJECT_KIND::BRIDGE_BUILDING:
		case (int)OBJECT_KIND::PLANE_ROCK:
			pColor = CResMgr::GetInst()->FindRes<CTexture>(L"Tile");
			pNormal = CResMgr::GetInst()->FindRes<CTexture>(L"Tile_n");


			pNewGameObj = new CGameObject;
			pNewGameObj->SetName(L"plane_rock");
			pNewGameObj->AddComponent(new CTransform);
			pNewGameObj->AddComponent(new CMeshRender);

			// MeshRender 설정
			pNewGameObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
			pNewGameObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"TexMtrl"));
			pNewGameObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pColor.GetPointer());
			pNewGameObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_1, pNormal.GetPointer());
			a = 1;
			pNewGameObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &a);

			//pColliderObject->SetName(L"grave_pillar_collider");

			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pNewGameObj);
			pNewGameObj->MeshRender()->SetDynamicShadow(true);

			break;
		case (int)OBJECT_KIND::PLAINS_GRASS:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Plains_Grass.fbx");
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Plains_Grass.mdat", L"MeshData\\Plains_Grass.mdat");
			pNewGameObj = pMeshData->Instantiate();
			pNewGameObj->SetName(L"plains_grass");
			//pColliderObject->SetName(L"grave_pillar_collider");
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pNewGameObj);
			//pNewGameObj->MeshRender()->SetDynamicShadow(true);

			break;

		case (int)OBJECT_KIND::SPRUCEA:
			//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\SpruceA.fbx");
			pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\SpruceA.mdat", L"MeshData\\SpruceA.mdat");
			pNewGameObj = pMeshData->Instantiate();
			pNewGameObj->SetName(L"sprucea");
			m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pNewGameObj);
			pNewGameObj->MeshRender()->SetDynamicShadow(true);

			break;
		case (int)OBJECT_KIND::BIGROCK_COLLIDER:
		case (int)OBJECT_KIND::BRIDGE_BUILDING_COLLIDER:
		case (int)OBJECT_KIND::DAMWALL_COLLIDER:
		case (int)OBJECT_KIND::GATE_COLLIDER:
		case (int)OBJECT_KIND::GRAVE_COLLIDER:
		case (int)OBJECT_KIND::GRAVE_PILLAR_COLLIDER:
		case (int)OBJECT_KIND::SHANTY_COLLIDER:
		case (int)OBJECT_KIND::SHANTY_UP_COLLIDER:
		case (int)OBJECT_KIND::HOUSE_COLLIDER:
		case (int)OBJECT_KIND::HOUSE_RIGHT_COLLIDER:
		case (int)OBJECT_KIND::HOUSE_LEFT_COLLIDER:
		case (int)OBJECT_KIND::HOUSE_SMALL_COLLIDER:
		case (int)OBJECT_KIND::ROCK_COLLIDER:
		case (int)OBJECT_KIND::TREE1_COLLIDER:
		case (int)OBJECT_KIND::PLANE_ROCK_COLLIDER:
			pNewGameObj = new CGameObject;
			pNewGameObj->SetName(L"static_collider");
			pNewGameObj->FrustumCheck(false);
			pNewGameObj->AddComponent(new CTransform);
			pNewGameObj->AddComponent(new CMeshRender);

			pNewGameObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
			pNewGameObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
			pNewGameObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);

			m_pCurScene->FindLayer(L"Enviroment_Collider")->AddGameObject(pNewGameObj);
			
			break;
		default:
			assert(nullptr);
			break;
		}



		//// MeshRender 설정
		//switch (eColliderKind)
		//{
		//case (int)COLLIDER_KIND::SPHERE:
		//	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
		//	pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
		//	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
		//	break;
		//case (int)COLLIDER_KIND::BOX:
		//	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"BoxMesh"));
		//	pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
		//	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);
		//	break;
		//case (int)COLLIDER_KIND::EMPTY:
		//	break;
		//default:
		//	break;
		//}



		ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
		vVec3.x = fValue;
		ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
		vVec3.y = fValue;
		ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
		vVec3.z = fValue;

		pNewGameObj->Transform()->SetLocalPos(vVec3);




		ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
		vVec3.x = fValue;
		ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
		vVec3.y = fValue;
		ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
		vVec3.z = fValue;

		pNewGameObj->Transform()->SetLocalRot(vVec3);

		ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
		vVec3.x = fValue;
		ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
		vVec3.y = fValue;
		ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
		vVec3.z = fValue;

		pNewGameObj->Transform()->SetLocalScale(vVec3);


		pNewGameObj->FrustumCheck(true);

		//pNewGameObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"PlaneMtrl"));

		// 더 이상 읽을 것이 없다면 dwByte가 0이 된다.
		if (dwByte == 0)
		{
			break;
		}



	}
}

void CSceneMgr::LoadPlane()
{
	//Ptr<CTexture> pColor = CResMgr::GetInst()->Load<CTexture>(L"Tile", L"Texture\\Tile\\TILE_03.tga");
	//Ptr<CTexture> pNormal = CResMgr::GetInst()->Load<CTexture>(L"Tile_n", L"Texture\\Tile\\TILE_03_N.tga");
	Ptr<CTexture> pColor = CResMgr::GetInst()->Load<CTexture>(L"Tile", L"Texture\\Tile\\grass_diffuse.jpg");
	Ptr<CTexture> pNormal = CResMgr::GetInst()->Load<CTexture>(L"Tile_n", L"Texture\\Tile\\grass_normal.jpg");

	CGameObject* pObject = nullptr;
	for (int i = 0; i < 20; ++i) {
		for (int j = 0; j < 20; ++j) {
			pObject = new CGameObject;
			pObject->SetName(L"Plane");
			pObject->AddComponent(new CTransform);
			pObject->AddComponent(new CMeshRender);
			float x = (float)i * 1000.f - 4000.f;
			float z = (float)j * 1000.f - 4000.f;
			// Transform 설정
			pObject->Transform()->SetLocalPos(Vec3(x, 1.f, z));
			pObject->Transform()->SetLocalScale(Vec3(1000.f, 1000.f, 1.f));
			pObject->Transform()->SetLocalRot(Vec3(XM_PI / 2.f, 0.f, 0.f));
			// MeshRender 설정
			pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
			pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"PlaneMtrl"));
			pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pColor.GetPointer());
			pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_1, pNormal.GetPointer());
			//int a = 1;
			//pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &a);
			CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"Enviroment")->AddGameObject(pObject);
			//pObject->MeshRender()->SetDynamicShadow(true);

		}
	}
}

void CSceneMgr::CreateLoadingSceneTexture()
{



	Ptr<CTexture> pTex = CResMgr::GetInst()->Load<CTexture>(L"Texture\\LoadingScene.png", L"Texture\\LoadingScene.png");

	tResolution res = CRenderMgr::GetInst()->GetResolution();

	CGameObject* pGameObject = new CGameObject;
	pGameObject->SetName(L"LoadingScene");
	pGameObject->AddComponent(new CTransform);
	pGameObject->Transform()->SetLocalScale(Vec3(res.fWidth, res.fHeight, 1.f));
	pGameObject->Transform()->SetLocalPos(Vec3(0.f, 0.f, 0.1f));

	pGameObject->AddComponent(new CMeshRender);
	pGameObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	pGameObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"PlaneMtrl"));

	pGameObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pTex.GetPointer());
	pGameObject->FrustumCheck(false);
	CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(pGameObject);






}