#include "stdafx.h"
#include "FakeLoginSceneScript.h"
#include "FontMgr.h"



CFakeLoginSceneScript::CFakeLoginSceneScript()
	: CScript((UINT)SCRIPT_TYPE::FAKELOGINSCRIPT)
	, m_bNext(false)
	, m_bDone(false)
	//, m_pOriginMtrl(nullptr)
	//, m_pCloneMtrl(nullptr)
{
}

CFakeLoginSceneScript::~CFakeLoginSceneScript()
{
}

void CFakeLoginSceneScript::awake()
{
	//m_pOriginMtrl = MeshRender()->GetSharedMaterial();
	//m_pCloneMtrl = m_pOriginMtrl->Clone();

	//int a = 1;
	//m_pCloneMtrl->SetData(SHADER_PARAM::INT_0, &a);
}

void CFakeLoginSceneScript::Init(Vec4 vFontColor, Vec4 vFontBackColor)
{
	m_vecIDFonts.reserve(20);
	m_vecPWFonts.reserve(20);


	//m_strValue = str;
	m_vFontColor = vFontColor;
	m_vFontBackColor = vFontBackColor;
}



void CFakeLoginSceneScript::update()
{
	Ptr<CTexture> pFontText = CFontMgr::GetInst()->GetFontTex();


	if (!m_bNext)
	{
		for (int i = 0; i < m_vecIDFonts.size(); ++i)
		{
			m_vecIDFonts[i]->SetDead();
		}
		m_vecIDFonts.clear();

		Vec3 vPos = Vec3(-120.f, -200.f, 1.f);
		Vec3 vScale = Vec3(20.f, 30.f, 1.f);

		//Vec3 vPos = Transform()->GetLocalPos();
		//Vec3 vScale = Transform()->GetLocalScale();

		if (KEY_AWAY(KEY_TYPE::KEY_0))
		{
			m_strID.push_back('0');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_1))
		{
			m_strID.push_back('1');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_2))
		{
			m_strID.push_back('2');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_3))
		{
			m_strID.push_back('3');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_4))
		{
			m_strID.push_back('4');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_5))
		{
			m_strID.push_back('5');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_6))
		{
			m_strID.push_back('6');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_7))
		{
			m_strID.push_back('7');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_8))
		{
			m_strID.push_back('8');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_9))
		{
			m_strID.push_back('9');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_L))
		{
			m_strID.push_back('.');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_RIGHT))
		{
			m_bNext = true;
		}



		for (int i = 0; i < m_strID.size(); ++i)
		{
			CGameObject * pFont = new CGameObject();

			pFont->AddComponent(new CTransform);
			pFont->AddComponent(new CMeshRender);
			pFont->Transform()->SetLocalPos(Vec3(vPos.x + (vScale.x * i), vPos.y, 1.f));
			pFont->Transform()->SetLocalScale(Vec3(vScale.x, vScale.y, 1.f));

			pFont->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
			pFont->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"FontMtrl"));
			pFont->MeshRender()->GetCloneMaterial()->SetData(SHADER_PARAM::TEX_0, pFontText.GetPointer());

			CharInfo tInfo = CFontMgr::GetInst()->GetFontInfo().mCharInfo[m_strID[i]];
			Ptr<CMaterial> pMtrl = pFont->MeshRender()->GetSharedMaterial();
			float sizeX = (float)CFontMgr::GetInst()->GetFontInfo().iScaleX;
			float sizeY = (float)CFontMgr::GetInst()->GetFontInfo().iScaleY;
			float startU = tInfo.ix / sizeX;
			float startV = tInfo.iy / sizeY;
			float widthU = tInfo.iWidth / sizeX;
			float HeightV = tInfo.iHeight / sizeY;

			pMtrl->SetData(SHADER_PARAM::FLOAT_0, &startU);
			pMtrl->SetData(SHADER_PARAM::FLOAT_1, &widthU);
			pMtrl->SetData(SHADER_PARAM::FLOAT_2, &startV);
			pMtrl->SetData(SHADER_PARAM::FLOAT_3, &HeightV);
			pMtrl->SetData(SHADER_PARAM::VEC4_0, &m_vFontColor);
			pMtrl->SetData(SHADER_PARAM::VEC4_1, &m_vFontBackColor);



			//AddChild(pFont);
			m_vecIDFonts.push_back(pFont);
			CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(pFont);
		}

	}
	else if (m_bNext)
	{
		for (int i = 0; i < m_vecPWFonts.size(); ++i)
		{
			m_vecPWFonts[i]->SetDead();
		}
		m_vecPWFonts.clear();

		Vec3 vPos = Vec3(-120.f, -200.f, 1.f);
		Vec3 vScale = Vec3(20.f, 30.f, 1.f);



		//vPos = Vec3(Transform()->GetLocalPos().x, Transform()->GetLocalPos().y - 65, Transform()->GetLocalPos().z);
		vPos = Vec3(vPos.x, vPos.y - 65, vPos.z);
		//vScale = Transform()->GetLocalScale();



		if (KEY_AWAY(KEY_TYPE::KEY_0))
		{
			m_strPW.push_back('0');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_1))
		{
			m_strPW.push_back('1');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_2))
		{
			m_strPW.push_back('2');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_3))
		{
			m_strPW.push_back('3');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_4))
		{
			m_strPW.push_back('4');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_5))
		{
			m_strPW.push_back('5');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_6))
		{
			m_strPW.push_back('6');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_7))
		{
			m_strPW.push_back('7');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_8))
		{
			m_strPW.push_back('8');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_9))
		{
			m_strPW.push_back('9');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_L))
		{
			m_strPW.push_back('.');
		}
		if (KEY_AWAY(KEY_TYPE::KEY_RIGHT))
		{
			m_bDone = true;

			for (size_t i = 0; i < m_vecIDFonts.size(); ++i)
			{
				m_vecIDFonts[i]->SetDead();
				m_vecIDFonts[i] = nullptr;
			}
			m_vecIDFonts.clear();


			for (size_t i = 0; i < m_vecPWFonts.size(); ++i)
			{
				m_vecPWFonts[i]->SetDead();
				m_vecPWFonts[i] = nullptr;
			}
			m_vecPWFonts.clear();
			



			this->SetDead();
			return;
		}

		for (int i = 0; i < m_strPW.size(); ++i)
		{
			CGameObject * pFont = new CGameObject();

			pFont->AddComponent(new CTransform);
			pFont->AddComponent(new CMeshRender);
			pFont->Transform()->SetLocalPos(Vec3(vPos.x + (vScale.x * i), vPos.y, 1.f));
			pFont->Transform()->SetLocalScale(Vec3(vScale.x, vScale.y, 1.f));

			pFont->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
			pFont->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"FontMtrl"));
			pFont->MeshRender()->GetCloneMaterial()->SetData(SHADER_PARAM::TEX_0, pFontText.GetPointer());

			CharInfo tInfo = CFontMgr::GetInst()->GetFontInfo().mCharInfo[m_strPW[i]];
			Ptr<CMaterial> pMtrl = pFont->MeshRender()->GetSharedMaterial();
			float sizeX = (float)CFontMgr::GetInst()->GetFontInfo().iScaleX;
			float sizeY = (float)CFontMgr::GetInst()->GetFontInfo().iScaleY;
			float startU = tInfo.ix / sizeX;
			float startV = tInfo.iy / sizeY;
			float widthU = tInfo.iWidth / sizeX;
			float HeightV = tInfo.iHeight / sizeY;

			pMtrl->SetData(SHADER_PARAM::FLOAT_0, &startU);
			pMtrl->SetData(SHADER_PARAM::FLOAT_1, &widthU);
			pMtrl->SetData(SHADER_PARAM::FLOAT_2, &startV);
			pMtrl->SetData(SHADER_PARAM::FLOAT_3, &HeightV);
			pMtrl->SetData(SHADER_PARAM::VEC4_0, &m_vFontColor);
			pMtrl->SetData(SHADER_PARAM::VEC4_1, &m_vFontBackColor);



			//AddChild(pFont);
			m_vecPWFonts.push_back(pFont);
			CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(pFont);
		}
	}



}


