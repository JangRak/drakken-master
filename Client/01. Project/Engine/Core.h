#pragma once

#include <thread>

class CCore
{
	SINGLE(CCore);
public:
	HWND m_hMainHwnd;

public:
	int init(HWND _hWnd, const tResolution& _resolution, bool _bWindow);
	void progress();

	void InitNetworkThread();
	std::thread CreateNetworkThread() { return std::thread([this] { this->DoNetwork();  }); }
	void DoNetwork();
	
private:
	void ChangeWindowSize(HWND _hWnd, const tResolution& _resolution);

private:
	std::thread m_networkThread;

	std::chrono::time_point<std::chrono::system_clock> m_prevTime;
	std::chrono::duration<float> m_elapsedTime;
};

