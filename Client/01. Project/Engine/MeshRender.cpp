#include "stdafx.h"
#include "MeshRender.h"

#include "Transform.h"
#include "ResMgr.h"
#include "Animator3D.h"
CMeshRender::CMeshRender()
	: CComponent(COMPONENT_TYPE::MESHRENDER)
	, m_bDynamicShadow(false)
{
	m_vecMtrl.resize(1);
}

CMeshRender::~CMeshRender()
{

}
void CMeshRender::render()
{
	if (IsActive() == false || nullptr == m_pMesh)
		return;

	int a = 1;
	for (size_t i = 0; i < m_vecMtrl.size(); ++i)
	{
		if (nullptr == m_vecMtrl[i] || nullptr == m_vecMtrl[i]->GetShader())
			continue;

		// Transform 정보 업데이트
		Transform()->UpdateData();

		// Animator3D 컴포넌트가 있는 경우...
		if (Animator3D())
		{
			Animator3D()->UpdateData();
			a = 1;
			m_vecMtrl[i]->SetData(SHADER_PARAM::INT_0, &a); // Animation Mesh 알리기
		}

		m_vecMtrl[i]->UpdateData();
		m_pMesh->render((UINT)i);

		a = 0;
		m_vecMtrl[i]->SetData(SHADER_PARAM::INT_0, &a); // Animation Mesh 알리기
	}
}


void CMeshRender::render(UINT _iMtrlIdx)
{
	if (IsActive() == false || nullptr == m_pMesh)
		return;

	int a = 1;

	if (nullptr == m_vecMtrl[_iMtrlIdx] || nullptr == m_vecMtrl[_iMtrlIdx]->GetShader())
		return;

	// Transform 정보 업데이트
	Transform()->UpdateData();

	// Animator3D 컴포넌트가 있는 경우...
	if (Animator3D())
	{
		Animator3D()->UpdateData();
		Animator3D()->GetFinalBoneMat()->UpdateData(TEXTURE_REGISTER::t7); // t7 레지스터에 최종행렬 데이터(구조버퍼) 바인딩

		a = 1;
		m_vecMtrl[_iMtrlIdx]->SetData(SHADER_PARAM::INT_0, &a); // Animation Mesh 알리기
	}

	m_vecMtrl[_iMtrlIdx]->UpdateData();
	m_pMesh->render((UINT)_iMtrlIdx);

	a = 0;
	m_vecMtrl[_iMtrlIdx]->SetData(SHADER_PARAM::INT_0, &a); // Animation Mesh 알리기
}

void CMeshRender::render_shadowmap()
{
	int a = 1;
	Ptr<CMaterial> pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"ShadowMapMtrl");

	for (UINT i = 0; i < m_pMesh->GetSubsetCount(); ++i)
	{
		if (Animator3D())
		{
			Animator3D()->UpdateData();
			pMtrl->SetData(SHADER_PARAM::INT_0, &a);	// Animation Mesh임을 알리기
		}

		Transform()->UpdateData();
		pMtrl->UpdateData();
		m_pMesh->render(i);
	}

	// 정리
	if (Animator3D())
	{
		a = 0;
		pMtrl->SetData(SHADER_PARAM::INT_0, &a);
	}
}

Ptr<CMaterial> CMeshRender::GetCloneMaterial(UINT _iSubSet)
{
	if (nullptr == m_vecMtrl[_iSubSet])
	{
		return nullptr;
	}
	m_vecMtrl[_iSubSet] = m_vecMtrl[_iSubSet]->Clone();
	return m_vecMtrl[_iSubSet];
}

void CMeshRender::SetMaterial(Ptr<CMaterial> _pMtrl, UINT _iSubSet)
{
	if (m_vecMtrl.size() <= (size_t)_iSubSet)
	{
		m_vecMtrl.resize(_iSubSet + 1);
	}

	m_vecMtrl[_iSubSet] = _pMtrl;
}

ULONG64 CMeshRender::GetInstID(UINT _iMtrlIdx)
{
	if (m_pMesh == NULL || m_vecMtrl[_iMtrlIdx] == NULL)
		return 0;

	uInstID id{ m_pMesh->GetID(), (WORD)m_vecMtrl[_iMtrlIdx]->GetID(), (WORD)_iMtrlIdx };
	return id.llID;
}

void CMeshRender::DeleteMesh()
{
	if (nullptr != m_pMesh.GetPointer())
	{
		delete m_pMesh.GetPointer();
		m_pMesh = nullptr;
	}
	else
		assert(nullptr);
}

void CMeshRender::DeleteMaterial()
{
	for (size_t i = 0; i < m_vecMtrl.size(); ++i)
	{
		if (nullptr != m_vecMtrl[i].GetPointer())
		{
			delete m_vecMtrl[i].GetPointer();
			m_vecMtrl[i] = nullptr;
		}
		else
			assert(nullptr);
	}
	m_vecMtrl.clear();

}

void CMeshRender::MakeMeshNull()
{
	if (nullptr != m_pMesh.GetPointer())
	{
		m_pMesh = nullptr;
	}
	else
		assert(nullptr);
}

void CMeshRender::MakeMaterialNull()
{
	for (size_t i = 0; i < m_vecMtrl.size(); ++i)
	{
		if (nullptr != m_vecMtrl[i].GetPointer())
		{
			m_vecMtrl[i] = nullptr;
		}
		else
			assert(nullptr);
	}
	m_vecMtrl.clear();
}








void CMeshRender::SaveToScene(FILE * _pFile)
{
	//UINT iType = (UINT)GetComponentType();
	//fwrite(&iType, sizeof(UINT), 1, _pFile);

	//// 존재여부 저장(nullptr 인지 아닌지)
	//fwrite(&m_pMesh, sizeof(void*), 1, _pFile);

	//if (nullptr != m_pMesh)
	//{
	//	SaveWString(_pFile, m_pMesh->GetName());
	//	SaveWString(_pFile, m_pMesh->GetPath());
	//}

	//// 존재여부 저장(nullptr 인지 아닌지)
	//fwrite(&m_pMtrl, sizeof(void*), 1, _pFile);

	//if (nullptr != m_pMtrl)
	//{
	//	SaveWString(_pFile, m_pMtrl->GetName());
	//	SaveWString(_pFile, m_pMtrl->GetPath());
	//}
}

void CMeshRender::LoadFromScene(FILE * _pFile)
{
	//void* pData = nullptr;
	//fread(&pData, sizeof(void*), 1, _pFile);

	//if (pData)
	//{
	//	wstring strMesh = LoadWString(_pFile);
	//	wstring strPath = LoadWString(_pFile);
	//	m_pMesh = CResMgr::GetInst()->FindRes<CMesh>(strMesh);

	//	if (nullptr == m_pMesh)
	//	{
	//		m_pMesh = CResMgr::GetInst()->Load<CMesh>(strMesh, strPath);
	//	}
	//}

	//fread(&pData, sizeof(void*), 1, _pFile);

	//if (pData)
	//{
	//	wstring strMtrl = LoadWString(_pFile);
	//	wstring strPath = LoadWString(_pFile);
	//	m_pMtrl = CResMgr::GetInst()->FindRes<CMaterial>(strMtrl);

	//	if (nullptr == m_pMtrl)
	//	{
	//		m_pMtrl = CResMgr::GetInst()->Load<CMaterial>(strMtrl, strPath);
	//	}
	//}
}
