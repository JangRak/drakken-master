#pragma once
#include "Script.h"

#define VEC3_PLAYERCAMERA_VIKING_OFFSET Vec3(0.f, 150.f, 200.f)
#define VEC3_PLAYERCAMERA_DRAGON_OFFSET Vec3(0.f, 300.f, 300.f)


class CGameObject;
class CPlayerCameraScript :
	public CScript
{
private:
	float			m_fSpeed;
	float			m_fScaleSpeed;
	CGameObject*	m_pPlayerObj;
	Vec3			m_vOffset;
public:
	virtual void update();

public:
	void SetOffset(Vec3 _vec3) { m_vOffset = _vec3; }
	Vec3 Getoffset() { return m_vOffset; }

public:
	void SetPlayer(CGameObject* _pPlayer) { m_pPlayerObj = _pPlayer; }
	const Vec3 GetOffset() const { return m_vOffset; }
public:
	CLONE(CPlayerCameraScript);

public:
	CPlayerCameraScript();
	virtual ~CPlayerCameraScript();
};