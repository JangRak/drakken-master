#include "stdafx.h"
#include "Core.h"

#include "NetworkMgr.h"

#include "Device.h"
#include "KeyMgr.h"
#include "TimeMgr.h"
#include "ResMgr.h"

#include "SceneMgr.h"
#include "EventMgr.h"
#include "RenderMgr.h"
#include "FontMgr.h"
#include "InstancingMgr.h"

#include "PathMgr.h"
#include "ConstantBuffer.h"


CCore::CCore() 
	: m_hMainHwnd(nullptr)
{
}

CCore::~CCore()
{
}

int CCore::init(HWND _hWnd, const tResolution & _resolution, bool _bWindow)
{
	m_prevTime = std::chrono::system_clock::now();

	m_hMainHwnd = _hWnd;
	ChangeWindowSize(m_hMainHwnd, _resolution);
	ShowWindow(_hWnd, true);

	if (FAILED(CDevice::GetInst()->init(_hWnd, _resolution, _bWindow)))
	{
		return E_FAIL;
	}
	CRenderMgr::GetInst()->init(_hWnd, _resolution, _bWindow);

	// 상수버퍼 만들기
	CDevice::GetInst()->CreateConstBuffer(L"TRANSFORM_MATRIX", sizeof(tTransform), 1024, CONST_REGISTER::b0);
	CDevice::GetInst()->CreateConstBuffer(L"MATERIAL_PARAM", sizeof(tMtrlParam), 1024, CONST_REGISTER::b1);
	CDevice::GetInst()->CreateConstBuffer(L"ANIM2D", sizeof(tMtrlParam), 1024, CONST_REGISTER::b2);

	// 전역 상수버퍼 변수(1프레임 동안 레지스터에서 지속되야함)
	CDevice::GetInst()->CreateConstBuffer(L"LIGHT2D", sizeof(tLight2DInfo), 1, CONST_REGISTER::b3, true);
	CDevice::GetInst()->CreateConstBuffer(L"LIGHT3D", sizeof(tLight3DInfo), 1, CONST_REGISTER::b4, true);
	CDevice::GetInst()->CreateConstBuffer(L"GLOBAL VALUE", sizeof(tGlobalValue), 1, CONST_REGISTER::b5);

	// 전역 상수버퍼 등록
	CDevice::GetInst()->SetGlobalConstBufferToRegister(CDevice::GetInst()->GetCB(CONST_REGISTER::b3), 0);
	CDevice::GetInst()->SetGlobalConstBufferToRegister(CDevice::GetInst()->GetCB(CONST_REGISTER::b4), 0);
	CDevice::GetInst()->SetGlobalConstBufferToRegister(CDevice::GetInst()->GetCB(CONST_REGISTER::b5), 0);

	// InstancingBuffer 초기화
	CInstancingMgr::GetInst()->init();

	// 매니저 초기화
	CPathMgr::init();
	CKeyMgr::GetInst()->init();
	CTimeMgr::GetInst()->init();

	CResMgr::GetInst()->init();
	CFontMgr::GetInst()->init();

	CSceneMgr::GetInst()->initLoginScene();

	return S_OK;
}

void CCore::ChangeWindowSize(HWND _hWnd, const tResolution & _resolution)
{
	RECT rt = { 0, 0, (int)_resolution.fWidth, (int)_resolution.fHeight };

	AdjustWindowRect(&rt, WS_OVERLAPPEDWINDOW, false);
	SetWindowPos(_hWnd, nullptr, 10, 10, rt.right - rt.left, rt.bottom - rt.top, 0);
}

void CCore::progress()
{
	// 위치 패킷 전송
	std::chrono::time_point<std::chrono::system_clock> curTime
		= std::chrono::system_clock::now();

	m_elapsedTime = curTime - m_prevTime;
	if (m_elapsedTime.count() > 0.2f) {
		if (true == CNetworkMgr::GetInst()->GetReady()) {
			m_prevTime = curTime;
			//CNetworkMgr::GetInst()->SendMousePacket();
			CNetworkMgr::GetInst()->SendPosPacket();
		}
	}

	// 매니저 업데이트
	CKeyMgr::GetInst()->update();
	CTimeMgr::GetInst()->update();
	CEventMgr::GetInst()->clear();
	{
		CSceneMgr::GetInst()->update();
		CRenderMgr::GetInst()->render();
	}
	CEventMgr::GetInst()->update();

	// 인스턴싱 버퍼 클리어
	CInstancingMgr::GetInst()->clear();

}

void CCore::InitNetworkThread()
{
	m_networkThread = CreateNetworkThread();
}

void CCore::DoNetwork()
{
	CNetworkMgr::GetInst()->DoNetwork();
}