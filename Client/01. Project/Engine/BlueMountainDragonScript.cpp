#include "stdafx.h"
#include "BlueMountainDragonScript.h"
#include "Animator3D.h"
#include "Status.h"
CBlueMountainDragonScript::CBlueMountainDragonScript()
	: CScript((UINT)SCRIPT_TYPE::BLUEMOUNTAINDRAGONSCRIPT)
{
}

CBlueMountainDragonScript::~CBlueMountainDragonScript()
{
}

void CBlueMountainDragonScript::awake()
{
}

void CBlueMountainDragonScript::update()
{
	Vec3 vPos = Transform()->GetLocalPos();
	Vec3 vRot = Transform()->GetLocalRot();

	Matrix matRot = XMMatrixRotationX(vRot.x);
	matRot *= XMMatrixRotationY(vRot.y);
	matRot *= XMMatrixRotationZ(vRot.z);

	Vec3 vLook = XMVector3TransformNormal(Vec3::Front, matRot);
	Vec3 vRight = XMVector3TransformNormal(Vec3::Right, matRot);
	Vec3 vUp = XMVector3TransformNormal(Vec3::Up, matRot);

	MOUNTAINDRAGON_STATE tempState = (MOUNTAINDRAGON_STATE)Status()->GetState();
	if (GetIsDying())
	{
		tempState = MOUNTAINDRAGON_STATE::DEATH;
	}
	Status()->SetState((int)tempState);

	if (MOUNTAINDRAGON_STATE::DEATH == ((MOUNTAINDRAGON_STATE)Status()->GetState()))
	{
		if (Animator3D()->GetIsEnd())
		{
			SetDead();
		}
	}

	Vec3 dir = Transform()->GetLook();

	if (MOUNTAINDRAGON_STATE::WALK == ((MOUNTAINDRAGON_STATE)Status()->GetState())) {
		vPos.x += DT * Status()->GetWalkSpeed() * dir.x;
		vPos.y += DT * Status()->GetWalkSpeed() * dir.y;
		vPos.z += DT * Status()->GetWalkSpeed() * dir.z;
	}

	else if (MOUNTAINDRAGON_STATE::RUN == ((MOUNTAINDRAGON_STATE)Status()->GetState())) {
		vPos.x += DT * Status()->GetRunSpeed() * dir.x;
		vPos.y += DT * Status()->GetRunSpeed() * dir.y;
		vPos.z += DT * Status()->GetRunSpeed() * dir.z;
	}

	Transform()->SetLocalPos(vPos);


	Animator3D()->SetCurAnimIdx(Status()->GetState());
}
