#include "stdafx.h"
#include "PlayerCameraScript.h"
#include "GameObject.h"
#include "Camera.h"
Vec3 Add(const Vec3& xmfVector1, const Vec3& xmfVector2, float fScalar)
{
	Vec3 xmf3Result;
	XMStoreFloat3(&xmf3Result, XMLoadFloat3(&xmfVector1) + (XMLoadFloat3)(&xmfVector2) * fScalar);
	return xmf3Result;
}

CPlayerCameraScript::CPlayerCameraScript()
	: CScript(0)
	, m_fSpeed(200.f)
	, m_fScaleSpeed(1.f)
{
	m_vOffset = VEC3_PLAYERCAMERA_VIKING_OFFSET;
}

CPlayerCameraScript::~CPlayerCameraScript()
{
}

void CPlayerCameraScript::update()
{
	Vec3 vPlayerPos = m_pPlayerObj->Transform()->GetLocalPos();
	Vec3 vPlayerRot = m_pPlayerObj->Transform()->GetLocalRot();
	Vec3 vPos = Transform()->GetLocalPos();

	Matrix matRot = XMMatrixRotationX(vPlayerRot.x);
	matRot *= XMMatrixRotationY(vPlayerRot.y);
	matRot *= XMMatrixRotationZ(vPlayerRot.z);

	Vec3 vPlayerLook = XMVector3TransformNormal(Vec3::Front, matRot);
	Vec3 vPlayerRight = XMVector3TransformNormal(Vec3::Right, matRot);
	Vec3 vPlayerUp = XMVector3TransformNormal(Vec3::Up, matRot);


	//Vec3 vLook = Transform()->GetWorldDir(DIR_TYPE::FRONT);
	//float fScale = Camera()->GetScale();
	//float fSpeed = m_fSpeed;

	vPos = vPlayerPos;

	Vec2 vDrag = CKeyMgr::GetInst()->GetDragDir();
	Vec3 vRot = Transform()->GetLocalRot();

	//vRot.x -= vDrag.y * DT * 3.f;
	//vRot.y += vDrag.x * DT * 1.5f;
	vRot.x -= vDrag.y * DT * 3.f;
	vRot.y += vDrag.x * DT * 1.5f;


	vPlayerRot.y += vDrag.x * DT * 1.5f;

	m_pPlayerObj->Transform()->SetLocalRot(vPlayerRot);

	matRot = {};
	matRot = XMMatrixRotationX(vPlayerRot.x);
	matRot *= XMMatrixRotationY(vPlayerRot.y);
	matRot *= XMMatrixRotationZ(vPlayerRot.z);

	vPlayerLook = XMVector3TransformNormal(Vec3::Front, matRot);
	vPlayerUp = XMVector3TransformNormal(Vec3::Up, matRot);



	Vec3 vPlayerToCamDir = -vPlayerLook + vPlayerUp;
	vPlayerToCamDir = vPlayerToCamDir.Normalize();


	vPos.x += m_vOffset.x * (vPlayerToCamDir.x);
	vPos.y += m_vOffset.y * (vPlayerToCamDir.y);
	vPos.z += m_vOffset.z * (vPlayerToCamDir.z);
	//vPos += m_vOffset;


	Transform()->SetLocalRot(vRot);
	Transform()->SetLocalPos(vPos);


	/////////////////////////////////////////////





	// 카메라 위치 

	Matrix mRotate = XMMatrixIdentity();
	Vec3 vRight = m_pPlayerObj->Transform()->GetLocalDir(DIR_TYPE::RIGHT);
	Vec3 vUp = m_pPlayerObj->Transform()->GetLocalDir(DIR_TYPE::UP);
	Vec3 vLook = m_pPlayerObj->Transform()->GetLocalDir(DIR_TYPE::FRONT);
	mRotate._11 = vRight.x; mRotate._21 = vUp.x; mRotate._31 = vLook.x;
	mRotate._12 = vRight.y; mRotate._22 = vUp.y; mRotate._32 = vLook.y;
	mRotate._13 = vRight.z; mRotate._23 = vUp.z; mRotate._33 = vLook.z;

	Vec3::Distance(Vec3{ 0, 0, 0 }, Vec3{ 4, 0, 0 });

	Vec3 vOffset = XMVector3TransformCoord(m_vOffset, mRotate);
	Vec3 vPosition = vPlayerPos + vOffset;
	Vec3 vDirection = vPosition - vPos;
	float fLength = vDirection.Length();
	vDirection = vDirection.Normalize();
	//float fTimeLagScale = (m_fTimeLag) ? fTimeElapsed * (1.0f / m_fTimeLag) : 1.0f;
	float fDistance = fLength/* * fTimeLagScale*/;
	if (fDistance > fLength) fDistance = fLength;
	if (fLength < 0.01f) fDistance = fLength;
	if (fDistance > 0)
	{
		vPos = Add(vPos, vDirection, fDistance);
	}
	Transform()->SetLocalPos(vPos);
}
