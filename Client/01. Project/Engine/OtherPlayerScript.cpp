#include "stdafx.h"
#include "OtherPlayerScript.h"
#include "Animator3D.h"
#include "Status.h"
COtherPlayerScript::COtherPlayerScript()
	: CScript((UINT)SCRIPT_TYPE::OTHERPLAYERSCRIPT)
	, m_pOriginMtrl(nullptr)
	, m_pCloneMtrl(nullptr)
{
}

COtherPlayerScript::~COtherPlayerScript()
{
}

void COtherPlayerScript::awake()
{
	m_pOriginMtrl = MeshRender()->GetSharedMaterial();
	m_pCloneMtrl = m_pOriginMtrl->Clone();

	int a = 1;
	m_pCloneMtrl->SetData(SHADER_PARAM::INT_0, &a);
}

void COtherPlayerScript::update()
{
	Vec3 vPos = Transform()->GetLocalPos();
	Vec3 vRot = Transform()->GetLocalRot();

	Matrix matRot = XMMatrixRotationX(vRot.x);
	matRot *= XMMatrixRotationY(vRot.y);
	matRot *= XMMatrixRotationZ(vRot.z);

	Vec3 vLook = XMVector3TransformNormal(Vec3::Front, matRot);
	Vec3 vRight = XMVector3TransformNormal(Vec3::Right, matRot);
	Vec3 vUp = XMVector3TransformNormal(Vec3::Up, matRot);

	//VIKING_STATE tempState = (VIKING_STATE)Status()->GetState();
	//if (GetIsDying())
	//{
	//	tempState = VIKING_STATE::DEATH;
	//}
	//Status()->SetState((int)tempState);

	//if (VIKING_STATE::DEATH == ((VIKING_STATE)Status()->GetState()))
	//{
	//	if (Animator3D()->GetIsEnd())
	//	{
	//		SetDead();
	//	}
	//}

	Vec3 dir = Transform()->GetLook();

	if (VIKING_STATE::WALK == ((VIKING_STATE)Status()->GetState()))
	{
		vPos.x -= DT * Status()->GetWalkSpeed() * dir.x;
		vPos.y -= DT * Status()->GetWalkSpeed() * dir.y;
		vPos.z -= DT * Status()->GetWalkSpeed() * dir.z;
	}
	else if (VIKING_STATE::RUN == ((VIKING_STATE)Status()->GetState())) {
		vPos.x -= DT * Status()->GetRunSpeed() * dir.x;
		vPos.y -= DT * Status()->GetRunSpeed() * dir.y;
		vPos.z -= DT * Status()->GetRunSpeed() * dir.z;
	}

	Transform()->SetLocalPos(vPos);

	Animator3D()->SetCurAnimIdx(Status()->GetState());
}
