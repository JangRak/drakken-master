#pragma once
#include "Script.h"
class COtherPlayerScript :
	public CScript
{
private:
	Ptr<CMaterial>		m_pOriginMtrl;
	Ptr<CMaterial>		m_pCloneMtrl;

public:
	virtual void awake();
	virtual void update();

public:
	CLONE(COtherPlayerScript);

public:
	COtherPlayerScript();
	virtual ~COtherPlayerScript();
};

