#include "stdafx.h"
#include "PlayerLightScript.h"
#include "Animator3D.h"
#include "Status.h"
#include "GameObject.h"
#include "Transform.h"
#include "Light3D.h"

CPlayerLightScript::CPlayerLightScript()
	: CScript((UINT)SCRIPT_TYPE::BLUEMOUNTAINDRAGONSCRIPT)
{
}

CPlayerLightScript::~CPlayerLightScript()
{
}

void CPlayerLightScript::awake()
{
}

void CPlayerLightScript::update()
{
	Vec3 vec3LightPos = m_pPlayerObj->Transform()->GetLocalPos();
	vec3LightPos.x -= 1000.f;
	vec3LightPos.y += 1000.f;
	vec3LightPos.z -= 1000.f;



	this->Light3D()->SetLightPos(vec3LightPos);
}

