#pragma once
#include "Component.h"

#include "Transform.h"
#include "MeshRender.h"
#include "Collider2D.h"

#include "EventMgr.h"
#include "TimeMgr.h"
#include "KeyMgr.h"
#include "ResMgr.h"
#include "SceneMgr.h"
#include "Scene.h"
#include "Layer.h"

enum class SCRIPT_TYPE
{
	BULLETSCRIPT,
	MONSTERSCRIPT,
	PLAYERSCRIPT,
	TESTSCRIPT,
	GRIFFONSCRIPT,
	BARGHESTSCRIPT,
	MOUNTAINDRAGONSCRIPT,
	VIKINGSCRIPT,
	OTHERPLAYERSCRIPT,
	CONSTANTSTRINGSCRIPT,
	QUESTSCRIPT,
	FAKELOGINSCRIPT,
	UISCRIPT,
	QUESTNPCSCRIPT,
	BLUEMOUNTAINDRAGONSCRIPT,
	PLAYERLIGHTSCRIPT,
	LEVELSCRIPT,
	END,
};

class CScript :
	public CComponent
{
private:
	UINT		m_iScriptType;

public:
	UINT GetScriptType() { return m_iScriptType; }

public:
	virtual void update() = 0;

public:
	static void CreateObject(CGameObject* _pNewObject, int _iLayerIdx);
	static void CreateObject(CGameObject* _pNewObject, const wstring& _strLayerName);

	static void DeleteObject(CGameObject* _pDeleteObject);
	
	void AddChild(CGameObject* _pChildObject);
	static void AddChild(CGameObject* _pParent, CGameObject* _pChild);

	void ClearParent();
	void TransferLayer(const wstring& _strLayerName, bool _bMoveAll);
	void TransferLayer(int _iLayer, bool _bMoveAll);
	static void TransferLayer(CGameObject* _pTarget, const wstring& _strLayerName, bool _bMoveAll);
	static void TransferLayer(CGameObject* _pTarget, int _iLayer, bool _bMoveAll);

	static void EnableObject(CGameObject* _pTarget);
	static void Disable(CGameObject* _pTarget);

protected:
	virtual void OnCollisionEnter(CCollider2D* _pOther) {}
	virtual void OnCollision(CCollider2D* _pOther) {}
	virtual void OnCollisionExit(CCollider2D* _pOther) {}

public:
	virtual void SaveToScene(FILE* _pFile) {}
	virtual void LoadFromScene(FILE* _pFile) {}

public:
	CScript(UINT _iScriptType);
	virtual ~CScript();

	friend class CCollider2D;
	friend class CCollider3D;
};

