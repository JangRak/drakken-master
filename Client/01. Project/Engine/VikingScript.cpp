//#include "stdafx.h"
//#include "VikingScript.h"
//
//#include "Animator3D.h"
//#include "Status.h"
//
//#include "NetworkMgr.h"
//
//
//CVikingScript::CVikingScript()
//   : CScript((UINT)SCRIPT_TYPE::PLAYERSCRIPT)
//   , m_pOriginMtrl(nullptr)
//   , m_pCloneMtrl(nullptr)
//{
//}
//
//CVikingScript::~CVikingScript()
//{
//}
//
//void CVikingScript::awake()
//{
//   m_pOriginMtrl = MeshRender()->GetSharedMaterial();
//   m_pCloneMtrl = m_pOriginMtrl->Clone();
//
//   int a = 1;
//   m_pCloneMtrl->SetData(SHADER_PARAM::INT_0, &a);
//}
//
//void CVikingScript::update()
//{
//   Vec3 vPos = Transform()->GetLocalPos();
//   Vec3 vRot = Transform()->GetLocalRot();
//
//   Matrix matRot = XMMatrixRotationX(vRot.x);
//   matRot *= XMMatrixRotationY(vRot.y);
//   matRot *= XMMatrixRotationZ(vRot.z);
//
//   Vec3 vLook = XMVector3TransformNormal(Vec3::Front, matRot);
//   Vec3 vRight = XMVector3TransformNormal(Vec3::Right, matRot);
//   Vec3 vUp = XMVector3TransformNormal(Vec3::Up, matRot);
//
//
//
//   VIKING_STATE tempState = VIKING_STATE::IDLE;
//
//   if (KEY_HOLD(KEY_TYPE::KEY_LSHIFT))
//   {
//      tempState = VIKING_STATE::RUN;
//      //CNetworkMgr::GetInst()->SendRunPacket(true);
//   }
//   else if (KEY_AWAY(KEY_TYPE::KEY_LSHIFT))
//   {
//      tempState = VIKING_STATE::WALK;
//      //CNetworkMgr::GetInst()->SendRunPacket(false);
//   }
//
//   if (KEY_HOLD(KEY_TYPE::KEY_W))
//   {
//      // 쉬프트 안눌렸으면
//      if (tempState != VIKING_STATE::RUN)
//      {
//         tempState = VIKING_STATE::WALK;
//         vPos.x -= DT * Status()->GetWalkSpeed() * vLook.x;
//         vPos.y -= DT * Status()->GetWalkSpeed() * vLook.y;
//         vPos.z -= DT * Status()->GetWalkSpeed() * vLook.z;
//      }
//      // 쉬프트 눌렸으면
//      else
//      {
//         vPos.x -= DT * Status()->GetRunSpeed() * vLook.x;
//         vPos.y -= DT * Status()->GetRunSpeed() * vLook.y;
//         vPos.z -= DT * Status()->GetRunSpeed() * vLook.z;
//      }
//      //CNetworkMgr::GetInst()->SendMovePacket(D_UP);
//   }
//   else if (KEY_AWAY(KEY_TYPE::KEY_W))
//   {
//      //CNetworkMgr::GetInst()->SendMoveStopPacket(D_UP);
//   }
//
//   if (KEY_HOLD(KEY_TYPE::KEY_S))
//   {
//      // 쉬프트 안눌렸으면
//      if (tempState != VIKING_STATE::RUN)
//      {
//         tempState = VIKING_STATE::WALK;
//         vPos.x += DT * Status()->GetWalkSpeed() * vLook.x / 2;
//         vPos.y += DT * Status()->GetWalkSpeed() * vLook.y / 2;
//         vPos.z += DT * Status()->GetWalkSpeed() * vLook.z / 2;
//      }
//      else
//      {
//         vPos.x += DT * Status()->GetRunSpeed() * vLook.x / 2;
//         vPos.y += DT * Status()->GetRunSpeed() * vLook.y / 2;
//         vPos.z += DT * Status()->GetRunSpeed() * vLook.z / 2;
//
//      }
//      //CNetworkMgr::GetInst()->SendMovePacket(D_DOWN);
//   }
//   else if (KEY_AWAY(KEY_TYPE::KEY_S))
//   {
//      //CNetworkMgr::GetInst()->SendMoveStopPacket(D_DOWN);
//   }
//
//   if (KEY_HOLD(KEY_TYPE::KEY_A))
//   {
//      if (tempState != VIKING_STATE::RUN)
//      {
//         tempState = VIKING_STATE::WALK;
//         vPos.x += DT * Status()->GetWalkSpeed() * vRight.x;
//         vPos.y += DT * Status()->GetWalkSpeed() * vRight.y;
//         vPos.z += DT * Status()->GetWalkSpeed() * vRight.z;
//      }
//      else
//      {
//         vPos.x += DT * Status()->GetRunSpeed() * vRight.x;
//         vPos.y += DT * Status()->GetRunSpeed() * vRight.y;
//         vPos.z += DT * Status()->GetRunSpeed() * vRight.z;
//      }
//      //CNetworkMgr::GetInst()->SendMovePacket(D_LEFT);
//   }
//   else if (KEY_AWAY(KEY_TYPE::KEY_A))
//   {
//      //CNetworkMgr::GetInst()->SendMoveStopPacket(D_LEFT);
//   }
//
//   if (KEY_HOLD(KEY_TYPE::KEY_D))
//   {
//      if (tempState != VIKING_STATE::RUN)
//      {
//         tempState = VIKING_STATE::WALK;
//         vPos.x -= DT * Status()->GetWalkSpeed() * vRight.x;
//         vPos.y -= DT * Status()->GetWalkSpeed() * vRight.y;
//         vPos.z -= DT * Status()->GetWalkSpeed() * vRight.z;
//      }
//      else
//      {
//         vPos.x -= DT * Status()->GetRunSpeed() * vRight.x;
//         vPos.y -= DT * Status()->GetRunSpeed() * vRight.y;
//         vPos.z -= DT * Status()->GetRunSpeed() * vRight.z;
//
//      }
//      //CNetworkMgr::GetInst()->SendMovePacket(D_RIGHT);
//   }
//   else if (KEY_AWAY(KEY_TYPE::KEY_D))
//   {
//      //CNetworkMgr::GetInst()->SendMoveStopPacket(D_RIGHT);
//   }
//
//   if (KEY_TAB(KEY_TYPE::KEY_SPACE))
//   {
//   }
//
//
//   if (KEY_TAB(KEY_TYPE::KEY_LBTN))
//   {
//      //CNetworkMgr::GetInst()->SendAttackPacket(A_BITE);
//      tempState = VIKING_STATE::ATTACK;
//   }
//   if (KEY_TAB(KEY_TYPE::KEY_RBTN))
//   {
//      //tempState = VIKING_STATE::CLAWATTACK;
//   }
//
//
//
//
//   Transform()->SetLocalPos(vPos);
//   Transform()->SetLocalRot(vRot);
//
//   if (Status()->GetState() != (int)tempState)
//   {
//      if (Animator3D()->GetIsEnd() == true ||
//         Status()->GetState() == (int)VIKING_STATE::WALK ||
//         Status()->GetState() == (int)VIKING_STATE::IDLE)
//      {
//         Animator3D()->InitCurAnimFrame();
//      }
//      else
//      {
//         tempState = (VIKING_STATE)Status()->GetState();
//      }
//   }
//
//
//   Status()->SetState((int)tempState);
//
//   Animator3D()->SetCurAnimIdx(Status()->GetState());
//}
//



//////////////

#include "stdafx.h"
#include "VikingScript.h"

#include "Animator3D.h"
#include "Status.h"
#include "NetworkMgr.h"
#include "QuestScript.h"

#include <iostream>

CVikingScript::CVikingScript()
	: CScript((UINT)SCRIPT_TYPE::VIKINGSCRIPT)
	, m_bChange(false)
	, m_bIsActive(false)
	, m_iFakeDeath((int)PLAYER_FAKE_DEATH::ALIVE)
{
}

CVikingScript::~CVikingScript()
{
}

void CVikingScript::awake()
{
}

void CVikingScript::update()
{
	Vec3 vPos = Transform()->GetLocalPos();
	Vec3 vRot = Transform()->GetLocalRot();

	Matrix matRot = XMMatrixRotationX(vRot.x);
	matRot *= XMMatrixRotationY(vRot.y);
	matRot *= XMMatrixRotationZ(vRot.z);

	Vec3 vLook = XMVector3TransformNormal(Vec3::Front, matRot);
	Vec3 vRight = XMVector3TransformNormal(Vec3::Right, matRot);
	Vec3 vUp = XMVector3TransformNormal(Vec3::Up, matRot);


	if (KEY_AWAY(KEY_TYPE::KEY_1))
	{
		CGameObject* pGameObj = CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"NPC")->GetObjects().back();
		float dist = Vec3::Distance(pGameObj->Transform()->GetLocalPos(), this->Transform()->GetLocalPos());
		if (dist < 200.f)
		{
			CSceneMgr::GetInst()->GetQuestObj()->GetScript<CQuestScript>()->SetQuestState((int)QUEST_STATE::QUESTACCEPT);
		}
	}


	switch (m_iFakeDeath)
	{
	case (int)PLAYER_FAKE_DEATH::ALIVE:






		if (m_ePlayerKind == (int)PLAYER_KIND::VIKING)
		{
			// Cheat
			if (KEY_AWAY(KEY_TYPE::KEY_3))
			{
				this->Status()->SetWalkSpeed(800.f);
			}
			if (KEY_AWAY(KEY_TYPE::KEY_4))
			{
				this->Status()->SetWalkSpeed(VIKING_WALK_SPEED);
			}
			if (KEY_AWAY(KEY_TYPE::KEY_5))
			{
				CNetworkMgr::GetInst()->SendInvPacket(true);
			}
			if (KEY_AWAY(KEY_TYPE::KEY_6))
			{
				CNetworkMgr::GetInst()->SendInvPacket(false);
			}


			VIKING_STATE tempState = VIKING_STATE::IDLE;

			if (KEY_HOLD(KEY_TYPE::KEY_LSHIFT))
			{
				tempState = VIKING_STATE::RUN;
				CNetworkMgr::GetInst()->SendRunPacket(true);
			}
			else if (KEY_AWAY(KEY_TYPE::KEY_LSHIFT))
			{
				tempState = VIKING_STATE::WALK;
				CNetworkMgr::GetInst()->SendRunPacket(false);
			}


			if (KEY_HOLD(KEY_TYPE::KEY_W))
			{
				// 쉬프트 안눌렸으면
				if (tempState != VIKING_STATE::RUN)
				{
					tempState = VIKING_STATE::WALK;
					vPos.x -= DT * Status()->GetWalkSpeed() * vLook.x;
					vPos.y -= DT * Status()->GetWalkSpeed() * vLook.y;
					vPos.z -= DT * Status()->GetWalkSpeed() * vLook.z;
				}
				// 쉬프트 눌렸으면
				else
				{
					vPos.x -= DT * Status()->GetRunSpeed() * vLook.x;
					vPos.y -= DT * Status()->GetRunSpeed() * vLook.y;
					vPos.z -= DT * Status()->GetRunSpeed() * vLook.z;
				}
				CNetworkMgr::GetInst()->SendMovePacket(D_UP);
			}
			else if (KEY_AWAY(KEY_TYPE::KEY_W))
			{
				CNetworkMgr::GetInst()->SendMoveStopPacket(D_UP);
			}

			if (KEY_HOLD(KEY_TYPE::KEY_S))
			{
				// 쉬프트 안눌렸으면
				if (tempState != VIKING_STATE::RUN)
				{
					tempState = VIKING_STATE::WALK;
					vPos.x += DT * Status()->GetWalkSpeed() * vLook.x / 2;
					vPos.y += DT * Status()->GetWalkSpeed() * vLook.y / 2;
					vPos.z += DT * Status()->GetWalkSpeed() * vLook.z / 2;
				}
				else
				{
					vPos.x += DT * Status()->GetRunSpeed() * vLook.x / 2;
					vPos.y += DT * Status()->GetRunSpeed() * vLook.y / 2;
					vPos.z += DT * Status()->GetRunSpeed() * vLook.z / 2;

				}
				CNetworkMgr::GetInst()->SendMovePacket(D_DOWN);
			}
			else if (KEY_AWAY(KEY_TYPE::KEY_S))
			{
				CNetworkMgr::GetInst()->SendMoveStopPacket(D_DOWN);
			}

			if (KEY_HOLD(KEY_TYPE::KEY_A))
			{
				if (tempState != VIKING_STATE::RUN)
				{
					tempState = VIKING_STATE::WALK;
					vPos.x += DT * Status()->GetWalkSpeed() * vRight.x;
					vPos.y += DT * Status()->GetWalkSpeed() * vRight.y;
					vPos.z += DT * Status()->GetWalkSpeed() * vRight.z;
				}
				else
				{
					vPos.x += DT * Status()->GetRunSpeed() * vRight.x;
					vPos.y += DT * Status()->GetRunSpeed() * vRight.y;
					vPos.z += DT * Status()->GetRunSpeed() * vRight.z;
				}
				CNetworkMgr::GetInst()->SendMovePacket(D_LEFT);
			}
			else if (KEY_AWAY(KEY_TYPE::KEY_A))
			{
				CNetworkMgr::GetInst()->SendMoveStopPacket(D_LEFT);
			}

			if (KEY_HOLD(KEY_TYPE::KEY_D))
			{
				if (tempState != VIKING_STATE::RUN)
				{
					tempState = VIKING_STATE::WALK;
					vPos.x -= DT * Status()->GetWalkSpeed() * vRight.x;
					vPos.y -= DT * Status()->GetWalkSpeed() * vRight.y;
					vPos.z -= DT * Status()->GetWalkSpeed() * vRight.z;
				}
				else
				{
					vPos.x -= DT * Status()->GetRunSpeed() * vRight.x;
					vPos.y -= DT * Status()->GetRunSpeed() * vRight.y;
					vPos.z -= DT * Status()->GetRunSpeed() * vRight.z;

				}
				CNetworkMgr::GetInst()->SendMovePacket(D_RIGHT);
			}
			else if (KEY_AWAY(KEY_TYPE::KEY_D))
			{
				CNetworkMgr::GetInst()->SendMoveStopPacket(D_RIGHT);
			}

			if (KEY_TAB(KEY_TYPE::KEY_SPACE))
			{
				//tempState = VIKING_STATE::JUMP;
				if (m_ePlayerKind == (int)PLAYER_KIND::VIKING)
				{
					SetPlayerChange(true);
					CNetworkMgr::GetInst()->SendTransformPacket();
				}


			}


			if (KEY_TAB(KEY_TYPE::KEY_LBTN))
			{
				CNetworkMgr::GetInst()->SendAttackPacket(A_BITE);
				tempState = VIKING_STATE::ATTACK;
			}
			if (KEY_HOLD(KEY_TYPE::KEY_RBTN)) {
				CNetworkMgr::GetInst()->SendDefencePacket(true);
				tempState = VIKING_STATE::DEFEND;
			}
			else if (KEY_AWAY(KEY_TYPE::KEY_RBTN)) {
				CNetworkMgr::GetInst()->SendDefencePacket(false);
				tempState = VIKING_STATE::IDLE;
			}

			int StatusState = Status()->GetState();

			if (Status()->GetState() != (int)tempState)
			{
				if (Animator3D()->GetIsEnd() == true ||
					Status()->GetState() == (int)VIKING_STATE::WALK ||
					Status()->GetState() == (int)VIKING_STATE::IDLE)
				{
					Animator3D()->InitCurAnimFrame();
				}
				else
				{
					tempState = (VIKING_STATE)Status()->GetState();
				}
			}

			//if (Status()->Status()->GetState() == (int)VIKING_STATE::JUMP)
			//{
			//   m_bChange = true;
			//}
			Status()->SetState((int)tempState);

			//CScene* pCurScene = CSceneMgr::GetInst()->GetCurScene();
			//vector<CGameObject*> tempVecObjects = pCurScene->GetLayer(5)->GetObjects();

			//bool collision = false;
			//for (UINT j = 0; j < tempVecObjects.size(); ++j) {
			//	float r = tempVecObjects[j]->Transform()->GetLocalScale().z;
			//	Vec3 c1 = Transform()->GetLocalPos();
			//	Vec3 c2 = tempVecObjects[j]->Transform()->GetLocalPos();
			//	float distance = Vec3::Distance(c1, c2);

			//	//float oldVel = Status()->GetWalkSpeed();
			//	//
			//	if (distance <= r) {
			//		//	float newVel;
			//		//	newVel = (0.5f - 1.f) / (0.5f + 1.f) * oldVel
			//		//		+ (2.f * 1.f) / (0.5f + 1.f) * 50.f;
			//		//	Status()->SetWalkSpeed(newVel);
			//		collision = true;

			//		if (c1.z < c2.z) {
			//			Vec3 nor = c2.Normalize();

			//			vPos.x -= DT * Status()->GetWalkSpeed() * nor.x;
			//			vPos.y -= DT * Status()->GetWalkSpeed() * nor.y;
			//			vPos.z -= DT * Status()->GetWalkSpeed() * nor.z;
			//		}
			//		else {
			//			Vec3 nor = c1.Normalize();

			//			vPos.x += DT * Status()->GetWalkSpeed() * nor.x;
			//			vPos.y += DT * Status()->GetWalkSpeed() * nor.y;
			//			vPos.z += DT * Status()->GetWalkSpeed() * nor.z;
			//		}

			//		Transform()->SetLocalPos(vPos);
			//		break;
			//	}
			//}

			//if (false == collision) {
			//	Transform()->SetLocalPos(vPos);
			//}

			CScene* tempScene = CSceneMgr::GetInst()->GetCurScene();
			vector<CGameObject*> tempVecObjects;

			tempVecObjects = tempScene->FindLayer(L"Enviroment_Collider")->GetObjects();

			bool check = false;
			for (UINT i = 0; i < tempVecObjects.size(); ++i) {
				Vec3 oPos = tempVecObjects[i]->Transform()->GetLocalPos();
				float r = tempVecObjects[i]->Transform()->GetLocalScale().z;

				float distance = Vec3::Distance(vPos, oPos);

				if (distance <= r) {
					check = true;
					break;
				}
			}

			if (false == check) Transform()->SetLocalPos(vPos);
			Transform()->SetLocalRot(vRot);

			//printf("%f, %f, %f\n", vPos.x, vPos.y, vPos.z);
		}

	else if ((int)PLAYER_KIND::MOUNTAIN_DRAGON == m_ePlayerKind)
	{
		Vec3 vPos = Transform()->GetLocalPos();
		Vec3 vRot = Transform()->GetLocalRot();

		Matrix matRot = XMMatrixRotationX(vRot.x);
		matRot *= XMMatrixRotationY(vRot.y);
		matRot *= XMMatrixRotationZ(vRot.z);

		Vec3 vLook = XMVector3TransformNormal(Vec3::Front, matRot);
		Vec3 vRight = XMVector3TransformNormal(Vec3::Right, matRot);
		Vec3 vUp = XMVector3TransformNormal(Vec3::Up, matRot);

		MOUNTAINDRAGON_STATE tempState = MOUNTAINDRAGON_STATE::IDLE;

		if (KEY_HOLD(KEY_TYPE::KEY_LSHIFT))
		{
			tempState = MOUNTAINDRAGON_STATE::RUN;

			if (false == CNetworkMgr::GetInst()->GetIsRunnig()) {
				CNetworkMgr::GetInst()->SetIsRunnig(true);
				CNetworkMgr::GetInst()->SendRunPacket(true);
			}
		}

		else if (KEY_AWAY(KEY_TYPE::KEY_LSHIFT))
		{
			tempState = MOUNTAINDRAGON_STATE::WALK;

			if (true == CNetworkMgr::GetInst()->GetIsRunnig()) {
				CNetworkMgr::GetInst()->SetIsRunnig(false);
				CNetworkMgr::GetInst()->SendRunPacket(false);
			}
		}


		if (KEY_HOLD(KEY_TYPE::KEY_W))
		{
			// 쉬프트 안눌렸으면
			if (tempState != MOUNTAINDRAGON_STATE::RUN)
			{
				tempState = MOUNTAINDRAGON_STATE::WALK;
				vPos.x -= DT * Status()->GetWalkSpeed() * vLook.x;
				vPos.y -= DT * Status()->GetWalkSpeed() * vLook.y;
				vPos.z -= DT * Status()->GetWalkSpeed() * vLook.z;
			}
			// 쉬프트 눌렸으면 
			else
			{
				vPos.x -= DT * Status()->GetRunSpeed() * vLook.x;
				vPos.y -= DT * Status()->GetRunSpeed() * vLook.y;
				vPos.z -= DT * Status()->GetRunSpeed() * vLook.z;
			}
			//CNetworkMgr::GetInst()->SendMovePacket(D_UP);
		}
		else if (KEY_AWAY(KEY_TYPE::KEY_W))
		{
//			CNetworkMgr::GetInst()->SendMoveStopPacket(D_UP);
		}

		if (KEY_HOLD(KEY_TYPE::KEY_S))
		{
			// 쉬프트 안눌렸으면
			if (tempState != MOUNTAINDRAGON_STATE::RUN)
			{
				tempState = MOUNTAINDRAGON_STATE::WALK;
				vPos.x += DT * Status()->GetWalkSpeed() * vLook.x / 2;
				vPos.y += DT * Status()->GetWalkSpeed() * vLook.y / 2;
				vPos.z += DT * Status()->GetWalkSpeed() * vLook.z / 2;
			}
			else
			{
				vPos.x += DT * Status()->GetRunSpeed() * vLook.x / 2;
				vPos.y += DT * Status()->GetRunSpeed() * vLook.y / 2;
				vPos.z += DT * Status()->GetRunSpeed() * vLook.z / 2;

			}
//			CNetworkMgr::GetInst()->SendMovePacket(D_DOWN);
		}
		else if (KEY_AWAY(KEY_TYPE::KEY_S))
		{
//			CNetworkMgr::GetInst()->SendMoveStopPacket(D_DOWN);
		}

		if (KEY_HOLD(KEY_TYPE::KEY_A))
		{
			if (tempState != MOUNTAINDRAGON_STATE::RUN)
			{
				tempState = MOUNTAINDRAGON_STATE::WALK;
				vPos.x += DT * Status()->GetWalkSpeed() * vRight.x;
				vPos.y += DT * Status()->GetWalkSpeed() * vRight.y;
				vPos.z += DT * Status()->GetWalkSpeed() * vRight.z;
			}
			else
			{
				vPos.x += DT * Status()->GetRunSpeed() * vRight.x;
				vPos.y += DT * Status()->GetRunSpeed() * vRight.y;
				vPos.z += DT * Status()->GetRunSpeed() * vRight.z;
			}
//			CNetworkMgr::GetInst()->SendMovePacket(D_LEFT);
		}
		else if (KEY_AWAY(KEY_TYPE::KEY_A))
		{
//			CNetworkMgr::GetInst()->SendMoveStopPacket(D_LEFT);
		}

		if (KEY_HOLD(KEY_TYPE::KEY_D))
		{
			if (tempState != MOUNTAINDRAGON_STATE::RUN)
			{
				tempState = MOUNTAINDRAGON_STATE::WALK;
				vPos.x -= DT * Status()->GetWalkSpeed() * vRight.x;
				vPos.y -= DT * Status()->GetWalkSpeed() * vRight.y;
				vPos.z -= DT * Status()->GetWalkSpeed() * vRight.z;
			}
			else
			{
				vPos.x -= DT * Status()->GetRunSpeed() * vRight.x;
				vPos.y -= DT * Status()->GetRunSpeed() * vRight.y;
				vPos.z -= DT * Status()->GetRunSpeed() * vRight.z;

			}
//			CNetworkMgr::GetInst()->SendMovePacket(D_RIGHT);
		}
		else if (KEY_AWAY(KEY_TYPE::KEY_D))
		{
//			CNetworkMgr::GetInst()->SendMoveStopPacket(D_RIGHT);
		}

		if (KEY_TAB(KEY_TYPE::KEY_SPACE))
		{
			if (m_ePlayerKind == (int)PLAYER_KIND::MOUNTAIN_DRAGON)
			{
				//SetPlayerChange(true);
				//CNetworkMgr::GetInst()->SendTransformPacket();
			}
		}
		if (KEY_TAB(KEY_TYPE::KEY_Q))
		{
			tempState = MOUNTAINDRAGON_STATE::BREATH;
		}


		if (KEY_TAB(KEY_TYPE::KEY_LBTN))
		{
			CNetworkMgr::GetInst()->SendAttackPacket(A_BITE);
			tempState = MOUNTAINDRAGON_STATE::BITE;
		}
		if (KEY_TAB(KEY_TYPE::KEY_RBTN))
		{

			tempState = MOUNTAINDRAGON_STATE::CLAWATTACK;
		}


		
		CScene* tempScene = CSceneMgr::GetInst()->GetCurScene();
		vector<CGameObject*> tempVecObjects;

		tempVecObjects = tempScene->FindLayer(L"Enviroment_Collider")->GetObjects();

		bool check = false;
		for (UINT i = 0; i < tempVecObjects.size(); ++i) {
			Vec3 oPos = tempVecObjects[i]->Transform()->GetLocalPos();
			float r = tempVecObjects[i]->Transform()->GetLocalScale().z;

			float distance = Vec3::Distance(vPos, oPos);

			if (distance <= r) {
				check = true;
				break;
			}
		}

		if (false == check) Transform()->SetLocalPos(vPos);

		Transform()->SetLocalRot(vRot);

		if (Status()->GetState() != (int)tempState)
		{
			if (Animator3D()->GetIsEnd() == true ||
				Status()->GetState() == (int)MOUNTAINDRAGON_STATE::WALK ||
				Status()->GetState() == (int)MOUNTAINDRAGON_STATE::IDLE)
			{
				Animator3D()->InitCurAnimFrame();
			}
			else
			{
				tempState = (MOUNTAINDRAGON_STATE)Status()->GetState();
			}
		}

		//tempState = MOUNTAINDRAGON_STATE::IDLE;
		Status()->SetState((int)tempState);

	}
	break;
	case (int)PLAYER_FAKE_DEATH::DYING:
		if (m_ePlayerKind == (int)PLAYER_KIND::MOUNTAIN_DRAGON)
		{
			Status()->SetState((int)MOUNTAINDRAGON_STATE::DEATH);
		}
		else
		{
			Status()->SetState((int)VIKING_STATE::DEATH);
		}


		if (Animator3D()->GetIsEnd())
		{
			m_iFakeDeath = (int)PLAYER_FAKE_DEATH::DEATH;
			if (m_ePlayerKind == (int)PLAYER_KIND::MOUNTAIN_DRAGON)
			{
				Status()->SetState((int)MOUNTAINDRAGON_STATE::IDLE);
			}
			else
			{
				Status()->SetState((int)VIKING_STATE::IDLE);
			}
			Status()->SetHP(Status()->GetMaxHP());

		}
		break;

	case (int)PLAYER_FAKE_DEATH::DEATH:
		this->Transform()->SetLocalPos(VEC3_TOWN_ENTRANCE_POS);
		m_iFakeDeath = (int)PLAYER_FAKE_DEATH::ALIVE;
		break;

	default:
		break;
	}

	Animator3D()->SetCurAnimIdx(Status()->GetState());

	//CGameObject* tempobj = CSceneMgr::GetInst()->GetUIObj();

	//this->Status()->GetHP();

}



