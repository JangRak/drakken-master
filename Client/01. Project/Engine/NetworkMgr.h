#pragma once

#include <Windows.h>
#include <WinSock2.h>
#include "../../../Server/Server/Protocol.h"

#pragma comment(lib, "Ws2_32.lib")

#pragma warning(disable:4996)

class CNetworkMgr
{
	SINGLE(CNetworkMgr);
public:
	int InitNetwork(char* addr, HWND mainWindow);
	int DoNetwork();

	int GetMyID() const { return m_myID; }

	bool GetReady() const { return m_ready; }

	bool GetIsRunnig() const { return m_isRunning; }
	void SetIsRunnig(bool b) { m_isRunning = b; }

	void SendLoginPacket(const char* name, const char* pw);
	void SendLogoutPacket();
	void SendMovePacket(int direction);
	void SendMoveStopPacket(int direction);
	void SendPosPacket();
	void SendMousePacket();
	void SendAttackPacket(char type);
	void SendRunPacket(bool b);
	void SendTransformPacket();
	void SendDefencePacket(bool b);
	void SendQuestDonePacket();
	void SendInvPacket(bool b);

public:
	void ProcessData();
	void ProcessPacket(char* ptr);

private:
	SOCKET m_socket;
	WSABUF m_recvBuffer;
	DWORD m_ioBytes;

	char m_buffer[MAX_BUFFER];

	char m_myName[MAX_ID_LEN + 1];
	int m_myID;

	int m_retval;
	//char m_buffer[MAX_BUFFER];

	bool m_isMoveDirection[4] = { false, false, false, false };

	bool m_isDefence;

	int m_bargNum = 1;
	int m_grifNum = 1;

	bool m_ready;
	bool m_isRunning = false;

	bool m_isFirstSend = false;
};


