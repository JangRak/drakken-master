#pragma once
#include "Script.h"
class CLevelScript :
	public CScript
{
private:
	Vec4 m_vFontColor;
	Vec4 m_vFontBackColor;


private:
	vector<CGameObject*> m_vecLevelNumFonts;
	wstring	m_strLevelNum;
	int m_iLevelNum;

private:
	CGameObject* m_pLevelStatus;

public:
	const int GetLevelNum() const { return m_iLevelNum; }

public:
	void SetLevelNum(int _iNum);

public:
	void Init(Vec4 vFrontColor, Vec4 vFontBackColor);

public:
	virtual void awake();
	virtual void update();

public:
	void UpdateFont();

public:
	CLONE(CLevelScript);


public:
	CLevelScript();
	virtual ~CLevelScript();

};

