#pragma once
#include "Script.h"
class CMountainDragonScript :
	public CScript
{
private:
	Ptr<CMaterial>		m_pOriginMtrl;
	Ptr<CMaterial>		m_pCloneMtrl;


private:
	bool m_bChange;
	bool m_bIsActive;

public:
	const bool GetIsPlayerChange() const { return m_bChange; }
	void SetPlayerChange(const bool& _bChange) { m_bIsActive = _bChange; }
	const bool GetIsScriptActive() const { return m_bIsActive; }
	void SetScriptActive(const bool& _bActive) { m_bIsActive = _bActive; }

public:
	virtual void awake();
	virtual void update();

public:
	CLONE(CMountainDragonScript);

public:
	CMountainDragonScript();
	virtual ~CMountainDragonScript();
};

