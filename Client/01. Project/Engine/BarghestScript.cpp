#include "stdafx.h"
#include "BarghestScript.h"
#include "Animator3D.h"
#include "Status.h"
CBarghestScript::CBarghestScript()
	: CScript((UINT)SCRIPT_TYPE::BARGHESTSCRIPT)
	, m_pOriginMtrl(nullptr)
	, m_pCloneMtrl(nullptr)
{
}

CBarghestScript::~CBarghestScript()
{
}

void CBarghestScript::awake()
{
	m_pOriginMtrl = MeshRender()->GetSharedMaterial();
	m_pCloneMtrl = m_pOriginMtrl->Clone();

	int a = 1;
	m_pCloneMtrl->SetData(SHADER_PARAM::INT_0, &a);
}

void CBarghestScript::update()
{
	Vec3 vPos = Transform()->GetLocalPos();
	Vec3 vRot = Transform()->GetLocalRot();

	Matrix matRot = XMMatrixRotationX(vRot.x);
	matRot *= XMMatrixRotationY(vRot.y);
	matRot *= XMMatrixRotationZ(vRot.z);

	Vec3 vLook = XMVector3TransformNormal(Vec3::Front, matRot);
	Vec3 vRight = XMVector3TransformNormal(Vec3::Right, matRot);
	Vec3 vUp = XMVector3TransformNormal(Vec3::Up, matRot);

	BARGHEST_STATE tempState = (BARGHEST_STATE)Status()->GetState();
	if (GetIsDying())
	{
		tempState = BARGHEST_STATE::DEATH;
	}
	Status()->SetState((int)tempState);

	if (BARGHEST_STATE::DEATH == ((BARGHEST_STATE)Status()->GetState()))
	{
		if (Animator3D()->GetIsEnd())
		{
			SetDead();
		}
	}

	Vec3 dir = Transform()->GetLook();

	if (BARGHEST_STATE::WALK == ((BARGHEST_STATE)Status()->GetState())) {
		vPos.x += DT * Status()->GetWalkSpeed() * dir.x;
		vPos.y += DT * Status()->GetWalkSpeed() * dir.y;
		vPos.z += DT * Status()->GetWalkSpeed() * dir.z;
	}

	else if (BARGHEST_STATE::RUN == ((BARGHEST_STATE)Status()->GetState())) {
		vPos.x += DT * Status()->GetRunSpeed() * dir.x;
		vPos.y += DT * Status()->GetRunSpeed() * dir.y;
		vPos.z += DT * Status()->GetRunSpeed() * dir.z;
	}

	Transform()->SetLocalPos(vPos);

	Animator3D()->SetCurAnimIdx(Status()->GetState());
}
