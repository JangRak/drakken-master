#include "stdafx.h"
#include "NetworkMgr.h"
#include "Scene.h"
#include "GameObject.h"
#include "Transform.h"
#include "Collider2D.h"
#include "MeshRender.h"
#include "Mesh.h"
#include "Material.h"
#include "ResMgr.h"
#include "PlayerScript.h"
#include "Status.h"
#include "Animator3D.h"
#include "GriffonScript.h"
#include "BarghestScript.h"
#include "OtherPlayerScript.h"
#include "QuestScript.h"
#include "BlueMountainDragonScript.h"
#include "VikingScript.h"
#include "PlayerCameraScript.h"
#include "LevelScript.h"

#include <iostream>
#include <random>
#include <chrono>

CNetworkMgr::CNetworkMgr()
	: m_isDefence(false)
{

}

CNetworkMgr::~CNetworkMgr()
{
	//if (true == m_ready) SendLogoutPacket();
}

int CNetworkMgr::InitNetwork(char* addr, HWND mainWindow)
{
	// 윈속 초기화    
	WSADATA wsa;
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
		return -1;

	// socket()
	m_socket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (INVALID_SOCKET == m_socket)
		printf("Init Socket error");

	// connect()
	SOCKADDR_IN serveraddr;
	ZeroMemory(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = inet_addr(addr);
	serveraddr.sin_port = htons(SERVER_PORT);
	WSAConnect(m_socket, (SOCKADDR *)&serveraddr, sizeof(serveraddr), NULL, NULL, NULL, NULL);

	int retval = WSAAsyncSelect(m_socket, mainWindow, WM_SOCKET, FD_CLOSE | FD_READ);
	if (retval) {
		printf("error");
	}

	if (retval == SOCKET_ERROR)
		printf("Connect error\n");

	m_recvBuffer.len = MAX_BUFFER;
	m_recvBuffer.buf = m_buffer;

	return 1;
}

int CNetworkMgr::DoNetwork()
{
	while (true) {
		memset(m_buffer, 0, sizeof(MAX_BUFFER));
		DWORD ioFlag = 0;
		m_retval = WSARecv(m_socket, &m_recvBuffer, 1, &m_ioBytes, &ioFlag, NULL, NULL);
		if (SOCKET_ERROR == m_retval) {
			std::cout << "recv error\n";
			DebugBreak();
		}

		ProcessData();
	}

	return 1;
}

void CNetworkMgr::SendLoginPacket(const char* name, const char* pw)
{
	cs_packet_login packet;
	packet.size = sizeof(packet);
	packet.type = CS_LOGIN;
	packet.loginType = NORMAL;
	sprintf_s(packet.name, name);
	strcpy_s(m_myName, packet.name);

	sprintf_s(packet.pw, pw);

	int retval = send(m_socket, reinterpret_cast<char*>(&packet), sizeof(packet), 0);
	if (SOCKET_ERROR == retval) {
		std::cout << "send error\n";
		DebugBreak();
	}
}

void CNetworkMgr::SendLogoutPacket()
{
	cs_packet_logout packet;
	packet.size = sizeof(packet);
	packet.type = CS_LOGOUT;

	int retval = send(m_socket, reinterpret_cast<char*>(&packet), sizeof(packet), 0);
	if (SOCKET_ERROR == retval) {
		std::cout << "send error\n";
		DebugBreak();
	}
}

void CNetworkMgr::SendMovePacket(int direction)
{
	if (m_isMoveDirection[direction] == true) return;

	m_isMoveDirection[direction] = true;

	cs_packet_move packet;
	packet.type = CS_WALK;
	packet.size = sizeof(packet);
	packet.direction = direction;

	int retval = send(m_socket, reinterpret_cast<char*>(&packet), sizeof(packet), 0);
	if (SOCKET_ERROR == retval) {
		std::cout << "send error\n";
		DebugBreak();
	}
}

void CNetworkMgr::SendMoveStopPacket(int direction)
{
	if (m_isMoveDirection[direction] == false) return;

	m_isMoveDirection[direction] = false;

	cs_packet_move_stop packet;
	packet.type = CS_STOP;
	packet.size = sizeof(packet);
	packet.direction = direction;

	int retval = send(m_socket, reinterpret_cast<char*>(&packet), sizeof(packet), 0);
	if (SOCKET_ERROR == retval) {
		std::cout << "send error\n";
		DebugBreak();
	}
}

void CNetworkMgr::SendPosPacket()
{
	int cnt = 0;
	for (int i = 0; i < 4; ++i) {
		if (true == m_isMoveDirection[i]) {
			m_isFirstSend = false;
			break;
		}
		else ++cnt;
	}
	if (4 == cnt) {
		if (false == m_isFirstSend) {
			m_isFirstSend = true;
		}
		else return;
	}

	cs_packet_position packet;
	packet.type = CS_POSITION;
	packet.size = sizeof(packet);

	CScene* tempScene = CSceneMgr::GetInst()->GetCurScene();
	vector<CGameObject*> tempVecObjects = tempScene->GetLayer(1)->GetObjects();

	for (UINT i = 0; i < tempVecObjects.size(); ++i) {
		if (L"My Player" == tempVecObjects[i]->GetName()) {
			Vec3 vPos = tempVecObjects[i]->Transform()->GetLocalPos();
			packet.xPos = vPos.x;
			packet.yPos = vPos.y;
			packet.zPos = vPos.z;
			Vec3 vRot = tempVecObjects[i]->Transform()->GetLocalRot();
			Matrix matRot = XMMatrixRotationX(vRot.x);
			matRot *= XMMatrixRotationY(vRot.y);
			matRot *= XMMatrixRotationZ(vRot.z);
			Vec3 l = XMVector3TransformNormal(Vec3::Front, matRot);
			packet.xLook = l.x;
			packet.yLook = l.y;
			packet.zLook = l.z;
			break;
		}
	}

	int retval = send(m_socket, reinterpret_cast<char*>(&packet), sizeof(packet), 0);
	if (SOCKET_ERROR == retval) {
		std::cout << "send error\n";
		DebugBreak();
	}
}

void CNetworkMgr::SendMousePacket()
{
	cs_packet_mouse_move packet;
	packet.type = CS_MOUSE_MOVE_PACKET;
	packet.size = sizeof(packet);

	CScene* tempScene = CSceneMgr::GetInst()->GetCurScene();
	vector<CGameObject*> tempVecObjects = tempScene->GetLayer(1)->GetObjects();

	Vec3 vPos;
	for (UINT i = 0; i < tempVecObjects.size(); ++i) {
		if (L"My Player" == tempVecObjects[i]->GetName()) {
			Vec3 vPos = tempVecObjects[i]->Transform()->GetLocalPos();
			Vec3 vRot = tempVecObjects[i]->Transform()->GetLocalRot();
			Matrix matRot = XMMatrixRotationX(vRot.x);
			matRot *= XMMatrixRotationY(vRot.y);
			matRot *= XMMatrixRotationZ(vRot.z);
			Vec3 l = XMVector3TransformNormal(Vec3::Front, matRot);
			Vec3 r = XMVector3TransformNormal(Vec3::Right, matRot);
			Vec3 u = XMVector3TransformNormal(Vec3::Up, matRot);

			packet.look[0] = l.x; packet.look[1] = l.y; packet.look[2] = l.z;
			packet.right[0] = r.x; packet.right[1] = r.y; packet.right[2] = r.z;
			packet.up[0] = u.x; packet.up[1] = u.y; packet.up[2] = u.z;
			break;
		}
	}

	int retval = send(m_socket, reinterpret_cast<char*>(&packet), sizeof(packet), 0);
	if (SOCKET_ERROR == retval) {
		std::cout << "send error\n";
		DebugBreak();
	}
}

void CNetworkMgr::SendAttackPacket(char type)
{
	cs_packet_attack packet;
	packet.type = CS_ATTACK;
	packet.size = sizeof(packet);
	packet.attackType = type;

	int retval = send(m_socket, reinterpret_cast<char*>(&packet), sizeof(packet), 0);
	if (SOCKET_ERROR == retval) {
		std::cout << "send error\n";
		DebugBreak();
	}
}

void CNetworkMgr::SendRunPacket(bool b)
{
	cs_packet_run packet;
	packet.type = CS_RUN;
	packet.size = sizeof(packet);
	packet.running = b;

	int retval = send(m_socket, reinterpret_cast<char*>(&packet), sizeof(packet), 0);
	if (SOCKET_ERROR == retval) {
		std::cout << "send error\n";
		DebugBreak();
	}
}

void CNetworkMgr::SendTransformPacket()
{
	cs_packet_transform packet;
	packet.type = CS_TRANSFORM;
	packet.size = sizeof(packet);

	int retval = send(m_socket, reinterpret_cast<char*>(&packet), sizeof(packet), 0);
	if (SOCKET_ERROR == retval) {
		std::cout << "send error\n";
		DebugBreak();
	}
}

void CNetworkMgr::SendDefencePacket(bool b)
{
	if (m_isDefence == b) return;

	m_isDefence = b;

	cs_packet_defence packet;
	packet.type = CS_DEFENCE;
	packet.size = sizeof(packet);
	packet.kind = b;

	int retval = send(m_socket, reinterpret_cast<char*>(&packet), sizeof(packet), 0);
	if (SOCKET_ERROR == retval) {
		std::cout << "send error\n";
		DebugBreak();
	}
}

void CNetworkMgr::SendQuestDonePacket()
{
	cs_packet_quest_done packet;
	packet.type = CS_QUEST_DONE;
	packet.size = sizeof(packet);

	int retval = send(m_socket, reinterpret_cast<char*>(&packet), sizeof(packet), 0);
	if (SOCKET_ERROR == retval) {
		std::cout << "send error\n";
		DebugBreak();
	}
}

void CNetworkMgr::SendInvPacket(bool b)
{
	cs_packet_invincibility packet;
	packet.type = CS_INV;
	packet.size = sizeof(packet);
	packet.kind = b;

	int retval = send(m_socket, reinterpret_cast<char*>(&packet), sizeof(packet), 0);
	if (SOCKET_ERROR == retval) {
		std::cout << "send error\n";
		DebugBreak();
	}
}

void CNetworkMgr::ProcessData()
{
	memset(m_buffer, 0, sizeof(MAX_BUFFER));
	DWORD ioFlag = 0;
	m_retval = WSARecv(m_socket, &m_recvBuffer, 1, &m_ioBytes, &ioFlag, NULL, NULL);
	if (SOCKET_ERROR == m_retval) {
		std::cout << "recv error\n";
		DebugBreak();
	}

	char* ptr = m_buffer;
	static size_t in_packet_size = 0;
	static size_t saved_packet_size = 0;
	static char packet_buffer[MAX_BUFFER];

	while (0 != m_ioBytes) {
		if (0 == in_packet_size) in_packet_size = ptr[0];
		if (m_ioBytes + saved_packet_size >= in_packet_size) {
			memcpy(packet_buffer + saved_packet_size, ptr, in_packet_size - saved_packet_size);
			ProcessPacket(packet_buffer);
			ptr += in_packet_size - saved_packet_size;
			m_ioBytes -= in_packet_size - saved_packet_size;
			in_packet_size = 0;
			saved_packet_size = 0;
		}
		else {
			memcpy(packet_buffer + saved_packet_size, ptr, m_ioBytes);
			saved_packet_size += m_ioBytes;
			m_ioBytes = 0;
		}
	}
}

void CNetworkMgr::ProcessPacket(char* ptr)
{
	//static bool first_time = true;

	switch (ptr[1]) {
	case SC_LOGIN_OK: {
		sc_packet_login_ok* packet = reinterpret_cast<sc_packet_login_ok*>(ptr);
		m_myID = packet->id;

		CScene* tempScene = CSceneMgr::GetInst()->GetCurScene();
		vector<CGameObject*> tempVecObjects = tempScene->GetLayer(1)->GetObjects();

		//std::cout << tempVecObjects.size() << std::endl;
		printf("My ID: %d\n", m_myID);
		Vec3 vPos;
		for (UINT i = 0; i < tempVecObjects.size(); ++i) {
			if (L"My Player" == tempVecObjects[i]->GetName()) {
				tempVecObjects[i]->SetServerID(m_myID);
				vPos.x = packet->x;
				vPos.y = packet->y;
				vPos.z = packet->z;
				tempVecObjects[i]->Transform()->SetLocalPos(vPos);
				tempVecObjects[i]->Status()->SetExp(packet->c_exp);
				tempVecObjects[i]->Status()->SetMaxExp(packet->m_exp);
				tempVecObjects[i]->Status()->SetHP(packet->c_hp);
				tempVecObjects[i]->Status()->SetMaxHP(packet->m_hp);
				tempVecObjects[i]->Status()->SetLevel(packet->level);
				//tempVecObjects[i]->Status()->SetDamage(packet->damage);
				
				break;
			}
		}

		m_ready = true;

		break;
	}
	case SC_LOGIN_FAIL: {
		sc_packet_login_fail* packet = reinterpret_cast<sc_packet_login_fail*>(ptr);

		printf("로그인 실패\n");

		closesocket(m_socket);

		exit(1);

		return;
	}
	//case SC_MOVE_DIRECTION: {
	//	sc_packet_move_direction* packet = reinterpret_cast<sc_packet_move_direction*>(ptr);
	//	int otherID = packet->id;
	//	int direction = packet->direction;
	//
	//	CScene* tempScene = CSceneMgr::GetInst()->GetCurScene();
	//	vector<CGameObject*> tempVecObjects;
	//
	//	Vec3 vPos;
	//
	//	if (otherID < NPC_ID_START) {
	//		tempVecObjects = tempScene->FindLayer(L"Player")->GetObjects();
	//		for (UINT i = 0; i < tempVecObjects.size(); ++i) {
	//			if (otherID == tempVecObjects[i]->GetID()) {
	//				//std::cout << tempVecObjects[i]->GetID() << std::endl;
	//				vPos = tempVecObjects[i]->Transform()->GetWorldPos();
	//				vPos.x = packet->x;
	//				vPos.y = packet->y;
	//				vPos.z = packet->z;
	//				tempVecObjects[i]->Transform()->SetLocalPos(vPos);
	//				tempVecObjects[i]->SetMoveDirection(direction, true);
	//				break;
	//			}
	//		}
	//	}
	//
	//	else {
	//		tempVecObjects = tempScene->FindLayer(L"Monster")->GetObjects();
	//
	//		for (UINT i = 0; i < tempVecObjects.size(); ++i)
	//		{
	//			if (otherID == tempVecObjects[i]->GetID()) {
	//				vPos = tempVecObjects[i]->Transform()->GetWorldPos();
	//				vPos.x = packet->x;
	//				vPos.y = packet->y;
	//				vPos.z = packet->z;
	//				tempVecObjects[i]->Transform()->SetLocalPos(vPos);
	//				tempVecObjects[i]->SetMoveDirection(direction, true);
	//				break;
	//			}
	//		}
	//	}
	//
	//	break;
	//}
	case SC_MOVE: {
		sc_packet_move* packet = reinterpret_cast<sc_packet_move*>(ptr);
		int otherID = packet->id;

		CScene* tempScene = CSceneMgr::GetInst()->GetCurScene();
		vector<CGameObject*> tempVecObjects;

		Vec3 vPos;
		Vec3 vDir;

		if (NPC_ID_START > otherID) {
			tempVecObjects = tempScene->GetLayer(1)->GetObjects();
			for (UINT i = 0; i < tempVecObjects.size(); ++i) {
				if (otherID == tempVecObjects[i]->GetServerID()) {
					vPos = tempVecObjects[i]->Transform()->GetLocalPos();
					Vec3 newPos;
					newPos.x = packet->x;
					newPos.y = packet->y;
					newPos.z = packet->z;

					vDir.x = packet->D_x;
					vDir.y = packet->D_y;
					vDir.z = packet->D_z;
					
					float speed;
					if (WALK == packet->status) {
						tempVecObjects[i]->Status()->SetState((int)VIKING_STATE::WALK);
						speed = tempVecObjects[i]->Status()->GetWalkSpeed();
					}
					else if (RUN == packet->status) {
						tempVecObjects[i]->Status()->SetState((int)VIKING_STATE::RUN);
						speed = tempVecObjects[i]->Status()->GetRunSpeed();
					}

					std::chrono::duration<float> time = std::chrono::system_clock::now() - packet->time;

					newPos.x -= DT * time.count() * speed;
					newPos.y -= DT * time.count() * speed;
					newPos.z -= DT * time.count() * speed;

					tempVecObjects[i]->Transform()->SetLocalPos(newPos);
					tempVecObjects[i]->Transform()->SetLook(vDir);
					tempVecObjects[i]->Transform()->LookAt(vDir);
					
					break;
				}
			}
		}
		else {
			tempVecObjects = tempScene->GetLayer(2)->GetObjects();

			for (UINT i = 0; i < tempVecObjects.size(); ++i) {
				if (otherID == tempVecObjects[i]->GetServerID()) {
					Vec3 newPos;
					newPos.x = packet->x;
					newPos.y = packet->y;
					newPos.z = packet->z;

					vDir.x = packet->D_x;
					vDir.y = packet->D_y;
					vDir.z = packet->D_z;

					float speed;
					if (L"Barghest" == tempVecObjects[i]->GetName()) {
						if (WALK == packet->status) {
							tempVecObjects[i]->Status()->SetState((int)BARGHEST_STATE::WALK);
							speed = tempVecObjects[i]->Status()->GetWalkSpeed();
						}
						else if (RUN == packet->status) {
							tempVecObjects[i]->Status()->SetState((int)BARGHEST_STATE::RUN);
							speed = tempVecObjects[i]->Status()->GetRunSpeed();
						}
					}
					else if (L"Griffon" == tempVecObjects[i]->GetName()) {
						if (WALK == packet->status) {
							tempVecObjects[i]->Status()->SetState((int)GRIFFON_STATE::WALK);
							speed = tempVecObjects[i]->Status()->GetRunSpeed();
						}
						//else if (RUN == packet->status) tempVecObjects[i]->Status()->SetState((int)GRIFFON_STATE::RUN);	// 그리폰 뛰는거 없음
					}
					else if (L"BlueMountainDragon" == tempVecObjects[i]->GetName()) {
						if (WALK == packet->status) {
							tempVecObjects[i]->Status()->SetState((int)MOUNTAINDRAGON_STATE::WALK);
							speed = tempVecObjects[i]->Status()->GetWalkSpeed();
						}
						else if (RUN == packet->status) {
							tempVecObjects[i]->Status()->SetState((int)MOUNTAINDRAGON_STATE::RUN);
							speed = tempVecObjects[i]->Status()->GetRunSpeed();
						}
					}

					std::chrono::duration<float> time = std::chrono::system_clock::now() - packet->time;

					newPos.x += DT * time.count() * speed;
					newPos.y += DT * time.count() * speed;
					newPos.z += DT * time.count() * speed;

					tempVecObjects[i]->Transform()->SetLocalPos(newPos);
					tempVecObjects[i]->Transform()->SetLook(vDir);
					tempVecObjects[i]->Transform()->LookAt(-vDir);

					break;
				}
			}
		}

		break;
	}
	case SC_STOP: {
		sc_packet_stop* packet = reinterpret_cast<sc_packet_stop*>(ptr);
		int otherID = packet->id;

		CScene* tempScene = CSceneMgr::GetInst()->GetCurScene();
		vector<CGameObject*> tempVecObjects;

		tempVecObjects = tempScene->FindLayer(L"Player")->GetObjects();
		for (UINT i = 0; i < tempVecObjects.size(); ++i) {
			if (otherID == tempVecObjects[i]->GetServerID()) {
				tempVecObjects[i]->Status()->SetState((int)VIKING_STATE::IDLE);
				break;
			}
		}
		
		break;
	}
	case SC_HP_CHANGE: {
		sc_packet_hp* packet = reinterpret_cast<sc_packet_hp*>(ptr);

		CScene* tempScene = CSceneMgr::GetInst()->GetCurScene();
		vector<CGameObject*> tempVecObjects;

		tempVecObjects = tempScene->FindLayer(L"Player")->GetObjects();
		for (UINT i = 0; i < tempVecObjects.size(); ++i) {
			if (m_myID == tempVecObjects[i]->GetServerID()) {
				tempVecObjects[i]->Status()->SetHP(packet->c_hp);
				break;
			}
		}

		break;
	}
	case SC_NPC_ATTACK: {
		sc_packet_npc_attack* packet = reinterpret_cast<sc_packet_npc_attack*>(ptr);
		int otherID = packet->id;

		CScene* tempScene = CSceneMgr::GetInst()->GetCurScene();
		vector<CGameObject*> tempVecObjects;

		tempVecObjects = tempScene->FindLayer(L"Monster")->GetObjects();

		for (UINT i = 0; i < tempVecObjects.size(); ++i) {
			if (otherID == tempVecObjects[i]->GetServerID()) {
				Vec3 pos;
				pos.x = packet->x;
				pos.y = 0.f;
				pos.z = packet->z;

				tempVecObjects[i]->Transform()->SetLocalPos(pos);
				if (tempVecObjects[i]->GetName() == L"Barghest")
				{
					printf("npc Barghest 공격\n");
					tempVecObjects[i]->Status()->SetState((int)BARGHEST_STATE::BITE);
				}
				else if (tempVecObjects[i]->GetName() == L"Griffon")
				{
					printf("npc Griffon 공격\n");
					switch (rand() % 2) {
					case 0:
						tempVecObjects[i]->Status()->SetState((int)GRIFFON_STATE::BITE);
						break;
					case 1:
						tempVecObjects[i]->Status()->SetState((int)GRIFFON_STATE::CLAWATTACK);
						break;
					}
				}
				else if (tempVecObjects[i]->GetName() == L"BlueMountainDragon") {
					printf("npc Dragon 공격\n");
					switch (rand() % 2) {
					case 0:
						tempVecObjects[i]->Status()->SetState((int)MOUNTAINDRAGON_STATE::BITE);
						break;
					case 1:
						tempVecObjects[i]->Status()->SetState((int)MOUNTAINDRAGON_STATE::CLAWATTACK);
						break;
					}
				}
				break;
			}
		}
		
		break;
	}
	case SC_PLAYER_ATTACK: {
		sc_packet_player_attack* packet = reinterpret_cast<sc_packet_player_attack*>(ptr);
		int otherID = packet->id;

		CScene* tempScene = CSceneMgr::GetInst()->GetCurScene();
		vector<CGameObject*> tempVecObjects;

		tempVecObjects = tempScene->FindLayer(L"Player")->GetObjects();

		for (UINT i = 0; i < tempVecObjects.size(); ++i) {
			if (otherID == tempVecObjects[i]->GetServerID()) {
				printf("Player 공격\n");
				tempVecObjects[i]->Status()->SetState((int)VIKING_STATE::ATTACK);
				break;
			}
		}

		break;
	}
	case SC_ENTER: {
		sc_packet_enter* packet = reinterpret_cast<sc_packet_enter*>(ptr);

		vector<pair<int, int>> vecAnimIndcies;
		CGameObject* pObject = nullptr;

		Vec3 vPos;
		vPos.x = packet->x;
		vPos.y = packet->y;
		vPos.z = packet->z;

		int id = packet->id;

		//printf("ID: %d Enter\n", id);

		if (O_PLAYER == packet->objectType) {
			Ptr<CMeshData> pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Viking.mdat", L"MeshData\\Viking.mdat");

			pObject = pMeshData->Instantiate();
			pObject->AddComponent(new CStatus);
			pObject->SetName(L"Player");
			pObject->SetServerID(id);
			pObject->FrustumCheck(false);
			pObject->Transform()->SetLocalPos(vPos);
			pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			pObject->Transform()->SetLocalRot(Vec3(0.f, XM_PI, 0.f));
			pObject->AddComponent(new COtherPlayerScript);
			pObject->MeshRender()->SetDynamicShadow(true);
			CSceneMgr::GetInst()->GetCurScene()->AddGameObject(L"Player", pObject, false);

			vecAnimIndcies.clear();
			vecAnimIndcies.reserve(8);
			vecAnimIndcies.push_back(make_pair(0, 60));
			vecAnimIndcies.push_back(make_pair(61, 93));
			vecAnimIndcies.push_back(make_pair(94, 114));
			vecAnimIndcies.push_back(make_pair(115, 160));
			vecAnimIndcies.push_back(make_pair(161, 230));
			vecAnimIndcies.push_back(make_pair(231, 250));
			vecAnimIndcies.push_back(make_pair(249, 250));
			vecAnimIndcies.push_back(make_pair(251, 307));
			pObject->Animator3D()->SetAnimFrameIndices(vecAnimIndcies);
			pObject->Status()->SetWalkSpeed(VIKING_WALK_SPEED);
			pObject->Status()->SetRunSpeed(VIKING_RUN_SPEED);
			pObject->Status()->SetState((int)VIKING_STATE::IDLE);
		}
		else if (O_DRAKKEN == packet->objectType) {
			Ptr<CMeshData> pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\MountainDragon.mdat", L"MeshData\\MountainDragon.mdat");

			pObject = pMeshData->Instantiate();
			pObject->AddComponent(new CStatus);
			pObject->SetName(L"Player");
			pObject->SetServerID(id);
			pObject->FrustumCheck(false);
			pObject->Transform()->SetLocalPos(vPos);
			pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			pObject->Transform()->SetLocalRot(Vec3(0.f, XM_PI, 0.f));

			pObject->AddComponent(new COtherPlayerScript);
			CSceneMgr::GetInst()->GetCurScene()->AddGameObject(L"Player", pObject, false);
			pObject->MeshRender()->SetDynamicShadow(true);

			vecAnimIndcies.clear();
			vecAnimIndcies.reserve(7);

			vecAnimIndcies.push_back(make_pair(0, 179));
			vecAnimIndcies.push_back(make_pair(180, 211));
			vecAnimIndcies.push_back(make_pair(212, 236));
			vecAnimIndcies.push_back(make_pair(237, 277));
			vecAnimIndcies.push_back(make_pair(278, 351));
			vecAnimIndcies.push_back(make_pair(352, 481));
			vecAnimIndcies.push_back(make_pair(482, 590));
			pObject->Animator3D()->SetAnimFrameIndices(vecAnimIndcies);
			pObject->Status()->SetWalkSpeed(DRAGON_WALK_SPEED);
			pObject->Status()->SetRunSpeed(DRAGON_RUN_SPEED);
			pObject->Status()->SetState((int)MOUNTAINDRAGON_STATE::IDLE);
		}
		else if (O_GRIFFON == packet->objectType) {
			Ptr<CMeshData> pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Griffon.mdat", L"MeshData\\Griffon.mdat");

			pObject = pMeshData->Instantiate();
			pObject->AddComponent(new CStatus);
			pObject->SetName(L"Griffon");
			pObject->SetServerID(id);
			pObject->FrustumCheck(false);
			pObject->Transform()->SetLocalPos(vPos);
			pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			pObject->AddComponent(new CGriffonScript);
			pObject->MeshRender()->SetDynamicShadow(true);
			CSceneMgr::GetInst()->GetCurScene()->AddGameObject(L"Monster", pObject, false);

			vecAnimIndcies.clear();
			vecAnimIndcies.reserve(5);
			vecAnimIndcies.push_back(make_pair(0, 60));
			vecAnimIndcies.push_back(make_pair(61, 90));
			vecAnimIndcies.push_back(make_pair(91, 120));
			vecAnimIndcies.push_back(make_pair(121, 160));
			vecAnimIndcies.push_back(make_pair(161, 250));
			pObject->Animator3D()->SetAnimFrameIndices(vecAnimIndcies);
			pObject->Status()->SetWalkSpeed(GRIFFON_WALK_SPEED);
			pObject->Status()->SetState((int)GRIFFON_STATE::WALK);		
		}

		else if (O_BARGHEST == packet->objectType) {
			Ptr<CMeshData> pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Barghest.mdat", L"MeshData\\Barghest.mdat");

			pObject = pMeshData->Instantiate();
			pObject->AddComponent(new CStatus);
			pObject->SetName(L"Barghest");
			pObject->SetServerID(id);
			pObject->FrustumCheck(false);
			pObject->Transform()->SetLocalPos(vPos);
			pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
			pObject->AddComponent(new CBarghestScript);
			CSceneMgr::GetInst()->GetCurScene()->AddGameObject(L"Monster", pObject, false);
			pObject->MeshRender()->SetDynamicShadow(true);

			vecAnimIndcies.clear();
			vecAnimIndcies.reserve(5);
			vecAnimIndcies.push_back(make_pair(0, 370));
			vecAnimIndcies.push_back(make_pair(371, 400));
			vecAnimIndcies.push_back(make_pair(402, 420));
			vecAnimIndcies.push_back(make_pair(421, 450));
			vecAnimIndcies.push_back(make_pair(451, 515));
			pObject->Animator3D()->SetAnimFrameIndices(vecAnimIndcies);
			pObject->Status()->SetWalkSpeed(BARGHEST_WALK_SPEED);
			pObject->Status()->SetRunSpeed(BARGHEST_RUN_SPEED);
			pObject->Status()->SetState((int)BARGHEST_STATE::WALK);
		}

		else if (O_DRAGON == packet->objectType) {
			Ptr < CMeshData> pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\BlueMountainDragon.mdat", L"MeshData\\BlueMountainDragon.mdat");
			pObject = pMeshData->Instantiate();
			pObject->AddComponent(new CStatus);
			pObject->SetName(L"BlueMountainDragon");
			pObject->SetServerID(id);
			pObject->FrustumCheck(false);
			pObject->Transform()->SetLocalPos(vPos);
			pObject->Transform()->SetLocalScale(Vec3(2.f, 2.f, 2.f));
			pObject->Transform()->SetLocalRot(Vec3(0.f, XM_PI, 0.f));
			pObject->MeshRender()->SetDynamicShadow(true);

			pObject->AddComponent(new CBlueMountainDragonScript);
			CSceneMgr::GetInst()->GetCurScene()->AddGameObject(L"Monster", pObject, false);

			vecAnimIndcies.clear();
			vecAnimIndcies.reserve(7);
			vecAnimIndcies.push_back(make_pair(0, 179));
			vecAnimIndcies.push_back(make_pair(180, 211));
			vecAnimIndcies.push_back(make_pair(212, 236));
			vecAnimIndcies.push_back(make_pair(237, 277));
			vecAnimIndcies.push_back(make_pair(278, 351));
			vecAnimIndcies.push_back(make_pair(352, 481));
			vecAnimIndcies.push_back(make_pair(482, 590));
			pObject->Animator3D()->SetAnimFrameIndices(vecAnimIndcies);
			pObject->Status()->SetWalkSpeed(DRAGON_WALK_SPEED);
			pObject->Status()->SetRunSpeed(DRAGON_RUN_SPEED);
			pObject->Status()->SetState((int)MOUNTAINDRAGON_STATE::IDLE);
		}
		//pObject->AddComponent(new CTransform);
		//pObject->AddComponent(new CMeshRender);
		//pObject->AddComponent(new CCollider2D);

		// Collider2D 설정	
		//pObject->Collider2D()->SetCollider2DType(COLLIDER2D_TYPE::RECT);
		//pObject->Collider2D()->SetOffsetScale(Vec3(1.f, 1.f, 1.f));

		///////////////////////////////////////////

		//////////////////////////////////////////

		// AddGameObject

		break;
	}
	case SC_NPC_DIE: {
		sc_packet_npc_die* packet = reinterpret_cast<sc_packet_npc_die*>(ptr);
		// 몬스터 죽었다.
		CScene* tempScene = CSceneMgr::GetInst()->GetCurScene();
		vector<CGameObject*> tempVecObjects = tempScene->FindLayer(L"Monster")->GetObjects();;
		int id = packet->id;
		char objType = packet->objectType;

		for (UINT i = 0; i < tempVecObjects.size(); ++i) {
			if (id == tempVecObjects[i]->GetServerID()) {
				//std::cout << "죽인다." << std::endl;
				//tempVecObjects[i]->SetDead();
				tempVecObjects[i]->SetDying(true);

				std::cout << "Bar: " << CSceneMgr::GetInst()->GetQuestObj()->GetScript<CQuestScript>()->GetBarghestNum() << std::endl;
				std::cout << "Gri: " << CSceneMgr::GetInst()->GetQuestObj()->GetScript<CQuestScript>()->GetGriffonNum() << std::endl;

				if (objType == O_GRIFFON)
				{
					CSceneMgr::GetInst()->GetQuestObj()->GetScript<CQuestScript>()->SetGriffonNum(CSceneMgr::GetInst()->GetQuestObj()->GetScript<CQuestScript>()->GetGriffonNum() + 1);
					//CSceneMgr::GetInst()->GetQuestObj()->GetScript<CQuestScript>()->UpdateFont();
				}
				else if (objType == O_BARGHEST)
				{
					CSceneMgr::GetInst()->GetQuestObj()->GetScript<CQuestScript>()->SetBarghestNum(CSceneMgr::GetInst()->GetQuestObj()->GetScript<CQuestScript>()->GetBarghestNum() + 1);
					//CSceneMgr::GetInst()->GetQuestObj()->GetScript<CQuestScript>()->UpdateFont();
				}
				else if (objType == O_DRAGON)
				{
					CSceneMgr::GetInst()->GetQuestObj()->GetScript<CQuestScript>()->SetDragonNum(CSceneMgr::GetInst()->GetQuestObj()->GetScript<CQuestScript>()->GetDragonNum() + 1);
					//CSceneMgr::GetInst()->GetQuestObj()->GetScript<CQuestScript>()->UpdateFont();
				}
				CSceneMgr::GetInst()->GetQuestObj()->GetScript<CQuestScript>()->UpdateFont();



				break;
			}
		}
		break;
	}
	case SC_PLAYER_DIE: {
		sc_packet_player_die* packet = reinterpret_cast<sc_packet_player_die*>(ptr);

		int otherID = packet->id;

		CScene* tempScene = CSceneMgr::GetInst()->GetCurScene();
		vector<CGameObject*> tempVecObjects = tempScene->FindLayer(L"Player")->GetObjects();

		for (UINT i = 0; i < tempVecObjects.size(); ++i) {
			if (otherID == tempVecObjects[i]->GetServerID()) {
				if (otherID == m_myID) {
					printf("나 죽음\n");
					// Hp는 50으로 만들고, exp는 현재exp - (레벨*50)
					tempVecObjects[i]->Status()->SetHP(50);
					short level = (short)tempVecObjects[i]->Status()->GetLevel();
					short exp = (short)tempVecObjects[i]->Status()->GetExp();
					short changeExp = exp - (level * 50);
					if (0 > changeExp) changeExp = 0;
					tempVecObjects[i]->GetScript<CVikingScript>()->SetPlayerFakeDeath((int)PLAYER_FAKE_DEATH::DYING);
					tempVecObjects[i]->Status()->SetExp(changeExp);
					tempVecObjects[i]->Status()->SetHP(50);
				}
				else {
					// 다른 플레이어 죽음
					printf("다른놈 죽음\n");
					tempVecObjects[i]->SetDying(true);
				}
			}
		}

		break;
	}
	case SC_TRANSFORM: {
		sc_packet_transform* packet = reinterpret_cast<sc_packet_transform*>(ptr);
	
		int otherID = packet->id;
		bool kind = packet->kind;
		CScene* tempScene = CSceneMgr::GetInst()->GetCurScene();
		vector<CGameObject*> tempVecObjects = tempScene->FindLayer(L"Player")->GetObjects();

		for (UINT i = 0; i < tempVecObjects.size(); ++i) {
			if (otherID == tempVecObjects[i]->GetServerID()) {
				if (m_myID == otherID) {	// 내가 변신한 거라면 체력 통만 바꿔주면 된다. (용->사람은 여기서 적용)
					if (true == kind) {	// 사람 -> 용
						tempVecObjects[i]->Status()->SetMaxHP(tempVecObjects[i]->Status()->GetMaxHP() * 2);
						if (tempVecObjects[i]->Status()->GetHP() * 2 + 100 < tempVecObjects[i]->Status()->GetMaxHP())
							tempVecObjects[i]->Status()->SetHP(tempVecObjects[i]->Status()->GetHP() * 2 + 100);
						else 
							tempVecObjects[i]->Status()->SetHP(tempVecObjects[i]->Status()->GetMaxHP());
						tempVecObjects[i]->GetScript<CVikingScript>()->SetPlayerKind((int)PLAYER_KIND::MOUNTAIN_DRAGON);

						break;
					}
					else if (false == kind) {	// 용 -> 사람
						tempVecObjects[i]->ChangeMeshData(L"MeshData\\Viking.mdat", L"MeshData\\Viking.mdat");
						tempVecObjects[i]->Status()->SetState((int)MOUNTAINDRAGON_STATE::IDLE);
						tempVecObjects[i]->Status()->SetMaxHP(tempVecObjects[i]->Status()->GetMaxHP() / 2);
						tempVecObjects[i]->Status()->SetHP(tempVecObjects[i]->Status()->GetHP() / 2);
						CSceneMgr::GetInst()->GetPlayerCamera()->GetScript<CPlayerCameraScript>()->SetOffset(VEC3_PLAYERCAMERA_VIKING_OFFSET);
						tempVecObjects[i]->GetScript<CVikingScript>()->SetPlayerKind((int)PLAYER_KIND::VIKING);

						break;
					}
				}
				else {	// 다른 사람이 변신한 거니까 해당 플레이어를 용이나 사람으로 변신 시킨다.
					if (true == kind) {	// 사람 -> 용
						tempVecObjects[i]->ChangeMeshData(L"MeshData\\MountainDragon.mdat", L"MeshData\\MountainDragon.mdat");
						tempVecObjects[i]->Status()->SetState((int)MOUNTAINDRAGON_STATE::IDLE);
						break;
					}
					else if (false == kind) { // 용 => 사람
						tempVecObjects[i]->ChangeMeshData(L"MeshData\\Viking.mdat", L"MeshData\\Viking.mdat");
						tempVecObjects[i]->Status()->SetState((int)VIKING_STATE::IDLE);
						break;
					}
				}
			}
		}
	
		break;
	}
	case SC_DEFENCE: {
		sc_packet_defence* packet = reinterpret_cast<sc_packet_defence*>(ptr);

		int otherID = packet->id;
		bool kind = packet->kind;

		CScene* tempScene = CSceneMgr::GetInst()->GetCurScene();
		vector<CGameObject*> tempVecObjects;

		tempVecObjects = tempScene->FindLayer(L"Player")->GetObjects();

		for (UINT i = 0; i < tempVecObjects.size(); ++i) {
			if (otherID == tempVecObjects[i]->GetServerID()) {
				if (true == kind) { // 방어
					printf("Player 방어\n");
					tempVecObjects[i]->Status()->SetState((int)VIKING_STATE::DEFEND);
				}
				else if (false == kind) { // 방어 품
					printf("Player 방어 해제\n");
					tempVecObjects[i]->Status()->SetState((int)VIKING_STATE::IDLE);
				}
				break;
			}
		}

		break;
	}
	case SC_LEAVE: {
		sc_packet_leave* packet = reinterpret_cast<sc_packet_leave*>(ptr);

		int id = packet->id;

		//printf("%d번 떠남 / 타입: %d\n", id, packet->objectType);

		CScene* tempScene = CSceneMgr::GetInst()->GetCurScene();
		vector<CGameObject*> tempVecObjects;

		if (O_PLAYER == packet->objectType || O_DRAKKEN == packet->objectType) {
			tempVecObjects = tempScene->FindLayer(L"Player")->GetObjects();
		}
		else {
			tempVecObjects = tempScene->FindLayer(L"Monster")->GetObjects();
		}

		for (UINT i = 0; i < tempVecObjects.size(); ++i) {
			if (id == tempVecObjects[i]->GetServerID()) {
				//std::cout << "시야에서 사라졌다!" << std::endl;
				tempVecObjects[i]->SetDead();
				//tempVecObjects[i]->SetDying(true);
				break;
			}
		}
		break;
	}
	case SC_STAT_CHANGE: {
		sc_packet_stat_change* packet = reinterpret_cast<sc_packet_stat_change*>(ptr);

		CScene* tempScene = CSceneMgr::GetInst()->GetCurScene();
		vector<CGameObject*> tempVecObjects;

		tempVecObjects = tempScene->FindLayer(L"Player")->GetObjects();

		for (UINT i = 0; i < tempVecObjects.size(); ++i) {
			if (m_myID == tempVecObjects[i]->GetServerID()) {
				tempVecObjects[i]->Status()->SetExp(packet->c_exp);
				tempVecObjects[i]->Status()->SetMaxExp(packet->m_exp);
				tempVecObjects[i]->Status()->SetHP(packet->c_hp);
				tempVecObjects[i]->Status()->SetMaxHP(packet->m_hp);
				tempVecObjects[i]->Status()->SetLevel(packet->level);
				CSceneMgr::GetInst()->GetLevelObj()->GetScript<CLevelScript>()->SetLevelNum(packet->level);
				CSceneMgr::GetInst()->GetLevelObj()->GetScript<CLevelScript>()->UpdateFont();
				// 레벨

				break;
			}
		}
		
		break;
	}
	case SC_CHAT: {
		sc_packet_chat* packet = reinterpret_cast<sc_packet_chat*>(ptr);

		break;
	}
	default:
		printf("Unknown PACKET type [%d]\n", ptr[1]);
		DebugBreak();
		exit(-1);
	}
}


