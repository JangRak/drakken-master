#include "stdafx.h"
#include "QuestNPCScript.h"
#include "Animator3D.h"
#include "Status.h"
CQuestNPCScript::CQuestNPCScript()
	: CScript((UINT)SCRIPT_TYPE::QUESTNPCSCRIPT)

{
}

CQuestNPCScript::~CQuestNPCScript()
{
}

void CQuestNPCScript::awake()
{
}

void CQuestNPCScript::update()
{
	QUESTNPC_STATE tempState = (QUESTNPC_STATE)Status()->GetState();
	if (GetIsDying())
	{
		SetDead();
	}
	Status()->SetState((int)tempState);

	Animator3D()->SetCurAnimIdx(Status()->GetState());
}
