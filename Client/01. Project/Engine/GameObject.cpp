#include "stdafx.h"
#include "GameObject.h"

#include "Component.h"
//#include "MeshRender.h"
//#include "Transform.h"
#include "Script.h"

#include "Animator3D.h"
#include "Status.h"

#include "Layer.h"
#include "Scene.h"
#include "SceneMgr.h"

#include "VikingScript.h"

CGameObject::CGameObject()
	: m_arrCom{}
	, m_pParentObj(nullptr)
	, m_iLayerIdx(-1)
	, m_bDead(false)
	, m_bActive(true)
	, m_bFrustumCheck(true)
	, m_bDying(false)
{
}

void CGameObject::ChangeMeshData(const wstring & _strKey, const wstring & _strPath)
{
	//Ptr<CMeshData> pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\MountainDragon.fbx");
	//pMeshData->Save(pMeshData->GetPath());
	//Ptr<CMeshData> pMeshData = CResMgr::GetInst()->Load<CMeshData>(_strKey, _strKey);
	//CGameObject* pGameObject = pMeshData->Instantiate();

	CGameObject* pGameObject = nullptr;
	if (this->GetScript<CVikingScript>()->GetPlayerKind() == (int)PLAYER_KIND::VIKING)
	{
		pGameObject = CSceneMgr::GetInst()->GetDragonObj();
	}
	else if (this->GetScript<CVikingScript>()->GetPlayerKind() == (int)PLAYER_KIND::MOUNTAIN_DRAGON)
	{
		pGameObject = CSceneMgr::GetInst()->GetVikingObj();
	}


	// MeshRender
	//this->MeshRender()->DeleteMesh();
	//this->MeshRender()->DeleteMaterial();
	this->MeshRender()->MakeMeshNull();
	this->MeshRender()->MakeMaterialNull();

	this->MeshRender()->SetMesh(pGameObject->MeshRender()->GetMesh());
	for (size_t i = 0; i < pGameObject->MeshRender()->GetMaterialCount(); i++)
	{
		this->MeshRender()->SetMaterial(pGameObject->MeshRender()->GetCloneMaterial(i), i);
	}


	// Animator3D
	this->Animator3D()->SetAnimClip(pGameObject->Animator3D()->GetAnimClip());
	this->Animator3D()->SetBones(pGameObject->Animator3D()->GetBones());



	if (_strKey == L"MeshData\\MountainDragon.mdat")
	{
		vector<pair<int, int>> vecAnimIndcies;

		vecAnimIndcies.clear();
		vecAnimIndcies.reserve(7);
		vecAnimIndcies.push_back(make_pair(0, 179));
		vecAnimIndcies.push_back(make_pair(180, 211));
		vecAnimIndcies.push_back(make_pair(212, 236));
		vecAnimIndcies.push_back(make_pair(237, 277));
		vecAnimIndcies.push_back(make_pair(278, 351));
		vecAnimIndcies.push_back(make_pair(352, 481));
		vecAnimIndcies.push_back(make_pair(482, 590));

		//vecAnimIndcies.push_back(make_pair(0, 179));
		//vecAnimIndcies.push_back(make_pair(180, 289));
		//vecAnimIndcies.push_back(make_pair(290, 419));
		//vecAnimIndcies.push_back(make_pair(420, 494));
		//vecAnimIndcies.push_back(make_pair(495, 534));
		//vecAnimIndcies.push_back(make_pair(535, 564));
		//vecAnimIndcies.push_back(make_pair(564, 590));


		this->Animator3D()->SetAnimFrameIndices(vecAnimIndcies);
		this->Status()->SetWalkSpeed(DRAGON_WALK_SPEED);
		this->Status()->SetRunSpeed(DRAGON_RUN_SPEED);



	} 
	else if (_strKey == L"MeshData\\Viking.mdat")
	{
		
		vector<pair<int, int>> vecAnimIndcies;
		vecAnimIndcies.clear();
		vecAnimIndcies.reserve(7);
		vecAnimIndcies.push_back(make_pair(0, 60));
		vecAnimIndcies.push_back(make_pair(61, 93));	// WALK
		vecAnimIndcies.push_back(make_pair(94, 112));	// RUN
		vecAnimIndcies.push_back(make_pair(113, 160));
		vecAnimIndcies.push_back(make_pair(161, 230));
		vecAnimIndcies.push_back(make_pair(231, 250));
		vecAnimIndcies.push_back(make_pair(249, 250));
		vecAnimIndcies.push_back(make_pair(251, 307));

		this->Animator3D()->SetAnimFrameIndices(vecAnimIndcies);
		this->Status()->SetWalkSpeed(VIKING_WALK_SPEED);
		this->Status()->SetRunSpeed(VIKING_RUN_SPEED);
	}
}

CGameObject::CGameObject(const CGameObject & _origin)
	: CEntity(_origin)
	, m_arrCom{}
	, m_iLayerIdx(-1)
	, m_bDead(false)
	, m_bActive(true)
	, m_bFrustumCheck(_origin.m_bFrustumCheck)
{
	for (UINT i = 0; i < (UINT)COMPONENT_TYPE::END; ++i)
	{
		if (nullptr != _origin.m_arrCom[i])
		{
			AddComponent(_origin.m_arrCom[i]->Clone());
		}
	}
}


CGameObject::~CGameObject()
{
	Safe_Delete_Array(m_arrCom);
	Safe_Delete_Vector(m_vecChild);
	Safe_Delete_Vector(m_vecScript);

}

void CGameObject::AddChild(CGameObject * _pChildObj)
{
	// 예외 1
	// 자기자신이 자식이 되는 경우
	assert(!(this == _pChildObj));
	
	// 예외 2
	// 자식으로 들어오는 오브젝트가 부모 계층 오브젝트인 경우
	assert(!IsAncestor(_pChildObj));

	// 예외 3
	// 자식 오브젝트가 이전에 다른 부모 오브젝트가 있었다면
	_pChildObj->ClearParent(this);

	// 자식 오브젝트가 Layer에 포함되어있지 않은 경우
	// 부모 오브젝트의 Layer로 지정한다.
	if (-1 == _pChildObj->m_iLayerIdx)
	{
		_pChildObj->m_iLayerIdx = m_iLayerIdx;
	}
}
void CGameObject::AddComponent(CComponent * _pCom)
{
	COMPONENT_TYPE eType = _pCom->GetComponentType();

	if (COMPONENT_TYPE::SCRIPT == eType)
	{
		m_vecScript.push_back((CScript*)_pCom);
	}
	else
	{
		m_arrCom[(UINT)eType] = _pCom;
	}

	_pCom->SetGameObject(this);
}

bool CGameObject::IsAncestor(CGameObject * _pObj)
{
	CGameObject* pParent = m_pParentObj;

	while (nullptr != pParent)
	{
		if (pParent == _pObj)
		{
			return true;
		}
		pParent = pParent->m_pParentObj;
	}

	return false;
}


void CGameObject::ClearParent(CGameObject * _pNextParent)
{
	// 부모가 없는 경우
	if (nullptr == m_pParentObj)
	{
		// 다음 부모가 지정된 경우
		if (_pNextParent)
		{
			_pNextParent->m_vecChild.push_back(this);
			m_pParentObj = _pNextParent;
		}
		else // 부모도 없고, 다음 부모도 없고
			return;

		// 최상위 부모 오브젝트,
		// 다음 부모가 지정됨 (Layer ParentList 에서 빠짐)
		if (-1 != m_iLayerIdx)
		{
			CLayer* pCurLayer = CSceneMgr::GetInst()->GetCurScene()->GetLayer(m_iLayerIdx);
			pCurLayer->CheckParentObj();
		}
	}
	else // 부모가 있는경우
	{
		// 부모로 부터 자신을 제거
		vector<CGameObject*>::iterator iter = m_pParentObj->m_vecChild.begin();
		for (size_t i = 0; iter != m_pParentObj->m_vecChild.end(); ++iter, ++i)
		{
			if ((*iter) == m_pParentObj->m_vecChild[i])
			{
				m_pParentObj->m_vecChild.erase(iter);
				break;
			}
		}

		// 다음 부모가 지정된 경우 해당 부모로 들어간다.
		if (_pNextParent)
		{
			_pNextParent->m_vecChild.push_back(this);
			m_pParentObj = _pNextParent;
		}
		else // 최상위 오브젝트가 됨
		{
			m_pParentObj = nullptr;

			if (!m_bDead)
			{
				CLayer* pCurLayer = CSceneMgr::GetInst()->GetCurScene()->GetLayer(m_iLayerIdx);
				m_iLayerIdx = -1;
				pCurLayer->AddGameObject(this);
			}
		}
	}
}

void CGameObject::awake()
{
	for (UINT i = 0; i < (UINT)COMPONENT_TYPE::END; ++i)
	{
		if (nullptr != m_arrCom[i])
			m_arrCom[i]->awake();
	}

	for (size_t i = 0; i < m_vecChild.size(); ++i)
	{
		if (m_vecChild[i]->IsActive())
			m_vecChild[i]->awake();
	}

	for (size_t i = 0; i < m_vecScript.size(); ++i)
	{
		m_vecScript[i]->awake();
	}
}

void CGameObject::start()
{
	for (UINT i = 0; i < (UINT)COMPONENT_TYPE::END; ++i)
	{
		if (nullptr != m_arrCom[i])
		{
			m_arrCom[i]->start();
		}
	}
	for (size_t i = 0; i < m_vecChild.size(); ++i)
	{
		if (m_vecChild[i]->IsActive())
		{
			m_vecChild[i]->start();
		}
	}

	for (size_t i = 0; i < m_vecScript.size(); ++i)
	{
		m_vecScript[i]->start();
	}
}

void CGameObject::update()
{
	for (UINT i = 0; i < (UINT)COMPONENT_TYPE::END; ++i)
	{
		if (nullptr != m_arrCom[i])
		{
			m_arrCom[i]->update();
		}
	}
	for (size_t i = 0; i < m_vecChild.size(); ++i)
	{
		if (m_vecChild[i]->IsActive())
		{
			m_vecChild[i]->update();
		}
	}

	for (size_t i = 0; i < m_vecScript.size(); ++i)
	{
		m_vecScript[i]->update();
	}
}

void CGameObject::lateupdate()
{
	for (UINT i = 0; i < (UINT)COMPONENT_TYPE::END; ++i)
	{
		if (nullptr != m_arrCom[i])
		{
			m_arrCom[i]->lateupdate();
		}
	}
	for (size_t i = 0; i < m_vecChild.size(); ++i)
	{
		if (m_vecChild[i]->IsActive())
		{
			m_vecChild[i]->lateupdate();
		}
	}

	for (size_t i = 0; i < m_vecScript.size(); ++i)
	{
		m_vecScript[i]->lateupdate();
	}
}

void CGameObject::finalupdate()
{
	for (UINT i = 0; i < (UINT)COMPONENT_TYPE::END; ++i)
	{
		if (nullptr != m_arrCom[i])
		{
			m_arrCom[i]->finalupdate();
		}
	}
	for (size_t i = 0; i < m_vecChild.size(); ++i)
	{
		if (m_vecChild[i]->IsActive())
		{
			m_vecChild[i]->finalupdate();
		}
	}
}

void CGameObject::enable()
{
	for (UINT i = 0; i < (UINT)COMPONENT_TYPE::END; ++i)
	{
		if (nullptr != m_arrCom[i] && m_arrCom[i]->IsActive())
		{
			m_arrCom[i]->enable();
		}
	}
	for (size_t i = 0; i < m_vecChild.size(); ++i)
	{
		m_vecChild[i]->enable();
	}
	for (size_t i = 0; i < m_vecScript.size(); ++i)
	{
		if (m_vecScript[i]->IsActive())
		{
			m_vecScript[i]->enable();
		}
	}
}

void CGameObject::disable()
{
	for (UINT i = 0; i < (UINT)COMPONENT_TYPE::END; ++i)
	{
		if (nullptr != m_arrCom[i] && m_arrCom[i]->IsActive())
			m_arrCom[i]->disable();
	}

	for (size_t i = 0; i < m_vecChild.size(); ++i)
	{
		m_vecChild[i]->disable();
	}

	for (size_t i = 0; i < m_vecScript.size(); ++i)
	{
		if (m_vecScript[i]->IsActive())
			m_vecScript[i]->disable();
	}
}

void CGameObject::SetActive(bool _bTrue)
{
	if (m_bActive)
	{
		if (!_bTrue)
		{
			// 비활성화
			tEvent event = {};
			event.eType = EVENT_TYPE::DEACTIVATE_GAMEOBJECT;
			event.wParam = (DWORD_PTR)this;

			CEventMgr::GetInst()->AddEvent(event);
		}
	}
	else
	{
		if (_bTrue)
		{
			// 활성화
			// 비활성화
			tEvent event = {};
			event.eType = EVENT_TYPE::ACTIVATE_GAMEOBJECT;
			event.wParam = (DWORD_PTR)this;

			CEventMgr::GetInst()->AddEvent(event);
		}
	}
}

void CGameObject::SetDead()
{
	m_bDead = true;

	for (size_t i = 0; i < m_vecChild.size(); ++i)
	{
		m_vecChild[i]->SetDead();
	}
}

void CGameObject::RegisterToLayer()
{
	// layer 에 등록
	assert(-1 != m_iLayerIdx);

	CLayer* pCurLayer = CSceneMgr::GetInst()->GetCurScene()->GetLayer(m_iLayerIdx);
	pCurLayer->RegisterObj(this);

	for (size_t i = 0; i < m_vecChild.size(); ++i)
	{
		m_vecChild[i]->RegisterToLayer();
	}
}
