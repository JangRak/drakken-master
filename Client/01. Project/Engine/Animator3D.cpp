#include "stdafx.h"
#include "Animator3D.h"

#include "TimeMgr.h"
#include "MeshRender.h"
#include "StructuredBuffer.h"
#include "ResMgr.h"


CAnimator3D::CAnimator3D()
	: m_iCurClip(0)
	, m_dCurTime(0.)
	, m_iFrameCount(30)
	, m_pBoneFinalMat(nullptr)
	, m_bFinalMatUpdate(false)
	, m_iCurAnimIdx(0)
	, m_bIsEnd(false)
	, CComponent(COMPONENT_TYPE::ANIMATOR3D)
{
	m_pBoneMtrl = CResMgr::GetInst()->FindRes<CMaterial>(L"Animation3DUpdateMtrl");
	m_pBoneFinalMat = new CStructuredBuffer;
}

CAnimator3D::~CAnimator3D()
{
	SAFE_DELETE(m_pBoneFinalMat);
}

void CAnimator3D::update()
{
}

void CAnimator3D::lateupdate()
{
}

void CAnimator3D::finalupdate()
{
	m_dCurTime = 0.;

	 //현재 재생중인 Clip의 시간을 누적한다.
	m_vecClipUpdateTime[m_iCurClip] += DT;

	//if (m_vecClipUpdateTime[m_iCurClip] >= m_pVecClip->at(m_iCurClip).dTimeLength)
	//{
	//	m_vecClipUpdateTime[m_iCurClip] = 0.f;
	//}

	//m_dCurTime = m_pVecClip->at(m_iCurClip).dStartTime + m_vecClipUpdateTime[m_iCurClip];

	//m_iFrameIdx = (int)(m_dCurTime * (float)m_iFrameCount);

	if (m_vecClipUpdateTime[m_iCurClip] >= GetCurAnimLength())
	{
		m_vecClipUpdateTime[m_iCurClip] = 0.f;
		m_bIsEnd = true;
	}
	else
	{
		m_bIsEnd = false;
	}

	m_dCurTime = GetCurAnimStartTime() + m_vecClipUpdateTime[m_iCurClip];

	double dFrameIdx = m_dCurTime * (double)m_iFrameCount;
	m_iFrameIdx = (int)(dFrameIdx);

	// 다음 프레임 인덱스
	if (m_iFrameIdx >= m_pVecClip->at(0).iFrameLength - 1)
		m_iNextFrameIdx = m_iFrameIdx;	// 끝이면 현재 인덱스를 유지
	else
		m_iNextFrameIdx = m_iFrameIdx + 1;

	// 프레임간의 시간에 따른 비율을 구해준다.
	m_fRatio = (float)(dFrameIdx - (double)m_iFrameIdx);

	m_bFinalMatUpdate = false;
}

void CAnimator3D::SetAnimClip(const vector<tMTAnimClip>* _vecAnimClip)
{
	m_pVecClip = _vecAnimClip;
	m_vecClipUpdateTime.resize(m_pVecClip->size());

	static float fTime = 0.f;
	fTime += 1.f;
	m_vecClipUpdateTime[0] = fTime;

}

void CAnimator3D::UpdateData()
{
	CGameObject* obj = this->GetObj();

	if (!m_bFinalMatUpdate)
	{
		// Bone Data Update
		Ptr<CMesh> pMesh = MeshRender()->GetMesh();
		pMesh->GetBoneFrameDataBuffer()->UpdateData_CS(TEXTURE_REGISTER::t10);
		pMesh->GetBoneOffsetBuffer()->UpdateData_CS(TEXTURE_REGISTER::t11);

		check_mesh(pMesh);
		m_pBoneFinalMat->UpdateRWData(UAV_REGISTER::u0);

		UINT iBoneCount = (UINT)m_pVecBones->size();
		UINT iRow = 0;


		m_pBoneMtrl->SetData(SHADER_PARAM::INT_0, &iBoneCount);
		m_pBoneMtrl->SetData(SHADER_PARAM::INT_1, &m_iFrameIdx);
		m_pBoneMtrl->SetData(SHADER_PARAM::INT_2, &m_iNextFrameIdx);
		m_pBoneMtrl->SetData(SHADER_PARAM::INT_3, &iRow);
		m_pBoneMtrl->SetData(SHADER_PARAM::FLOAT_0, &m_fRatio);


		UINT iGrounX = (iBoneCount / 256) + 1;
		m_pBoneMtrl->Dispatch(iGrounX, 1, 1);

		m_bFinalMatUpdate = true;
	}

	// t7 레지스터에 최종행렬 데이터(구조버퍼) 바인딩
	m_pBoneFinalMat->UpdateData(TEXTURE_REGISTER::t7);
}

void CAnimator3D::UpdateData_Inst(CStructuredBuffer * _pBoneBuffer, UINT _iRow)
{
	if (!m_bFinalMatUpdate)
	{
		// Bone Data Update	
		Ptr<CMesh> pMesh = MeshRender()->GetMesh();
		pMesh->GetBoneFrameDataBuffer()->UpdateData_CS(TEXTURE_REGISTER::t10);
		pMesh->GetBoneOffsetBuffer()->UpdateData_CS(TEXTURE_REGISTER::t11);

		check_mesh(pMesh);
		_pBoneBuffer->UpdateRWData(UAV_REGISTER::u0);

		UINT iBoneCount = (UINT)m_pVecBones->size();
		m_pBoneMtrl->SetData(SHADER_PARAM::INT_0, &iBoneCount);
		m_pBoneMtrl->SetData(SHADER_PARAM::INT_1, &m_iFrameIdx);
		m_pBoneMtrl->SetData(SHADER_PARAM::INT_3, &_iRow);
		m_pBoneMtrl->SetData(SHADER_PARAM::FLOAT_0, &m_fRatio);

		UINT iGrounX = (iBoneCount / 256) + 1;
		m_pBoneMtrl->Dispatch(iGrounX, 1, 1);

		m_bFinalMatUpdate = true;
	}
}

void CAnimator3D::SetAnimFrameIndices(vector<pair<int, int>> _vecAnimFrameIdices)
{
	m_vecAnimFrameIdices.clear();
	m_vecAnimFrameIdices.reserve(_vecAnimFrameIdices.size());
	m_vecAnimFrameIdices = _vecAnimFrameIdices;
}

const double CAnimator3D::GetCurAnimStartTime() const
{
	double dEntireAnimLength = m_pVecClip->at(m_iCurClip).dTimeLength;
	double dAnimStartFrame = m_vecAnimFrameIdices[m_iCurAnimIdx].first;
	double dAnimFrameLength = m_vecAnimFrameIdices.back().second;
	double dCurAnimStartTime = dEntireAnimLength * dAnimStartFrame / dAnimFrameLength;
	return dCurAnimStartTime;
}

const double CAnimator3D::GetCurAnimLength() const
{
	double dEntireAnimLength = m_pVecClip->at(m_iCurClip).dTimeLength;
	int iCurAnimLength = m_vecAnimFrameIdices[m_iCurAnimIdx].second - m_vecAnimFrameIdices[m_iCurAnimIdx].first + 1;
	double dCurAnimLength = m_pVecClip->at(m_iCurClip).dTimeLength * iCurAnimLength / m_vecAnimFrameIdices.back().second;
	return dCurAnimLength;
}

void CAnimator3D::InitCurAnimFrame()
{
	m_vecClipUpdateTime[m_iCurClip] = 0.f;
}

void CAnimator3D::check_mesh(Ptr<CMesh> _pMesh)
{
	UINT iBoneCount = _pMesh->GetBoneCount();
	if (m_pBoneFinalMat->GetElementCount() < iBoneCount)
	{
		m_pBoneFinalMat->Create(sizeof(Matrix), iBoneCount, nullptr);
	}
}



void CAnimator3D::SaveToScene(FILE * _pFile)
{
}

void CAnimator3D::LoadFromScene(FILE * _pFile)
{
}

