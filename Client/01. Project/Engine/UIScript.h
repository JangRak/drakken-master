#pragma once
#include "Script.h"
class CUIScript :
	public CScript
{
private:
	CGameObject* m_pHP;
	CGameObject* m_pExp;

private:
	float m_fMaxLength;
	float m_fFirstXPos;
public:
	void Init();

public:
	virtual void awake();
	virtual void update();


public:
	//void SetHPObject(CGameObject* _pHP);
	//void SetMPObject(CGameObject* _pMP);
	void SetHP(float MaxHP, float CurHP);
	void SetExp(float MaxMP, float CurMP);


public:
	CLONE(CUIScript);


public:
	CUIScript();
	virtual ~CUIScript();
};

