#include "stdafx.h"
#include "LevelScript.h"
#include "Texture.h"
#include "FontMgr.h"
#include "RenderMgr.h"

CLevelScript::CLevelScript()
	: CScript((UINT)SCRIPT_TYPE::LEVELSCRIPT)
	, m_iLevelNum(0)
{
	m_strLevelNum.clear();
	m_strLevelNum = std::to_wstring(m_iLevelNum);
}

CLevelScript::~CLevelScript()
{
}


void CLevelScript::SetLevelNum(int _iNum)
{
	m_iLevelNum = _iNum;
	m_strLevelNum.clear();
	m_strLevelNum = std::to_wstring(_iNum);
}



void CLevelScript::Init(Vec4 vFontColor, Vec4 vFontBackColor)
{
	m_vecLevelNumFonts.reserve(20);


	//m_strValue = str;
	m_vFontColor = vFontColor;
	m_vFontBackColor = vFontBackColor;


	tResolution res = CRenderMgr::GetInst()->GetResolution();

	m_pLevelStatus = new CGameObject;
	m_pLevelStatus->SetName(L"Level_Status");
	m_pLevelStatus->AddComponent(new CTransform);
	m_pLevelStatus->Transform()->SetLocalPos(Vec3((- res.fWidth / 2.f ) + 120.f, res.fHeight / 2 - 50.f, 1.f));
	m_pLevelStatus->Transform()->SetLocalScale(Vec3(100.f, 80.f, 1.f));

	m_pLevelStatus->FrustumCheck(false);
	CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(m_pLevelStatus);


	//UpdateFont();
}

void CLevelScript::awake()
{
}

void CLevelScript::update()
{
}

void CLevelScript::UpdateFont()
{
	Ptr<CTexture> pFontText = CFontMgr::GetInst()->GetFontTex();

	Vec3 vPos = m_pLevelStatus->Transform()->GetLocalPos();

	Vec3 vScale = m_pLevelStatus->Transform()->GetLocalScale();


	/////////////////
	// Level Num Font
	/////////////////

	for (int i = 0; i < m_vecLevelNumFonts.size(); ++i)
	{
		m_vecLevelNumFonts[i]->SetDead();
	}
	m_vecLevelNumFonts.clear();


	for (int i = 0; i < m_strLevelNum.size(); ++i)
	{
		CGameObject * pFont = new CGameObject();

		pFont->AddComponent(new CTransform);
		pFont->AddComponent(new CMeshRender);
		pFont->Transform()->SetLocalPos(Vec3(vPos.x + (vScale.x * i), vPos.y, 1.f));
		//pFont->Transform()->SetLocalScale(Vec3(10.f, 20.f, 1.f));
		pFont->Transform()->SetLocalScale(Vec3(vScale.x, vScale.y, 1.f));

		pFont->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
		pFont->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"FontMtrl"));
		pFont->MeshRender()->GetCloneMaterial()->SetData(SHADER_PARAM::TEX_0, pFontText.GetPointer());

		CharInfo tInfo = CFontMgr::GetInst()->GetFontInfo().mCharInfo[m_strLevelNum[i]];
		Ptr<CMaterial> pMtrl = pFont->MeshRender()->GetSharedMaterial();
		float sizeX = (float)CFontMgr::GetInst()->GetFontInfo().iScaleX;
		float sizeY = (float)CFontMgr::GetInst()->GetFontInfo().iScaleY;
		float startU = tInfo.ix / sizeX;
		float startV = tInfo.iy / sizeY;
		float widthU = tInfo.iWidth / sizeX;
		float HeightV = tInfo.iHeight / sizeY;

		pMtrl->SetData(SHADER_PARAM::FLOAT_0, &startU);
		pMtrl->SetData(SHADER_PARAM::FLOAT_1, &widthU);
		pMtrl->SetData(SHADER_PARAM::FLOAT_2, &startV);
		pMtrl->SetData(SHADER_PARAM::FLOAT_3, &HeightV);
		pMtrl->SetData(SHADER_PARAM::VEC4_0, &m_vFontColor);
		pMtrl->SetData(SHADER_PARAM::VEC4_1, &m_vFontBackColor);

		m_vecLevelNumFonts.push_back(pFont);
		CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"UI")->AddGameObject(pFont);
	}





}

