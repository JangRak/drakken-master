#include "stdafx.h"
#include "PlayerScript.h"

#include "NetworkMgr.h"

#include "BulletScript.h"
#include "Animator3D.h"
#include "Status.h"


CPlayerScript::CPlayerScript()
	: CScript((UINT)SCRIPT_TYPE::PLAYERSCRIPT)
	, m_pOriginMtrl(nullptr)
	, m_pCloneMtrl(nullptr)
{
}

CPlayerScript::~CPlayerScript()
{
}

void CPlayerScript::awake()
{
	m_pOriginMtrl = MeshRender()->GetSharedMaterial();
	m_pCloneMtrl = m_pOriginMtrl->Clone();

	int a = 1;
	m_pCloneMtrl->SetData(SHADER_PARAM::INT_0, &a);
}

void CPlayerScript::update()
{
	Vec3 vPos = Transform()->GetLocalPos();
	Vec3 vRot = Transform()->GetLocalRot();

	Matrix matRot = XMMatrixRotationX(vRot.x);
	matRot *= XMMatrixRotationY(vRot.y);
	matRot *= XMMatrixRotationZ(vRot.z);

	Vec3 vLook = XMVector3TransformNormal(Vec3::Front, matRot);
	Vec3 vRight = XMVector3TransformNormal(Vec3::Right, matRot);
	Vec3 vUp = XMVector3TransformNormal(Vec3::Up, matRot);



	MOUNTAINDRAGON_STATE tempState = MOUNTAINDRAGON_STATE::IDLE;

	//if (KEY_HOLD(KEY_TYPE::KEY_UP))
	//{
	//	vPos.x -= DT * Status()->GetWalkSpeed() * vLook.x;
	//	vPos.y -= DT * Status()->GetWalkSpeed() * vLook.y;
	//	vPos.z -= DT * Status()->GetWalkSpeed() * vLook.z;
	//	vRot.y = vLook.y + XM_PI;
	//	tempState = MOUNTAINDRAGON_STATE::WALK;
	//}

	//if (KEY_HOLD(KEY_TYPE::KEY_DOWN))
	//{
	//	vPos.x += DT * Status()->GetWalkSpeed() * vLook.x / 2;
	//	vPos.y += DT * Status()->GetWalkSpeed() * vLook.y / 2;
	//	vPos.z += DT * Status()->GetWalkSpeed() * vLook.z / 2;
	//	vRot.y = vLook.y - XM_PI;
	//	tempState = MOUNTAINDRAGON_STATE::WALK;
	//}

	//if (KEY_HOLD(KEY_TYPE::KEY_LEFT))
	//{
	//	vPos.x -= DT * Status()->GetWalkSpeed() * vLook.x;
	//	vPos.y -= DT * Status()->GetWalkSpeed() * vLook.y;
	//	vPos.z -= DT * Status()->GetWalkSpeed() * vLook.z;
	//	vRot.y = vLook.y + XM_PI / 2;
	//	tempState = MOUNTAINDRAGON_STATE::WALK;
	//}

	//if (KEY_HOLD(KEY_TYPE::KEY_RIGHT))
	//{
	//	vPos.x -= DT * Status()->GetWalkSpeed() * vLook.x;
	//	vPos.y -= DT * Status()->GetWalkSpeed() * vLook.y;
	//	vPos.z -= DT * Status()->GetWalkSpeed() * vLook.z;
	//	vRot.y = vLook.y - XM_PI / 2;
	//	tempState = MOUNTAINDRAGON_STATE::WALK;
	//}

	if (KEY_HOLD(KEY_TYPE::KEY_UP))
	{
		vPos.x -= DT * Status()->GetWalkSpeed() * vLook.x;
		vPos.y -= DT * Status()->GetWalkSpeed() * vLook.y;
		vPos.z -= DT * Status()->GetWalkSpeed() * vLook.z;

		//Transform()->SetLocalRot(vLook);

		tempState = MOUNTAINDRAGON_STATE::WALK;

		CNetworkMgr::GetInst()->SendMovePacket(D_UP);
	}
	else if (KEY_AWAY(KEY_TYPE::KEY_UP)) 
	{
		CNetworkMgr::GetInst()->SendMoveStopPacket(D_UP);
	}

	if (KEY_HOLD(KEY_TYPE::KEY_DOWN))
	{
		vPos.x += DT * Status()->GetWalkSpeed() * vLook.x / 2;
		vPos.y += DT * Status()->GetWalkSpeed() * vLook.y / 2;
		vPos.z += DT * Status()->GetWalkSpeed() * vLook.z / 2;

		//Transform()->SetLocalRot(-vLook);

		tempState = MOUNTAINDRAGON_STATE::WALK;

		CNetworkMgr::GetInst()->SendMovePacket(D_DOWN);
	}  
	else if (KEY_AWAY(KEY_TYPE::KEY_DOWN))
	{
		CNetworkMgr::GetInst()->SendMoveStopPacket(D_DOWN);
	}

	if (KEY_HOLD(KEY_TYPE::KEY_LEFT))
	{
		vPos.x += DT * Status()->GetWalkSpeed() * vRight.x;
		vPos.y += DT * Status()->GetWalkSpeed() * vRight.y;
		vPos.z += DT * Status()->GetWalkSpeed() * vRight.z;
		//vRot.y = - XM_PI / 2;

		//Transform()->SetLocalRot(-vRight);

		tempState = MOUNTAINDRAGON_STATE::WALK;

		CNetworkMgr::GetInst()->SendMovePacket(D_LEFT);
	}
	else if (KEY_AWAY(KEY_TYPE::KEY_LEFT))
	{
		CNetworkMgr::GetInst()->SendMoveStopPacket(D_LEFT);
	}

	if (KEY_HOLD(KEY_TYPE::KEY_RIGHT))
	{
		vPos.x -= DT * Status()->GetWalkSpeed() * vRight.x;
		vPos.y -= DT * Status()->GetWalkSpeed() * vRight.y;
		vPos.z -= DT * Status()->GetWalkSpeed() * vRight.z;

		//vRot.y = XM_PI / 2;

		tempState = MOUNTAINDRAGON_STATE::WALK;

		CNetworkMgr::GetInst()->SendMovePacket(D_RIGHT);
	}
	else if (KEY_AWAY(KEY_TYPE::KEY_RIGHT))
	{
		CNetworkMgr::GetInst()->SendMoveStopPacket(D_RIGHT);
	}

	if (KEY_TAB(KEY_TYPE::KEY_SPACE))
	{
	}


	if (KEY_TAB(KEY_TYPE::KEY_LBTN))
	{
		CNetworkMgr::GetInst()->SendAttackPacket(A_BITE);
		tempState = MOUNTAINDRAGON_STATE::BITE;
	}
	if (KEY_TAB(KEY_TYPE::KEY_RBTN))
	{
		CNetworkMgr::GetInst()->SendAttackPacket(A_BITE);
		tempState = MOUNTAINDRAGON_STATE::CLAWATTACK;
	}

	//// z 키를 누르면 z 축 회전
	//if (KEY_HOLD(KEY_TYPE::KEY_Z))
	//{
	//	vRot.z += DT * XM_PI;

	//	// 복사 메테리얼을 MeshRender 에 세팅
	//	MeshRender()->SetMaterial(m_pCloneMtrl);
	//}
	//else if (KEY_AWAY(KEY_TYPE::KEY_Z))
	//{
	//	// z 키를 떼면 원레 메테리얼로 돌아감
	//	MeshRender()->SetMaterial(m_pOriginMtrl);
	//}


	Transform()->SetLocalPos(vPos);
	Transform()->SetLocalRot(vRot);

	if (Status()->GetState() != (int)tempState)
	{
		if (Animator3D()->GetIsEnd() == true || 
			Status()->GetState() == (int)MOUNTAINDRAGON_STATE::WALK || 
			Status()->GetState() == (int)MOUNTAINDRAGON_STATE::IDLE)
		{
			Animator3D()->InitCurAnimFrame();
		}
		else
		{
			tempState = (MOUNTAINDRAGON_STATE)Status()->GetState();
		}
	}


	Status()->SetState((int)tempState);

	Animator3D()->SetCurAnimIdx(Status()->GetState());
}
