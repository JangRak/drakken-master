#include "stdafx.h"
#include "ToolMgr.h"

#include "Scene.h"
#include "Layer.h"
#include "GameObject.h"

#include "ResMgr.h"
#include "Shader.h"
#include "Mesh.h"
#include "Texture.h"
#include "Transform.h"
#include "MeshRender.h"
#include "Collider2D.h"
#include "Light3D.h"
#include "ParticleSystem.h"
#include "Animator3D.h"
#include "Status.h"

#include "TimeMgr.h"
#include "KeyMgr.h"
#include "Camera.h"

#include "CollisionMgr.h"
#include "EventMgr.h"
#include "RenderMgr.h"
#include "Device.h"
#include "Core.h"

#include "ToolCamScript.h"
#include "GridScript.h"


#include "Transform.h"

CScene * CToolMgr::GetCurScene()
{
	return m_pCurScene;
}

void CToolMgr::ChangeScene(CScene * _pNextScene)
{
	SAFE_DELETE(m_pCurScene);
	m_pCurScene = _pNextScene;
}

CToolMgr::CToolMgr()
	: m_pCurScene(nullptr)
{
}

CToolMgr::~CToolMgr()
{
	SAFE_DELETE(m_pCurScene);
}



void CToolMgr::init()
{
	// =================
	// 필요한 리소스 로딩
	// =================
	// Texture 로드
	Ptr<CTexture> pTex = CResMgr::GetInst()->Load<CTexture>(L"TestTex", L"Texture\\Health.png");
	Ptr<CTexture> pExplosionTex = CResMgr::GetInst()->Load<CTexture>(L"Explosion", L"Texture\\Explosion\\Explosion80.png");
	Ptr<CTexture> pBlackTex = CResMgr::GetInst()->Load<CTexture>(L"Black", L"Texture\\asd.png");
	Ptr<CTexture> pSky01 = CResMgr::GetInst()->Load<CTexture>(L"Sky01", L"Texture\\Skybox\\Sky01.png");
	Ptr<CTexture> pSky02 = CResMgr::GetInst()->Load<CTexture>(L"Sky02", L"Texture\\Skybox\\Sky02.jpg");

	Ptr<CTexture> pColor = CResMgr::GetInst()->Load<CTexture>(L"Tile", L"Texture\\Tile\\TILE_03.tga");
	Ptr<CTexture> pNormal = CResMgr::GetInst()->Load<CTexture>(L"Tile_n", L"Texture\\Tile\\TILE_03_N.tga");


	// ===============
	// Test Scene 생성
	// ===============
	m_pCurScene = new CScene;
	m_pCurScene->SetName(L"Test Scene");

	// ===============
	// Layer 이름 지정
	// ===============
	m_pCurScene->GetLayer(0)->SetName(L"Default");
	m_pCurScene->GetLayer(1)->SetName(L"Player");
	m_pCurScene->GetLayer(2)->SetName(L"Monster");
	m_pCurScene->GetLayer(3)->SetName(L"Bullet");
	m_pCurScene->GetLayer(4)->SetName(L"Enviroment");
	m_pCurScene->GetLayer(31)->SetName(L"Tool");

	CGameObject* pObject = nullptr;

	// ==================
	// Camera Object 생성
	// ==================
	CGameObject* pMainCam = new CGameObject;
	pMainCam->SetName(L"MainCam");
	pMainCam->AddComponent(new CTransform);
	pMainCam->AddComponent(new CCamera);
	pMainCam->AddComponent(new CToolCamScript);

	pMainCam->Camera()->SetProjType(PROJ_TYPE::PERSPECTIVE);
	pMainCam->Camera()->SetFar(100000.f);
	pMainCam->Camera()->SetLayerAllCheck();

	pMainCam->Transform()->SetLocalPos(Vec3(0.f, 1000.f, 0.f));

	m_pCurScene->FindLayer(L"Default")->AddGameObject(pMainCam);



	// ====================
	// 3D Light Object 추가
	// ====================
	pObject = new CGameObject;
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CLight3D);

	pObject->Light3D()->SetLightPos(Vec3(0.f, 200.f, 1000.f));
	pObject->Light3D()->SetLightType(LIGHT_TYPE::DIR);
	pObject->Light3D()->SetDiffuseColor(Vec3(1.f, 1.f, 1.f));
	pObject->Light3D()->SetSpecular(Vec3(0.3f, 0.3f, 0.3f));
	pObject->Light3D()->SetAmbient(Vec3(0.1f, 0.1f, 0.1f));
	pObject->Light3D()->SetLightDir(Vec3(1.f, -1.f, 1.f));
	pObject->Light3D()->SetLightRange(500.f);

	m_pCurScene->FindLayer(L"Default")->AddGameObject(pObject);





	// =============
	// FBX 파일 로드
	// =============
	Ptr<CMeshData> pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\house.fbx");
	//pMeshData->Save(pMeshData->GetPath());
	// MeshData 로드
	//Ptr<CMeshData> pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\House.mdat", L"MeshData\\monster.mdat");

	pObject = pMeshData->Instantiate();
	pObject->SetName(L"House");
	pObject->FrustumCheck(false);
	pObject->Transform()->SetLocalPos(Vec3(0.f, 220.f, 0.f));
	pObject->Transform()->SetLocalScale(Vec3(1.f, 1.f, 1.f));
	m_pCurScene->AddGameObject(L"Default", pObject, false);

	//// ===================
	//// Plane 오브젝트 생성
	//// ===================
	//for (int i = 0; i < 15; ++i) {
	//	for (int j = 0; j < 15; ++j) {
	//		pObject = new CGameObject;
	//		pObject->SetName(L"Plane");
	//		pObject->AddComponent(new CTransform);
	//		pObject->AddComponent(new CMeshRender);
	//		float x = (float)i * 1000.f - 4000.f;
	//		float z = (float)j * 1000.f - 4000.f;
	//		// Transform 설정
	//		pObject->Transform()->SetLocalPos(Vec3(x, -200.f, 1000.f + z));
	//		pObject->Transform()->SetLocalScale(Vec3(1000.f, 1000.f, 1.f));
	//		pObject->Transform()->SetLocalRot(Vec3(XM_PI / 2.f, 0.f, 0.f));
	//		// MeshRender 설정
	//		pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	//		pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"Std3DMtrl"));
	//		pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pColor.GetPointer());
	//		pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_1, pNormal.GetPointer());
	//		int a = 1;
	//		pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &a);
	//		m_pCurScene->FindLayer(L"Default")->AddGameObject(pObject);
	//	}
	//}

	pObject = new CGameObject;
	pObject->SetName(L"Plane Object");
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CMeshRender);

	// Transform 설정
	pObject->Transform()->SetLocalPos(Vec3(0.f, 0.f, 0.f));
	//pObject->Transform()->SetLocalPos(Vec3(0.f, 0.f, 1000.f));
	pObject->Transform()->SetLocalScale(Vec3(5000.f, 5000.f, 1.f));
	pObject->Transform()->SetLocalRot(Vec3(XM_PI / 2.f, 0.f, 0.f));

	// MeshRender 설정
	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"Std3DMtrl"));
	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pColor.GetPointer());
	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_1, pNormal.GetPointer());

	int a = 1;
	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &a);

	m_pCurScene->FindLayer(L"Default")->AddGameObject(pObject);

	m_vPlanePoint1 = Vec3(-100000.f, 0.f, -100000.f);
	m_vPlanePoint2 = Vec3(0.f, 0.f, 100000.f);
	m_vPlanePoint3 = Vec3(100000.f, 0.f, -100000.f);

	//m_vPlanePoint1 = Vec3(-1.f, 0.f, -1.f);
	//m_vPlanePoint2 = Vec3(0.f, 0.f, 1.f);
	//m_vPlanePoint3 = Vec3(1.f, 0.f, -1.f);


	// ====================
	// Skybox 오브젝트 생성
	// ====================
	pObject = new CGameObject;
	pObject->SetName(L"SkyBox");
	pObject->FrustumCheck(false);
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CMeshRender);

	// MeshRender 설정
	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
	pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"SkyboxMtrl"));
	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pSky02.GetPointer());

	// AddGameObject
	m_pCurScene->FindLayer(L"Default")->AddGameObject(pObject);

	// ====================
	// Grid 오브젝트 생성
	// ====================
	pObject = new CGameObject;
	pObject->SetName(L"Grid");
	pObject->FrustumCheck(false);
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CMeshRender);
	pObject->AddComponent(new CGridScript);

	// Transform 설정
	pObject->Transform()->SetLocalScale(Vec3(100000.f, 100000.f, 1.f));
	pObject->Transform()->SetLocalRot(Vec3(XM_PI / 2.f, 0.f, 0.f));

	// MeshRender 설정
	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
	pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"GridMtrl"));

	// Script 설정	
	pObject->GetScript<CGridScript>()->SetToolCamera(pMainCam);
	pObject->GetScript<CGridScript>()->SetGridColor(Vec3(0.8f, 0.2f, 0.2f));

	// AddGameObject
	m_pCurScene->FindLayer(L"Tool")->AddGameObject(pObject);











	pObject = new CGameObject;
	pObject->SetName(L"Enviroment");
	pObject->AddComponent(new CTransform);
	pObject->AddComponent(new CMeshRender);

	// Transform 설정
	pObject->Transform()->SetLocalPos(Vec3{ 0.f, 0.f, 0.f });
	pObject->Transform()->SetLocalScale(Vec3(100.f, 100.f, 100.f));
	//pObject->Transform()->SetLocalRot(Vec3(XM_PI / 2.f, 0.f, 0.f));

	// MeshRender 설정
	pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
	pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"TestMtrl"));
	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pColor.GetPointer());
	pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_1, pNormal.GetPointer());


	// AddGameObject
	m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pObject);


	// =================================
	// CollisionMgr 충돌 그룹(Layer) 지정
	// =================================
	// Player Layer 와 Monster Layer 는 충돌 검사 진행
	CCollisionMgr::GetInst()->CheckCollisionLayer(L"Player", L"Monster");
	CCollisionMgr::GetInst()->CheckCollisionLayer(L"Bullet", L"Monster");

	m_pCurScene->awake();
	m_pCurScene->start();



}

void CToolMgr::update()
{
	m_pCurScene->update();
	m_pCurScene->lateupdate();

	// RenderMgr 카메라 초기화
	CRenderMgr::GetInst()->ClearCamera();

	m_pCurScene->finalupdate();

	// 충돌처리
	CCollisionMgr::GetInst()->update();
}

void CToolMgr::update_tool()
{
	// RenderMgr 카메라 초기화
	CRenderMgr::GetInst()->ClearCamera();
	m_pCurScene->finalupdate();
}

void CToolMgr::FindGameObjectByTag(const wstring & _strTag, vector<CGameObject*>& _vecFindObj)
{
	for (int i = 0; i < MAX_LAYER; ++i)
	{
		const vector<CGameObject*>& vecObject = m_pCurScene->GetLayer(i)->GetObjects();
		for (size_t j = 0; j < vecObject.size(); ++j)
		{
			if (_strTag == vecObject[j]->GetName())
			{
				_vecFindObj.push_back(vecObject[j]);
			}
		}
	}
}


void CToolMgr::Pick(int iScreenX, int iScreenY, HWND _hWnd, int ePickType, int eObjectKind)
{
	// ===========================
	// 마우스 피킹을 위한 좌표계 변환
	// ===========================
	RECT rcRect = {};
	GetWindowRect(_hWnd, &rcRect);

	Vec3 vMousePos = {};

	Matrix MatProj = m_pCurScene->GetLayer(0)->GetParentObj().front()->Camera()->GetProjMat();
	Matrix MatInvProj = XMMatrixInverse(nullptr, MatProj);

	vMousePos.x = ((2.0f * iScreenX) / (rcRect.right - rcRect.left) - 1.0f);
	vMousePos.y = -((2.0f * iScreenY) / (rcRect.bottom - rcRect.top) - 1.0f);
	vMousePos.z = 0.f;

	// 투영좌표계로 변환
	vMousePos = XMVector3TransformCoord(vMousePos, MatInvProj);


	// 카메라 좌표가 Ray의 시작점
	Vec3 CameraPos = CSceneMgr::GetInst()->GetCurScene()->GetLayer(0)->GetParentObj().front()->Transform()->GetLocalPos();
	Vec3 vRayDir = { vMousePos }, vRayPos = { CameraPos };
	Matrix MatView = m_pCurScene->GetLayer(0)->GetParentObj().front()->Camera()->GetViewMat();
	wstring tempString = m_pCurScene->GetLayer(0)->GetParentObj().front()->GetName();
	Matrix MatInvView = XMMatrixInverse(nullptr, MatView);

	// 방향을 월드 좌표계로 변환
	vRayDir = XMVector3TransformNormal(vRayDir, MatInvView);

	// Ray 생성 및 초기화
	DirectX::SimpleMath::Ray ray = DirectX::SimpleMath::Ray();
	ray.position = vRayPos;
	ray.direction = vRayDir;
	ray.direction.Normalize();

	float fDist = 0.f;
	// 충돌하지 않는다면 리턴
	if (!ray.Intersects(m_vPlanePoint1, m_vPlanePoint2, m_vPlanePoint3, fDist))
	{
		return;
	}

	// 충돌 위치
	Vec3 vPickedPosition = ray.position + (fDist * ray.direction);


	// ===================
	// 피킹 종류에 따른 작업
	// ===================

	CGameObject* pObject = nullptr;
	Ptr<CTexture> pColor = nullptr;
	Ptr<CTexture> pNormal = nullptr;


	switch (ePickType)
	{
	case (int)PICK_TYPE::CREATE:
		pColor = CResMgr::GetInst()->FindRes<CTexture>(L"Tile");
		pNormal = CResMgr::GetInst()->FindRes<CTexture>(L"Tile_n");

		pObject = new CGameObject;
		pObject->SetName(L"Enviroment");
		pObject->AddComponent(new CTransform);
		pObject->AddComponent(new CMeshRender);

		// Transform 설정
		pObject->Transform()->SetLocalPos(vPickedPosition);
		pObject->Transform()->SetLocalScale(Vec3(100.f, 100.f, 100.f));

		// MeshRender 설정
		pObject->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
		pObject->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"TestMtrl"));
		pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pColor.GetPointer());
		pObject->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_1, pNormal.GetPointer());


		// AddGameObject
		m_pCurScene->FindLayer(L"Enviroment")->AddGameObject(pObject);

		break;
	case (int)PICK_TYPE::REMOVE:
		break;
	case (int)PICK_TYPE::MODIFY:
		break;
	default:
		assert(nullptr);
	}
}