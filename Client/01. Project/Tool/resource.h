﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// Tool.rc에서 사용되고 있습니다.
//
#define IDC_MYICON                      2
#define IDP_OLE_INIT_FAILED             100
#define IDD_CLIENT_DIALOG               102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_CLIENT                      107
#define IDI_SMALL                       108
#define IDC_CLIENT                      109
#define IDR_MAINFRAME                   128
#define IDR_ToolTYPE                    130
#define IDD_MYFORM                      310
#define IDD_MAPTAB                      311
#define IDD_TEMPTAB                     312
#define IDC_TAB1                        1001
#define IDC_LIST1                       1003
#define IDC_BUTTON4                     1004
#define IDC_LIST2                       1004
#define IDC_EDIT1                       1005
#define IDC_CHECK1                      1006
#define IDC_CHECK2                      1007
#define IDC_COMBO1                      1008
#define IDC_EDIT3                       1010
#define IDC_EDIT4                       1011
#define IDC_EDIT5                       1013
#define IDC_EDIT6                       1014
#define IDC_EDIT7                       1015
#define IDC_EDIT8                       1016
#define IDC_EDIT9                       1017
#define IDC_EDIT10                      1018
#define IDC_EDIT11                      1019
#define IDC_EDIT12                      1020
#define IDC_BUTTON1                     1021
#define IDC_BUTTON2                     1022
#define IDC_RADIO1                      1023
#define IDC__SELECT_RADIO0              1023
#define IDC_RADIO2                      1024
#define IDC_SELECT_RADIO1               1024
#define IDC_RADIO3                      1025
#define IDC__SELECT_RADIO2              1025
#define IDC_RADIO4                      1026
#define IDC_COLLIDER_SPHERE             1026
#define IDC_RADIO5                      1027
#define IDC_COLLIDER_BOX                1027
#define IDC_RADIO6                      1028
#define IDC_COLLIDER_RADIO_EMPTY        1028
#define IDC_COLLIDER_EMPTY              1028
#define IDC_EDIT13                      1029
#define IDC_EDIT14                      1030
#define IDC_EDIT15                      1031
#define IDC_BUTTON3                     1032
#define IDC_COLLIDER_YES                1033
#define IDC_COLLIDER_NO                 1034

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1035
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
