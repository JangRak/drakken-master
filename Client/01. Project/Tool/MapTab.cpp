﻿// MapTab.cpp: 구현 파일
//
#include "pch.h"


#include <Engine/global.h>
#include <Engine/Transform.h>
#include <Engine/GameObject.h>
#include <Engine/MeshData.h>
#include <Engine/SceneMgr.h>
#include <Engine/Scene.h>
#include <Engine/Layer.h>
#include <Engine/ResMgr.h>
#include <Engine/Entity.h>
#include <Engine/MeshRender.h>

#ifdef _DEBUG
#pragma comment(lib, "Engine/Engine_debug.lib")
#else
#pragma comment(lib, "Engine/Engine.lib")
#endif

#include "Tool.h"
#include "MapTab.h"
#include "afxdialogex.h"
// MapTab 대화 상자

IMPLEMENT_DYNAMIC(MapTab, CDialogEx)

std::vector<CGameObject*> MapTab::m_vGameObjectsMapTab;

MapTab::MapTab(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MAPTAB, pParent)
	//, m_fPositionX(0.f)
	//, m_fPositionY(0.f)
	//, m_fPositionZ(0.f)
	, m_fPositionX(0)
	, m_fPositionY(0)
	, m_fPositionZ(0)
	, m_fRotationX(0)
	, m_fRotationY(0)
	, m_fRotationZ(0)
	, m_fScaleX(0)
	, m_fScaleY(0)
	, m_fScaleZ(0)
	, m_bApply(false)
	, m_iRadioSelect(0)
	, m_iRadioCollider(0)
	, iColliderKind(0)
	, m_bLoad(false)
{

}

MapTab::~MapTab()
{
}

void MapTab::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST2, m_StaticObjectList);
	DDX_Text(pDX, IDC_EDIT13, m_fPositionX);
	DDX_Text(pDX, IDC_EDIT14, m_fPositionY);
	DDX_Text(pDX, IDC_EDIT15, m_fPositionZ);
	DDX_Text(pDX, IDC_EDIT7, m_fRotationX);
	DDX_Text(pDX, IDC_EDIT8, m_fRotationY);
	DDX_Text(pDX, IDC_EDIT9, m_fRotationZ);
	DDX_Text(pDX, IDC_EDIT10, m_fScaleX);
	DDX_Text(pDX, IDC_EDIT11, m_fScaleY);
	DDX_Text(pDX, IDC_EDIT12, m_fScaleZ);
	DDX_Radio(pDX, IDC_COLLIDER_SPHERE, iColliderKind);
}




BEGIN_MESSAGE_MAP(MapTab, CDialogEx)
//	ON_BN_CLICKED(IDC_BUTTON1, &MapTab::OnBnClickedButton1)
	ON_EN_CHANGE(IDC_EDIT4, &MapTab::OnEnChangeEdit4)
	ON_BN_CLICKED(IDC_BUTTON1, &MapTab::ClickSave)
	ON_BN_CLICKED(IDC_BUTTON2, &MapTab::ClickLoad)
	ON_EN_CHANGE(IDC_EDIT13, &MapTab::OnChangePositionX)
	ON_BN_CLICKED(IDC_BUTTON3, &MapTab::ClickTransformApply)
END_MESSAGE_MAP()


// MapTab 메시지 처리기


void MapTab::OnEnChangeEdit4()
{
	// TODO:  RICHEDIT 컨트롤인 경우, 이 컨트롤은
	// CDialogEx::OnInitDialog() 함수를 재지정 
	//하고 마스크에 OR 연산하여 설정된 ENM_CHANGE 플래그를 지정하여 CRichEditCtrl().SetEventMask()를 호출하지 않으면
	// 이 알림 메시지를 보내지 않습니다.

	// TODO:  여기에 컨트롤 알림 처리기 코드를 추가합니다.
}


void MapTab::ClickSave()
{
	// FALSE : 파일에 쓰기
	// L"dat" : .dat 포멧으로 저장하겠다. txt포멧도 상관 없음
	// L"제목없음.dat" : Default 값
	// OFN_OVERWRITEPROMPT : 덮어쓰기 옵션 나머지는 모름
	// 
	CFileDialog Dlg(FALSE, L"dat", L"제목없음.dat", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		L"Data Files(*.dat)|*.dat||", this);

	// 현재 폴더 위치 가져오기
	TCHAR szCurPath[256] = L"";
	GetCurrentDirectory(256, szCurPath);

	// 경로 중 가장 상위 폴더 지워주기
	PathRemoveFileSpec(szCurPath);
	PathRemoveFileSpec(szCurPath);
	lstrcat(szCurPath, L"\\02. File\\bin\\content\\Scene");

	// 초기 폴더
	Dlg.m_ofn.lpstrInitialDir = szCurPath;


	if (IDOK == Dlg.DoModal())
	{
		CString strFileName = Dlg.GetPathName();

		// CreateFile 호출로 파일이 만들어짐
		HANDLE hFile = CreateFile(strFileName.GetString(), GENERIC_WRITE, 0, 0,
			CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);

		// 잘못된 경로일 경우 리턴
		if (INVALID_HANDLE_VALUE == hFile)
			return;

		DWORD dwByte = 0;
		float fValue = 0.f;
		wstring tempName = {};
		Vec3 vVec3 = {};
		
		for (int i = 0; i < m_vGameObjectsMapTab.size(); ++i)
		{
			//dwByte값은 쓸때는 중요치않지만 읽을 때는 아주 중요하다.
			tempName = m_vGameObjectsMapTab[i]->GetName();
			if (tempName == L"bigrock")
			{
				fValue = (float)OBJECT_KIND::BIGROCK;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"bridge_building")
			{
				fValue = (float)OBJECT_KIND::BRIDGE_BUILDING;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"damwall")
			{
				fValue = (float)OBJECT_KIND::DAMWALL;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"gate")
			{
				fValue = (float)OBJECT_KIND::GATE;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"grave")
			{
				fValue = (float)OBJECT_KIND::GRAVE;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"grave_pillar")
			{
				fValue = (float)OBJECT_KIND::GRAVE_PILLAR;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"shanty")
			{
				fValue = (float)OBJECT_KIND::SHANTY;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"shanty_up")
			{
				fValue = (float)OBJECT_KIND::SHANTY_UP;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"house")
			{
				fValue = (float)OBJECT_KIND::HOUSE;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"house_left")
			{
				fValue = (float)OBJECT_KIND::HOUSE_LEFT;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"house_right")
			{
				fValue = (float)OBJECT_KIND::HOUSE_RIGHT;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"house_small")
			{
				fValue = (float)OBJECT_KIND::HOUSE_SMALL;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"rock")
			{
				fValue = (float)OBJECT_KIND::ROCK;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"plane_rock")
			{
				fValue = (float)OBJECT_KIND::PLANE_ROCK;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"sprucea")
			{
				fValue = (float)OBJECT_KIND::SPRUCEA;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"plains_grass")
			{
				fValue = (float)OBJECT_KIND::PLAINS_GRASS;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}


			// Collider
			else if (tempName == L"bigrock_collider")
			{
				fValue = (float)OBJECT_KIND::BIGROCK_COLLIDER;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"bridge_building_collider")
			{
				fValue = (float)OBJECT_KIND::BRIDGE_BUILDING_COLLIDER;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"damwall_collider")
			{
				fValue = (float)OBJECT_KIND::DAMWALL_COLLIDER;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"gate_collider")
			{
				fValue = (float)OBJECT_KIND::GATE_COLLIDER;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"grave_collider")
			{
				fValue = (float)OBJECT_KIND::GRAVE_COLLIDER;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"grave_pillar_collider")
			{
				fValue = (float)OBJECT_KIND::GRAVE_PILLAR;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"shanty_collider")
			{
				fValue = (float)OBJECT_KIND::SHANTY_COLLIDER;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"shanty_up_collider")
			{
				fValue = (float)OBJECT_KIND::SHANTY_UP_COLLIDER;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"house_collider")
			{
				fValue = (float)OBJECT_KIND::HOUSE_COLLIDER;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"house_left_collider")
			{
				fValue = (float)OBJECT_KIND::HOUSE_LEFT_COLLIDER;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"house_right_collider")
			{
				fValue = (float)OBJECT_KIND::HOUSE_RIGHT_COLLIDER;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"house_small_collider")
			{
				fValue = (float)OBJECT_KIND::HOUSE_SMALL_COLLIDER;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"rock_collider")
			{
				fValue = (float)OBJECT_KIND::ROCK_COLLIDER;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else if (tempName == L"plane_rock_collider")
			{
				assert(nullptr);
				fValue = (float)OBJECT_KIND::PLANE_ROCK_COLLIDER;
				WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			}
			else
			{
				assert(nullptr);
			}

			vVec3 = MapTab::m_vGameObjectsMapTab[i]->Transform()->GetLocalPos();
			fValue = vVec3.x;
			WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			fValue = vVec3.y;
			WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			fValue = vVec3.z;
			WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);

			vVec3 = MapTab::m_vGameObjectsMapTab[i]->Transform()->GetLocalRot();
			fValue = vVec3.x;
			WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			fValue = vVec3.y;
			WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			fValue = vVec3.z;
			WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);

			vVec3 = MapTab::m_vGameObjectsMapTab[i]->Transform()->GetLocalScale();
			fValue = vVec3.x;
			WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			fValue = vVec3.y;
			WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			fValue = vVec3.z;
			WriteFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);

		}

		// 모든 행동을 끝낸 후 CloseHandle
		// 해주지 않는다면 터진다.
		CloseHandle(hFile);
	}


}




void MapTab::ClickLoad()
{
	// TRUE : Save , Load와 TRUE인 것만 다르다.
	CFileDialog Dlg(TRUE, L"dat", L"제목없음.dat", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		L"Data Files(*.dat)|*.dat||", this);

	// 현재 폴더 위치 가져오기
	TCHAR szCurPath[256] = L"";
	GetCurrentDirectory(256, szCurPath);
	 

	// 경로 중 가장 상위 폴더 지워주기
	PathRemoveFileSpec(szCurPath);
	PathRemoveFileSpec(szCurPath);
	lstrcat(szCurPath, L"\\02. File\\bin\\content\\Scene");

	// 초기 폴더
	Dlg.m_ofn.lpstrInitialDir = szCurPath;

	if (IDOK == Dlg.DoModal())
	{
		CString strFileName = Dlg.GetPathName();

		// CreateFile 호출로 파일이 만들어짐

		// GENERIC_READ : 읽기
		// OPEN_EXISTING : 존재하는 것을 열겠다.
		LPCWSTR temp = strFileName.GetString();

		HANDLE hFile = CreateFile(strFileName.GetString(), GENERIC_READ, 0, 0,
			OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

		// 잘못된 경로일 경우 리턴
		if (INVALID_HANDLE_VALUE == hFile)
			return;

		// 우리가 읽어들일때는 몇개의 파일이 어떤 자료형으로 있는지 모른다.
		float fValue = 0;
		DWORD dwByte = 0;

		// 기존 오브젝트들 지우기
		CSceneMgr::GetInst()->GetCurScene()->GetLayer(4)->Clear();

		for (size_t i = 0; i < m_vGameObjectsMapTab.size(); i++)
		{
			delete m_vGameObjectsMapTab[i];
			m_vGameObjectsMapTab[i] = nullptr;

			//if (m_vGameObjectsMapTab[i] != nullptr)
			//{
			//	delete m_vGameObjectsMapTab[i];
			//	m_vGameObjectsMapTab[i] = nullptr;
			//}
		}
		m_vGameObjectsMapTab.clear();
		//Safe_Delete_Vector(m_vGameObjects);
		

		CGameObject * pNewGameObj = nullptr;
		Ptr<CMeshData> pMeshData = nullptr;
		Vec3 vVec3 = {};

		int bColliderShow = 1;
		int a = 1;
		Ptr<CTexture> pColor = CResMgr::GetInst()->Load<CTexture>(L"Tile", L"Texture\\Tile\\TILE_03.tga");
		Ptr<CTexture> pNormal = CResMgr::GetInst()->Load<CTexture>(L"Tile_n", L"Texture\\Tile\\TILE_03_N.tga");

		while (1)
		{

			// 그러므로 우리는 사이즈 / 메모리 순서로 저장을 하거나 통일된 자료형(구조체)를
			// 사용하는 것이 맞다.

			// dwByte : 현재 
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);

			if (dwByte == 0)
			{
				break;
			}

			switch ((int)fValue)
			{
			case (int)OBJECT_KIND::BIGROCK:
				pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\BigRock.mdat", L"MeshData\\BigRock.mdat");

				pNewGameObj = pMeshData->Instantiate();
				pNewGameObj->SetName(L"bigrock");

				break;
			case (int)OBJECT_KIND::GRAVE:
				pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Grave.mdat", L"MeshData\\Grave.mdat");

				pNewGameObj = pMeshData->Instantiate();
				pNewGameObj->SetName(L"grave");

				break;
			case (int)OBJECT_KIND::HOUSE:
				pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\house.fbx");
				//pMeshData->Save(pMeshData->GetPath());

				//pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\house.mdat", L"MeshData\\house.mdat");
				//pMeshData->Save(pMeshData->GetPath());


				pNewGameObj = pMeshData->Instantiate();
				pNewGameObj->SetName(L"house");

				break;
			case (int)OBJECT_KIND::HOUSE_RIGHT:
				pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\house_right.fbx");

				pNewGameObj = pMeshData->Instantiate();
				pNewGameObj->SetName(L"house_right");

				break;
			case (int)OBJECT_KIND::HOUSE_SMALL:
				pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\house_small.mdat", L"MeshData\\house_small.mdat");

				pNewGameObj = pMeshData->Instantiate();
				pNewGameObj->SetName(L"house_small");

				break;
			case (int)OBJECT_KIND::ROCK:
				pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\rock.mdat", L"MeshData\\rock.mdat");
				//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\rock.fbx");
				//pMeshData->Save(pMeshData->GetPath());

				pNewGameObj = pMeshData->Instantiate();
				pNewGameObj->SetName(L"rock");

				break;
			case (int)OBJECT_KIND::HOUSE_LEFT:
				pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\house_left.mdat", L"MeshData\\house_left.mdat");
				//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\house_left.fbx");
				pMeshData->Save(pMeshData->GetPath());

				pNewGameObj = pMeshData->Instantiate();
				pNewGameObj->SetName(L"house_left");

				break;

				// 추가

			case (int)OBJECT_KIND::BRIDGE_BUILDING:
				pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Bridge_Building.fbx");
				pMeshData->Save(pMeshData->GetPath());
				pNewGameObj = pMeshData->Instantiate();
				pNewGameObj->SetName(L"bridge_building");
				break;
			case (int)OBJECT_KIND::DAMWALL:
				pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\DamWall.fbx");
				pMeshData->Save(pMeshData->GetPath());
				pNewGameObj = pMeshData->Instantiate();
				pNewGameObj->SetName(L"damwall");
				break;
			case (int)OBJECT_KIND::GATE:
				//pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Gate.mdat", L"MeshData\\Gate.mdat");
				pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Gate.fbx");
				pMeshData->Save(pMeshData->GetPath());
				pNewGameObj = pMeshData->Instantiate();
				pNewGameObj->SetName(L"gate");
				break;
			case (int)OBJECT_KIND::GRAVE_PILLAR:
				pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Grave_Pillar.fbx");
				pMeshData->Save(pMeshData->GetPath());
				pNewGameObj = pMeshData->Instantiate();
				pNewGameObj->SetName(L"grave_pillar");
				break;
			case (int)OBJECT_KIND::SHANTY:
				//pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Shanty.mdat", L"MeshData\\Shanty.mdat");

				pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Shanty.fbx");
				//pMeshData->Save(pMeshData->GetPath());
				pNewGameObj = pMeshData->Instantiate();
				pNewGameObj->SetName(L"shanty");
				break;
			case (int)OBJECT_KIND::SHANTY_UP:
				pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Shanty_Up.mdat", L"MeshData\\Shanty_Up.mdat");
				//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Shanty_Up.fbx");
				//pMeshData->Save(pMeshData->GetPath());
				pNewGameObj = pMeshData->Instantiate();
				pNewGameObj->SetName(L"shanty_up");
				break;
			case (int)OBJECT_KIND::PLANE_ROCK:
				pNewGameObj = new CGameObject;
				pNewGameObj->SetName(L"plane_rock");
				pNewGameObj->AddComponent(new CTransform);
				pNewGameObj->AddComponent(new CMeshRender);
				// MeshRender 설정
				pNewGameObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
				pNewGameObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"Std3DMtrl"));
				pNewGameObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pColor.GetPointer());
				pNewGameObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_1, pNormal.GetPointer());

				break;

			case (int)OBJECT_KIND::SPRUCEA:
				pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\SpruceA.mdat", L"MeshData\\SpruceA.mdat");
				//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\SpruceA.fbx");
				//pMeshData->Save(pMeshData->GetPath());
				pNewGameObj = pMeshData->Instantiate();
				pNewGameObj->SetName(L"sprucea");
				break;

			case (int)OBJECT_KIND::PLAINS_GRASS:
				pMeshData = CResMgr::GetInst()->Load<CMeshData>(L"MeshData\\Plains_Grass.mdat", L"MeshData\\Plains_Grass.mdat");
				//pMeshData = CResMgr::GetInst()->LoadFBX(L"FBX\\Plains_Grass.fbx");
				//pMeshData->Save(pMeshData->GetPath());
				pNewGameObj = pMeshData->Instantiate();
				pNewGameObj->SetName(L"plains_grass");
				break;

			// Test
			case (int)OBJECT_KIND::BIGROCK_COLLIDER:
				pNewGameObj = new CGameObject;
				pNewGameObj->SetName(L"bigrock_collider");
				pNewGameObj->FrustumCheck(false);
				pNewGameObj->AddComponent(new CTransform);
				pNewGameObj->AddComponent(new CMeshRender);

				pNewGameObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pNewGameObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pNewGameObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);

				break;
			case (int)OBJECT_KIND::GRAVE_COLLIDER:
				pNewGameObj = new CGameObject;
				pNewGameObj->SetName(L"grave_collider");
				pNewGameObj->FrustumCheck(false);
				pNewGameObj->AddComponent(new CTransform);
				pNewGameObj->AddComponent(new CMeshRender);

				pNewGameObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pNewGameObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pNewGameObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);

				break;
			case (int)OBJECT_KIND::HOUSE_COLLIDER:
				pNewGameObj = new CGameObject;
				pNewGameObj->SetName(L"house_collider");
				pNewGameObj->FrustumCheck(false);
				pNewGameObj->AddComponent(new CTransform);
				pNewGameObj->AddComponent(new CMeshRender);

				pNewGameObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pNewGameObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pNewGameObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);

				break;
			case (int)OBJECT_KIND::HOUSE_RIGHT_COLLIDER:
				pNewGameObj = new CGameObject;
				pNewGameObj->SetName(L"house_right_collider");
				pNewGameObj->FrustumCheck(false);
				pNewGameObj->AddComponent(new CTransform);
				pNewGameObj->AddComponent(new CMeshRender);

				pNewGameObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pNewGameObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pNewGameObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);

				break;
			case (int)OBJECT_KIND::HOUSE_SMALL_COLLIDER:
				pNewGameObj = new CGameObject;
				pNewGameObj->SetName(L"house_small_collider");
				pNewGameObj->FrustumCheck(false);
				pNewGameObj->AddComponent(new CTransform);
				pNewGameObj->AddComponent(new CMeshRender);

				pNewGameObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pNewGameObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pNewGameObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);

				break;
			case (int)OBJECT_KIND::ROCK_COLLIDER:
				pNewGameObj = new CGameObject;
				pNewGameObj->SetName(L"rock_collider");
				pNewGameObj->FrustumCheck(false);
				pNewGameObj->AddComponent(new CTransform);
				pNewGameObj->AddComponent(new CMeshRender);

				pNewGameObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pNewGameObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pNewGameObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);

				break;
			case (int)OBJECT_KIND::HOUSE_LEFT_COLLIDER:
				pNewGameObj = new CGameObject;
				pNewGameObj->SetName(L"house_left_collider");
				pNewGameObj->FrustumCheck(false);
				pNewGameObj->AddComponent(new CTransform);
				pNewGameObj->AddComponent(new CMeshRender);

				pNewGameObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pNewGameObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pNewGameObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);

				break;

				// 추가

			case (int)OBJECT_KIND::BRIDGE_BUILDING_COLLIDER:
				pNewGameObj = new CGameObject;
				pNewGameObj->SetName(L"bridge_building_collider");
				pNewGameObj->FrustumCheck(false);
				pNewGameObj->AddComponent(new CTransform);
				pNewGameObj->AddComponent(new CMeshRender);

				pNewGameObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pNewGameObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pNewGameObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);

				break;
			case (int)OBJECT_KIND::DAMWALL_COLLIDER:
				pNewGameObj = new CGameObject;
				pNewGameObj->SetName(L"damwall_collider");
				pNewGameObj->FrustumCheck(false);
				pNewGameObj->AddComponent(new CTransform);
				pNewGameObj->AddComponent(new CMeshRender);

				pNewGameObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pNewGameObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pNewGameObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);

				break;
			case (int)OBJECT_KIND::GATE_COLLIDER:
				pNewGameObj = new CGameObject;
				pNewGameObj->SetName(L"gate_collider");
				pNewGameObj->FrustumCheck(false);
				pNewGameObj->AddComponent(new CTransform);
				pNewGameObj->AddComponent(new CMeshRender);

				pNewGameObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pNewGameObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pNewGameObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);

				break;
			case (int)OBJECT_KIND::GRAVE_PILLAR_COLLIDER:
				pNewGameObj = new CGameObject;
				pNewGameObj->SetName(L"grave_pillar_collider");
				pNewGameObj->FrustumCheck(false);
				pNewGameObj->AddComponent(new CTransform);
				pNewGameObj->AddComponent(new CMeshRender);

				pNewGameObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pNewGameObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pNewGameObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);

				break;
			case (int)OBJECT_KIND::SHANTY_COLLIDER:
				pNewGameObj = new CGameObject;
				pNewGameObj->SetName(L"shanty_collider");
				pNewGameObj->FrustumCheck(false);
				pNewGameObj->AddComponent(new CTransform);
				pNewGameObj->AddComponent(new CMeshRender);

				pNewGameObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pNewGameObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pNewGameObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);

				break;
			case (int)OBJECT_KIND::SHANTY_UP_COLLIDER:
				pNewGameObj = new CGameObject;
				pNewGameObj->SetName(L"shanty_up_collider");
				pNewGameObj->FrustumCheck(false);
				pNewGameObj->AddComponent(new CTransform);
				pNewGameObj->AddComponent(new CMeshRender);

				pNewGameObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"SphereMesh"));
				pNewGameObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"WireFrameMtrl"));
				pNewGameObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::INT_0, &bColliderShow);

				break;
			default:
				assert(nullptr);
			}

			pNewGameObj->FrustumCheck(false);
			m_vGameObjectsMapTab.push_back(pNewGameObj);

			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			vVec3.x = fValue;
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			vVec3.y = fValue;
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			vVec3.z = fValue;

			pNewGameObj->Transform()->SetLocalPos(vVec3);

			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			vVec3.x = fValue;
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			vVec3.y = fValue;
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			vVec3.z = fValue;

			pNewGameObj->Transform()->SetLocalRot(vVec3);

			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			vVec3.x = fValue;
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			vVec3.y = fValue;
			ReadFile(hFile, &fValue, sizeof(float), &dwByte, nullptr);
			vVec3.z = fValue;

			pNewGameObj->Transform()->SetLocalScale(vVec3);

			assert(pNewGameObj);

			// 더 이상 읽을 것이 없다면 dwByte가 0이 된다.
			if (dwByte == 0)
			{
				break;
			}

			// 여기서 읽어들인 값을 저장
		}




		CSceneMgr::GetInst()->GetCurScene()->GetLayer(0)->SetName(L"Default");
		CSceneMgr::GetInst()->GetCurScene()->GetLayer(1)->SetName(L"Player");
		CSceneMgr::GetInst()->GetCurScene()->GetLayer(2)->SetName(L"Monster");
		CSceneMgr::GetInst()->GetCurScene()->GetLayer(3)->SetName(L"Bullet");
		CSceneMgr::GetInst()->GetCurScene()->GetLayer(4)->SetName(L"Enviroment");
		CSceneMgr::GetInst()->GetCurScene()->GetLayer(31)->SetName(L"UI");
		CSceneMgr::GetInst()->GetCurScene()->GetLayer(31)->SetName(L"Tool");

		// 읽어들인 오브젝트들을 Scene에 추가

		for (size_t i = 0; i < m_vGameObjectsMapTab.size(); ++i)
		{
			if (!m_vGameObjectsMapTab[i])
			{
				assert(nullptr);
			}
			pNewGameObj = m_vGameObjectsMapTab[i];
			CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"Enviroment")->AddGameObject(pNewGameObj);
		}

		for (int i = 0; i < 20; ++i) {
			for (int j = 0; j < 20; ++j) {
				pNewGameObj = new CGameObject;
				pNewGameObj->SetName(L"plane_rock");
				pNewGameObj->AddComponent(new CTransform);
				pNewGameObj->AddComponent(new CMeshRender);
				float x = (float)i * 1000.f - 4000.f;
				float z = (float)j * 1000.f - 4000.f;
				// Transform 설정
				pNewGameObj->Transform()->SetLocalPos(Vec3(x, 1.f, z));
				pNewGameObj->Transform()->SetLocalScale(Vec3(1000.f, 1000.f, 1.f));
				pNewGameObj->Transform()->SetLocalRot(Vec3(XM_PI / 2.f, 0.f, 0.f));
				// MeshRender 설정
				pNewGameObj->MeshRender()->SetMesh(CResMgr::GetInst()->FindRes<CMesh>(L"RectMesh"));
				pNewGameObj->MeshRender()->SetMaterial(CResMgr::GetInst()->FindRes<CMaterial>(L"Std3DMtrl"));
				pNewGameObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_0, pColor.GetPointer());
				pNewGameObj->MeshRender()->GetSharedMaterial()->SetData(SHADER_PARAM::TEX_1, pNormal.GetPointer());
				CSceneMgr::GetInst()->GetCurScene()->FindLayer(L"Enviroment")->AddGameObject(pNewGameObj);
				pNewGameObj->MeshRender()->SetDynamicShadow(true);


			}
		}
		// 모든 행동을 끝낸 후 CloseHandle
		// 해주지 않는다면 터진다.
		CloseHandle(hFile);
	}

	m_bLoad = true;
}

//IDC_EDIT13, m_fPositionX
//IDC_EDIT14, m_fPositionY
//IDC_EDIT15, m_fPositionZ
//IDC_EDIT7, m_fRotationX)
//IDC_EDIT8, m_fRotationY)
//IDC_EDIT9, m_fRotationZ)
//IDC_EDIT10, m_fScaleX);
//IDC_EDIT11, m_fScaleY);
//IDC_EDIT12, m_fScaleZ);



void MapTab::UpdateGameObjects(std::vector<CGameObject*> vGameObjects)
{
	m_vGameObjectsMapTab.clear();
	m_vGameObjectsMapTab.resize(vGameObjects.size());
	m_vGameObjectsMapTab = vGameObjects;
}

void MapTab::OnChangePositionX()
{
	//CString str = {};
	//GetDlgItemText(IDC_EDIT13, str);
	//float fValue = _tstof(str);

	//UpdateData(TRUE);
	//m_fPositionX = fValue;
	//UpdateData(FALSE);
}


void MapTab::ClickTransformApply()
{
	m_bApply = true;


	CString str = {};
	float fValue = 0.f;
	UpdateData(TRUE);

	GetDlgItemText(IDC_EDIT13, str);
	fValue = (float)_tstof(str);
	m_fPositionX = fValue;

	GetDlgItemText(IDC_EDIT14, str);
	fValue = (float)_tstof(str);
	m_fPositionY = fValue;

	GetDlgItemText(IDC_EDIT15, str);
	fValue = (float)_tstof(str);
	m_fPositionZ = fValue;

	GetDlgItemText(IDC_EDIT7, str);
	fValue = (float)_tstof(str);
	m_fRotationX = fValue;

	GetDlgItemText(IDC_EDIT8, str);
	fValue = (float)_tstof(str);
	m_fRotationY = fValue;

	GetDlgItemText(IDC_EDIT9, str);
	fValue = (float)_tstof(str);
	m_fRotationZ = fValue;

	GetDlgItemText(IDC_EDIT10, str);
	fValue = (float)_tstof(str);
	m_fScaleX = fValue;

	GetDlgItemText(IDC_EDIT11, str);
	fValue = (float)_tstof(str);
	m_fScaleY = fValue;

	GetDlgItemText(IDC_EDIT12, str);
	fValue = (float)_tstof(str);
	m_fScaleZ = fValue;


	switch (iColliderKind)
	{
	// Sphere
	case 0:
		break;
	// Box
	case 1:
		break;
	// Empty
	case 2:
		break;
	default:
		break;
	}


	UpdateData(FALSE);

}