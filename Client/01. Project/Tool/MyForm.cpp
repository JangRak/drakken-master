﻿// MyForm.cpp: 구현 파일
//

#include "pch.h"
#include "Tool.h"
#include "MyForm.h"
#include <Engine/global.h>

// MyForm

IMPLEMENT_DYNCREATE(MyForm, CFormView)

MyForm::MyForm()
	: CFormView(IDD_MYFORM)
{

}

MyForm::~MyForm()
{
	SAFE_DELETE(m_MapTab);
	SAFE_DELETE(m_TempTab);
}

void MyForm::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB1, m_TabControl);
}

BEGIN_MESSAGE_MAP(MyForm, CFormView)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB1, &MyForm::OnTabChange)
END_MESSAGE_MAP()


// MyForm 진단

#ifdef _DEBUG
void MyForm::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void MyForm::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// MyForm 메시지 처리기

// 초기화 함수
// 최초 1회만 호출됨
void MyForm::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	m_TabControl.InsertItem(0, L"MapTab");
	m_TabControl.InsertItem(1, L"TempTab");

	// 탭컨트롤 사이즈에 맞게 생성하기 위해 값을 받아옴
	CRect rc = {};
	m_TabControl.GetWindowRect(&rc);

	m_MapTab = new MapTab;
	//							탭 컨트롤을 부를때 호출하기 때문에 탭컨트롤을 넣는 것
	m_MapTab->Create(IDD_MAPTAB, &m_TabControl);
	// 어디서부터 그릴지 결정
	m_MapTab->MoveWindow(0, 25/*보기좋게 하려고 넣은 값*/, rc.Width(), rc.Height());
	m_MapTab->ShowWindow(SW_SHOW);

	m_TempTab = new TempTab;
	m_TempTab->Create(IDD_TEMPTAB, &m_TabControl);
	m_TempTab->MoveWindow(0, 25/*보기좋게 하려고 넣은 값*/, rc.Width(), rc.Height());
	m_TempTab->ShowWindow(SW_HIDE);




	m_MapTab->m_StaticObjectList.InsertString((int)OBJECT_KIND::BIGROCK, L"BigRock");
	m_MapTab->m_StaticObjectList.InsertString((int)OBJECT_KIND::BRIDGE_BUILDING, L"BRIDGE_BUILDING");
	m_MapTab->m_StaticObjectList.InsertString((int)OBJECT_KIND::DAMWALL, L"DAMWALL");
	m_MapTab->m_StaticObjectList.InsertString((int)OBJECT_KIND::GATE, L"GATE");
	m_MapTab->m_StaticObjectList.InsertString((int)OBJECT_KIND::GRAVE, L"Grave");
	m_MapTab->m_StaticObjectList.InsertString((int)OBJECT_KIND::GRAVE_PILLAR, L"GRAVE_PILLAR");
	m_MapTab->m_StaticObjectList.InsertString((int)OBJECT_KIND::SHANTY, L"SHANTY");
	m_MapTab->m_StaticObjectList.InsertString((int)OBJECT_KIND::SHANTY_UP, L"SHANTY_UP");
	m_MapTab->m_StaticObjectList.InsertString((int)OBJECT_KIND::HOUSE, L"house");
	m_MapTab->m_StaticObjectList.InsertString((int)OBJECT_KIND::HOUSE_RIGHT, L"House_right");
	m_MapTab->m_StaticObjectList.InsertString((int)OBJECT_KIND::HOUSE_LEFT, L"House_LEFT");
	m_MapTab->m_StaticObjectList.InsertString((int)OBJECT_KIND::HOUSE_SMALL, L"House_small");
	m_MapTab->m_StaticObjectList.InsertString((int)OBJECT_KIND::ROCK, L"ROCK");
	m_MapTab->m_StaticObjectList.InsertString((int)OBJECT_KIND::TREE1, L"TREE1");
	m_MapTab->m_StaticObjectList.InsertString((int)OBJECT_KIND::PLANE_ROCK, L"PLANE_ROCK");
	m_MapTab->m_StaticObjectList.InsertString((int)OBJECT_KIND::PLANE_ROCK + 1, L"SPRUCEA");
	m_MapTab->m_StaticObjectList.InsertString((int)OBJECT_KIND::PLANE_ROCK + 2, L"PLAINS_GRASS");
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
}


void MyForm::OnTabChange(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	int iIndex = m_TabControl.GetCurSel();


	// Error
	if (-1 == iIndex)
	{
		return;
	}

	switch (iIndex)
	{
	// MapTab
	case 0:
		m_MapTab->ShowWindow(SW_SHOW);
		m_TempTab->ShowWindow(SW_HIDE);
		break;
	// TempTab
	case 1:
		m_MapTab->ShowWindow(SW_HIDE);
		m_TempTab->ShowWindow(SW_SHOW);
		break;
	default:
		break;
	}

	*pResult = 0;
}

