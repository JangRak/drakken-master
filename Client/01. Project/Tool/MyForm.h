﻿#pragma once
#include "MapTab.h"
#include "TempTab.h"

// MyForm 폼 보기

class MyForm : public CFormView
{
	DECLARE_DYNCREATE(MyForm)

protected:
	MyForm();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~MyForm();

public:
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MYFORM };
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	CTabCtrl m_TabControl;

	MapTab* m_MapTab = nullptr;
	TempTab* m_TempTab = nullptr;

	afx_msg void OnTabChange(NMHDR *pNMHDR, LRESULT *pResult);

public:
};


