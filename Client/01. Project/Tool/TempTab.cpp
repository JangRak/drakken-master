﻿// TempTab.cpp: 구현 파일
//

#include "pch.h"
#include "TempTab.h"
#include "Tool.h"
#include "afxdialogex.h"


// TempTab 대화 상자

IMPLEMENT_DYNAMIC(TempTab, CDialogEx)

TempTab::TempTab(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TEMPTAB, pParent)
	, m_fposX(0.f)
	, m_fposY(0.f)
{

}

TempTab::~TempTab()
{
}

void TempTab::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_fposX);
	DDX_Control(pDX, IDC_CHECK1, m_Option1);
	DDX_Control(pDX, IDC_CHECK2, m_Option2);
	DDX_Control(pDX, IDC_COMBO1, m_ComboBox);
	DDX_Control(pDX, IDC_LIST1, m_ListBox);
	DDX_Text(pDX, IDC_EDIT4, m_fposY);
}


BEGIN_MESSAGE_MAP(TempTab, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON4, &TempTab::ClickExcuteButton)
	ON_BN_CLICKED(IDC_CHECK1, &TempTab::ClickOption1)
END_MESSAGE_MAP()


// TempTab 메시지 처리기




void TempTab::ClickExcuteButton()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	// 변수 값은 UpdateData(TRUE/FALSE) 사이에서 수정해야된다.
	UpdateData(TRUE);

	m_fposX++;

	UpdateData(FALSE);
}


void TempTab::ClickOption1()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);

	if (m_Option1.GetCheck())
	{
		m_Option2.SetCheck(true);
	}

	UpdateData(FALSE);
}


BOOL TempTab::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	// 정해진 것은 아니고
	// 런타임 중에 동적으로 값이 추가될 것이라면 ListBox를
	// 정적으로 사용할 것이라면 ComboBox를 사용하자



	m_ComboBox.InsertString(0, L"옵션1");
	m_ComboBox.InsertString(1, L"옵션2");
	m_ComboBox.InsertString(2, L"옵션3");
	m_ComboBox.InsertString(3, L"옵션4");

	int iIndex = m_ComboBox.GetCurSel();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}
