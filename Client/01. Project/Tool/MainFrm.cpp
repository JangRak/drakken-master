﻿
// MainFrm.cpp: CMainFrame 클래스의 구현
//

#include "pch.h"
#include "framework.h"
#include "Tool.h"

#include "MainFrm.h"
#include "ToolView.h"
#include "MyForm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//#include <Engine/global.h>
//#include <Engine/core.h>
//
//#ifdef _DEBUG
//#pragma comment(lib, "Engine/Engine_debug.lib")
//#else
//#pragma comment(lib, "Engine/Engine.lib")
//#endif



// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // 상태 줄 표시기
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

// CMainFrame 생성/소멸

CMainFrame::CMainFrame() noexcept
{
	// TODO: 여기에 멤버 초기화 코드를 추가합니다.
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;



	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.


	// 여기서 툴의 크기를 정해준다.
	cs.cx = 1920;
	cs.cy = 1080;

	return TRUE;
}

// CMainFrame 진단

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}
#endif //_DEBUG


// CMainFrame 메시지 처리기


// 클라이언트 생성 시 자동 호출 함수
BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	// 여기서 창 분할을 한다.
	// 분할을 하려면 윈도우가 필요함

	// 이 코드 내용은 몰라도 된다
	// 창 1개를 2개로 분할한다.
	// Row, Col은 2차원 배열이라 생각하면된다. 
	m_pSplitWnd.CreateStatic(this, 1, 2);
	// RUNTIME_CLASS 런타임중에 이 클래스를 사용하겠다.
	// pContext는 시스템 변수, 뭔지 몰라도 상관없다.
	m_pSplitWnd.CreateView(0, 0, RUNTIME_CLASS(MyForm), CSize(600, 1080), pContext);
	// 1920을 넣어도 알아서 600을 빼주긴한다.
	m_pSplitWnd.CreateView(0, 1, RUNTIME_CLASS(CToolView), CSize(1920, 1080), pContext);


	return TRUE;
}
