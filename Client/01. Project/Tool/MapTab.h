﻿#pragma once
#include <vector>

// MapTab 대화 상자
class CGameObject;
class MapTab : public CDialogEx
{
	DECLARE_DYNAMIC(MapTab)

public:
	MapTab(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~MapTab();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MAPTAB };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
//	afx_msg void OnBnClickedButton1();
	afx_msg void OnEnChangeEdit4();
	afx_msg void ClickSave();
	afx_msg void ClickLoad();

	CListBox m_StaticObjectList;
public:
	float m_fPositionX;
	float m_fPositionY;
	float m_fPositionZ;
	float m_fRotationX;
	float m_fRotationY;
	float m_fRotationZ;
	float m_fScaleX;
	float m_fScaleY;
	float m_fScaleZ;

public:
	bool m_bApply;
	static std::vector<CGameObject*> m_vGameObjectsMapTab;


public:
	bool m_bLoad;

public:
	void UpdateGameObjects(std::vector<CGameObject*> m_vGameObjects);
public:
	afx_msg void OnChangePositionX();
	afx_msg void ClickTransformApply();
private:
public:
	int m_iRadioSelect;
	int m_iRadioCollider;
private:
	int iColliderKind;
};
