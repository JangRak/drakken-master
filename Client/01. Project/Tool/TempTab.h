﻿#pragma once


// TempTab 대화 상자

class TempTab : public CDialogEx
{
	DECLARE_DYNAMIC(TempTab)

public:
	TempTab(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~TempTab();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TEMPTAB };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void ClickExcuteButton();
	float m_fposX;
	float m_fposY;
	CButton m_Option1;
	CButton m_Option2;
	afx_msg void ClickOption1();
	CComboBox m_ComboBox;
	CListBox m_ListBox;
	virtual BOOL OnInitDialog();
};
