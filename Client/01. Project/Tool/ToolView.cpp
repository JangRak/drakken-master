﻿// ToolView.cpp: CToolView 클래스의 구현
//

#include "pch.h"
#include <Engine/global.h>
#include <Engine/ToolCore.h>
#include <Engine/SceneMgr.h>
#include <Engine/Transform.h>
#include <Engine/GameObject.h>


#ifdef _DEBUG
#pragma comment(lib, "Engine/Engine_debug.lib")
#else
#pragma comment(lib, "Engine/Engine.lib")
#endif

#include "framework.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "Tool.h"
#endif

#include "MyForm.h"
#include "MainFrm.h"

#include "ToolDoc.h"
#include "ToolView.h"

#include "MapTab.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CToolView


//ATOM MyRegisterClass(HINSTANCE hInstance)
//{
//	WNDCLASSEXW wcex;
//
//	wcex.cbSize = sizeof(WNDCLASSEX);
//
//	wcex.style = CS_HREDRAW | CS_VREDRAW;
//	wcex.lpfnWndProc = WndProc;
//	wcex.cbClsExtra = 0;
//	wcex.cbWndExtra = 0;
//	wcex.hInstance = hInstance;
//	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_CLIENT));
//	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
//	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
//	wcex.lpszMenuName = nullptr;// MAKEINTRESOURCEW(IDC_CLIENT);
//	wcex.lpszClassName = szWindowClass;
//	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));
//
//	return RegisterClassExW(&wcex);
//}

IMPLEMENT_DYNCREATE(CToolView, CView)

BEGIN_MESSAGE_MAP(CToolView, CView)
	// 표준 인쇄 명령입니다.
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

// CToolView 생성/소멸

CToolView::CToolView() noexcept 
	: m_bIsInit(false)
	, m_pTempTab(nullptr)
	, m_pMapTab(nullptr)
{
	// TODO: 여기에 생성 코드를 추가합니다.
	   // 윈도우 핸들
		// 전역 문자열을 초기화합니다.

}

CToolView::~CToolView()
{
	SAFE_DELETE(m_pMapTab);
	SAFE_DELETE(m_pTempTab);
}

BOOL CToolView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.
	//CCore::GetInst()->init(m_hWnd, tResolution{ 1280, 768 }, true);

	return CView::PreCreateWindow(cs);
}

// CToolView 그리기
// 여기서 모든 렌더링 코드를 작성
void CToolView::OnDraw(CDC* /*pDC*/)
{
	if (!m_bIsInit)
	{
		m_bIsInit = true;
		//CToolCore::GetInst()->init(m_hWnd, tResolution{ 1280, 768 }, true);
		CToolCore::GetInst()->init(m_hWnd, tResolution{ 1280, 768 }, true);
	}
	HWND temp = m_hWnd;
	CToolDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;
	// TODO: 여기에 원시 데이터에 대한 그리기 코드를 추가합니다.


	// Game Running
	CToolCore::GetInst()->progress();

	Vec3 vTempScale = {};

	if (m_pMapTab->m_bApply)
	{
		UpdateData(TRUE);
		m_pMapTab->m_bApply = false;
		vTempScale = MapTab::m_vGameObjectsMapTab.back()->Transform()->GetLocalScale();
		MapTab::m_vGameObjectsMapTab.back()->Transform()->SetLocalPos(Vec3{ (float)m_pMapTab->m_fPositionX, (float)m_pMapTab->m_fPositionY, (float)m_pMapTab->m_fPositionZ });
		MapTab::m_vGameObjectsMapTab.back()->Transform()->SetLocalRot(Vec3{ (float)m_pMapTab->m_fRotationX, (float)m_pMapTab->m_fRotationY, (float)m_pMapTab->m_fRotationZ });
		MapTab::m_vGameObjectsMapTab.back()->Transform()->SetLocalScale(Vec3{ (float)m_pMapTab->m_fScaleX, (float)m_pMapTab->m_fScaleY, (float)m_pMapTab->m_fScaleZ });

		if (MapTab::m_vGameObjectsMapTab.back()->GetName() != L"plane_rock")
		{
			if (MapTab::m_vGameObjectsMapTab.size() >= 2)
			{
				wstring tempString = MapTab::m_vGameObjectsMapTab[MapTab::m_vGameObjectsMapTab.size() - 2]->GetName();
				if (wstring::npos != tempString.find(L"collider"))
				{
					vTempScale = MapTab::m_vGameObjectsMapTab[MapTab::m_vGameObjectsMapTab.size() - 2]->Transform()->GetLocalScale();

					MapTab::m_vGameObjectsMapTab[MapTab::m_vGameObjectsMapTab.size() - 2]->Transform()->SetLocalPos(Vec3{ m_pMapTab->m_fPositionX, m_pMapTab->m_fPositionY, m_pMapTab->m_fPositionZ });
					MapTab::m_vGameObjectsMapTab[MapTab::m_vGameObjectsMapTab.size() - 2]->Transform()->SetLocalRot(Vec3{ m_pMapTab->m_fRotationX, m_pMapTab->m_fRotationY, m_pMapTab->m_fRotationZ });
					//m_vGameObjects[m_vGameObjects.size() - 2]->Transform()->SetLocalScale(Vec3{ vTempScale.x * m_pMapTab->m_fScaleX, vTempScale.y * m_pMapTab->m_fScaleY, vTempScale.z * m_pMapTab->m_fScaleZ });
					MapTab::m_vGameObjectsMapTab[MapTab::m_vGameObjectsMapTab.size() - 2]->Transform()->SetLocalScale(Vec3{ vTempScale.x * m_pMapTab->m_fScaleX, vTempScale.y * m_pMapTab->m_fScaleX, vTempScale.z * m_pMapTab->m_fScaleX });
				}

			}
		}

		UpdateData(FALSE);
	}

	//// 
	//if (m_pMapTab->m_bLoad)
	//{
	//	m_pMapTab->m_bLoad = false;
	//	MapTab::m_vGameObjectsMapTab.clear();
	//	MapTab::m_vGameObjectsMapTab.resize(m_pMapTab->m_vGameObjectsMapTab.size());
	//	MapTab::m_vGameObjectsMapTab = m_pMapTab->m_vGameObjectsMapTab;
	//}
	


	//m_pMapTab->UpdateGameObjects(m_vGameObjects);

}


// CToolView 인쇄

BOOL CToolView::OnPreparePrinting(CPrintInfo* pInfo)
{

	// 기본적인 준비
	return DoPreparePrinting(pInfo);
}

void CToolView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄하기 전에 추가 초기화 작업을 추가합니다.
}

void CToolView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 인쇄 후 정리 작업을 추가합니다.
}


// CToolView 진단

#ifdef _DEBUG
void CToolView::AssertValid() const
{
	CView::AssertValid();
}

void CToolView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CToolDoc* CToolView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CToolDoc)));
	return (CToolDoc*)m_pDocument;
}
#endif //_DEBUG


// CToolView 메시지 처리기


void CToolView::OnInitialUpdate()
{
	CView::OnInitialUpdate();

	CMainFrame* pMainFrm = dynamic_cast<CMainFrame*>(AfxGetApp()->GetMainWnd());
	MyForm* pMyForm = dynamic_cast<MyForm*>(pMainFrm->m_pSplitWnd.GetPane(0, 0));

	m_pMapTab = pMyForm->m_MapTab;
	m_pTempTab = pMyForm->m_TempTab;



	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
}


void CToolView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CView::OnLButtonDown(nFlags, point);

	m_pTempTab->UpdateData(TRUE);
	m_pTempTab->m_fposX = (float)point.x;
	m_pTempTab->m_fposY = (float)point.y;


	//CToolMgr::GetInst()->Pick(point.x, point.y, m_hWnd, 0, 0);

	int iObjKind = -1;

	switch (m_pMapTab->m_StaticObjectList.GetCurSel())
	{
	case -1:
		m_pTempTab->UpdateData(FALSE);
		return;

	case (int)OBJECT_KIND::BIGROCK:
		iObjKind = (int)OBJECT_KIND::BIGROCK;
		break;
	case (int)OBJECT_KIND::GRAVE:
		iObjKind = (int)OBJECT_KIND::GRAVE;
		break;
	case (int)OBJECT_KIND::HOUSE:
		iObjKind = (int)OBJECT_KIND::HOUSE;
		break;
	case (int)OBJECT_KIND::HOUSE_RIGHT:
		iObjKind = (int)OBJECT_KIND::HOUSE_RIGHT;
		break;
	case (int)OBJECT_KIND::HOUSE_SMALL:
		iObjKind = (int)OBJECT_KIND::HOUSE_SMALL;
		break;
	case (int)OBJECT_KIND::ROCK:
		iObjKind = (int)OBJECT_KIND::ROCK;
		break;
	case (int)OBJECT_KIND::HOUSE_LEFT:
		iObjKind = (int)OBJECT_KIND::HOUSE_LEFT;
		break;

	case (int)OBJECT_KIND::BRIDGE_BUILDING:
		iObjKind = (int)OBJECT_KIND::BRIDGE_BUILDING;
		break;
	case (int)OBJECT_KIND::DAMWALL:
		iObjKind = (int)OBJECT_KIND::DAMWALL;
		break;
	case (int)OBJECT_KIND::GATE:
		iObjKind = (int)OBJECT_KIND::GATE;
		break;
	case (int)OBJECT_KIND::GRAVE_PILLAR:
		iObjKind = (int)OBJECT_KIND::GRAVE_PILLAR;
		break;
	case (int)OBJECT_KIND::PLANE_ROCK:
		iObjKind = (int)OBJECT_KIND::PLANE_ROCK;
		break;
	case (int)OBJECT_KIND::PLANE_ROCK + 1:
		iObjKind = (int)OBJECT_KIND::SPRUCEA;
		break;
	case (int)OBJECT_KIND::PLANE_ROCK + 2:
		iObjKind = (int)OBJECT_KIND::PLAINS_GRASS;
		break;

	case (int)OBJECT_KIND::SHANTY:
		iObjKind = (int)OBJECT_KIND::SHANTY;
		break;
	case (int)OBJECT_KIND::SHANTY_UP:
		iObjKind = (int)OBJECT_KIND::SHANTY_UP;
		break;

	//case (int)OBJECT_KIND::BIGROCK_COLLIDER:
	//	iObjKind = (int)OBJECT_KIND::BIGROCK_COLLIDER;
	//	break;
	//case (int)OBJECT_KIND::GRAVE_COLLIDER:
	//	iObjKind = (int)OBJECT_KIND::GRAVE_COLLIDER;
	//	break;
	//case (int)OBJECT_KIND::HOUSE_COLLIDER:
	//	iObjKind = (int)OBJECT_KIND::HOUSE_COLLIDER;
	//	break;
	//case (int)OBJECT_KIND::HOUSE_RIGHT_COLLIDER:
	//	iObjKind = (int)OBJECT_KIND::HOUSE_RIGHT_COLLIDER;
	//	break;
	//case (int)OBJECT_KIND::HOUSE_SMALL_COLLIDER:
	//	iObjKind = (int)OBJECT_KIND::HOUSE_SMALL_COLLIDER;
	//	break;
	//case (int)OBJECT_KIND::ROCK_COLLIDER:
	//	iObjKind = (int)OBJECT_KIND::ROCK_COLLIDER;
	//	break;
	//case (int)OBJECT_KIND::HOUSE_LEFT_COLLIDER:
	//	iObjKind = (int)OBJECT_KIND::HOUSE_LEFT_COLLIDER;
	//	break;

	//case (int)OBJECT_KIND::BRIDGE_BUILDING_COLLIDER:
	//	iObjKind = (int)OBJECT_KIND::BRIDGE_BUILDING_COLLIDER;
	//	break;
	//case (int)OBJECT_KIND::DAMWALL_COLLIDER:
	//	iObjKind = (int)OBJECT_KIND::DAMWALL_COLLIDER;
	//	break;
	//case (int)OBJECT_KIND::GATE_COLLIDER:
	//	iObjKind = (int)OBJECT_KIND::GATE_COLLIDER;
	//	break;
	//case (int)OBJECT_KIND::GRAVE_PILLAR_COLLIDER:
	//	iObjKind = (int)OBJECT_KIND::GRAVE_PILLAR_COLLIDER;
	//	break;
	//case (int)OBJECT_KIND::PLANE_ROCK_COLLIDER:
	//	iObjKind = (int)OBJECT_KIND::PLANE_ROCK_COLLIDER;
	//	break;
	//case (int)OBJECT_KIND::SHANTY_COLLIDER:
	//	iObjKind = (int)OBJECT_KIND::SHANTY_COLLIDER;
	//	break;
	//case (int)OBJECT_KIND::SHANTY_UP_COLLIDER:
	//	iObjKind = (int)OBJECT_KIND::SHANTY_UP_COLLIDER;
	//	break;

	default:
		break;
	}


	CGameObject* pGameObj = nullptr;
	CGameObject* pCollider = nullptr;
	CSceneMgr::GetInst()->Pick(point.x, point.y, m_hWnd, 0, iObjKind, (int)COLLIDER_KIND::SPHERE, &pGameObj, &pCollider);



	if (pCollider)
	{
		MapTab::m_vGameObjectsMapTab.push_back(pCollider);
	};

	if (pGameObj)
	{
		MapTab::m_vGameObjectsMapTab.push_back(pGameObj);

		m_pMapTab->UpdateData(TRUE);
		Vec3 Cur = pGameObj->Transform()->GetLocalPos();
		m_pMapTab->m_fPositionX = Cur.x;
		m_pMapTab->m_fPositionY = Cur.y;
		m_pMapTab->m_fPositionZ = Cur.z;
		Cur = pGameObj->Transform()->GetLocalRot();
		m_pMapTab->m_fRotationX = Cur.x;
		m_pMapTab->m_fRotationY = Cur.y;
		m_pMapTab->m_fRotationZ = Cur.z;
		Cur = pGameObj->Transform()->GetLocalScale();
		m_pMapTab->m_fScaleX = Cur.x;
		m_pMapTab->m_fScaleY = Cur.y;
		m_pMapTab->m_fScaleZ = Cur.z;
		m_pMapTab->UpdateData(FALSE);

	}

	m_pTempTab->UpdateData(FALSE);



}
